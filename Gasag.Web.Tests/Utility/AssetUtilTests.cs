﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Gasag.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.FakeDb;

namespace Gasag.Web.Utility.Tests
{
    [TestClass()]
    public class AssetUtilTests
    {
        [TestMethod()]
        public void CssPathsTest()
        {
            using (var db = new Db())
            {
                db.Configuration.Settings["Assets.Css"] = "/css/style.css?v=755, /css/main.css?v=755";

                var expected = new List<Asset>
                {
                    new Asset
                    {
                        Attributes = new List<string>(),
                        Path = "/css/style.css?v=755"
                    },
                    new Asset
                    {
                        Attributes = new List<string>(),
                        Path = "/css/main.css?v=755"
                    }
                };
                var actual = AssetUtil.CssPaths().ToList();;
                Assert.AreEqual(expected.Count(), actual.Count());
                for (int i = 0; i < expected.Count(); ++i)
                {
                    var expectedAsset = expected[i];
                    var actualAsset = actual[i];
                    Assert.AreEqual(expectedAsset.Path, actualAsset.Path);
                    Assert.AreEqual(expectedAsset.Attributes.Count(), actualAsset.Attributes.Count());
                }
            }
        }

        [TestMethod()]
        public void JavascriptPathsTest()
        {
            using (var db = new Db())
            {
                db.Configuration.Settings["Assets.Javascript.Body"] = "/js/main.js?v=755 require, /js/foo.js?v=755, /js/bar.js?v=755 async modernizr";

                var expected = new List<Asset>
                {
                    new Asset
                    {
                        Attributes = new List<string> { "require" },
                        Path = "/js/main.js?v=755"
                    },
                    new Asset
                    {
                        Attributes = new List<string>(),
                        Path = "/js/foo.js?v=755"
                    },
                    new Asset
                    {
                        Attributes = new List<string> { "async", "modernizr" },
                        Path = "/js/bar.js?v=755"
                    }
                };
                var actual = AssetUtil.JavascriptBodyPaths().ToList();;
                Assert.AreEqual(expected.Count(), actual.Count());
                for (int i = 0; i < expected.Count(); ++i)
                {
                    var expectedAsset = expected[i];
                    var actualAsset = actual[i];
                    Assert.AreEqual(expectedAsset.Path, actualAsset.Path);
                    Assert.AreEqual(expectedAsset.Attributes.Count(), actualAsset.Attributes.Count());
                }
            }
        }
    }
}