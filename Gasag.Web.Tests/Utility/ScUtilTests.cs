﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Gasag.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using Gizmo.SitecoreGizmo.Items;
using Sitecore.Globalization;
using Sitecore.FakeDb;
using Sitecore.Data;

namespace Gasag.Web.Utility.Tests
{
    [TestClass()]
    public class ScUtilTests
    {
        [TestMethod()]
        public void AllPageTemplateIdsTest()
        {
            using (var db = new Db {
                new DbItem("Blog Overview Page", new ID("a9f662e3-fae1-4340-9aec-bfd7f85ab190")),
                new DbItem("Blog Page", new ID("662823d7-5b81-43c1-adfc-c55a72455199")),
                new DbItem("Generic Content Page", new ID("50f6ad72-2bba-4109-85ef-02e8a770dfa4")),
                new DbItem("Generic Overview Page", new ID("e52e2025-0060-4be1-81f6-29e91d43d030")),
                new DbItem("Home Page Page", new ID("6a8cbe63-f26e-4795-a1f9-5d7799ff2ad2")),
                new DbItem("Landing Page Page", new ID("a0ca1072-a943-4d57-ba87-ceb3e00e7b6b")),
                new DbItem("Search Page Page", new ID("b515f8c3-5ee5-47a6-810c-d75b2910991a")),
                new DbItem("References")
                {
                    ParentID = Sitecore.ItemIDs.SystemRoot,
                    Children =
                    {
                        new DbItem("Blog Overview Page Template") { new DbField("Item") { Type = "DropTree", Value = "a9f662e3-fae1-4340-9aec-bfd7f85ab190" } },
                        new DbItem("Blog Page Template") { new DbField("Item") { Type = "DropTree", Value = "662823d7-5b81-43c1-adfc-c55a72455199" } },
                        new DbItem("Generic Content Page Template") { new DbField("Item") { Type = "DropTree", Value = "50f6ad72-2bba-4109-85ef-02e8a770dfa4" } },
                        new DbItem("Generic Overview Page Template") { new DbField("Item") { Type = "DropTree", Value = "e52e2025-0060-4be1-81f6-29e91d43d030" } },
                        new DbItem("Home Page Template") { new DbField("Item") { Type = "DropTree", Value = "6a8cbe63-f26e-4795-a1f9-5d7799ff2ad2" } },
                        new DbItem("Landing Page Template") { new DbField("Item") { Type = "DropTree", Value = "a0ca1072-a943-4d57-ba87-ceb3e00e7b6b" } },
                        new DbItem("Search Page Template") { new DbField("Item") { Type = "DropTree", Value = "b515f8c3-5ee5-47a6-810c-d75b2910991a" } }
                    }
                }
            })
            {
                var expected = 7;
                var actual = ScUtil.AllPageTemplateIds().Count();
                Assert.AreEqual(expected, actual);
            }  
        }

        [TestMethod()]
        public void IsNavigationPageTest()
        {
            var naviTemplateId = new ID("a9f662e3-fae1-4340-9aec-bfd7f85ab190");
            var notNaviTempalteId = new ID("662823d7-5b81-43c1-adfc-c55a72455199");
            var naviId = new ID("50f6ad72-2bba-4109-85ef-02e8a770dfa4");
            var notNaviId = new ID("a0ca1072-a943-4d57-ba87-ceb3e00e7b6b");
            var naviTypeId = new ID("b515f8c3-5ee5-47a6-810c-d75b2910991a");
            using (var db = new Db {
                new DbTemplate("Navi Template", naviTemplateId) { new DbField("Type") { Type = "DropTree" } },
                new DbTemplate("Not Navi Template", notNaviTempalteId),
                new DbItem("Navi Type", naviTypeId) { new DbField("Value") { Type = "Text", Value = "in-navi" } },
                new DbItem("Navi", naviId, naviTemplateId) { { "Type", naviTypeId.ToString() } },
                new DbItem("Not Navi", notNaviId, notNaviTempalteId),
            })
            {
                var ids = new List<ID>() { naviTemplateId };
                var naviItem = db.GetItem(naviId);
                var notNaviItem = db.GetItem(notNaviId);

                var expected = true;
                var actual = ScUtil.IsNavigationPage(naviItem, ids);
                Assert.AreEqual(expected, actual);

                expected = false;
                actual = ScUtil.IsNavigationPage(notNaviItem, ids);
                Assert.AreEqual(expected, actual);
            }  
        }

        [TestMethod()]
        public void ServiceUrlTest()
        {
            using (var db = new Db())
            {
                using (new LanguageSwitcher("en"))
                {
                    var expected = "/api/sitecore/controller/method?language=en";
                    var actual = ScUtil.ServiceUrl("controller", "method");
                    Assert.AreEqual(expected, actual);

                    expected = "/api/sitecore/controller/method?param=1&param2=abc";
                    actual = ScUtil.ServiceUrl("controller", "method", false, new Dictionary<string, string>() { {"param", "1"}, {"param2", "abc"} });
                    Assert.AreEqual(expected, actual);
                }
            }  
        }
    }
}