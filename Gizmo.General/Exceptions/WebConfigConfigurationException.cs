﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gizmo.General.Exceptions
{
    public class WebConfigConfigurationException : ConfigurationException
    {
        public WebConfigConfigurationException(string message) : base("Da stimmt was in der web.config nicht (schau mal nach):" + message) { }
    }
}
