﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gizmo.General.Math
{
    /// <summary>
    /// Math related utility functions.
    /// </summary>
    public static class MathUtil
    {
        /// <summary>
        /// Convert strings in a fraction format to numbers. This will also apply double.TryParse(fraction).
        /// </summary>
        /// <see cref="http://stackoverflow.com/a/13903677/1931663"/>
        /// <example>
        /// MathUtil.TryFractionToDouble("4 1/5"); -> true and 4.2
        /// MathUtil.TryFractionToDouble("4"); -> true and 4
        /// MathUtil.TryFractionToDouble("1/5"); -> true and 0.2
        /// MathUtil.TryFractionToDouble("abc"); -> false
        /// </example>
        /// <param name="fraction">The string representation of the fraction.</param>
        /// <param name="result">The floating point value of the fraction.</param>
        /// <returns>True if conversion successful.</returns>
        public static bool TryFractionToDouble(string fraction, out double result)
        {
            if(double.TryParse(fraction, out result))
            {
                return true;
            }

            Fraction frac;
            if (TryParseFraction(fraction, out frac))
            {
                result = frac.Value();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Parses strings in a fraction format.
        /// </summary>
        /// <see cref="http://stackoverflow.com/a/13903677/1931663"/>
        /// <example>
        /// MathUtil.TryParseFraction("4 1/5"); -> true
        /// MathUtil.TryParseFraction("4"); -> true
        /// MathUtil.TryParseFraction("1/5"); -> true
        /// MathUtil.TryParseFraction("abc"); -> false
        /// </example>
        /// <param name="fraction">The string representation of the fraction.</param>
        /// <param name="result">The result as a fraction object.</param>
        /// <returns>True if conversion successful.</returns>
        public static bool TryParseFraction(string fraction, out Fraction result)
        {
            // If necessary add additional characters to this list.
            var splitCharacters = new char[] { ' ', '/', '⁄' };
            string[] split = fraction.Split(splitCharacters);
            if(split.Length == 2 || split.Length == 3)
            {
                int a, b;
                if(int.TryParse(split[0], out a) && int.TryParse(split[1], out b))
                {
                    if(split.Length == 2)
                    {
                        result = new Fraction(numerator: a, denominator: b);
                        return true;
                    }

                    int c;
                    if(int.TryParse(split[2], out c))
                    {
                        result = new Fraction(wholeNumber: a, numerator: b, denominator: c);
                        return true;
                    }
                }
            }
            else if (split.Length == 1)
            {
                int a;
                if(int.TryParse(split[0], out a))
                {
                    result = new Fraction(wholeNumber: a);
                    return true;
                }
            }

            result = new Fraction();
            return false;
        }
    }

    /// <summary>
    /// Represents a mathematical fraction.
    /// </summary>
    /// <example>
    /// 1 3/4 is represented as
    /// 
    /// Fraction.WholeNumber = 1
    /// Fraction.Numerator = 3
    /// Fraction.Denominator = 4
    /// </example>
    public class Fraction
    {
        public int WholeNumber { get; set; }
        public int Numerator { get; set; }
        public int Denominator { get; set; }

        public Fraction(int wholeNumber = 0, int numerator = 0, int denominator = 1)
        {
            this.WholeNumber = wholeNumber;
            this.Numerator = numerator;
            this.Denominator = denominator;
        }

        public double Value()
        {
            return WholeNumber + (double)Numerator / Denominator;
        }

        public override string ToString()
        {
            var result = "";

            if (WholeNumber != 0)
            {
                result = $"{WholeNumber}";
            }
            
            if (Numerator != 0 && Denominator != 0)
            {
                if (!string.IsNullOrEmpty(result))
                {
                    result += " ";
                }

                result += $"{Numerator}/{Denominator}";
            }

            return result;
        }
    }
}
