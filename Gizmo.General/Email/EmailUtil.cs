﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net.Mail;

namespace Gizmo.General.Email
{
    public static class EmailUtil
    {
        /// <summary>
        /// Check if given address is an valid email address.
        /// </summary>
        /// <param name="address">The address to check.</param>
        /// <returns>True if address is valid else false.</returns>
        public static bool IsMailAddress(string address)
        {
            // Based on http://emailregex.com/
            string pattern = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";
            Regex rx = new Regex(pattern, RegexOptions.IgnoreCase);
            Match match = rx.Match(address);
            return match.Success;
        }


        private static string[] _splitters = new string[] { ",", ";" };
        /// <summary>
        /// Returns an Enumerable of MailAddress
        /// </summary>
        /// <param name="raw">The raw string of mail Addresses comma or semicolon seperated.</param>
        /// <returns>Enumerable of MailAddress</returns>
        public static IEnumerable<MailAddress> ToMailAddresses(this string raw)
        {
            if (raw != null)
            {
                var rawEmails = raw.Split(_splitters, StringSplitOptions.RemoveEmptyEntries).Where(x => !string.IsNullOrWhiteSpace(x)).Distinct();
                foreach (var email in rawEmails)
                {
                    MailAddress ma = new MailAddress(email.ToString().Trim());
                    yield return ma;
                }
            }
        }
    }
}
