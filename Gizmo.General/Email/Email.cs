﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Configuration;
using System.Net.Configuration;

namespace Gizmo.General.Email
{
    /// <summary>
    /// Create and send a mail using a fluent interface.
    /// </summary>
    public class Email
    {
        /// <summary>
        /// Contains the last exception.
        /// </summary>
        public Exception Error { get; set; }

        /// <summary>
        /// The mail message.
        /// </summary>
        public MailMessage MailMessage { get; set; }

        /// <summary>
        /// The smtp host.
        /// </summary>
        public string SmtpHost { get; set; }

        /// <summary>
        /// The smtp port.
        /// </summary>
        public int SmtpPort { get; set; }

        public Email()
        {
            MailMessage = new MailMessage();
            SmtpPort = -1;
        }

        public static Email Instance()
        {
            return new Email();
        }
    }

    /// <summary>
    /// Implement all common operation on Email using extensions.
    /// </summary>
    public static class EmailExtension
    {
        /// <summary>
        /// Add attachment to email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="attachment">The attachment.</param>
        /// <returns>The email.</returns>
        public static Email AddAttachment(this Email email, Attachment attachment)
        {
            email.MailMessage.Attachments.Add(attachment);
            return email;
        }

        /// <summary>
        /// Add given receiver to email as bcc.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="to">The receiver.</param>
        /// <returns>The email.</returns>
        public static Email AddBcc(this Email email, string to)
        {
            email.AddBcc(new MailAddress(to));
            return email;
        }

        /// <summary>
        /// Add given receiver to email as bcc.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="to">The receiver.</param>
        /// <returns>The email.</returns>
        public static Email AddBcc(this Email email, MailAddress to)
        {
            email.MailMessage.Bcc.Add(to);
            return email;
        }

        /// <summary>
        /// Add given receivers to email as bcc.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="tos">The receiver list.</param>
        /// <returns>The email.</returns>
        public static Email AddBcc(this Email email, IEnumerable<string> tos)
        {
            foreach (var to in tos)
            {
                email.AddBcc(to);
            }
            return email;
        }

        /// <summary>
        /// Add given receivers to email as bcc.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="tos">The receiver list.</param>
        /// <returns>The email.</returns>
        public static Email AddBcc(this Email email, IEnumerable<MailAddress> tos)
        {
            foreach (var to in tos)
            {
                email.AddBcc(to);
            }
            return email;
        }

        /// <summary>
        /// Add bcc receiver(s) from configuration. If delimiter is set it is used to seperate a string list of receivers.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="configKey">The configuration key.</param>
        /// <param name="delimiter">Optional string list delimiter. Default ",".</param>
        /// <returns>The email.</returns>
        public static Email AddBccFromConfig(this Email email, string configKey = "Gizmo.General.Email.Bcc", string delimiter = ",")
        {
            if (string.IsNullOrEmpty(delimiter))
            {
                email.AddBcc(ConfigurationManager.AppSettings[configKey]);
            }
            else
            {
                var tos = ConfigurationManager.AppSettings[configKey].Split(new string[] { delimiter }, StringSplitOptions.RemoveEmptyEntries);
                email.AddBcc(tos);
            }
            return email;
        }

        /// <summary>
        /// Add given receiver to email as cc.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="to">The receiver.</param>
        /// <returns>The email.</returns>
        public static Email AddCc(this Email email, string to)
        {
            email.AddCc(new MailAddress(to));
            return email;
        }

        /// <summary>
        /// Add given receiver to email as cc.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="to">The receiver.</param>
        /// <returns>The email.</returns>
        public static Email AddCc(this Email email, MailAddress to)
        {
            email.MailMessage.CC.Add(to);
            return email;
        }

        /// <summary>
        /// Add given receivers to email as cc.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="tos">The receiver list.</param>
        /// <returns>The email.</returns>
        public static Email AddCc(this Email email, IEnumerable<string> tos)
        {
            foreach (var to in tos)
            {
                email.AddCc(to);
            }
            return email;
        }

        /// <summary>
        /// Add given receivers to email as cc.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="tos">The receiver list.</param>
        /// <returns>The email.</returns>
        public static Email AddCc(this Email email, IEnumerable<MailAddress> tos)
        {
            foreach (var to in tos)
            {
                email.AddCc(to);
            }
            return email;
        }

        /// <summary>
        /// Add cc receiver(s) from configuration. If delimiter is set it is used to seperate a string list of receivers.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="configKey">The configuration key.</param>
        /// <param name="delimiter">Optional string list delimiter. Default ",".</param>
        /// <returns>The email.</returns>
        public static Email AddCcFromConfig(this Email email, string configKey = "Gizmo.General.Email.Cc", string delimiter = ",")
        {
            if (string.IsNullOrEmpty(delimiter))
            {
                email.AddCc(ConfigurationManager.AppSettings[configKey]);
            }
            else
            {
                var tos = ConfigurationManager.AppSettings[configKey].Split(new string[] { delimiter }, StringSplitOptions.RemoveEmptyEntries);
                email.AddCc(tos);
            }
            return email;
        }

        /// <summary>
        /// Add given receiver to email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="to">The receiver.</param>
        /// <returns>The email.</returns>
        public static Email AddTo(this Email email, string to)
        {
            email.AddTo(new MailAddress(to));
            return email;
        }

        /// <summary>
        /// Add given receiver to email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="to">The receiver.</param>
        /// <returns>The email.</returns>
        public static Email AddTo(this Email email, MailAddress to)
        {
            email.MailMessage.To.Add(to);
            return email;
        }

        /// <summary>
        /// Add given receivers to email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="tos">The receiver list.</param>
        /// <returns>The email.</returns>
        public static Email AddTo(this Email email, IEnumerable<string> tos)
        {
            foreach (var to in tos)
            {
                email.AddTo(to);
            }
            return email;
        }

        /// <summary>
        /// Add given receivers to email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="tos">The receiver list.</param>
        /// <returns>The email.</returns>
        public static Email AddTo(this Email email, IEnumerable<MailAddress> tos)
        {
            foreach (var to in tos)
            {
                email.AddTo(to);
            }
            return email;
        }

        /// <summary>
        /// Add receiver(s) from configuration. If delimiter is set it is used to seperate a string list of receivers.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="configKey">The configuration key.</param>
        /// <param name="delimiter">Optional string list delimiter. Default ",".</param>
        /// <returns>The email.</returns>
        public static Email AddToFromConfig(this Email email, string configKey = "Gizmo.General.Email.To", string delimiter = ",")
        {
            if (string.IsNullOrEmpty(delimiter))
            {
                email.AddTo(ConfigurationManager.AppSettings[configKey]);
            }
            else
            {
                var tos = ConfigurationManager.AppSettings[configKey].Split(new string[] { delimiter }, StringSplitOptions.RemoveEmptyEntries);
                email.AddTo(tos);
            }
            return email;
        }

        /// <summary>
        /// Send email. If it fails and a exception occured it is stored in Email.Error.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>True if sending was successful else false.</returns>
        public static bool Send(this Email email)
        {
            bool result = false;

            if (string.IsNullOrEmpty(email.SmtpHost))
            {
                email.Error = new ArgumentException("Email.SmtpHost not set.");
                return result;
            }
            
            try
            {
                SmtpClient smtp;
                if (email.SmtpPort < 0)
                {
                    smtp = new SmtpClient(email.SmtpHost);
                }
                else
                {
                    smtp = new SmtpClient(email.SmtpHost, email.SmtpPort);
                }
                smtp.Send(email.MailMessage);
                result = true;
            }
            catch (Exception ex)
            {
                email.Error = ex;
            }
            return result;
        }

        /// <summary>
        /// Set body of email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="body">The body text.</param>
        /// <returns>The email.</returns>
        public static Email SetBody(this Email email, string body)
        {
            email.MailMessage.Body = body;
            return email;
        }

        /// <summary>
        /// Set if body is html or normal text (default).
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="isBodyHtml">If true the body is considered html else text.</param>
        /// <returns>The email.</returns>
        public static Email SetBodyHtml(this Email email, bool isBodyHtml = true)
        {
            email.MailMessage.IsBodyHtml = isBodyHtml;
            return email;
        }

        /// <summary>
        /// Set sender of email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="from">The sender.</param>
        /// <returns>The email.</returns>
        public static Email SetFrom(this Email email, string from)
        {
            email.SetFrom(new MailAddress(from));
            return email;
        }

        /// <summary>
        /// Set sender of email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="from">The sender.</param>
        /// <returns>The email.</returns>
        public static Email SetFrom(this Email email, MailAddress from)
        {
            email.MailMessage.From = from;
            return email;
        }

        /// <summary>
        /// Set sender from configuration.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="configKey">The configuration key.</param>
        /// <returns>The email.</returns>
        public static Email SetFromFromConfig(this Email email, string configKey = "Gizmo.General.Email.From")
        {
            email.SetFrom(ConfigurationManager.AppSettings[configKey]);
            return email;
        }

        /// <summary>
        /// Set smtp host.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="smtpHost">The smtp host.</param>
        /// <returns>The email.</returns>
        public static Email SetSmtpHost(this Email email, string smtpHost)
        {
            email.SmtpHost = smtpHost;
            return email;
        }

        /// <summary>
        /// Read smtp host from configuration.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="configKey">The configuration key.</param>
        /// <returns>The email.</returns>
        public static Email SetSmtpHostFromConfig(this Email email, string configKey = "Gizmo.General.Email.SmtpHost")
        {
            email.SmtpHost = ConfigurationManager.AppSettings[configKey];
            return email;
        }

        /// <summary>
        /// Read smtp host and port from system.net/mailSettings/smtp.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>The email.</returns>
        public static Email SetSmtpHostPortFromSystemNet(this Email email)
        {
            var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
            email.SmtpHost = smtpSection.Network.Host;
            email.SmtpPort = smtpSection.Network.Port;
            return email;
        }

        /// <summary>
        /// Set smtp port.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="smtpPort">The smtp port.</param>
        /// <returns>The email.</returns>
        public static Email SetSmtpPort(this Email email, int smtpPort)
        {
            email.SmtpPort = smtpPort;
            return email;
        }

        /// <summary>
        /// Read smtp port from configuration.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="configKey">The configuration key.</param>
        /// <returns>The email.</returns>
        public static Email SetSmtpPortFromConfig(this Email email, string configKey = "Gizmo.General.Email.SmtpPort")
        {
            int smptPort;
            if (int.TryParse(ConfigurationManager.AppSettings[configKey], out smptPort))
            {
                email.SmtpPort = smptPort;
            }
            return email;
        }

        /// <summary>
        /// Set email subject.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="subject">The subject.</param>
        /// <returns>The email.</returns>
        public static Email SetSubject(this Email email, string subject)
        {
            email.MailMessage.Subject = subject;
            return email;
        }
    }
}
