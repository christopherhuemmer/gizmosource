﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Configuration;
using System.Net;
using Gizmo.General.Exceptions;

namespace Gizmo.General
{
    public static class GizmoSettings
    {
        private static NameValueCollection _settings = ConfigurationManager.GetSection("Gizmo") as NameValueCollection;

        private static string GetValue(string key, string defaultValue = "")
        {
            if (_settings == null)
            {
                return defaultValue;
            }
            string setting = _settings[key];
            if(string.IsNullOrWhiteSpace(setting))
            {
                return defaultValue;
            }
            return setting;
        }

        public static int CacheTimeoutMinutes
        {
            get
            {
                var strValue = GetValue("Cache.TimeoutMinutes").Trim();
                int value;
                return int.TryParse(strValue, out value) ? value : 60;
            }
        }

        public static string SavesAsFilesPath
        {
            get
            {
                return GetValue("FormValidator.SavesAsFilesAction");
            }
        }

        private static WebProxy _webProxy = null;
        private static bool ProxyInited = false;
        public static WebProxy WebProxy
        {
            get
            {
                if(!ProxyInited)
                {
                    var proxySetting = GetValue("Web.Proxy");
                    if (!string.IsNullOrWhiteSpace(proxySetting))
                    {
                        var parts = proxySetting.Split(':');
                        if (parts.Length < 2)
                        {
                            throw new WebConfigConfigurationException("Gizmo Web.Proxy einstellugnen passen nicht");
                        }
                        int port;
                        _webProxy = new WebProxy(parts[0], int.TryParse(parts[1], out port) ? port : 8080);
                    }
                    ProxyInited = true;
                }
                return _webProxy;
            }
        }
    }
}
