﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Globalization;

namespace Gizmo.General
{
    public class LocaleSwitch : IDisposable
    {
        private CultureInfo OrigCulture;
        private CultureInfo OrigUICulture;
        /// <summary>
        /// Initializes a new instance of the <see cref="LocaleSwitch"/> class. This is desigend to be an easy Locale switch in a using block.
        /// Dispose automatically will assign the previous locale value.
        /// </summary>
        /// <param name="currentCulture">The culture to set</param>
        /// <param name="currentUICulture">The UI culture to set. If null, currentCulture will also be used for currentUICulture</param>
        public LocaleSwitch(CultureInfo currentCulture, CultureInfo currentUICulture = null)
        {
            OrigCulture = Thread.CurrentThread.CurrentCulture;
            OrigUICulture = Thread.CurrentThread.CurrentUICulture;

            Thread.CurrentThread.CurrentCulture = currentCulture;
            if (currentUICulture == null)
            {
                Thread.CurrentThread.CurrentUICulture = currentCulture;
            }
            else
            {
                Thread.CurrentThread.CurrentUICulture = currentUICulture;
            }
        }
        public void Dispose()
        {
            Thread.CurrentThread.CurrentCulture = OrigCulture;
            Thread.CurrentThread.CurrentUICulture = OrigUICulture;
        }
    }
}
