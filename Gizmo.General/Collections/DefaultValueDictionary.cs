﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gizmo.General
{
    /// <summary>
    /// Very minimal implementation just to allow default values on [] indexing
    /// THIS IS UNTESTED
    /// </summary>
    /// <typeparam name="TKey">The type of the key.</typeparam>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public class DefaultValueDictionary<TKey, TValue> : Dictionary<TKey, TValue>
    {
        public new TValue this[TKey key]
        {
            get
            {
                TValue v;
                if (base.TryGetValue(key, out v))
                {
                    return v;
                }
                return default(TValue);
            }
            set
            {
                base[key] = value;
            }
        }
    }
}
