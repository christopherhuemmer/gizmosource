﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Caching;

namespace Gizmo.General.Caching
{
    /// <summary>
    /// Simplified access to .NET Cache
    /// 
    /// IMPORTANT: The underlying MemoryCache doesn't allow to set null values. Trying to set a 
    /// null value will instead call the Remove(key) method. This prevents null values and makes
    /// setting a null value equivalent to removing the value from cache.
    /// </summary>
    public static class Cache
    {
        /// <summary>
        /// Get value from cache or return default value.
        /// </summary>
        /// <param name="key">The cache key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>The value in cache or the default value.</returns>
        public static object Get(string key, object defaultValue = null)
        {
            object value = MemoryCache.Default.Get(key);
            return value ?? defaultValue;
        }

        /// <summary>
        /// Check if cache contains value for given key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>True if cache contains value.</returns>
        public static bool Has(string key)
        {
            return MemoryCache.Default.Contains(key);
        }

        /// <summary>
        ///  Remove value from key.
        /// </summary>
        /// <param name="key">The key.</param>
        public static void Remove(string key)
        {
            MemoryCache.Default.Remove(key);
        }

        /// <summary>
        /// Store value in cache. Value expires after default number of minutes. See GizmoSettings.CacheTimeoutMinutes.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value object.</param>
        public static void Set(string key, object value)
        {
            Set(key, value, GizmoSettings.CacheTimeoutMinutes);
        }

        /// <summary>
        /// Store value in cache. Value expires after given number of minutes.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="expireMinutes">Number of minutes after which value expires.</param>
        /// <param name="slidingExpiration">If set to true sliding expiration will be used else absolute expiration.</param>
        /// <param name="policy">If set you can provide a custom cache item policy. Only SlidingExpiration or AbsoluteExpiration will be overriden with given expireMinutes.</param>
        public static void Set(string key, object value, int expireMinutes, bool slidingExpiration = false, CacheItemPolicy policy = null)
        {
            if (value == null)
            {
                // MemoryCache doesn't allow null values. Normally setting the value to null means to "unset"/"delete" the value.
                // So we will call remove instead.
                Remove(key);
                return;
            }

            policy = policy ?? new CacheItemPolicy();
            if (slidingExpiration)
            {
                policy.SlidingExpiration = TimeSpan.FromMinutes(expireMinutes);
            }
            else
            {
                policy.AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(expireMinutes);
            }
            MemoryCache.Default.Set(key, value, policy);
        }
    }
}
