﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Gizmo.General.Configuration
{
    /// <summary>
    /// The GenericConfigurationManager allows you to map different sections in a config file Directly to an object
    /// All properties matching they key in the config file will automatically be set on the object using the GenericBinder
    /// </summary>
    /* Example
        <configSections>
            <section name="TestConfig" type="Gizmo.General.Configuration.MappingSectionHandler" />
        </configSections>
        <TestConfig type="Gizmo.General.Configuration.TestConfig, GizmoGeneral" configLocation = "App_Data\Gizmo.Test.config">
            <add key="TestValueWebConfig" value="If you see this value .. great!"/>
            <add key="TestIntList" value="1,2,3;4;5" />
            <add seperator="|" key="TestStringList" value="AB|CD" />
            <object key="TestProp">
                <add key="SubValue" value="xyz" />
            </object>
        </TestConfig>
     */
    public static class GenericConfigurationManager
    {
        private static object _mutex = new object();
        private static Dictionary<string, MappingSection> _cachedSections = new Dictionary<string, MappingSection>();
        private static MappingSection GetSection(string sectionName, bool throwOnMissing)
        {
            MappingSection section;
            lock (_mutex)
            {
                if (!_cachedSections.TryGetValue(sectionName, out section))
                {
                    section = (MappingSection)ConfigurationManager.GetSection(sectionName);
                    if (section == null && throwOnMissing)
                    {
                        throw new ConfigurationErrorsException($"No configuration for '{sectionName}' could be found!");
                    }
                    _cachedSections[sectionName] = section;
                }
            }
            return section;
        }
        public static T Get<T>(string sectionName, bool throwOnMissing = false) where T: class, new()
        {
            var section = GetSection(sectionName, throwOnMissing);
            if(section == null)
            {
                //no config info return settings object
                return (T)Activator.CreateInstance(typeof(T));
            }
            else
            {
                return (T)section.GetInitializedObject();
            }
        }
        
        public static void Get(string sectionName, object baseObject, bool throwOnMissing = false)
        {
            var section = GetSection(sectionName, throwOnMissing);
            if (section != null)
            {
                section.UpdateObject(baseObject);
            }
        }
    }

   
}