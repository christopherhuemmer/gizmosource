﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;

namespace Gizmo.General.Configuration
{
    public class MappingSection
    {
        public string Name { get; internal set; }
        public string TypeName { get; internal set; }
        public Type TargetType { get; internal set; }
        public XElement ConfigData {get; set;}
        
        private Dictionary<string, string[]> GetMergedMapping()
        {
            return null;
        }
        private object _initedObject = null;
        public object GetInitializedObject()
        {
            if (_initedObject == null)
            {
                var obj = Activator.CreateInstance(TargetType);
                InitializeObject(obj, ConfigData);
                _initedObject = obj;
            }
            return _initedObject;
        }

        private void InitializeObject(object obj, XElement currNode)
        {
            Dictionary<string, PropertyInfo> objectProperties = obj.GetType().GetProperties().ToDictionary(k => k.Name, v => v); ;
            Dictionary<string, string[]> bindableValues = new Dictionary<string, string[]>();
            PropertyInfo propInfo;

            string seperator, propertyName, propertyValue;
            //prepare values vor generic binder
            foreach (var elem in currNode.Elements().Where(x => x.Name == "add"))
            {
                propertyName = elem.Attributes("key").First().Value;
                if (!objectProperties.TryGetValue(propertyName, out propInfo)) { continue; } // object doesn't have this property
                propertyValue = elem.Attributes("value").First().Value;

                if (propInfo.PropertyType.IsArray
                    || (propInfo.PropertyType.IsGenericType && propInfo.PropertyType.GetGenericTypeDefinition() == typeof(List<>)))

                {
                    seperator = elem.Attributes("seperator").FirstOrDefault()?.Value ?? ",;"; //read seperators or default to , and ;
                    bindableValues[propertyName] = propertyValue.Split(seperator.ToCharArray());
                }
                else
                {
                    bindableValues[propertyName] = new string[] { propertyValue }; 
                }
            }
            //set value properties
            GenericBinder.GenericBinder.Update(obj, bindableValues);

            //check for sub objects and bind those if needed
            foreach (var elem in currNode.Elements().Where(x => x.Name == "object"))
            {
                if (!objectProperties.TryGetValue(elem.Attributes("key").First().Value, out propInfo)) { continue; }

                //check sub object and crerate one if needed
                object subObj = propInfo.GetValue(obj, null);
                if (subObj == null)
                {
                    subObj = Activator.CreateInstance(propInfo.PropertyType);
                    propInfo.SetValue(obj, subObj, null);
                }
                //had the need for a generic Dictionary<string, List<string>> fully supporting generics is out of scope atm
                if (propInfo.PropertyType.IsGenericType && (propInfo.PropertyType.GetGenericTypeDefinition() == typeof(Dictionary<,>)))
                {
                    //init the dictionary
                    InitializeDictionary(subObj, elem);
                }
                else
                {
                    //init sub object
                    InitializeObject(subObj, elem);
                }
            }
        }
        //IValueProxy proxy classes are used to pass array and List<> initialization on dictionaries to InitializeObject
        private class ListProxy<T> : IValueProxy
        {
            public List<T> ListValue { get; set; }
            public object GetValue()
            {
                return ListValue;
            }
        }
        private class ArrayProxy<T> : IValueProxy 
        {
            public T[] ArrayValue { get; set; }
            public object GetValue()
            {
                return ArrayValue;
            }
        }
        private interface IValueProxy
        {
            object GetValue();
        }

        private void InitializeDictionary(object obj, XElement currNode)
        {
            //check if these are simple iconvertible - only support those atm
            var types = obj.GetType().GetGenericArguments();
            if (!typeof(IConvertible).IsAssignableFrom(types[0]))
            {
                throw new NotSupportedException("For Dictionaries only keys that support IConvertible can be used");
            }
            var valType = types[1];
            var isList = false;
            var isArray = false;
            Type subObjectType = null;
            if (valType.IsGenericType && (valType.GetGenericTypeDefinition() == typeof(List<>)))
            {
                subObjectType = valType.GetGenericArguments().First();
                if (!typeof(IConvertible).IsAssignableFrom(subObjectType))
                {
                    throw new NotSupportedException("For Dictionaries only List<> values that support IConvertible can be used");
                }
                isList = true;
            }
            else if (valType.IsArray)
            {
                subObjectType = valType.GetElementType();
                if (!typeof(IConvertible).IsAssignableFrom(subObjectType))
                {
                    throw new NotSupportedException("For Dictionaries only [] values that support IConvertible can be used");
                }
                isArray = true;
            }
            else if (!typeof(IConvertible).IsAssignableFrom(valType))
            {
                throw new NotSupportedException("For Dictionaries only values, List<> and [] are supported where IConvertible can be used");
            }

            object dicKey, dicValue;
            IValueProxy containerObject;

            var itemProp = obj.GetType().GetProperty("Item");
            foreach (var kvp in currNode.Elements().Where(x => x.Name == "add"))
            {
                dicKey = Convert.ChangeType(kvp.Attributes("key").First().Value, types[0]);

                if(isList || isArray)
                {
                    //prepare data so we can use InitializeObject
                    var tmpNode = new XElement("object");
                    var tmpNodeVal = new XElement(kvp);
                    tmpNodeVal.Attributes("key").Single().Value = isList ? "ListValue" : "ArrayValue";
                    tmpNode.Add(tmpNodeVal);
                    var containerType = isList ? typeof(ListProxy<>) : typeof(ArrayProxy<>);
                    var concreteType = containerType.MakeGenericType(subObjectType);
                    containerObject = Activator.CreateInstance(concreteType) as IValueProxy;
                    InitializeObject(containerObject, tmpNode); //call InitializeObject with our proxy object
                    itemProp.SetValue(obj, containerObject.GetValue(), new[] { dicKey });
                }
                else
                {
                    dicValue = Convert.ChangeType(kvp.Attributes("value").First().Value, types[1]);
                    itemProp.SetValue(obj, dicValue, new[] { dicKey });
                }
            }
        }

        public void UpdateObject(object toUpdate)
        {
            InitializeObject(toUpdate, ConfigData);
        }


    }
}
