﻿using Gizmo.General.IO;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace Gizmo.General.Configuration
{
    
    public class MappingSectionHandler : IConfigurationSectionHandler
    {
        private string GetFullPath(string configPath, bool getSubSetPath)
        {
            var path = configPath;
            //fully qualified path?
            if (!(configPath.StartsWith("\\") || configPath.Contains(":")))
            {
                path = Path.Combine(FileUtil.ApplicationDirectory, path);
            }
            if(getSubSetPath)
            {
                path = AddSubSetPath(path);
            }
            return path;
        }
        private string AddSubSetPath(string configPath)
        {
            var subSet = ConfigurationManager.AppSettings["configSubSet"];
            if(!string.IsNullOrWhiteSpace(subSet))
            {
                var path = Path.GetDirectoryName(configPath);
                var file = Path.GetFileName(configPath);
                return Path.Combine(path, subSet, file);
            }
            return configPath;
        }

        private XElement GetSettingsNode(string path, string expectedName)
        {
            var fi = new FileInfo(path);
            if (fi.Exists)
            {
                var configXml = XDocument.Load(path);
                var node = configXml.Elements().FirstOrDefault();
                if (expectedName != node?.Name)
                {
                    throw new ConfigurationErrorsException($"Sectionname ({expectedName}) in web.config does not match first element in external file ({node?.Name})");
                }
                foreach(var n in node.Descendants())
                {
                    n.Add(new XAttribute("origin", path));
                }
                return node;

            }
            return null;
        }


        class ConfigNodeEquality : IEqualityComparer<XElement>
        {
            public bool Equals(XElement x, XElement y)
            {
                //can not merge add with object
                if (x.Name != y.Name) { return false; }

                var xKey = x.Attribute("key");
                var yKey = y.Attribute("key");


                if (xKey == null || yKey == null) return false;

                return xKey.Value == yKey.Value;
            }

            public int GetHashCode(XElement obj)
            {
                return base.GetHashCode();
            }
        }

        private XElement MergeNodes(XElement e1, XElement e2)
        {
            if (e1 == null) { return e2; }
            if (e2 == null) { return e1; }

            if(e1.Name != e2.Name)
            {
                throw new ArgumentException("Only nodes with the same name can be merged");
            }

            var result = new XElement(e1.Name, e1.Attribute("key"));
            var comparer = new ConfigNodeEquality();
            var allElems = e1.Elements().Distinct(comparer).Concat(e2.Elements().Distinct(comparer));
            var elemGroups = allElems.GroupBy(x => x, y => y, comparer);

            foreach(var grp in elemGroups)
            {
                var child1 = grp.First();
                var child2 = grp.Last();
                if (child2.Name == "add" || child1 == child2)
                {
                    result.Add(child2);
                }
                else
                {
                    result.Add(MergeNodes(child1, child2));
                }
            }
            
            return result;
        }
        public object Create(object parent, object configContext, XmlNode section)
        {
            var configLocation = section.Attributes["configLocation"]?.Value;
            var subSet = ConfigurationManager.AppSettings["configSubSet"];
            var typeName = section.Attributes["type"]?.Value;
            if(typeName == null)
            {
                throw new ConfigurationErrorsException($"Section '{section.Name}' is missing a type attribute");
            }

            var targetType = Type.GetType(typeName);
            if (targetType == null)
            {
                throw new ConfigurationErrorsException($"The Type '{typeName}' for section '{section.Name}' is could not be found");
            }

            var retSection = new MappingSection()
            {
                Name = section.LocalName,
                TypeName = typeName,
                TargetType = targetType
            };

            //using XElement makes the xml Merging A LOT easier
            var configData = XElement.Load(section.CreateNavigator().ReadSubtree());

            //custom location
            if (configLocation != null)
            {
                var path = GetFullPath(configLocation, false);
                var settingNode = GetSettingsNode(path, section.Name);
                configData = MergeNodes(configData, settingNode);
            }

            //custom sub set overrides
            if (configLocation != null && subSet != null)
            {
                var path = GetFullPath(configLocation, true);
                var settingNode = GetSettingsNode(path, section.Name);
                configData = MergeNodes(configData, settingNode);
            }

            retSection.ConfigData = configData;

            return retSection;
            
        }
        
    }

    #region TestConfig
#if DEBUG
    public class TestConfig
    {
        public class TestPropClass1
        {
            public string SubValue { get; set; } = "CodeSubValue";
            public TestPropClass2 SubClass { get; set; }
        }
        public class TestPropClass2
        {
            public string SubSubValue { get; set; } = "CodeSubSubValue";
        }
        public string TestValue { get; set; } = "HAHAHAHA";
        public string TestValue2 { get; set; } = "Default from Sourcecode...";
        public string TestValueWebConfig { get; set; } = "hrm you should not see this";
        public List<int> TestIntList { get; set; }
        public List<string> TestStringList { get; set; }
        public TestPropClass1 TestProp { get; set; }
        public TestPropClass1 TestProp2 { get; set; }

    }
#endif

    #endregion
}
