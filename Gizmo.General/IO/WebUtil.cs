﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace Gizmo.General.IO
{
    /// <summary>
    /// Collection of common web related helper methods.
    /// </summary>
    public static class WebUtil
    {
        /// <summary>
        /// Call given url and retrieve content.
        /// </summary>
        /// <param name="url">The url.</param>
        /// <returns>The content.</returns>
        public static string FetchUrl(string url)
        {
            using (WebClient client = new WebClient())
            {
                return client.DownloadString(url);
            }
        }

        /// <summary>
        /// Call given url and retrieve content.
        /// </summary>
        /// <param name="url">The url.</param>
        /// <returns>The content.</returns>
        public static string FetchUrl(Uri url)
        {
            return FetchUrl(url.ToString());
        }
    }
}
