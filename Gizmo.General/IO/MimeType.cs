﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;

namespace Gizmo.General.IO
{
    /// <summary>
    /// Collection of mime type constants.
    /// </summary>
    public static class MimeType
    {
        public static class Application
        {
            public const string Doc = "application/msword";
            public const string Docx = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            public const string FormUrlencoded = "application/x-www-form-urlencoded";
            public const string Javascript = "application/javascript";
            public const string Json = "application/json";
            public const string Octet = MediaTypeNames.Application.Octet;
            public const string Pdf = MediaTypeNames.Application.Pdf;
            public const string Ppt = "application/vnd.ms-powerpoint";
            public const string Pptx = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
            public const string Rtf = MediaTypeNames.Application.Rtf;
            public const string Soap = MediaTypeNames.Application.Soap;
            public const string Xml = "application/xml";
            public const string Xls = "application/vnd.ms-excel";
            public const string Xlsx = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            public const string Zip = MediaTypeNames.Application.Zip;
        }

        public static class Image
        {
            public const string Gif = MediaTypeNames.Image.Gif;
            public const string Jpeg = MediaTypeNames.Image.Jpeg;
            public const string Png = "image/png";
            public const string Tiff = MediaTypeNames.Image.Tiff;
        }

        public static class Text
        {
            public const string Html = MediaTypeNames.Text.Html;
            public const string Plain = MediaTypeNames.Text.Plain;
            public const string RichText = MediaTypeNames.Text.RichText;
        }
    }
}
