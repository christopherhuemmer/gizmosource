﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gizmo.General.String
{
    /// <summary>
    /// Extension method for strings.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Allows direct call of FormatTemplate() on a template string.
        /// </summary>
        /// <param name="format">The template string.</param>
        /// <param name="args">Variable list of arguments.</param>
        /// <returns>The formated string</returns>
        public static string FormatWith(this string format, params object[] args)
        {
            return string.Format(format, args);
        }

        /// <summary>
        /// Checks if two strings are equal. With optional parameter case comparison 
        /// can be disabled.
        /// </summary>
        /// <param name="strA">The first string.</param>
        /// <param name="strB">The second string.</param>
        /// <param name="ignoreCase">If true case is ignored.</param>
        /// <returns>True if both strings are equal.</returns>
        public static bool IsEqual(this string strA, string strB, bool ignoreCase = false)
        {
            return ignoreCase ? string.Equals(strA, strB, StringComparison.OrdinalIgnoreCase) : string.Equals(strA, strB);
        }

        /// <summary>
        /// Checks if two strings are case insensitive equal. 
        /// </summary>
        /// <param name="strA">The first string.</param>
        /// <param name="strB">The second string.</param>
        /// <returns>True if both strings are case insensitive equal.</returns>
        public static bool IsLowerEqual(this string strA, string strB)
        {
            return strA.IsEqual(strB, true);
        }

        /// <summary>
        /// Checks if a string starts with another string in case insensitive manner. 
        /// </summary>
        /// <param name="strA">The first string.</param>
        /// <param name="strB">The second string.</param>
        /// <returns>True if strA starts with strB in case insensitive manner. False if either strA or strB are null</returns>
        public static bool LowerStartsWith(this string strA, string strB)
        {
            if (strA == null || strB == null) { return false; }

            return strA.StartsWith(strB, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Checks if a string ends with another string in case insensitive manner. 
        /// </summary>
        /// <param name="strA">The first string.</param>
        /// <param name="strB">The second string.</param>
        /// <returns>True if strA ends with strB in case insensitive manner. False if either strA or strB are null</returns>
        public static bool LowerEndsWith(this string strA, string strB)
        {
            if (strA == null || strB == null) { return false; }

            return strA.EndsWith(strB, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Always returns a string even if the source string was null (empty string in this case).
        /// </summary>
        /// <param name="str">The string which maybe null.</param>
        /// <returns>The unchanged string or an empty string if it was null.</returns>
        public static string NullSafe(this string str)
        {
            return str ?? string.Empty;
        }

        /// <summary>
        /// Converts a string to the represented boolean value. Following string values convert 
        /// to true (by default): "1", "true".
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns>True if string represents a boolean value.</returns>
        public static bool ToBool(this string str)
        {
            return ToBool(str, "1", "true");
        }

        /// <summary>
        /// Converts a string to the represented boolean value. Following string values convert 
        /// to true (by default): "1", "true".
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="boolStrings">List of boolean strings. If not set the default values are choosen.</param>
        /// <returns>True if string represents a boolean value.</returns>
        public static bool ToBool(this string str, params string[] boolStrings)
        {
            return boolStrings.Any(x => x.IsEqual(str, true));
        }

        /// <summary>
        /// Convert string to a list. Uses comma and semicolon as delimiter. Discards empty entries.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns>The string list.</returns>
        public static IEnumerable<string> ToList(this string str)
        {
            return ToList(str, false, ",", ";");
        }

        /// <summary>
        /// Convert string to a list.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="keepEmpty">If true empty entries are not removed.</param>
        /// <param name="delimiter">List of delimiter strings.</param>
        /// <returns>The string list.</returns>
        public static IEnumerable<string> ToList(this string str, bool keepEmpty, params string[] delimiter)
        {
            if (string.IsNullOrEmpty(str))
            {
                return new string[0];
            }
            StringSplitOptions option = StringSplitOptions.None;
            if (!keepEmpty)
            {
                option = StringSplitOptions.RemoveEmptyEntries;
            }
            return str.Split(delimiter, option);
        }

        /// <summary>
        /// Removes all occurances of strings in toRemove from the original string.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="toRemove">The strings to remove.</param>
        /// <param name="sc">The sc.</param>
        /// <returns></returns>
        public static string RemoveAll(this string str, IEnumerable<string> toRemove, StringComparison sc = StringComparison.CurrentCulture)
        {

            int pos, lastPos;
            var sb = new StringBuilder(str.Length);

            foreach (string delThis in toRemove)
            {
                pos = 0;
                lastPos = 0;
                while ((pos = str.IndexOf(delThis, pos, sc)) != -1)
                {
                    sb.Append(str.Substring(lastPos, pos - lastPos));
                    pos += delThis.Length;
                    lastPos = pos;
                }
                sb.Append(str.Substring(lastPos));
                str = sb.ToString();
                sb.Clear();
            }

            return str;
        }

        /// <summary>
        /// Tolower index of.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="value">The value.</param>
        /// <param name="start">The start.</param>
        /// <returns></returns>
        public static int ToLowerIndexOf(this string source, string value, int start = 0)
        {
            return source.IndexOf(value, start, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Removes all Whitespace in the string.
        /// </summary>
        /// <param name="source">The string.</param>
        /// <returns>The source without whitespace.</returns>
        //this is about 6 times faster than a Regex.Replace(str, @"\s+", "");
        //about 4 times faster then Regex _rec = new Regex(@"\s+", RegexOptions.Compiled); (regex only created once)
        //and 2-2.5 times the speed of new string(s.ToCharArray().Where(c => !Char.IsWhiteSpace(c)).ToArray());
        public static string RemoveWhitespace(this string source)
        {

            int start = 0, i;
            char[] ca = source.ToCharArray();
            if (source.Length > 19)
            {
                var sb = new StringBuilder(source.Length);
                for (i = 0; i < ca.Length; i++)
                {
                    if (Char.IsWhiteSpace(ca[i]))
                    {
                        //add ok chunk if not at start
                        if (i != 0)
                        {
                            sb.Append(source.Substring(start, i - start));
                        }
                        //skip all whitespace thats coming...
                        i++; // we already checked this char
                        while (i < ca.Length && Char.IsWhiteSpace(ca[i]))
                        {
                            i++;
                        }
                        //skipped whitespace - this is our new start
                        start = i;
                    }
                }
                //add the last chunk - if there is one
                if (start != source.Length || start == 0)
                {
                    if (start == 0)
                    {
                        //nothing found..
                        return source;
                    }
                    sb.Append(source.Substring(start, i - start));
                }
                return sb.ToString();
            }
            else
            {
                string ret = string.Empty;
                for (i = 0; i < ca.Length; i++)
                {
                    if (Char.IsWhiteSpace(ca[i]))
                    {
                        //add ok chunk if not at start
                        if (i != 0)
                        {
                            ret += source.Substring(start, i - start);
                        }
                        //skip all whitespace thats coming...
                        i++; // we already checked this char
                        while (i < ca.Length && Char.IsWhiteSpace(ca[i]))
                        {
                            i++;
                        }
                        //skipped whitespace - this is our new start
                        start = i;
                    }
                }
                //add the last chunk - if there is one
                if (start != source.Length || start == 0)
                {
                    if (start == 0)
                    {
                        //nothing found..
                        return source;
                    }
                    ret += source.Substring(start, i - start);
                }
                return ret;
            }
        }

    }
}
