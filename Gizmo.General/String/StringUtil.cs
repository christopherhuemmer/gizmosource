﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Gizmo.General.String
{
    /// <summary>
    /// Collection of string related helper methods.
    /// </summary>
    public static class StringUtil
    {
        /// <summary>
        /// Checks if two strings are equal. With optional parameter case comparison 
        /// can be disabled.
        /// </summary>
        /// <param name="strA">The first string.</param>
        /// <param name="strB">The second string.</param>
        /// <param name="ignoreCase">If true case is ignored.</param>
        /// <returns>True if both strings are equal.</returns>
        public static bool AreEqual(string strA, string strB, bool ignoreCase = false)
        {
            return strA.IsEqual(strB, ignoreCase);
        }

        /// <summary>
        /// Format size in bytes with unit.
        /// </summary>
        /// <param name="sizeInBytes">The size in bytes.</param>
        /// <param name="format">The format string. Adjust the format string to your preferences. 
        /// For example "{0:0.#}{1}" would show a single decimal place, and no space.</param>
        /// <returns>The formated string.</returns>
        private static readonly string[] _sizes = { "B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        public static string SizeWithUnit(long sizeInBytes, string format = "{0:0} {1}")
        {

            double len = sizeInBytes;
            int order = 0;
            while (len >= 1024 && order + 1 < _sizes.Length)
            {
                order++;
                len = len / 1024;
            }

            return string.Format(format, len, _sizes[order]);
        }

        private const string _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,;";
        private const string _ws = "  \r\n\t";
        private static Random _rng = new Random();

        /// <summary>
        /// Creates a random string.
        /// </summary>
        /// <param name="size">The size of the string to return.</param>
        /// <param name="alpha">The alphabet.</param>
        /// <returns>A random string with length size containing only characters from alpha</returns>
        public static string RandomString(int size, string alpha = _chars)
        {
            char[] buffer = new char[size];

            for (int i = 0; i < size; i++)
            {
                buffer[i] = alpha[_rng.Next(alpha.Length)];
            }
            return new string(buffer);
        }
        /// <summary>
        /// Creates a random string with whitespace characters only.
        /// </summary>
        /// <param name="size">The size of the string to return.</param>
        /// <param name="alpha">The alphabet.</param>
        /// <returns>A random string with length size containing only characters from alpha</returns>
        public static string RandomWhitespace(int size, string alpha = _ws)
        {
            return RandomString(size, alpha);
        }


        /// <summary>
        /// Checks the string and replace possible accents, umlauts, etc.
        /// </summary>
        /// <param name="text">The Source String</param>
        /// <returns>The clear String</returns>
        /// Solution from http://stackoverflow.com/questions/331279/how-to-change-diacritic-characters-to-non-diacritic-ones?lq=1
        private readonly static Regex nonSpacingMarkRegex = new Regex(@"\p{Mn}", RegexOptions.Compiled);
        private readonly static Regex invalidFilenameChars = new Regex(@"[\\'\\""\\<\\>\\?\\*\\/\\\\\|]");
        public static string ClearFilenameString(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return string.Empty;
            }

            var t = text;
            t = t.Replace("ä", "ae");
            t = t.Replace("ö", "oe");
            t = t.Replace("ü", "ue");
            t = t.Replace("Ä", "Ae");
            t = t.Replace("Ö", "Oe");
            t = t.Replace("Ü", "Ue");
            t = t.Replace("ß", "ss");
            
            var normalizedText = t.Normalize(NormalizationForm.FormD);

            normalizedText = nonSpacingMarkRegex.Replace(normalizedText, string.Empty);
            normalizedText = invalidFilenameChars.Replace(normalizedText, string.Empty);

            return normalizedText;

        }
        /// <summary>
        /// Cleans the string and only allows a-z0-9, replaces invalid chars, makes string lowercase 
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static string CleanString(string str)
        {
            var result = str.Trim().ToLower();
            result = result.Replace("ü", "ue");
            result = result.Replace("ö", "oe");
            result = result.Replace("ä", "ae");
            result = result.Replace("ß", "ss");
            result = Regex.Replace(result, @"[^a-z0-9-]+", "-");
            result = Regex.Replace(result, @"-+", "-");
            return result;
        }


        // http://stackoverflow.com/questions/397250/unicode-regex-invalid-xml-characters/961504#961504
        // From xml spec valid chars: 
        // #x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]     
        // any Unicode character, excluding the surrogate blocks, FFFE, and FFFF. 

        // filters control characters but allows only properly-formed surrogate sequences
        private static Regex _invalidXMLChars = new Regex(
            @"(?<![\uD800-\uDBFF])[\uDC00-\uDFFF]|[\uD800-\uDBFF](?![\uDC00-\uDFFF])|[\x00-\x08\x0B\x0C\x0E-\x1F\x7F-\x9F\uFEFF\uFFFE\uFFFF]",
            RegexOptions.Compiled);

        /// <summary>
        /// removes any unusual unicode characters that can't be encoded into XML
        /// </summary>
        public static string RemoveInvalidXMLChars(string text)
        {
            if (string.IsNullOrEmpty(text)) return "";
            return _invalidXMLChars.Replace(text, "");
        }

    }
}
