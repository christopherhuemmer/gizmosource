﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gizmo.General.GenericBinder
{
    public interface IInitFromString
    {
        void InitFromString(string data);
    }
}
