﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gizmo.General.GenericBinder
{
    public class ModelAlias : Attribute
    {
        public string Name { get; set; }
        public ModelAlias(string name)
        {
            Name = name;
        }
    }
}
