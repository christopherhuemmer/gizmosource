﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Reflection;
using System.Linq.Expressions;
using Gizmo.General.String;

namespace Gizmo.General.GenericBinder
{
    /// <summary>
    /// Maps an IDictionary<string, IEnumerable<string>> to an object and updates the properties of said object.
    /// The key represents the propertyname and the IEnumerable<string> is converted to match the type of the property
    /// Property mapping is case insensitive! so a param with name "nocamelcase" will still bind to a property NoCamelCase
    /// If there are multiple values for one parameter the first one will be used unless you use a List<> or an Array - then 
    /// all values are bound to the List<> / Array
    /// 
    /// Naming:
    /// You can use the Attribute PropertyAlias to name your property differently than the key
    /// E.g.
    /// [PropertyAlias("_formId")]
    /// public string FormId { get; set; }
    /// 
    /// To ignore properties for binding use:
    /// [BindIgnore]
    ///
    /// Type conversion:
    /// IConvertible takes care of Boolean, SByte, Byte, Int16, UInt16, Int32, UInt32, Int64, UInt64, Single, Double, Decimal, DateTime, Char, and String.
    /// For more complex values create a class that implements IInitFromString and use that class as a property type in you model object
    /// The class will be instantiated and InitFromString is called with the value in IEnumerable<string>. List<IInitFromString> is also supported. 
    /// Multiple objects will be instantiated and added to the list.
    /// </summary>
    public class GenericBinder
    {
        [Flags]
        protected enum PropInfo 
        {
            Empty = 0,
            Ignore = 1,
            Convertible = 1 << 1,
            InitFromString = 1 << 2,
            List = 1 << 3,
            Nullable = 1 << 4,
            Array = 1 << 5,
            Guid = 1 << 6,
        }

        /// <summary>
        /// Gets the object notation names to replace for a given object. 
        /// This is either the object type name or the ModelAliases and the object type name
        /// </summary>
        /// <param name="toUpdate">Object to update.</param>
        /// <returns></returns>
        protected static string[] GetReplaceValues(object toUpdate)
        {
            var aliasNames = toUpdate.GetType().GetCustomAttributes(typeof(ModelAlias), true).Select(x => ((ModelAlias)x).Name + ".");
            aliasNames = aliasNames.Concat(Enumerable.Repeat(toUpdate.GetType().Name + ".", 1));
            return aliasNames.ToArray();
        }
        
        /// <summary>
        /// Updates the specified object with the Data in paramDictioanry
        /// </summary>
        /// <param name="toUpdate">The object to update</param>
        /// <param name="paramDictionary">The parameter dictionary.</param>
        public static void Update(object toUpdate, IDictionary<string, List<string>> paramDictionary)
        {
            var replace = GetReplaceValues(toUpdate);
            var pd = paramDictionary.ToDictionary(k => k.Key.RemoveAll(replace, StringComparison.OrdinalIgnoreCase), v => v.Value.AsEnumerable(), StringComparer.OrdinalIgnoreCase);
            InternalUpdate(toUpdate, pd);
        }
        /// <summary>
        /// Updates the specified object with the Data in paramDictioanry
        /// </summary>
        /// <param name="toUpdate">The object to update</param>
        /// <param name="paramDictionary">The parameter dictionary.</param>
        public static void Update(object toUpdate, IDictionary<string, string[]> paramDictionary)
        {
            var replace = GetReplaceValues(toUpdate);
            var pd = paramDictionary.ToDictionary(k => k.Key.RemoveAll(replace, StringComparison.OrdinalIgnoreCase), v => v.Value.AsEnumerable(), StringComparer.OrdinalIgnoreCase);
            InternalUpdate(toUpdate, pd);
        }
        /// <summary>
        /// Updates the specified object with the Data in paramDictioanry
        /// </summary>
        /// <param name="toUpdate">The object to update</param>
        /// <param name="paramDictionary">The parameter dictionary.</param>
        public static void Update(object toUpdate, IDictionary<string, IEnumerable<string>> paramDictionary)
        {
            var replace = GetReplaceValues(toUpdate);
            var pd = paramDictionary.ToDictionary(k => k.Key.RemoveAll(replace, StringComparison.OrdinalIgnoreCase), v => v.Value, StringComparer.OrdinalIgnoreCase);
            InternalUpdate(toUpdate, pd);
        }
        
        
        /// <summary>
        /// Updates the specified object with the Data in paramDictioanry
        /// </summary>
        /// <param name="toUpdate">The object to update</param>
        /// <param name="paramDictionary">The parameter dictionary.</param>
        public static void InternalUpdate(object toUpdate, IDictionary<string, IEnumerable<string>> paramDictionary)
        {
            PropertyAlias nameAttrib;
            string fieldname;
            PropInfo pi;
            Type objectType;
            IEnumerable<string> rawValues;
            bool dataAvailable = false;

            //go through properties
            foreach (var prop in toUpdate.GetType().GetProperties())
            {
                dataAvailable = true;

                //get info about property
                pi = GetPropInfo(prop, out objectType);
                //ignore if not supported
                if (pi.HasFlag(PropInfo.Ignore)) { continue; }

                //get field name 
                nameAttrib = (PropertyAlias)prop.GetCustomAttributes(typeof(PropertyAlias), true).LastOrDefault();
                fieldname = (nameAttrib == null ? prop.Name : nameAttrib.Name);

                //check raw data
                paramDictionary.TryGetValue(fieldname, out rawValues);
                if (rawValues == null || !rawValues.Any())
                {
                    //there is no data to bind;
                    dataAvailable = false;
                }

                
                //create the list - if it is one
                if (pi.HasFlag(PropInfo.List))
                {
                    var listObj = prop.GetValue(toUpdate, null);
                    //only create list if its not there already, or we have new data to override the old present data
                    if (listObj == null || dataAvailable)
                    {
                        var listType = typeof(List<>);
                        var genericArgs = prop.PropertyType.GetGenericArguments();
                        var concreteType = listType.MakeGenericType(genericArgs);
                        var newList = Activator.CreateInstance(concreteType);
                        prop.SetValue(toUpdate, newList, null);
                    }
                }

                if (!dataAvailable)
                {
                    //no data to set - we inited Lists if needed but cant fill it with data, bail out...
                    continue;
                }

                //used for .Invoke 
                object[] paramArray = new object[1];

                //for everything that is supported by Convert
                if (pi.HasFlag(PropInfo.Convertible) || pi.HasFlag(PropInfo.Guid))
                {
                    var convertedValues = new List<object>();
                    var isBool = prop.PropertyType == typeof(bool);
                    //2017.06.06 //An Empty string can be a valid value.
                    //foreach (var raw in rawValues.Where(x => !string.IsNullOrEmpty(x)))
                    foreach (var raw in rawValues.Where(x => x != null))
                    {
                        try
                        {
                            object obj;
                            //AWI 2017.05.11 added special case for bool
                            if (isBool)
                            {
                                obj = raw.ToBool();
                            }
                            else if (pi.HasFlag(PropInfo.Guid))
                            {
                                obj = new Guid(raw);
                            }
                            else
                            {
                                obj = Convert.ChangeType(raw, objectType);
                            }
                            convertedValues.Add(obj);
                        }
                        catch { }
                    }

                    if (pi.HasFlag(PropInfo.Nullable) && !convertedValues.Any())
                    {
                        prop.SetValue(toUpdate, null, null);
                    }
                    else if (pi.HasFlag(PropInfo.List))
                    {
                        var listObj = prop.GetValue(toUpdate, null);
                        var addMethod = listObj.GetType().GetMethods().Where(x => x.Name == "Add").First();
                        foreach (var obj in convertedValues)
                        {
                            paramArray[0] = obj;
                            addMethod.Invoke(listObj, paramArray);
                        }
                    }
                    else if (pi.HasFlag(PropInfo.Array))
                    {
                        var arrVal = Array.CreateInstance(prop.PropertyType.GetElementType(), convertedValues.Count);
                        for(int i = 0; i< arrVal.Length; i ++ )
                        {
                            arrVal.SetValue(convertedValues[i], i);
                        }
                        prop.SetValue(toUpdate, arrVal, null);
                    }
                    else if(convertedValues.Any())
                    {
                        prop.SetValue(toUpdate, convertedValues.First(), null);
                    }
                }
                //for more complex objects
                else if(pi.HasFlag(PropInfo.InitFromString))
                {
                    if (pi.HasFlag(PropInfo.List))
                    {
                        var listObj = prop.GetValue(toUpdate, null);
                        var addMethod = listObj.GetType().GetMethods().Where(x => x.Name == "Add").First();

                        foreach (var str in rawValues)
                        {
                            var temp = (IInitFromString)Activator.CreateInstance(objectType);
                            temp.InitFromString(str);
                            paramArray[0] = temp;
                            addMethod.Invoke(listObj, paramArray);
                        }
                    }
                    else
                    {
                        var temp = (IInitFromString)Activator.CreateInstance(objectType);
                        temp.InitFromString(rawValues.First());
                        prop.SetValue(toUpdate, temp, null);
                    }
                }
            }

        }


        /// <summary>
        /// Gets the property information.
        /// </summary>
        /// <param name="prop">The property.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>Flagged Enum containing the meta information needed to create the correct objects</returns>
        private static PropInfo GetPropInfo(PropertyInfo prop, out Type objectType)
        {
            objectType = null;
            if (prop.GetCustomAttributes(typeof(BindIgnore), true).LastOrDefault() != null) { return PropInfo.Ignore; }

            PropInfo retInfo = PropInfo.Empty;
            if (prop.PropertyType.IsGenericType && (prop.PropertyType.GetGenericTypeDefinition() == typeof(List<>)))
            {
                //its a generic list
                retInfo = retInfo | PropInfo.List;
                objectType = prop.PropertyType.GetGenericArguments()[0];
            }
            else if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                retInfo = retInfo | PropInfo.Nullable;
                objectType = prop.PropertyType.GetGenericArguments()[0];
            }
            else if(prop.PropertyType.IsArray)
            {
                retInfo = retInfo | PropInfo.Array;
                objectType = prop.PropertyType.GetElementType();
            }
            else
            {
                objectType = prop.PropertyType;
            }

            if (typeof(IConvertible).IsAssignableFrom(objectType))
            {
                retInfo = retInfo | PropInfo.Convertible;
                return retInfo;
            }
            if (typeof(IInitFromString).IsAssignableFrom(objectType))
            {
                retInfo = retInfo | PropInfo.InitFromString;
                return retInfo;
            }
            if (prop.PropertyType == typeof(Guid))
            {
                retInfo = retInfo | PropInfo.Guid;
                return retInfo;
            }

            return PropInfo.Ignore;
        }

    }
}
