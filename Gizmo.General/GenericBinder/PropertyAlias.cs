﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gizmo.General.GenericBinder
{
    public class PropertyAlias : Attribute
    {
        public string Name { get; set; }
        public PropertyAlias(string name)
        {
            Name = name;
        }
    }
}
