﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Gizmo.General.IO
{
    public static class FileUtil
    {
        /// <summary>
        /// Copies a directory with all containing sub directories and files.
        /// </summary>
        /// <see cref="http://stackoverflow.com/a/3822913/1931663"/>
        /// <param name="sourcePath">Path of the directory to copy.</param>
        /// <param name="targetPath">Path of the new directory.</param>
        public static void CopyPath(string sourcePath, string targetPath)
        {
            // Now Create all of the directories
            var sourceDirs = Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories);
            foreach (string dirPath in sourceDirs)
            {
                Directory.CreateDirectory(dirPath.Replace(sourcePath, targetPath));
            }

            // Copy all the files & Replaces any files with the same name
            var sourceFiles = Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories);
            foreach (string newPath in sourceFiles)
            {
                System.IO.File.Copy(newPath, newPath.Replace(sourcePath, targetPath), true);
            }
        }

        /// <summary>
        /// Create all directories in a given path if not already existing.
        /// </summary>
        /// <param name="path">A directory or file path.</param>
        public static void CreatePath(string path)
        {
            // Get directory if path is a file.
            if (System.IO.File.Exists(path))
            {
                path = Path.GetDirectoryName(path);
            }
            Directory.CreateDirectory(path);
        }

        /// <summary>
        /// Delete directory.
        /// </summary>
        /// <param name="path">The directory path.</param>
        /// <param name="recursive">If true sub directories and files are deleted.</param>
        public static void DeletePath(string path, bool recursive = true)
        {
            if (Directory.Exists(path))
            {
                Directory.Delete(path, recursive);
            }
        }

        /// <summary>
        /// Check if a file is locked.
        /// IMPORTANT: Only use this in certain cases. Recommend best practice is to simply access 
        /// the file and handle failure accordingly.
        /// </summary>
        /// <see cref="http://stackoverflow.com/a/937558/1931663"/>
        /// <param name="file">The file.</param>
        /// <returns>True if file is locked else false.</returns>
        public static bool IsFileLocked(string file)
        {
            return IsFileLocked(new FileInfo(file));
        }

        /// <summary>
        /// Check if a file is locked.
        /// IMPORTANT: Only use this in certain cases. Recommend best practice is to simply access 
        /// the file and handle failure accordingly.
        /// </summary>
        /// <see cref="http://stackoverflow.com/a/937558/1931663"/>
        /// <param name="file">The file.</param>
        /// <returns>True if file is locked else false.</returns>
        public static bool IsFileLocked(FileInfo file)
        {
            bool result = false;

            try
            {
                using (var stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                { }
            }
            catch (IOException)
            {
                // The file is unavailable because it is:
                // - Still being written to.
                // - Being processed by another thread.
                // - Does not exist.
                result = true;
            }

            return result;
        }

        /// <summary>
        /// Copies a directory and its contents and deletes the source afterwards.
        /// </summary>
        /// <param name="sourcePath">Path of the directory to move.</param>
        /// <param name="targetPath">Path of the new directory.</param>
        public static void MovePath(string sourcePath, string targetPath)
        {
            CopyPath(sourcePath, targetPath);
            DeletePath(sourcePath);
        }

        /// <summary>
        /// Normalizes a path so it always ends with a slash (or another character if necessary).
        /// Using normal slash allows to use this method with file paths and urls.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="endChar">The end character.</param>
        /// <returns>The normalized path.</returns>
        public static string NormalizePath(string path, string endChar = "/")
        {
            return string.Concat(path.TrimEnd('/', '\\'), endChar);
        }
    }
}
