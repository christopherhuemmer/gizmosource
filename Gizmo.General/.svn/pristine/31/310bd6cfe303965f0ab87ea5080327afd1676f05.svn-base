﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Gizmo.General.IO;

namespace Gizmo.General.Debug
{
    /// <summary>
    /// Wrapper for a common logging solution.
    /// </summary>
    public static class Log
    {
        private static readonly string LogFile = @"log4net.config";
        private static log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Statically initialize logger the first time it is used.
        /// </summary>
        static Log()
        {
            // See http://stackoverflow.com/a/33675039/1931663
            var binDir = AppDomain.CurrentDomain.RelativeSearchPath; // bin folder for Web Apps 
            if (string.IsNullOrEmpty(binDir))
            {
                binDir = AppDomain.CurrentDomain.BaseDirectory; //exe folder for WinForms, Consoles, Windows Services
            }
            var configFile = new FileInfo(FileUtil.NormalizePath(binDir) + LogFile);
            log4net.Config.XmlConfigurator.ConfigureAndWatch(configFile);
        }

        /// <summary>
        /// Log debug message.
        /// </summary>
        /// <param name="message">The message.</param>
        public static void Debug(string message)
        {
            Logger.Debug(message);
        }

        /// <summary>
        /// Log debug message with format parameters..
        /// </summary>
        /// <param name="format">The format template for the message.</param>
        /// <param name="args">The format parameters.</param>
        public static void Debug(string format, params object[] args)
        {
            Logger.DebugFormat(format, args);
        }

        /// <summary>
        /// Log error message.
        /// </summary>
        /// <param name="message">The message.</param>
        public static void Error(string message)
        {
            Logger.Error(message);
        }

        /// <summary>
        /// Log error message with exception. Always prefer this over manually generating a message based on the exception.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        public static void Error(string message, Exception exception)
        {
            Logger.Error(message, exception);
        }

        /// <summary>
        /// Log error message with format parameters..
        /// </summary>
        /// <param name="format">The format template for the message.</param>
        /// <param name="args">The format parameters.</param>
        public static void Error(string format, params object[] args)
        {
            Logger.ErrorFormat(format, args);
        }

        /// <summary>
        /// Log info message.
        /// </summary>
        /// <param name="message">The message.</param>
        public static void Info(string message)
        {
            Logger.Info(message);
        }

        /// <summary>
        /// Log info message with format parameters..
        /// </summary>
        /// <param name="format">The format template for the message.</param>
        /// <param name="args">The format parameters.</param>
        public static void Info(string format, params object[] args)
        {
            Logger.InfoFormat(format, args);
        }

        /// <summary>
        /// Log warning message.
        /// </summary>
        /// <param name="message">The message.</param>
        public static void Warn(string message)
        {
            Logger.Warn(message);
        }

        /// <summary>
        /// Log warning message with format parameters..
        /// </summary>
        /// <param name="format">The format template for the message.</param>
        /// <param name="args">The format parameters.</param>
        public static void Warn(string format, params object[] args)
        {
            Logger.WarnFormat(format, args);
        }
    }
}
