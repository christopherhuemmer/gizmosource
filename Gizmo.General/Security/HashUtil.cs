﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace Gizmo.General.Security
{
    /// <summary>
    /// Hash related functions.
    /// </summary>
    public static class HashUtil
    {
        /// <summary>
        /// Format sha1 hash bytes.
        /// </summary>
        /// <param name="hash"></param>
        /// <returns></returns>
        public static string HashBytesToString(byte[] hash)
        {
            string result = BitConverter.ToString(hash).ToLower().Replace("-", "");
            return result;
        }

        /// <summary>
        /// Calculate hash from string.
        /// </summary>
        /// <param name="text">The string.</param>
        /// <param name="cryptoProvider">The crypto provider.</param>
        /// <returns>The hash.</returns>
        public static string Hash(string text, HashAlgorithm cryptoProvider)
        {
            var bytes = Encoding.UTF8.GetBytes(text);
            var hash = cryptoProvider.ComputeHash(bytes);
            return HashBytesToString(hash);
        }

        /// <summary>
        /// Calculate hash from file.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="cryptoProvider">The crypto provider.</param>
        /// <returns>The hash.</returns>
        public static string Hash(FileInfo file, HashAlgorithm cryptoProvider)
        {
            using (FileStream fs = new FileStream(file.FullName, FileMode.Open))
            {
                var hash = cryptoProvider.ComputeHash(fs);
                return HashBytesToString(hash);
            }
        }

        /// <summary>
        /// Calculate SHA1 hash from string.
        /// </summary>
        /// <param name="text">The string.</param>
        /// <returns>The hash.</returns>
        [Obsolete]
        public static string Sha1(string text)
        {
            using (var cryptoProvider = new SHA1CryptoServiceProvider())
            {
                return Hash(text, cryptoProvider);
            }
        }

        /// <summary>
        /// Calculate SHA1 hash from file.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>The hash.</returns>
        [Obsolete]
        public static string Sha1(FileInfo file)
        {
            using (var cryptoProvider = new SHA1CryptoServiceProvider())
            {
                return Hash(file, cryptoProvider);
            }
        }

        /// <summary>
        /// Calculate SHA256 hash from string.
        /// </summary>
        /// <param name="text">The string.</param>
        /// <returns>The hash.</returns>
        public static string Sha256(string text)
        {
            using (var cryptoProvider = new SHA256CryptoServiceProvider())
            {
                return Hash(text, cryptoProvider);
            }
        }

        /// <summary>
        /// Calculate SHA256 hash from file.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>The hash.</returns>
        public static string Sha256(FileInfo file)
        {
            using (var cryptoProvider = new SHA256CryptoServiceProvider())
            {
                return Hash(file, cryptoProvider);
            }
        }

        /// <summary>
        /// Calculate SHA512 hash from string.
        /// </summary>
        /// <param name="text">The string.</param>
        /// <returns>The hash.</returns>
        public static string Sha512(string text)
        {
            using (var cryptoProvider = new SHA512CryptoServiceProvider())
            {
                return Hash(text, cryptoProvider);
            }
        }

        /// <summary>
        /// Calculate SHA512 hash from file.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>The hash.</returns>
        public static string Sha512(FileInfo file)
        {
            using (var cryptoProvider = new SHA512CryptoServiceProvider())
            {
                return Hash(file, cryptoProvider);
            }
        }
    }
}
