/**
 * Configure RequireJS
 * @url: http://requirejs.org/docs/api.html
 */
requirejs.config({
	baseUrl: '/js/vendor',
	paths: {
		'main': '../main',
		'app': '../app',
		'jquery': 'jquery-2.1.4', // Google CDN: //ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery
		'underscore': 'underscore-1.8.3',
		'backbone': 'backbone-1.2.3',
		'responsiveTabs': 'jquery.responsiveTabs.min',
		'slick': 'slick-1.5.9.min',
		'colorBox': 'jquery.colorbox-min',
		'equalHeight': 'jquery.equalHeight-1.1',
		'formRequired': 'jquery.form-required-1.0.0',
		'mustache': 'mustache.min',
		'jquery-magnific-popup': 'jquery.magnific-popup-1.0.0',
		'fooTable': 'footable-custom',
		'socialshareprivacy': 'jquery.socialshareprivacy.min',
		'socialshareprivacyDe': 'jquery.socialshareprivacy.min.de',
		'jquery-autocomplete': 'jquery.autocomplete',
		'nouislider': 'nouislider.min',
		'jquery-dotdotdot': 'jquery.dotdotdot.min',
		'jquery-detectmobilebrowser': 'jquery.detectmobilebrowser',
		'select2': 'select2.min'
	},
	/**
	 * Map libs to use noconflict mode
	 * @url http://requirejs.org/docs/jquery.html#noconflictmap
	 */
	map: {
		'*': {
			'underscore': 'underscore-noconflict',
			'backbone': 'backbone-noconflict'
		},
		'underscore-noconflict': {
			'underscore': 'underscore'
		},
		'backbone-noconflict': {
			'backbone': 'backbone'
		}
	},

	shim: {
		'socialshareprivacyDe': ['socialshareprivacy']
	}
});

require([
	'app/app'
], function(Application) {

	'use strict';

	// Create the app and initialize it
	var app = Application.getInstance();
	window.app = app;
	app.initialize();
});
