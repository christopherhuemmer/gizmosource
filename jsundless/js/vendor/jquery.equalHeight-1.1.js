/**
 * jQuery plugin to adjust all selected elements to the same height.
 *
 * Example:
 * $('.my-widget').equalHeight(); // Where there are more than one elements with the .my-widget class.
 */
(function($) {
	'use strict';

	/**
	 * @param {[jQuery]} list
	 * @param {number} minHeight
	 */
	function processList(list, minHeight) {
		$.each(list, function(index, $element) {
			$element.css('height', minHeight);
		});
	}

	/**
	 * @param {{}} options
	 */
	$.fn.equalHeight = function(options) {
		var minHeight = 0,
			defaults = {
				resetOnly: false,
				columns: Infinity
			},
			list = [];

		options = $.extend({}, defaults, options);

		this.css('height', '');

		if (options.resetOnly) {
			return;
		}

		$.each(this, function() {
			var $this = $(this),
				height = $this.outerHeight();

			list.push($this);

			if (height > minHeight) {
				minHeight = height;
			}

			if (list.length >= options.columns) {
				processList(list, minHeight);
				list = [];
				minHeight = 0;
			}
		});

		processList(list, minHeight);
	};
})(jQuery);
