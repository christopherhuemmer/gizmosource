/**
 * A sample jQuery plugin that uses either AMD (e.g. via RequireJS)
 * or browser globals (via traditional JS includes)
 *
 * @url https://github.com/umdjs/umd
 */
/* global jQuery */
(function(factory) {

	'use strict';

	if (typeof define === 'function' && define.amd) {
		// AMD. Register as an anonymous module
		define(['jquery'], factory);
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function($) {

	'use strict';

	$.fn.samplePlugin = function(options) {
		var defaults = {
				setting1: 'default value',
				setting2: 'another default value'
			},
			opts = $.extend({}, defaults, options);

		return this.each(function() {
			var $this = $(this);

			$this.attr('data-setting1', opts.setting1);
			$this.attr('data-setting2', opts.setting2);
		});
	};
}));
