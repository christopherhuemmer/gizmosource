/* globals jQuery */

(function ($) {

	'use strict';

	$.fn.formRequired = function (options) {
		var defaults = {
			onClick: function () { }
		};

		options = $.extend({}, defaults, options);

		this.each(function (index, element) {
			var $element = $(element),
				$requiredOptions = $element.find('.required-option');

			$requiredOptions.on('click', function () {
				var $requiredInputs = $element.find('.required-input'),
					requiredClass = $(this).data('required');

				$requiredInputs.removeAttr('required');
				$element.find('.' + requiredClass).prop('required', true);

				options.onClick($requiredInputs);
			});

			$requiredOptions.filter(':checked').click();
		});
	};

})(jQuery);
