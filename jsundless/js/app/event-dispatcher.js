define([
	'underscore',
	'backbone'
], function(_, Backbone) {

	'use strict';

	/**
	 * Make sure that there is only a single instance of the EventDispatcher object
	 * @url http://addyosmani.com/resources/essentialjsdesignpatterns/book/#singletonpatternjavascript
	 */
	return (function() {

		// Store a reference to the single EventDispatcher instance
		var instance;

		/**
		 * @constructor
		 */
		function EventDispatcher() {
			this.initialize();
		}

		EventDispatcher.prototype.initialize = function() {
			_.extend(this, Backbone.Events);
		};

		return {
			// Get the single EventDispatcher instance if one exists or create one if it doesn't
			getInstance: function() {
				if (instance === undefined) {
					instance = new EventDispatcher();
				}

				return instance;
			}
		};
	}());
});
