define(['jquery', 'mustache'], function($, Mustache) {

	'use strict';

	function OrderProcessSummary() {}

	/**
	 * @param {jQuery} $elementList
	 * @param {jQuery} $template
	 * @param {string} bankName
	 * @returns {*}
	 */
	OrderProcessSummary.prototype.createSummary = function ($elementList, $template, bankName) {
		var summary = {
			summaryBox1: {},
			summaryBox2: {},
			summaryBox3: {
				bankName: bankName
			}
		};

		this.extractData($elementList, summary);

		/*
		 * For mustache cannot apply template logic at the template we gotta do it all here,
		 * resulting in a complicated to understand structure.
		 */

		this.refineCustomerData(summary);
		this.refinePartnerData(summary);
		this.refineDeliveryAddressData(summary);
		this.refineBillingAddressData(summary);
		this.refineRequestTypeData(summary);

		return Mustache.render($template.html(), summary);
	};

	/**
	 * @param {jQuery} $elementList
	 * @param {*} summary
	 */
	OrderProcessSummary.prototype.extractData = function ($elementList, summary) {
		$elementList.each(function (index, element) {
			var $element = $(element),
				parts,
				summaryBox,
				summaryKey,
				value;

			if (
				!$element.is(':visible') &&
				$element.attr('type') !== 'radio' // They are invisible but must be checked.
			) {
				return;
			}

			value = $element.val();

			if ($element.is('select')) {
				value = $element.val() // Do not use selectedIndex === 0 as preselected towns are delivered as single entries.
					? $element.find('option:selected').text()
					: '';
			}

			if ($element.attr('type') === 'radio') {
				if ($element.is(':checked')) {
					value = $element.next().text();
				}
				else {
					return; // Do not overwrite value of other options.
				}
			}

			parts = $element.data('summary').split('.');
			summaryBox = parts[0];
			summaryKey = parts[1];

			summary[summaryBox][summaryKey] = value;
		});
	};

	/**
	 * @param {*} summary
	 */
	OrderProcessSummary.prototype.refineCustomerData = function (summary) {
		summary.summaryBox1.hasCustomerSalutationTitle = (
			summary.summaryBox1.customerSalutation ||
			summary.summaryBox1.customerTitle
		);

		summary.summaryBox1.hasCustomerForenameSurname = (
			summary.summaryBox1.customerForename ||
			summary.summaryBox1.customerSurname
		);

		summary.summaryBox1.hasCustomerBirthday = (
			summary.summaryBox1.customerBirthdayDay &&
			summary.summaryBox1.customerBirthdayMonth &&
			summary.summaryBox1.customerBirthdayYear
		);
	};

	/**
	 * @param {*} summary
	 */
	OrderProcessSummary.prototype.refinePartnerData = function (summary) {
		summary.summaryBox1.hasPartnerSalutationTitle = (
			summary.summaryBox1.partnerSalutation ||
			summary.summaryBox1.partnerTitle
		);

		summary.summaryBox1.hasPartnerForenameSurname = (
			summary.summaryBox1.partnerForename ||
			summary.summaryBox1.partnerSurname
		);

		summary.summaryBox1.hasPartnerBirthday = (
			summary.summaryBox1.partnerBirthdayDay &&
			summary.summaryBox1.partnerBirthdayMonth &&
			summary.summaryBox1.partnerBirthdayYear
		);

		summary.summaryBox1.hasContractPartner = (
			summary.summaryBox1.hasPartnerSalutationTitle ||
			summary.summaryBox1.hasPartnerForenameSurname ||
			summary.summaryBox1.hasPartnerBirthday ||
			summary.summaryBox1.partnerEmail ||
			summary.summaryBox1.partnerPhoneNumber
		);
	};

	/**
	 * @param {*} summary
	 */
	OrderProcessSummary.prototype.refineDeliveryAddressData = function (summary) {
		summary.summaryBox1.hasDeliveryAddressStreetStreetNumber = (
			summary.summaryBox1.deliveryAddressStreet ||
			summary.summaryBox1.deliveryAddressStreetNumber
		);

		summary.summaryBox1.hasDeliveryAddressZipCodeTown = (
			summary.summaryBox1.deliveryAddressZipCode ||
			summary.summaryBox1.deliveryAddressTown
		);
	};

	/**
	 * @param {*} summary
	 */
	OrderProcessSummary.prototype.refineBillingAddressData = function (summary) {
		summary.summaryBox1.hasBillingAddressForenameSurname = (
			summary.summaryBox1.billingAddressForename ||
			summary.summaryBox1.billingAddressSurname
		);

		summary.summaryBox1.hasBillingAddressStreetStreetNumber = (
			summary.summaryBox1.billingAddressStreet ||
			summary.summaryBox1.billingAddressStreetNumber
		);

		summary.summaryBox1.hasBillingAddressZipCodeTown = (
			summary.summaryBox1.billingAddressZipCode ||
			summary.summaryBox1.billingAddressTown
		);

		summary.summaryBox1.hasBillingAddress = (
			summary.summaryBox1.hasBillingAddressForenameSurname ||
			summary.summaryBox1.hasBillingAddressStreetStreetNumber ||
			summary.summaryBox1.hasBillingAddressZipCodeTown
		);
	};

	/**
	 * @param {*} summary
	 */
	OrderProcessSummary.prototype.refineRequestTypeData = function (summary) {
		summary.summaryBox2.hasRequestDate = (
			summary.summaryBox2.requestMeterDay &&
			summary.summaryBox2.requestMeterMonth &&
			summary.summaryBox2.requestMeterYear
		);
	};

	return OrderProcessSummary;

});
