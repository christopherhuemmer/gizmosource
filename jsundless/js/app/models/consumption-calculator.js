define([], function() {

	'use strict';

	function ConsumptionCalculator() {}

	ConsumptionCalculator.prototype.calculateWarmth = function (squareMeters) {
		var energy = 1;

		if(squareMeters < 50) {
			energy = 133.333 + (squareMeters - 30) * 0.335;
		}else if(squareMeters >= 50 && squareMeters < 75) {
			energy = 140 + (squareMeters - 50) * -0.26668;
		}else if(squareMeters >= 75 && squareMeters < 100) {
			energy = 133.333 + (squareMeters - 75) * 0.26668;
		}else if(squareMeters >= 100 && squareMeters < 200) {
			energy = 140 + (squareMeters - 100) * 0.35;
		}else if(squareMeters >= 200 && squareMeters < 300) {
			energy = 175 + (squareMeters - 200) * 0.583;
		}else {
			energy = 233.333;
		}

		energy = Math.ceil(squareMeters * energy);

		return energy;
	};

	ConsumptionCalculator.prototype.calculateElectricity = function (persons) {
		var energy = 1;

		if(persons === 1) {
			energy = 1500;
		}else if(persons === 2) {
			energy = 2800;
		}else if(persons >= 3) {
			energy = persons * 1000;
		}

		return energy;
	};

	return ConsumptionCalculator;

});
