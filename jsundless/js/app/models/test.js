define([], function() {

	'use strict';

	function TestClass() {}

	TestClass.prototype.get = function () {
		return 'get';
	};

	return TestClass;

});
