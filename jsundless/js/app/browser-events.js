define([
	'underscore'
], function(_) {

	'use strict';

	/**
	 * Make sure that there is only a single instance of the BrowserEvents object
	 * @url http://addyosmani.com/resources/essentialjsdesignpatterns/book/#singletonpatternjavascript
	 */
	return (function() {

		// Store a reference to the single BrowserEvents instance
		var instance;

		/**
		 * @constructor
		 */
		function BrowserEvents() {
			this.initialize();
		}

		BrowserEvents.prototype.initialize = function() {
			this.eventDispatcher = window.app.eventDispatcher;

			window.addEventListener('scroll', _.throttle(this.onWindowScroll.bind(this), 250, { leading: false }));
			window.addEventListener('resize', _.debounce(this.onWindowResize.bind(this), 100));
		};

		BrowserEvents.prototype.onWindowScroll = function(event) {
			this.eventDispatcher.trigger('window:scroll', event);
		};

		BrowserEvents.prototype.onWindowResize = function(event) {
			this.eventDispatcher.trigger('window:resize', event);
		};

		return {
			// Get the single BrowserEvents instance if one exists or create one if it doesn't
			getInstance: function() {
				if (instance === undefined) {
					instance = new BrowserEvents();
				}

				return instance;
			}
		};
	}());
});
