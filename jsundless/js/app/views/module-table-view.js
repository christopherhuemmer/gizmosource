define([
	'jquery',
	'underscore',
	'backbone',
	'fooTable'
], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({
		initialize: function(options) {
			/*
			 * Source and documentation of the plugin.
			 *
			 * @link http://fooplugins.github.io/FooTable/
			 */
			this.$el.find('.table').footable();

			return this;
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
