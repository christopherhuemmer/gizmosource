define([
	'jquery',
	'underscore',
	'backbone',
	'mustache',
	'equalHeight'

], function($, _, Backbone, Mustache) {

	'use strict';

	var View = Backbone.View.extend({

		events: {
			'click .load-more': 'loadMore'
		},

		initialize: function(options) {
			this.ajaxHandler = window.app.ajaxHandler;

			this.imageTextTeaserJson = null;

			this.$imageTextContainer = this.$el.find('.image-text-teaser-holder');

			this.loadMore = this.$el.find('.load-more');

			/**
			 * @type {String} - The html template for mustache is stored here.
			 */
			this.$imageTextTemplate = null;

			/**
			 * @type {String} - The rendered mustache html templates are stored here.
			 */
			this.renderedTemplates = '';

			this.$loadMoreUrl = '';

			this.$count = this.$el.find('.count');

			/**
			 * @type {Integer} - The count of the current visible imageText teaser is stored here.
			 */
			this.currentShownElements = 0;

			this.setupListeners();

			this.render();

			return this;
		},

		render: function() {
			this.loadDocuments(false);

			return this;
		},

		setupListeners: function() {
			this.listenTo(window.app.eventDispatcher, 'window:resize', this.resize);
			this.listenTo(window.app.eventDispatcher, 'filter:change', function (parameters) {
				this.reset();
				this.getFilters(parameters);
			}.bind(this));
		},

		/**
		 * Set the counter (current, max) of the displayed and available service docs
		 */
		setCounter: function() {
			this.currentShownElements = this.$el.find('.image-text-teaser').length;
			this.$count.find('.current').html(this.currentShownElements);
			this.$count.find('.max').html(this.imageTextTeaserJson.maxAvailableInnovations);
		},

		/**
		 * Load more matching elements. It depends on the last set filter criteria
		 *
		 * @param event {Object}
		 */
		loadMore: function(event) {
			event.preventDefault();

			window.app.eventDispatcher.trigger('filter:loadMore', this.getFilters.bind(this));
		},

		loadDocuments: function(parameters) {
			this.$loadMoreUrl = this.$el.attr('data-url')+'?current='+this.currentShownElements+parameters;

			this.ajaxHandler.request({
				customSuccess: this.appendDocuments.bind(this),
				url: this.$loadMoreUrl
			});
		},

		getFilters: function(parameters) {
			var filters = '';
			if(parameters != undefined){
				filters = '&';
				for(var i=0; i<parameters.length; i++){
					if(i === (parameters.length-1)) {
						filters += parameters[i]
					}else{
						filters += parameters[i]+'&';
					}
				}
			}
			this.loadDocuments(filters);
		},

		appendDocuments: function(data) {
			this.imageTextTeaserJson = JSON.parse(data);

			this.imageTextTeaserJson.innovations.forEach(this.createimageTextTeaserEntry.bind(this));

			this.$imageTextContainer.append(this.renderedTemplates);

			if(this.imageTextTeaserJson.lastTeaser) {
				this.loadMore.hide(); //hide "Load More" button after getting the last teaser
			}

			this.setCounter();

			this.$el.find('.image-text-teaser').equalHeight();

			this.renderedTemplates = '';
		},

		/**
		 * Build the complete imageTexts teaser entry string
		 *
		 * @param newsTeasers {Object} - The given imageTexts teaser object
		 * @param index {Integer} - Appends one single imageTexts teaser
		 */
		createimageTextTeaserEntry: function(imageTextTeaser, index) {
			this.$imageTextTemplate = this.$el.find('#image-text-teaser-tmpl').html();

			this.renderedTemplates += Mustache.render(this.$imageTextTemplate, imageTextTeaser);
		},

		resize: function() {
			this.$el.find('.image-text-teaser').equalHeight();
		},

		reset: function() {
			this.currentShownElements = 0;
			this.$imageTextContainer.empty();
			this.loadMore.show();
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
