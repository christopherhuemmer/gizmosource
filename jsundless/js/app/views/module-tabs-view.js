define([
	'jquery',
	'underscore',
	'backbone',
	'responsiveTabs'
], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({
		initialize: function(options) {
			var collapsible = this.$el.data('collapsible');

			if (collapsible === undefined) {
				collapsible = 'accordion';
			}

			/*
			 * Source and documentation of the plugin.
			 *
			 * @link https://github.com/jellekralt/Responsive-Tabs
			 */
			this.$el.find('> div').responsiveTabs({
				collapsible: collapsible,
				startCollapsed: false,
				scrollToAccordionOnLoad: false,
				scrollToAccordion: true
			});

			return this;
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
