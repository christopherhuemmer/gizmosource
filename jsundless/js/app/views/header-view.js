define([
	'jquery',
	'underscore',
	'backbone'
], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		initialize: function(options) {
			// Remove el and $el from the options
			this.options = _.extend({}, this.defaults, _.omit(options, ['el', '$el']));

			this.eventDispatcher = window.app.eventDispatcher;
			this.setupListeners();

			this.render();
		},

		setupListeners: function() {
			this.listenTo(this.eventDispatcher, 'window:scroll', this.render);
			this.listenTo(this.eventDispatcher, 'viewport:change', this.render);
		},

		render: function() {
			return this;
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
