define([
	'jquery',
	'underscore',
	'backbone'

], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		events: {
			'change .inputfile': 'changeText',
			'click .file-delete': 'deleteFile'
		},

		initialize: function(options) {
			this.render();
		},

		render: function() {
			;
		},

		changeText: function(event) {
			var fileName = '',
				$label = $(event.target).next(),
				labelVal = $label.find('span').text();

			fileName = event.target.value.split( '\\' ).pop();

			if(fileName) {
				$label.find('span').removeClass('upload-icon');
				$label.next().show();
				$label.find('span').text(fileName);
			}else {
				$label.find('span').text(labelVal);
			}
		},

		deleteFile: function(event) {
			var $this = $(event.target),
				$input = $this.prev().prev(),
				$label = $this.prev().find('span'),
				$text = $label.attr('data-text');
			$input.val('');
			$this.hide();
			$label.text($text);
			$label.addClass('upload-icon');
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
