define([
	'jquery',
	'underscore',
	'backbone',
	'mustache',
	'equalHeight',
	'jquery-dotdotdot'

], function($, _, Backbone, Mustache) {

	'use strict';

	var View = Backbone.View.extend({

		events: {
			'click .load-more': 'loadMore'
		},

		initialize: function(options) {
			this.ajaxHandler = window.app.ajaxHandler;

			this.downloadTeaserJson = null;

			this.$downloadContainer = this.$el.find('.download-teaser-holder');

			this.loadMore = this.$el.find('.load-more');

			/**
			 * @type {String} - The html template for mustache is stored here.
			 */
			this.$downloadTemplate = null;

			/**
			 * @type {String} - The rendered mustache html templates are stored here.
			 */
			this.renderedTemplates = '';

			this.$loadMoreUrl = '';

			this.$count = this.$el.find('.count');

			/**
			 * @type {Integer} - The count of the current visible download teaser is stored here.
			 */
			this.currentShownElements = '';

			this.setupListeners();

			this.resize();

			this.render();

			return this;
		},

		render: function() {
			this.loadDocuments();

			return this;
		},

		setupListeners: function() {
			this.listenTo(window.app.eventDispatcher, 'window:resize', this.resize);
			this.listenTo(window.app.eventDispatcher, 'filter:change', function (parameters) {
				this.reset();
				this.getFilters(parameters);
			}.bind(this));
		},

		/**
		 * Set the counter (current, max) of the displayed and available service docs
		 */
		setCounter: function() {
			this.currentShownElements = this.$el.find('.download-teaser').length;
			this.$count.find('.current').html(this.currentShownElements);
			this.$count.find('.max').html(this.downloadTeaserJson.maxAvailableDownloads);
		},

		/**
		 * Load more matching elements. It depends on the last set filter criteria
		 *
		 * @param event {Object}
		 */
		loadMore: function(event) {
			event.preventDefault();

			window.app.eventDispatcher.trigger('filter:loadMore', this.getFilters.bind(this));
		},

		loadDocuments: function(parameters) {
			this.$loadMoreUrl = this.$el.attr('data-url')+'?current='+this.currentShownElements+parameters;

			this.ajaxHandler.request({
				customSuccess: this.appendDocuments.bind(this),
				url: this.$loadMoreUrl
			});
		},

		getFilters: function(parameters) {
			var filters = '';
			if(parameters != undefined){
				filters = '&';
				for(var i=0; i<parameters.length; i++){
					if(i === (parameters.length-1)) {
						filters += parameters[i]
					}else{
						filters += parameters[i]+'&';
					}
				}
			}
			this.loadDocuments(filters);
		},

		appendDocuments: function(data) {
			this.downloadTeaserJson = JSON.parse(data);
			this.downloadTeaserJson.downloads.forEach(this.createDownloadTeaserEntry.bind(this));

			this.$downloadContainer.append(this.renderedTemplates);

			this.dotdotdot(function($wrapper) {
				$wrapper.trigger('destroy'); // Kill old ones.
				$wrapper.dotdotdot({ // (Re-) Initialize plugin for all wrappers.
					height: 100, // About 2 lines in desktop size. Also set in css dotdotdot wrapper.
					wrap: 'letter'
				});
			});

			if(this.downloadTeaserJson.lastTeaser) {
				this.loadMore.hide(); //hide "Load More" button after getting the last teaser
			}

			this.setCounter();

			window.app.eventDispatcher.trigger('equalHeight:update');
			//this.$el.find('.download-teaser').equalHeight();

			this.renderedTemplates = '';
		},

		/**
		 * Build the complete downloads teaser entry string
		 *
		 * @param downloadTeaser {Object} - The given downloads teaser object
		 * @param index {Integer} - Appends one single downloads teaser
		 */
		createDownloadTeaserEntry: function(downloadTeaser, index) {
			this.$downloadTemplate = this.$el.find('#download-teaser-tmpl').html();

			this.renderedTemplates += Mustache.render(this.$downloadTemplate, downloadTeaser);
		},

		/**
		 * Passes the dotdotdot wrapper(s) to the passed function to execute actions on them.
		 *
		 * @param {function} action
		 */
		dotdotdot: function(action) {
			action(this.$downloadContainer.find('.dotdotdot-wrapper'));
		},

		resize: function() {
			this.dotdotdot(function($wrapper) {
				$wrapper.trigger('update');
			});
		},

		reset: function() {
			this.currentShownElements = 0;
			this.$downloadContainer.empty();
			this.loadMore.show();
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
