define([
	'jquery',
	'underscore',
	'backbone'
], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		events: {
			'click .list-group-textual li:first-of-type': 'toggleAccordion'
		},

		initialize: function(options) {
			this.$accordionTitle = this.$el.find('.list-group-textual li:first-of-type');
		},

		toggleAccordion: function(event) {
			$(event.target).parent().toggleClass('active');
		},

		render: function() {
			return this;
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
