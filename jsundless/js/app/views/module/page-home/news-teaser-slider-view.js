define([
	'jquery',
	'underscore',
	'backbone',
	'slick'
], function($, _, Backbone) {

	'use strict';

	var View;
	View = Backbone.View.extend({

		initialize: function() {
			this.$slider = this.$el.find('.news-teaser-holder');

			/*
			 * Configuration options:
			 *
			 * @link http://kenwheeler.github.io/slick/
			 */
			this.$slider.slick({
				slidesToShow: 1,
				arrows: true,
				autoplay: true,
				autoplaySpeed: 10000
			});

			this.$slider.on('afterChange', this.setCount.bind(this));

			this.render();
		},

		render: function() {
			this.setCount();
		},

		setCount: function() {
			var max = this.$slider.slick('getSlick').slideCount,
				current = parseInt(this.$slider.find('.slick-active').last().attr('data-slick-index')) + 1;
			this.$el.find('.max').text(max);
			this.$el.find('.current').text(current);
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
