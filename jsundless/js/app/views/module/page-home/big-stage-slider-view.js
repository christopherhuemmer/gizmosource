define([
	'jquery',
	'underscore',
	'backbone',
	'slick'
], function($, _, Backbone) {

	'use strict';

	var View;
	View = Backbone.View.extend({

		initialize: function() {
			var $slider = this.$el;

			/*
			 * Configuration options:
			 *
			 * @link http://kenwheeler.github.io/slick/
			 */
			$slider.slick({
				slidesToShow: 1,
				arrows: false,
				dots: true
			});

			return this;
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
