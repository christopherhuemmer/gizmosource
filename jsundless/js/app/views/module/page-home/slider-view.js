define([
	'jquery',
	'underscore',
	'backbone',
	'slick'
], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({
		initialize: function() {
			var $slider = this.$el.find('.stage-slider');

			/*
			 * Configuration options:
			 *
			 * @link http://kenwheeler.github.io/slick/
			 */
			$slider.slick({
				slidesToShow: 1,
				arrows: false,
				dots: true,
				autoplay: true,
				autoplaySpeed: 10000
			});

			return this;
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
