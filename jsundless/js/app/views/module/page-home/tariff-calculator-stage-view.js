define([
	'jquery',
	'underscore',
	'backbone'
], function($, _, Backbone) {

	'use strict';

	var View;
	View = Backbone.View.extend({

		events: {
			'click .nav-tabs li > a': 'toggleCalculator'
		},

		initialize: function() {
			this.$tabs = this.$el.find('.nav-tabs li');
			this.$tabsContent = this.$el.find('.tariff-calculator-tab');

			var self = this,
				rawUrl = this.$el.data('url'),
				responseFixture,
				autocompleteSetup;

			autocompleteSetup = {
				minLength: 1,
				appendMethod: 'replace',
				valid: undefined,
				autoselect: true,
				source: [],
				valueKey: 'title',
				titleKey: 'title',
				dropdownWidth: '100%'

				// Do not set any of these as the result set cannot be scrolled on mobile devices.
				// visibleLimit: 4
				// visibleHeight: 200
			};

			if (rawUrl) {
				// Live or real data testing environment.

				autocompleteSetup.valid = function() {
					return true; // Enforces all results to be shown, since they already got server-filtered anyway.
				};

				autocompleteSetup.source.push(function(query, add) {
					var url = rawUrl.replace('{query}', encodeURIComponent(query.split(',')[0]));

					$.getJSON(url, function(response) {
						self.refineData(response);
						add(response);
					});
				});
			}
			else {
				// Prototype or testing environment.
				// Keep list in sync with: #search-phases
				responseFixture = [
					{ zip: '72108', town: 'Rottenburg am Neckar' },
					{ zip: '10115', town: 'Berlin Mitte' },
					{ zip: '10117', town: 'Berlin Mitte' },
					{ zip: '71083', town: 'Herrenberg' }
				];

				this.refineData(responseFixture);

				autocompleteSetup.source.push(function(query, add) {
					add(responseFixture);
				});
			}

			this.$el.find('input.tariff-calculator-zip').autocomplete(autocompleteSetup);

			return this;
		},

		toggleCalculator: function(e) {
			e.preventDefault();
			var $tabId = $(e.currentTarget).attr('href');
			this.$tabs.removeClass('active');
			$(e.currentTarget).parent().addClass('active');
			this.$tabsContent.removeClass('active-tab');
			$('#' + $tabId).addClass('active-tab');
		},

		refineData: function(list) {
			_.forEach(list, function(index, data) {
				index.title = index.zip + ', ' + index.town;
			});
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
