define([
	'jquery',
	'underscore',
	'backbone',
	'mustache'

], function($, _, Backbone, Mustache) {

	'use strict';

	var View = Backbone.View.extend({

		events: {
			'click .load-more': 'loadMore',
			'click .tags-teaser > ul > li': 'loadNew',
			'click .reset-filter': 'resetFilter',
			'click .back-to-top > a': 'backToTop'
		},

		initialize: function(options) {
			this.ajaxHandler = window.app.ajaxHandler;

			this.blogEntrysJson = null;

			this.$blogContainer = this.$el.find('.blog-box-holder');

			this.$blogContainerEmpty = false;

			this.tagList = this.$el.find('.tags-teaser > ul');

			this.resetFilter = this.$el.find('.reset-filter');

			this.loadMore = this.$el.find('.load-more');

			/**
			 * @type {String} - The html template for mustache is stored here.
			 */
			this.$downloadTemplate = null;

			/**
			 * @type {String} - The rendered mustache html templates are stored here.
			 */
			this.renderedTemplates = '';

			this.$loadMoreUrl = '';

			/**
			 * @type {Integer} - The count of the current visible service documents is stored here.
			 */
			this.currentShownElements = 0;

			this.render();

			return this;
		},

		render: function() {
			//this.loadDocuments('');

			return this;
		},

		/**
		 * Load more matching elements.
		 *
		 * @param event {Object}
		 */
		loadMore: function(event) {
			event.preventDefault();

			this.getFilters();
		},

		loadNew: function(event) {
			event.preventDefault();
			var $current = $(event.currentTarget);

			if($current.hasClass('active')) {
				$current.removeClass('active');
			}else{
				$current.addClass('active');
			}

			this.reset();

			this.getFilters();
		},

		resetFilter: function(event) {
			event.preventDefault();
			this.tagList.find('li').removeClass('active');

			this.reset();

			this.getFilters();
		},

		getFilters: function() {
			var filters = '',
				$activeFilter = this.tagList.find('.active');
			if($activeFilter.length){
				this.resetFilter.show();

				filters = '&tag=';
				for(var i=0; i<$activeFilter.length;i++) {
					if(i === ($activeFilter.length-1)) {
						filters += $($activeFilter[i]).attr('data-tag');
					}else{
						filters += $($activeFilter[i]).attr('data-tag')+'&tag=';
					}
				}
			}else{
				this.resetFilter.hide();
			}
			this.loadDocuments(filters);
		},

		loadDocuments: function(parameters) {
			this.currentShownElements = this.$el.find('.box').length;
			this.$loadMoreUrl = this.$el.attr('data-url')+'?current='+this.currentShownElements+parameters;

			this.ajaxHandler.request({
				customSuccess: this.appendDocuments.bind(this),
				url: this.$loadMoreUrl
			});
		},

		appendDocuments: function(data) {
			this.blogEntrysJson = JSON.parse(data);

			//set flag for first element if blogContainer is empty
			if(this.$blogContainerEmpty) {
				this.blogEntrysJson.results[0].isFirst = true;
			}

			this.blogEntrysJson.results.forEach(this.createBlogEntry.bind(this));

			this.$blogContainer.append(this.renderedTemplates);
			this.$blogContainerEmpty = false;

			//trigger respimage if browser dont support sizes/srcset
			if(!Modernizr.sizes||!Modernizr.srcset){
				var $images = this.$blogContainer.find('img');
				respimage({elements: $images, reevaluate: true});
			}

			this.$blogContainer.find('.box:hidden').each(function(index) {
				$(this).delay(300*index).fadeIn(400);
			});

			if(this.blogEntrysJson.lastTeaser) {
				this.loadMore.hide(); //hide "Load More" button after getting the last teaser
			}

			this.renderedTemplates = '';
		},

		/**
		 * Build the complete downloads teaser entry string
		 *
		 * @param blogEntry {Object} - The given blog entrys object
		 * @param index {Integer} - Appends one single blog entry
		 */
		createBlogEntry: function(blogEntry, index) {
			this.$blogTemplate = this.$el.find('#blog-box-tmpl').html();

			this.renderedTemplates += Mustache.render(this.$blogTemplate, blogEntry);
		},

		backToTop: function(event) {
			event.preventDefault();

			$('body,html').animate({
				scrollTop: 0
			}, 800);
		},

		reset: function() {
			this.currentShownElements = 0;
			this.$blogContainer.empty();
			this.$blogContainerEmpty = true;
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
