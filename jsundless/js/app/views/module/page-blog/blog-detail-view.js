define([
	'jquery',
	'underscore',
	'backbone'

], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		events: {
			'click .back-to-top > a': 'backToTop'
		},

		initialize: function(options) {
			// Remove el and $el from the options
			this.options = _.extend({}, this.defaults, _.omit(options, ['el', '$el']));

			this.$socialToolbar = $('.social-toolbar');

			this.$windowTop = '';
			this.$socialTop = this.$socialToolbar.offset().top;

			this.eventDispatcher = window.app.eventDispatcher;
			this.setupListeners();

			this.initSocialToolbar();

			return this;
		},

		setupListeners: function() {
			this.listenTo(this.eventDispatcher, 'window:scroll', this.stickySocial);
			this.listenTo(this.eventDispatcher, 'window:resize', this.resize);
		},

		render: function() {

			return this;
		},

		initSocialToolbar: function() {
			var width = this.$socialToolbar.width(),
				$socialList = this.$socialToolbar.find('ul');
			this.$socialToolbar.width(width);
			$socialList.width(width);

			this.stickySocial();
		},

		stickySocial: function() {
			this.$windowTop = $(window).scrollTop();
			if (this.$windowTop > this.$socialTop) {
				this.$socialToolbar.addClass('fixed');
			}else {
				this.$socialToolbar.removeClass('fixed');
			}
		},

		backToTop: function(event) {
			event.preventDefault();

			$('body,html').animate({
				scrollTop: 0
			}, 800);
		},

		resize: function() {
			this.initSocialToolbar();
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
