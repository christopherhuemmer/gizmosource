define([
	'jquery',
	'underscore',
	'backbone',
	'mustache',
	'app/models/order-process-summary',
	'equalHeight',
	'formRequired'
], function ($, _, Backbone, Mustache, OrderProcessSummary) {

	'use strict';

	/**
	 * This view/class heavily breaks the single responsibility principle as
	 * it cares about many concerns at once like bindings, validations and
	 * template modifications of many different components.
	 *
	 * These components should be extracted using extract class refactoring
	 * and be placed into business model classes while the view only remains
	 * for setting up the bindings, as intended by backbone.
	 */
	var View = Backbone.View.extend({

		$userZip: undefined,
		$userStreet: undefined,
		$userStreetNumber: undefined,
		$userStreetNumberAddition: undefined,
		$userCity: undefined,

		events: {
			'click .show-extra-container': 'loadMore',
			'click .hide-extra-container': 'removeMore',
			'keyup input': 'updateSummayOptions',
			'change select, input[type=radio]': 'updateSummayOptions',
			'change #city': 'onChangeCity',
			'change #street': 'onChangeStreet',
			'keyup #iban': 'ibanChanged',
			'keyup #coupon-code': 'checkCouponCode'
		},

		initialize: function () {
			var rawUrl = this.$el.data('url-delivery'),
				responseFixture,
				autocompleteSetup;

			this.ajaxHandler = window.app.ajaxHandler;

			this.$ibanUrl = '';
			this.ibanJson = null;

			this.bankName = '';
			this.$bankNameHTML = $('.bank-name').html();

			/**
			 * @type {String} - The html template for mustache is stored here.
			 */
			this.$summaryTemplate = null;

			/**
			 * @type {String} - The rendered mustache html templates are stored here.
			 */
			this.renderedTemplates = '';

			this.$summaryContainer = this.$el.find('.summary-container');

			autocompleteSetup = {
				minLength: 1,
				appendMethod: 'replace',
				valid: undefined,
				autoselect: true,
				source: [],
				valueKey: 'title',
				titleKey: 'title',
				dropdownWidth: '100%'
			};

			if (rawUrl) {
				// Live or real data testing environment.

				autocompleteSetup.valid = function () {
					return true; // Enforces all results to be shown, since they already got server-filtered anyway.
				};

				autocompleteSetup.source.push(function (query, add) {
					var url = rawUrl.replace('{query}', encodeURIComponent(query));

					$.getJSON(url, function (response) {
						add(response);
					});
				});
			}
			else {
				// Prototype or testing environment.
				// Keep list in sync with: #search-phases
				responseFixture = [
					{ title: 'Test' },
					{ title: 'Lorem Ipsum' },
					{ title: 'Dorladas asdas' },
					{ title: 'Ttasd12 asdsa' }
				];

				autocompleteSetup.source.push(function (query, add) {
					add(responseFixture);
				});
			}

			this.$el.find('#supplier').autocomplete(autocompleteSetup);

			this.handleConditionalRequiredFields();
			this.render();

			return this;
		},

		render: function () {
			this.$userZip = this.$el.find('#zip');
			this.$userCity = this.$el.find('#city');
			this.$userStreet = this.$el.find('#street');
			this.$userStreetNumber = this.$el.find('#house-number');
			this.$userStreetNumberAddition = this.$el.find('#house-number-addition');
			this.$paymentCouponCode = this.$el.find('#coupon-code');

			this.renderSummaryBox();
			this.$userZip.trigger('keyup');

			// Initialize the form, in case the user does not click on any optional box anchor.
			this.updateHiddenRequireFields();
		},

		/**
		 * @param {Event} event
		 */
		loadMore: function (event) {
			event.preventDefault();

			$(event.currentTarget).hide().next('.extra-container').show();

			this.updateHiddenRequireFields();
			this.renderSummaryBox();
		},

		/**
		 * @param {Event} event
		 */
		removeMore: function (event) {
			event.preventDefault();

			$(event.currentTarget).parent().hide().prev().show();

			this.updateHiddenRequireFields();
			this.renderSummaryBox();
		},

		updateSummayOptions: function () {
			var $checkedRadioDelivery = this.$el.find('.step-2-content').find($('input[type=radio]:checked')),
				$checkedRadioPayment = this.$el.find('.step-3-content').find($('input[type=radio]:checked'));

			//special hide and show elements on radio change
			if ($checkedRadioDelivery.hasClass('delivery-exchange')) {
				this.$el.find('.delivery-supplier').show();
				this.$el.find('.delivery-reading').show();
				this.$el.find('.delivery-date').hide();
			}
			else {
				this.$el.find('.delivery-supplier').hide();
				this.$el.find('.delivery-reading').hide(); // Even when newly moving in there is a value present, this will cause customers to pay consumption already payed at construction.
				this.$el.find('.delivery-date').show();
			}

			if ($checkedRadioPayment.hasClass('radio-payment-transfer')) {
				this.$el.find('.payment-sepa').hide();
				this.$el.find('.payment-transfer').show();
				this.$el.find('.checkbox-sepa').hide();
			}
			else if ($checkedRadioPayment.hasClass('radio-payment-sepa')) {
				this.$el.find('.payment-sepa').show();
				this.$el.find('.payment-transfer').hide();
				this.$el.find('.checkbox-sepa').show();
			}

			this.renderSummaryBox();
		},

		renderSummaryBox: function () {
			var summaryHtml;

			this.orderProcessSummary = this.orderProcessSummary || new OrderProcessSummary();

			summaryHtml = this.orderProcessSummary.createSummary(
				this.$el.find('[data-summary]'),
				this.$el.find('#summary-boxes-tmpl'),
				this.bankName,
				this.couponData
			);

			this.$summaryContainer.html(summaryHtml);

			this.$el.find('.summary-box').equalHeight();
		},

		removeIbanError: function () {
			this.$el.find('.bank-name-error').hide();
			this.$el.find('#iban').get(0).setCustomValidity('');
		},

		/**
		 * @param {boolean} showError set to true displays the red error message.
		 */
		setIbanError: function (showError) {
			var $bankNameError = this.$el.find('.bank-name-error');

			if (showError) {
				$bankNameError.show();
			}

			this.$el.find('#iban').get(0).setCustomValidity(
				$bankNameError.text()
			);
		},

		/**
		 * @param {Event} event
		 */
		ibanChanged: function (event) {
			var value = $(event.currentTarget).val().replace(/\s+/g, ''),
				$bankName = this.$el.find('.bank-name');

			this.bankName = '';

			if (value.length > 21) {
				this.$ibanUrl = this.$el.attr('data-url-iban') + '?iban=' + value;

				this.ajaxHandler.request({
					customSuccess: this.setBankFromIban.bind(this),
					url: this.$ibanUrl
				});
			}
			else {
				this.setIbanError();
				$bankName.show().html(this.$bankNameHTML);
			}

			this.renderSummaryBox();
		},

		/**
		 * @param {{}|string} data
		 */
		setBankFromIban: function (data) {
			var $bankName = this.$el.find('.bank-name');

			this.ibanJson = typeof data === 'string'
				? JSON.parse(data)
				: data;

			if (this.ibanJson.success) {
				$bankName.show();

				if (this.ibanJson.bank !== '') {
					$bankName.html('<strong>' + this.ibanJson.bank + '</strong>');

					this.bankName = this.ibanJson.bank;
					this.renderSummaryBox();
				}

				this.removeIbanError();
			}
			else {
				$bankName.hide();

				this.setIbanError(true);
			}
		},

		/**
		 * @param {Event} event
		 */
		onChangeCity: function (event) {
			event.preventDefault();

			var rawUrl = this.$userCity.attr('data-url'),
				userCityValue = this.$userCity.prop('value');

			if (userCityValue) {
				if (rawUrl) {
					this.ajaxHandler.request({
						customSuccess: this.updateStreet.bind(this),
						data: {
							zip: this.$userZip.prop('value'),
							town: userCityValue
						},
						dataType: 'json',
						url: rawUrl
					});
				}
				else {
					// Prototype or testing environment.
					// Keep list in sync with: #search-phases
					var responseFixture = [
						{ label: 'Test', id: 1 },
						{ label: 'Lorem Ipsum', id: 2 },
						{ label: 'Dorladas asdas', id: 3 },
						{ label: 'Ttasd12 asdsa', id: 4 }
					];
					this.updateStreet(responseFixture, 'success', null);
				}
			}
			else {
				this.disableAdressComponents();
				this.clearStreetSelect();
			}
		},

		/**
		 * Callback for ajax request triggered by changing city selet
		 * @param response {Object} - Responce object from webservice
		 * @param status {String} - String with status message of ajax call
		 * @param jqXHR
		 */
		updateStreet: function (response, status, jqXHR) {
			if (status === 'success') {
				this.clearStreetSelect();

				var options = '';

				if (response) {
					response.forEach(function (item) {
						options += '<option value="' + item.id + '">' + item.label + '</option>';
					});

					this.$userStreet.append(options);
					this.enableStreet();
				}
			}
		},

		onChangeStreet: function (event) {
			if (this.$userStreet.prop('value')) {
				this.enableStreetComponents();
			} else {
				this.disableStreetComponents();
			}
		},

		clearStreetSelect: function () {
			this.$userStreet.find('option:gt(0)').remove();
			this.$userStreet.find('option').prop('selected', 'selected');
		},

		enableStreet: function () {
			this.$userStreet.prop('disabled', false);
		},

		enableStreetComponents: function () {
			this.$userStreetNumber.prop('disabled', false);
			this.$userStreetNumberAddition.prop('disabled', false);
		},

		disableStreetComponents: function () {
			this.$userStreetNumber.prop('disabled', 'disabled');
			this.$userStreetNumberAddition.prop('disabled', 'disabled');
		},

		disableAdressComponents: function () {
			this.$userStreet.prop('disabled', 'disabled');
			this.$userStreetNumber.prop('disabled', 'disabled');
			this.$userStreetNumberAddition.prop('disabled', 'disabled');
		},

		/**
		 * Note:
		 * There are no specific rules concerning the coupon code that have
		 * to be checked or taken into account.
		 */
		updateCouponCode: function () {
			var rawUrl = this.$paymentCouponCode.data('url'),
				couponCode = this.$paymentCouponCode.val(),
				$couponData = this.$el.find('.coupon-data'),
				$userSettingsBox = $('.user-settings .box');

			// In case one removes the code again.
			$couponData.hide();
			$userSettingsBox.removeClass('has-coupon');

			if (!couponCode) {
				return;
			}

			this.ajaxHandler.request({
				url: rawUrl.replace('{couponCode}', couponCode),
				dataType: 'json',
				customSuccess: function (response) {
					if (!response) {
						return;
					}

					// This is kinda evil, accessing another component globally... adjusting several nodes.
					$('.coupon-title').html(response.title);
					$('.coupon-value').html(response.value);

					$userSettingsBox.addClass('has-coupon');
					$couponData.show();
				}.bind(this)
			});
		},

		/**
		 * This is kinda stupid...
		 * The debouncer is required to not fire against the api on every key event.
		 * Not only to save performance, but also because responses can return in
		 * mixed order, causing timing bugs! (Still okay until now.)
		 *
		 * Thus the debouncer returns a function to be called every time the action
		 * is requested. This function must be connected to the binding, but the
		 * binding is done by Backbone before the initialize method gets called.
		 * Thus the pointer has to be available right from the start and must not
		 * be changed afterwards, which results in this over-complicated setup.
		 */
		checkCouponCode: function () {
			if (!this.checkCouponCodeDebouncer) {
				this.checkCouponCodeDebouncer = _.debounce(this.updateCouponCode, 300);
			}

			this.checkCouponCodeDebouncer();
		},

		/**
		 * Background:
		 * Invisible require fields cannot be targeted by the browser and thus create and
		 * error preventing the from from submission.
		 *
		 * Error example from google chrome:
		 * An invalid form control with name='[...]' is not focusable.
		 *
		 * Solution:
		 * Every time when the visibility of elements changes, this method checks the
		 * input fields marked by .required-if-visible and removes or adds the
		 * required property, depending on the visibility of that element.
		 */
		updateHiddenRequireFields: function () {
			this.$el.find('.required-if-visible').each(function (index, element) {
				var $element = $(element);

				if ($element.is(':visible')) {
					$element.prop('required', true);
				}
				else {
					$element.removeAttr('required');
				}
			});

			this.updateInputPlaceholders();
		},

		/**
		 * Required to update placeholders for switching required fields.
		 */
		updateInputPlaceholders: function () {
			this.$el.find('input').each(function (index, element) {
				var $element = $(element),
					placeholder;

				placeholder = $element.attr('placeholder');

				if (!placeholder) {
					return;
				}

				placeholder = placeholder.replace(' *', '');

				if ($element.attr('required')) {
					placeholder += ' *';
				}

				$element.attr('placeholder', placeholder);
			});
		},

		/**
		 * Compared to the hidden require fields, these fields may be always visible
		 * and may be required with one option but not required with another option.
		 * Thus their required state depends on what option is active, what implies
		 * solving the same issue as the hidden fields, but with a different trigger
		 * and logic.
		 */
		handleConditionalRequiredFields: function () {
			this.$el.find('.required-group').formRequired({
				onClick: this.updateInputPlaceholders.bind(this)
			});

			this.updateInputPlaceholders();
		}
	});

	return function (options) {
		return this instanceof View ? this : new View(options);
	};
});
