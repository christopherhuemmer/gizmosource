define([
	'jquery',
	'underscore',
	'backbone',
	'slick'
], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		initialize: function() {
			this.$elements = this.$el.attr('data-elements');
			this.desktopElements = 3;
			this.tabletElements = 2;
			this.mobileElements = 1;

			this.render();

			return this;
		},

		render: function() {
			if(this.$elements === "2") {
				this.desktopElements = 2;
				this.tabletElements = 2;
				this.mobileElements = 1;
			}

			this.initSlider();

			/* hide count if slider is inactive */
			if(this.$el.find('.prev').attr('tabindex') === '-1') {
				this.$el.find('.count').hide();
			}

			this.setupListeners();

			this.setCount();
		},

		setupListeners: function() {
			this.listenTo(window.app.eventDispatcher, 'window:resize', this.resize);
		},

		initSlider: function() {
			this.$el.find('.slide').slick({
				infinite: false,
				adaptiveHeight: true,
				slidesToShow: this.desktopElements,
				slidesToScroll: 1,
				draggable: false,
				prevArrow: this.$el.find($('.prev')),
				nextArrow: this.$el.find($('.next')),
				responsive: [
					{
						breakpoint: 1025,
						settings: {
							slidesToShow: this.tabletElements,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 769,
						settings: {
							slidesToShow: this.mobileElements,
							slidesToScroll: 1
						}
					}
				]
			});

			/*
				show pagination after slider is initialized
			 */
			this.$el.find('.tariff-result-pagination').show();

			this.$el.find('.row').on('afterChange', this.setCount.bind(this));
		},

		setCount: function() {
			var current = parseInt(this.$el.find('.row').find('.slick-active').last().attr('data-slick-index')) + 1;
			this.$el.find('.current').text(current);
		},

		resize: function() {
			/*
				hide/show count if slider is inactive/active
			*/
			if(this.$el.find('.prev').attr('tabindex') === '-1') {
				this.$el.find('.count').hide();
			}else{
				this.$el.find('.count').show();
			}
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
