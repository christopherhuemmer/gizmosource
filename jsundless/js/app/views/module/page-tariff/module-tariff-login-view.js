define([
	'jquery',
	'underscore',
	'backbone'
], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		events: {
			'click input[type=submit]': 'checkLogin',
			'change input[type=radio]': 'toggleRequired'
		},

		initialize: function() {
			this.inputUsername = this.$el.find('#username');
			this.inputPassword = this.$el.find('#password');

			this.$loginUrl = '';

			this.render();

			return this;
		},

		checkLogin: function(event) {
			if ($('input.set-required:checked') && this.inputUsername.val().length && this.inputPassword.val().length) {
				event.preventDefault();

				var username = this.inputUsername.val(),
					password = this.inputPassword.val(),
					$errorMessage = this.$el.find('.tariff-login .error-message'),
					$parent = this.inputUsername.parent(),
					data = { username: username, password: password };

				this.$loginUrl = this.$el.attr('data-url');

				$.ajax({
					type: 'POST',
					url: this.$loginUrl,
					data: JSON.stringify(data),
					contentType: 'application/json; charset=utf-8',
					dataType: 'json',
					success: function(data) {
						if (data.success) {
							window.location.href = data.redirectUrl;
						} else {
							$errorMessage.text(data.errorMessage).show();
							$parent.addClass('error');
						}
					}
				});
			}
		},

		toggleRequired: function(event) {
			var $this = $(event.currentTarget);

			if ($this.hasClass('set-required')) {
				this.inputUsername.attr('required', '');
				this.inputPassword.attr('required', '');
			} else {
				this.inputUsername.removeAttr('required');
				this.inputPassword.removeAttr('required');
				this.inputUsername.val('');
				this.inputPassword.val('');
			}
		},

		render: function() {

		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
