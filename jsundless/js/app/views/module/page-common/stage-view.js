define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		initialize: function () {
			this.$el.on('click', this.stageClick);
		},

		/**
		 * @param {Event} event
		 */
		stageClick: function (event) {
			var $targetElement,
				mediaQuery,
				targetUrl;

			$targetElement = $(event.target);
			if ($targetElement.parents('.intercept-stage-clicks').length) {
				return;
			}

			mediaQuery = window.app.matchMedia.getCurrentMediaQuery();
			if (mediaQuery === 'media-from-l') {
				return;
			}

			targetUrl = $(this).find('.text a').attr('href');
			if (targetUrl) {
				window.location.href = targetUrl;
			}
		}

	});

	return function (options) {
		return this instanceof View ? this : new View(options);
	};
});
