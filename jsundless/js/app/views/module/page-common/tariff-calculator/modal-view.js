define([
	'jquery',
	'underscore',
	'backbone',
	'nouislider',
	'app/models/consumption-calculator'

], function($, _, Backbone, noUiSlider, ConsumptionCalculator) {

	'use strict';

	var View = Backbone.View.extend({

		consumptionCalculator: new ConsumptionCalculator(),

		initialize: function(options) {
			this.initializeSlider();

			if(this.$el.attr('id') == 'anker'){
				this.scrollToView();
			}
		},

		initializeSlider: function() {
			var self = this,
				$this = this.$el,
				$indicators = $this.find('.tariff-indicator'),
				$slider = $this.find('.tariff-calculator-slider .noUi-target'),
				$singular = $this.find('.tariff-label-singular'),
				$plural = $this.find('.tariff-label-plural'),
				$number = $this.find('.tariff-calculator-label-number'),
				slider = $slider.get(0),
				spaceMin = this.getDataNumber('space-min'),
				spaceMax = this.getDataNumber('space-max'),
				spaceStep = this.getDataNumber('space-step');

			noUiSlider.create(slider, {
				start: [spaceMin],
				step: spaceStep,
				range: {
					'min': [spaceMin],
					'max': [spaceMax]
				}
			});

			slider.noUiSlider.on('update', function(values, handle) {
				var value = parseInt(values[handle], 10),
					kWh = 0;

				$singular.toggleClass('hide', value !== 1);
				$plural.toggleClass('hide', value === 1);

				$number.text(value);
				$indicators.addClass('tariff-indicator-active');
				$indicators.slice(value).removeClass('tariff-indicator-active');

				if ($this.hasClass('tariff-calculator-natural-gas')) {
					kWh = self.consumptionCalculator.calculateWarmth(value);
					$this.find('.tariff-calculator-natural-gas-input').val(kWh);
				}
				else if ($this.hasClass('tariff-calculator-electricity')) {
					kWh = self.consumptionCalculator.calculateElectricity(value);
					$this.find('.tariff-calculator-electricity-input').val(kWh);
				}
			});
		},

		scrollToView: function() {
			$('html, body').animate({
				scrollTop: $('#anker').offset().top - 30
			}, 600);
		},

		/**
		 * @param {string} name
		 * @returns {Number}
		 */
		getDataNumber: function(name) {
			var value = this.$el.data(name);

			return parseInt(value, 10);
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
