define([
	'jquery',
	'underscore',
	'backbone'
], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		initialize: function() {
			var $form = this.$el.find('form'),
				loginUrl = this.$el.data('url');

			$form.on('submit', function(event) {
				var message,
					data;

				event.preventDefault();

				if (loginUrl.toLowerCase().indexOf('https') === -1) {
					message = [
						'Logging in requires a secure connection and is disabled until provided.',
						'Current URL: ' + loginUrl
					].join('\n');

					alert(message);
					return;
				}

				data = {
					username: $form.find('.login-username').val(),
					password: $form.find('.login-password').val()
				};

				// @TODO Adjust request processing when the process/backend is defined.
				$.post(loginUrl, data)
					.done(function(response) {
						$form
							.find('.error-message')
							.addClass('hide'); // Just in case we had an error before to hide until the reload is finished.
					})
					.fail(function() {
						$form
							.find('.error-message')
							.removeClass('hide')
							.text('Failed to log in.');
					});
			});

			$form.find('button').on('click', function() {
				$form.submit();
			});
		}

	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
