define([
	'jquery',
	'underscore',
	'backbone',
	'select2',
	'jquery-detectmobilebrowser'
], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		events: {
			'change select': 'filterChange'
		},

		initialize: function() {
			this.$selects = this.$el.find('select');

			if (!jQuery.browser.mobile) {
				/*
				 * Configuration options:
				 *
				 * @link https://select2.github.io/options.html
				 *
				 */
				this.$selects.each(function() {
					$(this).select2({
						minimumResultsForSearch: Infinity,
						dropdownParent: $(this).parent(),
						width: '100%'
					});
				});

				this.$selects.hide();

				$('body').addClass('expose-inputs-for-validation');
			}

			this.setupListeners();

			return this;
		},

		setupListeners: function() {
			this.listenTo(window.app.eventDispatcher, 'filter:loadMore', function (callback) {
				callback(this.getParameter());
			});
		},

		filterChange: function() {
			window.app.eventDispatcher.trigger('filter:change', this.getParameter());
		},

		getParameter: function() {
			var parameters = [];

			this.$selects.each(function() {
				parameters.push($(this).attr('name')+'='+$(this).val());
			});

			return parameters;
		}

	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
