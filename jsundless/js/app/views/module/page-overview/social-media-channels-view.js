define([
	'jquery',
	'underscore',
	'backbone'
], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({
		initialize: function() {
			var self = this;

			function overWriteTimelineSettings() {
				var $twitterTimeline = self.$el.find('iframe.twitter-timeline');

				if (
					$twitterTimeline.length > 0 && // The element has been created but not configured yet.
					$twitterTimeline.outerHeight() === 600 // Having the config height confirms all twitter code has finished.
				) {
					$twitterTimeline.css({
						position: 'absolute',
						width: '100%',
						height: '100%',
						minHeight: 0
					});

					return;
				}

				window.setTimeout(overWriteTimelineSettings, 50);
			}

			$(window).on('load', overWriteTimelineSettings);

			return this;
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
