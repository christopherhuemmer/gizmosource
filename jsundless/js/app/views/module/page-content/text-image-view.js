define([
	'jquery',
	'underscore',
	'backbone',
	'jquery-magnific-popup'

], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		initialize: function() {
			this.render();
		},

		render: function() {
			this.popUp();
		},

		popUp: function() {
			this.$el.find('.lightbox').magnificPopup({
				type: 'image',
				mainClass: 'mfp-fade',
				removalDelay: 500,
				image: {
					cursor: 'null',
					titleSrc: function(item) {
						return item.el.parent().find('figcaption').text();
					}
				}
			});
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
