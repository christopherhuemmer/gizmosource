define([
	'jquery',
	'underscore',
	'backbone'
], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		$content: null,

		events: {
			'click .sidebar-nav-level > li > span': 'toggleActiveLevel'
		},

		initialize: function() {
			this.$content = $('body .page-wrapper > .container');

			this.resize();
			this.setupListeners();

			return this;
		},

		setupListeners: function() {
			this.listenTo(window.app.eventDispatcher, 'window:resize', this.resize);
		},

		resize: function() {
			var $content = this.$content,
				$sidebar = this.$el;

			$content.css('min-height', '');
			$sidebar.css('bottom', 'auto');

			window.setTimeout(function () {
				$content.css('min-height', $sidebar.height());
				$sidebar.css('bottom', ''); // Reset to css default.
			}, 0);
		},

		toggleActiveLevel: function(event) {
			var $link = $(event.target),
				$element = $link.closest('li');

			this.stopEvent(event);

			if ($element.hasClass('active')) {
				$element
					.parent()
					.find('li')
					.removeClass('active');

				this.resize();

				return;
			}

			$element.addClass('active');

			$element
				.siblings()
				.removeClass('active')
				.find('li')
				.removeClass('active');

			this.resize();
		},

		/**
		 * @param {Event} event
		 */
		stopEvent: function(event) {
			event.stopPropagation();
			event.preventDefault();
		}

	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
