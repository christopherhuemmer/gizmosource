define([
	'jquery',
	'underscore',
	'backbone'

], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		events: {
			'click .load-more': 'loadMore'
		},

		/**
		 * Trigger loading of further results.
		 *
		 * @param {Event} event
		 */
		loadMore: function(event) {
			event.preventDefault();

			window.app.eventDispatcher.trigger('search:loadMore');
		}

	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
