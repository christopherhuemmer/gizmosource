define([
	'jquery',
	'underscore',
	'backbone',
	'mustache'

], function($, _, Backbone, Mustache) {

	'use strict';

	var View = Backbone.View.extend({
		ajaxHandler: undefined,

		initialize: function() {
			this.ajaxHandler = window.app.ajaxHandler;

			this.render();
			this.setupListeners();

			return this;
		},

		setupListeners: function() {
			this.listenTo(window.app.eventDispatcher, 'search:loadMore', this.loadMore.bind(this));
		},

		render: function() {
			this.ajaxHandler.request({
				customSuccess: function(response) {
					var data = JSON.parse(response);

					this.appendResults(data.company, this.$el.find('#tab-company'));
					this.appendResults(data.electricity, this.$el.find('#tab-electricity'));
					this.appendResults(data.naturalGas, this.$el.find('#tab-natural-gas'));

				}.bind(this),
				url: this.$el.attr('data-url')
			});

			return this;
		},

		/**
		 * Load more matching elements, but only for the currently opened tab.
		 */
		loadMore: function() {
			var $openTab = this.$el.find('.results-tab').filter(':visible');

			if ($openTab.length === 0) {
				return; // All tabs closed, should not be allowed.
			}

			this.ajaxHandler.request({
				customSuccess: function(response) {
					this.appendResults(
						JSON.parse(response),
						$openTab
					);
				}.bind(this),
				url: $openTab.data('url')
			});
		},

		/**
		 * @param {{ loadNextUrl: {string}, results: {[]}, [totalResults]: {number} }} dataSet
		 * @param {jQuery} $tab to append results to.
		 */
		appendResults: function(dataSet, $tab) {
			var template = this.$el.find('#search-results-tmpl').html(),
				html = '';

			if (dataSet.results === undefined) {
				$tab.find('.results-none-found').removeClass('hide');
			}
			else {
				dataSet.results.forEach(function(result) {
					html += Mustache.render(template, result);
				});
			}

			if (dataSet.totalResults !== undefined) {
				this.$el
					.find($tab.data('counter'))
					.text('(' + dataSet.totalResults + ')');
			}

			if (dataSet.loadNextUrl === undefined) {
				$tab.find('.load-more').hide();
			}

			$tab.data('url', dataSet.loadNextUrl);
			$tab.find('.results-container').append(html);
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
