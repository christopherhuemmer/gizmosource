define([
	'jquery',
	'underscore',
	'backbone'

], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		events: {
			'click .header-menu-activator': 'toggleMenu',
			'click .header-nav-activator': 'toggleActiveLevel'
		},

		/**
		 * @param {Event} event
		 */
		toggleMenu: function(event) {
			var $activator = $(event.target),
				$menu = $activator.next();

			$activator.toggleClass('active');
			$menu.toggleClass('open');

			if ($menu.hasClass('open')) {
				$menu.find('li').removeClass('active inactive');
			}

			this.stopEvent(event);
		},

		/**
		 * @param {Event} event
		 */
		toggleActiveLevel: function(event) {
			var $link = $(event.target),
				$element = $link.closest('li');

			this.stopEvent(event);

			if ($element.hasClass('active')) {
				$element
					.parent() // Closing this level, reset all elements.
					.find('li')
					.removeClass('active inactive');

				return;
			}

			$element
				.removeClass('inactive')
				.addClass('active');

			$element
				.siblings()
				.removeClass('active')
				.addClass('inactive')
				.find('li') // Now close all sub levels of a possible other link that may have been opened.
				.removeClass('active inactive');
		},

		/**
		 * @param {Event} event
		 */
		stopEvent: function(event) {
			event.stopPropagation();
			event.preventDefault();
		}

	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
