define([
	'jquery',
	'underscore',
	'backbone'

], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		events: {
			'click .modal': 'stopEvent',
			'click .navbar-icon-action a': 'toggleModal'
		},

		initialize: function() {
			var self = this;

			$('body').on('click', function() {
				self.closeModals();
			});
		},

		/**
		 * @param {Event} event
		 */
		toggleModal: function(event) {
			var $element = $(event.target).closest('li'),
				openModal = !$element.hasClass('active');

			this.stopEvent(event);
			this.closeModals();

			if (openModal) {
				$element.addClass('active');
				$element.find('.modal').removeClass('hide');
			}
		},

		closeModals: function() {
			this.$el.find('.navbar-icon-action').removeClass('active');
			this.$el.find('.modal').addClass('hide');
		},

		/**
		 * @param {Event} event
		 */
		stopEvent: function(event) {
			event.stopPropagation();
			event.preventDefault();
		}

	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
