define([
	'jquery',
	'underscore',
	'backbone'
], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		initialize: function(options) {
			// Remove el and $el from the options
			this.options = _.extend({}, this.defaults, _.omit(options, ['el', '$el']));

			this.setupListeners();

			this.render();
		},

		setupListeners: function() {
			this.listenTo(window.app.eventDispatcher, 'viewport:change', this.render);
			this.listenTo(window.app.eventDispatcher, 'ajax:loadingStart', this.loadingStart);
			this.listenTo(window.app.eventDispatcher, 'ajax:loadingEnd', this.loadingEnd);
		},

		loadingStart: function() {
			this.$body.addClass('loading');
		},

		loadingEnd: function() {
			this.$body.removeClass('loading');
		},

		render: function() {
			this.$body = $('body');
			return this;
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
