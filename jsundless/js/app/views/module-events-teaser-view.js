define([
	'jquery',
	'underscore',
	'backbone',
	'mustache',
	'equalHeight',
	'jquery-dotdotdot'

], function($, _, Backbone, Mustache) {

	'use strict';

	var View = Backbone.View.extend({

		events: {
			'click .load-more': 'loadMore'
		},

		initialize: function(options) {
			this.ajaxHandler = window.app.ajaxHandler;

			this.eventsTeaserJson = null;

			this.$eventsContainer = this.$el.find('.events-teaser-holder');

			this.loadMore = this.$el.find('.load-more');

			/**
			 * @type {String} - The html template for mustache is stored here.
			 */
			this.$eventsTemplate = null;

			/**
			 * @type {String} - The rendered mustache html templates are stored here.
			 */
			this.renderedTemplates = '';

			this.$loadMoreUrl = '';

			this.$count = this.$el.find('.count');

			/**
			 * @type {Integer} - The count of the current visible service documents is stored here.
			 */
			this.currentShownElements = 0;

			this.setupListeners();

			this.render();

			return this;
		},

		render: function() {
			this.loadDocuments();

			return this;
		},

		setupListeners: function() {
			this.listenTo(window.app.eventDispatcher, 'window:resize', this.resize);
			this.listenTo(window.app.eventDispatcher, 'filter:change', function (parameters) {
				this.reset();
				this.getFilters(parameters);
			}.bind(this));
		},

		/**
		 * Set the counter (current, max) of the displayed and available service docs
		 */
		setCounter: function() {
			this.currentShownElements = this.$el.find('.events-teaser').length;
			this.$count.find('.current').html(this.currentShownElements);
			this.$count.find('.max').html(this.eventsTeaserJson.maxAvailableEvents);
		},

		/**
		 * Load more matching elements. It depends on the last set filter criteria
		 *
		 * @param event {Object}
		 */
		loadMore: function(event) {
			event.preventDefault();

			window.app.eventDispatcher.trigger('filter:loadMore', this.getFilters.bind(this));
		},

		loadDocuments: function(parameters) {
			this.$loadMoreUrl = this.$el.attr('data-url')+'?current='+this.currentShownElements+parameters;

			this.ajaxHandler.request({
				customSuccess: this.appendDocuments.bind(this),
				url: this.$loadMoreUrl
			});
		},

		getFilters: function(parameters) {
			var filters = '';
			if(parameters != undefined){
				filters = '&';
				for(var i=0; i<parameters.length; i++){
					if(i === (parameters.length-1)) {
						filters += parameters[i]
					}else{
						filters += parameters[i]+'&';
					}
				}
			}
			this.loadDocuments(filters);
		},

		appendDocuments: function(data) {
			this.eventsTeaserJson = JSON.parse(data);

			this.eventsTeaserJson.events.forEach(this.createEventsTeaserEntry.bind(this));

			this.$eventsContainer.append(this.renderedTemplates);

			this.dotdotdot(function($wrapper) {
				$wrapper.trigger('destroy'); // Kill old ones.
				$wrapper.dotdotdot({ // (Re-) Initialize plugin for all wrappers.
					height: 100, // About 2 lines in desktop size. Also set in css dotdotdot wrapper.
					wrap: 'letter'
				});
			});

			if(this.eventsTeaserJson.lastTeaser) {
				this.loadMore.hide(); //hide "Load More" button after getting the last teaser
			}

			this.setCounter();

			this.$el.find('.events-teaser').equalHeight();

			this.renderedTemplates = '';
		},

		/**
		 * Build the complete events teaser entry string
		 *
		 * @param eventsTeasers {Object} - The given events teaser object
		 * @param index {Integer} - Appends one single events teaser
		 */
		createEventsTeaserEntry: function(eventsTeasers, index) {
			this.$eventsTemplate = this.$el.find('#events-teaser-tmpl').html();

			this.renderedTemplates += Mustache.render(this.$eventsTemplate, eventsTeasers);
		},

		/**
		 * Passes the dotdotdot wrapper(s) to the passed function to execute actions on them.
		 *
		 * @param {function} action
		 */
		dotdotdot: function(action) {
			action(this.$eventsContainer.find('.dotdotdot-wrapper'));
		},

		resize: function() {
			this.dotdotdot(function($wrapper) {
				$wrapper.trigger('update');
			});

			this.$el.find('.events-teaser').equalHeight();
		},

		reset: function() {
			this.currentShownElements = 0;
			this.$eventsContainer.empty();
			this.loadMore.show();
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
