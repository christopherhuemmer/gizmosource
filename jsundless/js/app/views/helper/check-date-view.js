define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		events: {
			'keyup .date': 'checkDate'
		},

		initialize: function () {
			this.$day = this.$el.find('.day');
			this.$month = this.$el.find('.month');
			this.$year = this.$el.find('.year');

			this.dayInt = 0;
			this.monthInt = 0;
			this.yearInt = 0;

			this.$errorMessage = this.$el.find('.error-date');

			this.dataCheck = this.$el.data('check');

			return this;
		},

		checkDate: function (event) {
			var $target = $(event.currentTarget),
				date;

			if ($target.hasClass('day')) {
				this.dayInt = parseInt(this.$day.val());
			}
			else if ($target.hasClass('month')) {
				this.monthInt = parseInt(this.$month.val());
			}
			else if ($target.hasClass('year')) {
				this.yearInt = parseInt(this.$year.val());
			}

			// Month is an array ky starting with 0, thus month 0 = January.
			date = new Date(
				this.yearInt,
				this.monthInt - 1, // Subtract 1 to convert month to month key.
				this.dayInt
			);

			if (this.dataCheck === 'age') {
				this.checkAdultAge(date);
			}

			if (this.dataCheck === 'date') {
				if (!this.checkWeeks(date, 6)) {
					this.setError();
				}
				else {
					this.removeError();
				}
			}
		},

		checkAdultAge: function (birthDate) {
			if (
				this.dayInt === 0 ||
				this.monthInt === 0 ||
				this.yearInt === 0 ||
				this.$year.val().length <= 3 // Why check string length fro year 1000 and not int value for year 1900?
			) {
				return;
			}

			if (this.chekAge(birthDate, 18)) {
				this.removeError();
			}
			else {
				this.setError();
			}
		},

		/**
		 * Error prone calculations are advised to be test driven developed.
		 *
		 * @param {Date} birthDate
		 * @param {number} minAge
		 * @returns {boolean}
		 */
		chekAge: function (birthDate, minAge) {
			var tempDate = new Date(birthDate.getFullYear() + minAge, birthDate.getMonth(), birthDate.getDate());

			return (tempDate <= new Date());
		},

		/**
		 * Error prone calculations are advised to be test driven developed.
		 *
		 * @param {Date} date
		 * @param {number} weeks
		 * @returns {boolean}
		 */
		checkWeeks: function (date, weeks) {
			var weeksInDays = weeks * 7,
				tempDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()),
				dateFuture = new Date(),
				datePast = new Date();

			dateFuture.setDate(dateFuture.getDate() + weeksInDays);
			datePast.setDate(datePast.getDate() - weeksInDays);

			return (tempDate <= dateFuture && tempDate >= datePast);
		},

		setError: function () {
			this.$day.parent().addClass('error');
			this.$month.parent().addClass('error');
			this.$year.parent().addClass('error');
			this.$year[0].setCustomValidity(this.$errorMessage.text());
			this.$errorMessage.show();
		},

		removeError: function () {
			this.$day.parent().removeClass('error');
			this.$month.parent().removeClass('error');
			this.$year.parent().removeClass('error');
			this.$year[0].setCustomValidity('');
			this.$errorMessage.hide();
		}
	});

	return function (options) {
		return this instanceof View ? this : new View(options);
	};
});
