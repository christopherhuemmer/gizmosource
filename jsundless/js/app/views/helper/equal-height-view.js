define([
	'jquery',
	'underscore',
	'backbone',
	'equalHeight'
], function($, _, Backbone) {

	'use strict';

	/**
	 * Applies equal height to containing .well elements by default and
	 * can be configured using data attributes.
	 *
	 * Options:
	 * data-selector=".my-class"                           (.well is default)
	 * data-breakpoints="media-to-l"                       (leave out to apply for all breakpoints)
	 * data-breakpoints="media-from-m-to-l,media-from-l"   (apply to more than one media query)
	 * data-breakpoints="media-to-l#3"                     (define the amount of columns for that breakpoint)
	 *
	 * Re trigger calculation if necessary:
	 * window.app.eventDispatcher.trigger('window:resize');
	 */
	var View = Backbone.View.extend({
		selector: undefined,
		initialize: function() {
			this.selector = this.$el.data('selector') || '.well';

			this.resize();
			this.setupListeners();

			return this;
		},
		setupListeners: function() {
			this.listenTo(window.app.eventDispatcher, 'window:resize', this.resize);
			this.listenTo(window.app.eventDispatcher, 'equalHeight:update', this.resize);
		},
		resize: function() {
			/**
			 * @param {string} breakpoints
			 * @returns {{ resetOnly: [boolean], columns: [number] }}
			 */
			function getConfig(breakpoints) {
				var mediaQuery = window.app.matchMedia.getCurrentMediaQuery(),
					config = {};

				if (!breakpoints) {
					return config;
				}

				config.resetOnly = true;

				$.each(breakpoints.split(','), function(key, breakpoint) {
					var parts = breakpoint.split('#'),
						columns;

					breakpoint = parts[0];
					columns = parts[1]; // May result in undefined.

					if (breakpoint === mediaQuery) {
						config.resetOnly = false; // Breakpoint found in include list.
						config.columns = columns; // Use the current given column count.

						return false;
					}
				});

				return config;
			}

			this.$el.find(this.selector).equalHeight(
				getConfig(this.$el.data('breakpoints'))
			);
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
