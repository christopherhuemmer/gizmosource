define([
	'jquery',
	'underscore',
	'backbone'
], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({
		initialize: function() {
			var $body = $('body');

			this.$el.on('click', function() {
				$body.toggleClass('debug-outline');
			});
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
