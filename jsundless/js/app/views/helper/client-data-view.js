define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({
		initialize: function () {
			this.$el.find('.user-agent').text(window.navigator.userAgent);
		}
	});

	return function (options) {
		return this instanceof View ? this : new View(options);
	};
});
