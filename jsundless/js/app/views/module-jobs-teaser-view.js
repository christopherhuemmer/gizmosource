define([
	'jquery',
	'underscore',
	'backbone',
	'mustache',
	'equalHeight'

], function($, _, Backbone, Mustache) {

	'use strict';

	var View = Backbone.View.extend({

		events: {
			'click .load-more': 'loadMore'
		},

		initialize: function(options) {
			this.ajaxHandler = window.app.ajaxHandler;

			this.jobsTeaserJson = null;

			this.$jobsContainer = this.$el.find('.jobs-teaser-holder');

			this.loadMore = this.$el.find('.load-more');

			/**
			 * @type {String} - The html template for mustache is stored here.
			 */
			this.$jobsTemplate = null;

			/**
			 * @type {String} - The rendered mustache html templates are stored here.
			 */
			this.renderedTemplates = '';

			this.$loadMoreUrl = '';

			this.$count = this.$el.find('.count');

			/**
			 * @type {Integer} - The count of the current visible service documents is stored here.
			 */
			this.currentShownElements = 0;

			this.setupListeners();

			this.render();

			return this;
		},

		render: function() {
			this.loadDocuments();

			return this;
		},

		setupListeners: function() {
			this.listenTo(window.app.eventDispatcher, 'filter:change', function (parameters) {
				this.reset();
				this.getFilters(parameters);
			}.bind(this));
		},

		/**
		 * Set the counter (current, max) of the displayed and available service docs
		 */
		setCounter: function() {
			this.currentShownElements = this.$el.find('.jobs-teaser').length;
			this.$count.find('.current').html(this.currentShownElements);
			this.$count.find('.max').html(this.jobsTeaserJson.maxAvailableJobs);
		},

		/**
		 * Load more matching elements. It depends on the last set filter criteria
		 *
		 * @param event {Object}
		 */
		loadMore: function(event) {
			event.preventDefault();

			window.app.eventDispatcher.trigger('filter:loadMore', this.getFilters.bind(this));
		},

		loadDocuments: function(parameters) {
			this.$loadMoreUrl = this.$el.attr('data-url')+'?current='+this.currentShownElements+parameters;

			this.ajaxHandler.request({
				customSuccess: this.appendDocuments.bind(this),
				url: this.$loadMoreUrl
			});
		},

		getFilters: function(parameters) {
			var filters = '';
			if(parameters != undefined){
				filters = '&';
				for(var i=0; i<parameters.length; i++){
					if(i === (parameters.length-1)) {
						filters += parameters[i]
					}else{
						filters += parameters[i]+'&';
					}
				}
			}
			this.loadDocuments(filters);
		},

		appendDocuments: function(data) {
			this.jobsTeaserJson = JSON.parse(data);

			this.jobsTeaserJson.jobs.forEach(this.createjobsTeaserEntry.bind(this));

			this.$jobsContainer.append(this.renderedTemplates);

			if(this.jobsTeaserJson.lastTeaser) {
				this.loadMore.hide(); //hide "Load More" button after getting the last teaser
			}

			this.setCounter();

			window.app.eventDispatcher.trigger('equalHeight:update');

			this.renderedTemplates = '';
		},

		/**
		 * Build the complete jobs teaser entry string
		 *
		 * @param jobsTeasers {Object} - The given jobs teaser object
		 * @param index {Integer} - Appends one single jobs teaser
		 */
		createjobsTeaserEntry: function(jobsTeasers, index) {
			this.$jobsTemplate = this.$el.find('#jobs-teaser-tmpl').html();

			this.renderedTemplates += Mustache.render(this.$jobsTemplate, jobsTeasers);
		},

		reset: function() {
			this.currentShownElements = 0;
			this.$jobsContainer.empty();
			this.loadMore.show();
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
