define([
	'jquery',
	'underscore',
	'backbone',
	'jquery-autocomplete'
], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		initialize: function() {
			var rawUrl = this.$el.data('url'),
				responseFixture,
				autocompleteSetup;

			autocompleteSetup = {
				minLength: 1,
				appendMethod: 'replace',
				valid: undefined,
				source: [],
				valueKey: 'title',
				titleKey: 'title',
				dropdownWidth: '100%'
			};

			if (rawUrl) {
				// Live or real data testing environment.

				autocompleteSetup.valid = function() {
					return true; // Enforces all results to be shown, since they already got server-filtered anyway.
				};

				autocompleteSetup.source.push(function(query, add) {
					var url = rawUrl.replace('{query}', encodeURIComponent(query));

					$.getJSON(url, function(response) {
						add(response);
					});
				});
			}
			else {
				// Prototype or testing environment.
				// Keep list in sync with: #search-phases
				responseFixture = [
					{ url: '/', title: 'abc def ghi' },
					{ url: '/', title: 'home page' },
					{ url: '/', title: 'gasag info page' },
					{ url: '/', title: 'test text' }
				];

				autocompleteSetup.source.push(function(query, add) {
					add(responseFixture);
				});
			}

			this.$el.find('input.search-text')
				.autocomplete(autocompleteSetup)
				.on('selected.xdsoft', function(event, item) {
					window.location.href = item.url;
				});
		}

	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
