define([
	'jquery',
	'underscore',
	'backbone',
	'slick',
	'jquery-magnific-popup',
	'jquery-dotdotdot'

], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		initialize: function(options) {
			this.$carousel = this.$el.find('.gallery');
			this.$captions = this.$el.find('figcaption');

			this.$carousel.slick({
				infinite: false,
				adaptiveHeight: true,
				slidesToShow: 3,
				slidesToScroll: 3,
				draggable: false,
				responsive: [
					{
						breakpoint: 769,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2
						}
					},
					{
						breakpoint: 361,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}
				]
			});

			this.$captions.each(function (index, element) {
				element.uncutText = element.innerText;
			});

			this.$captions.dotdotdot({
				height: 100, // About 3 lines in desktop size. Also set in css dotdotdot wrapper.
				wrap: 'letter'
			});

			this.$carousel.on('afterChange', this.setCount.bind(this));

			this.popUp();

			this.setupListeners();

			this.render();
		},

		render: function() {
			this.setCount();
		},

		setupListeners: function() {
			this.listenTo(window.app.eventDispatcher, 'window:resize', this.resize);
		},

		resize: function () {
			this.$captions.trigger('update');
		},

		setCount: function() {
			var max = this.$carousel.slick('getSlick').slideCount,
				current = parseInt(this.$carousel.find('.slick-active').last().attr('data-slick-index')) + 1;
			this.$el.find('.max').text(max);
			this.$el.find('.current').text(current);
		},

		popUp: function(e) {
			this.$carousel.find('.lightbox').magnificPopup({
				type: 'image',
				mainClass: 'mfp-fade',
				removalDelay: 500,
				gallery: {
					enabled: true,
					arrowMarkup: '<button title="%title%" type="button" class="arrow-%dir%"></button>',
					tCounter: ''
				},
				image: {
					cursor: 'null',
					titleSrc: function(item) {
						return item.el.parent().find('figcaption').get(0).uncutText;
					}
				},
				callbacks: {
					buildControls: function() {
						// re-appends controls inside the main container
						this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
					}
				}
			});
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
