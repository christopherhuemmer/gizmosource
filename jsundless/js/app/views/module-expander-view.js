define([
	'jquery',
	'underscore',
	'backbone'
], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		$buttons: undefined,
		$expandArea: undefined,

		events: {
			'click button': 'toggle'
		},

		initialize: function() {
			this.$buttons = this.$el.find('button');
			this.$expandArea = this.$el.find('.module-expander-area');
		},

		toggle: function() {
			this.$expandArea.slideToggle();
			this.$buttons.toggleClass('hide');
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
