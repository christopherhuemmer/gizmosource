define([
	'jquery',
	'underscore',
	'backbone',
	'socialshareprivacy',
	'socialshareprivacyDe'

], function($, _, Backbone) {

	'use strict';

	var View = Backbone.View.extend({

		initialize: function(options) {
			this.$share = this.$el.find('.share');
			this.$infourl = $('.share').data('protection-url');
			this.$share.socialSharePrivacy({
				'layout':'line',
				'info_link': this.$infourl,
				'info_link_target': '_self',
				'css_path': false,
				'path_prefix': '/img/social-share/',
				'services':{
					'buffer':{
						'status':false
					},
					'delicious':{
						'status':false
					},
					'disqus':{
						'status':false
					},
					'fbshare':{
						'status':false
					},
					'flattr':{
						'status':false
					},
					'hackernews':{
						'status':false
					},
					'linkedin':{
						'status':false
					},
					'pinterest':{
						'status':false
					},
					'mail':{
						'status':false
					},
					'reddit':{
						'status':false
					},
					'stumbleupon':{
						'status':false
					},
					'tumblr':{
						'status':false
					}
				}
			});

			this.render();
		},

		render: function() {
			;
		}
	});

	return function(options) {
		return this instanceof View ? this : new View(options);
	};
});
