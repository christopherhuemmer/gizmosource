define([
	'jquery',
	'underscore',
	'backbone'
], function($, _, Backbone) {

	'use strict';

	/**
	 * Make sure that there is only a single instance of the MatchMedia object
	 * @url http://addyosmani.com/resources/essentialjsdesignpatterns/book/#singletonpatternjavascript
	 */
	return (function() {

		// Store a reference to the single MatchMedia instance
		var instance;

		// Private vars
		var breakpointShim = document.getElementById('breakpoint-shim'),
			mediaQueryShim = document.getElementById('media-query-shim'),
			currentMediaQuery,
			lastMediaQuery,
			breakpoints;

		/**
		 * Private functions
		 */
		function getPseudoContent(element, pseudoElement) {
			var content = window.getComputedStyle(element, pseudoElement).getPropertyValue('content');

			// Some browsers are not able to read all properties from pseudo elements
			if (!content || content === 'none') {
				content = window.getComputedStyle(element).getPropertyValue('font-family');
			}

			content = content.trim();
			content = content.replace(/^"|^'|"$|'$|\\/g, '');

			return content;
		}

		function getBreakpoints() {
			var content = getPseudoContent(breakpointShim, ':before');
			return JSON.parse(content);
		}

		function getCurrentMediaQuery() {
			return getPseudoContent(mediaQueryShim, ':before');
		}

		/**
		 * @constructor
		 */
		function MatchMedia() {
			this.initialize();
		}

		MatchMedia.prototype.initialize = function() {
			_.extend(this, Backbone.Events);

			if (!breakpointShim) {
				breakpointShim = $('<div id="breakpoint-shim"/>').appendTo($('body')).get(0);
			}

			if (!mediaQueryShim) {
				mediaQueryShim = $('<div id="media-query-shim"/>').appendTo($('body')).get(0);
			}

			breakpoints = getBreakpoints();

			this.eventDispatcher = window.app.eventDispatcher;

			this.setupListeners();

			this.updateMediaQueries();

			// @ifdef DEV
			/* Only on dev: show actual breakpoint */
			this.$breakpointDiv = $('<div id="breakpoint-visual"></div>').appendTo($('body'));
			this.$breakpointDiv.text(this.getMediaQueries().currentMediaQuery);

			this.listenTo(window.app.eventDispatcher, 'viewport:change', function(event) {
				this.$breakpointDiv.text(event.currentMediaQuery);
			});
			// @endif
		};

		MatchMedia.prototype.setupListeners = function() {
			this.listenTo(this.eventDispatcher, 'window:resize', this.updateMediaQueries.bind(this));
		};

		MatchMedia.prototype.updateMediaQueries = function() {
			var mediaQuery = getCurrentMediaQuery();

			if (mediaQuery !== currentMediaQuery) {
				lastMediaQuery = currentMediaQuery;
				currentMediaQuery = mediaQuery;

				var eventViewportChange = $.Event('change', {
					lastMediaQuery: lastMediaQuery,
					currentMediaQuery: currentMediaQuery
				});

				this.eventDispatcher.trigger('viewport:change', eventViewportChange);
			}
		};

		/**
		 * getBreakpoints
		 *
		 * Returns an objcect with breakpoint key/value pairs
		 * e.g.
		 * {
		 *     'screen-xs-max': '640px',
		 *     'screen-s-max': '768px',
		 *     'screen-m-max': '1024px'
		 * }
		 *
		 * @returns {object}
		 */
		MatchMedia.prototype.getBreakpoints = function() {
			return breakpoints;
		};

		/**
		 * getBreakpoint
		 *
		 * Returns the value for a given breakpoint
		 *
		 * @param breakpoint
		 * @returns {string}
		 */
		MatchMedia.prototype.getBreakpoint = function(breakpoint) {
			return breakpoints[breakpoint];
		};

		/**
		 * getMediaQueries
		 *
		 * Returns an objcect with the current and last media query
		 * e.g.
		 * {
		 *     currentMediaQuery: 'screen-m-max'
		 *     lastMediaQuery: 'screen-s-max'
		 * }
		 *
		 * When no viewport change has happened (e.g. right
		 * after page load) lastMediaQuery is undefined
		 *
		 * @returns {{currentMediaQuery: *, lastMediaQuery: *}}
		 */
		MatchMedia.prototype.getMediaQueries = function() {
			return {
				currentMediaQuery: currentMediaQuery,
				lastMediaQuery: lastMediaQuery
			};
		};

		/**
		 * getCurrentMediaQuery
		 *
		 * Returns the current media query
		 *
		 * @returns {string}
		 */
		MatchMedia.prototype.getCurrentMediaQuery = function() {
			return currentMediaQuery;
		};

		return {
			// Get the single MatchMedia instance if one exists or create one if it doesn't
			getInstance: function() {
				if (instance === undefined) {
					instance = new MatchMedia();
				}

				return instance;
			}
		};
	}());
});
