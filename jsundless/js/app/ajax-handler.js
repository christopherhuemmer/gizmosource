define([
	'jquery',
	'underscore',
	'backbone'
], function($, _, Backbone) {

	'use strict';

	/**
	 * Make sure that there is only a single instance of the AjaxHandler object
	 * @url http://addyosmani.com/resources/essentialjsdesignpatterns/book/#singletonpatternjavascript
	 */
	return (function() {

		// Store a reference to the single AjaxHandler instance
		var instance;

		/**
		 * @constructor
		 */
		function AjaxHandler() {
			this.initialize();
		}

		AjaxHandler.prototype.initialize = function() {
			_.extend(this, Backbone.Events);

			// default ajax options which can be overwritten
			this.defaultOpts = {
				data: null,
				dataType: 'html',
				method: 'GET',
				customSuccess: this.defaultCustomSuccess,
				beforeSend: null,
				url: ''
			};
			this.eventDispatcher = window.app.eventDispatcher;
		};

		// This default callback function logs the given request data in the browser console and should be overwritten
		AjaxHandler.prototype.defaultCustomSuccess = function(data) {
			console.log(data);
		};

		AjaxHandler.prototype.defaultBeforeSend = function() {
			instance.eventDispatcher.trigger('ajax:loadingStart');
		};

		AjaxHandler.prototype.promiseDone = function() {
			instance.eventDispatcher.trigger('ajax:loadingEnd');
		};

		// This function needs to be called to send an ajax request. The given options overwrite the default options above
		AjaxHandler.prototype.request = function(opts) {
			_.extend(this.defaultOpts, opts);

			var request = $.ajax({
				data: this.defaultOpts.data,
				dataType: this.defaultOpts.dataType,
				method: this.defaultOpts.method,
				success: this.defaultOpts.customSuccess || instance.defaultCustomSuccess,
				beforeSend: this.defaultOpts.beforeSend || instance.defaultBeforeSend,
				url: this.defaultOpts.url
			});

			//@TODO: implement error handler
			request.always(instance.promiseDone);
		};

		return {
			// Get the single AjaxHandler instance if one exists or create one if it doesn't
			getInstance: function() {
				if (instance === undefined) {
					instance = new AjaxHandler();
				}

				return instance;
			}
		};
	}());
});
