define([
	'jquery',
	'underscore',
	'backbone',
	'app/event-dispatcher',
	'app/browser-events',
	'app/match-media',
	'app/ajax-handler',
	'app/router',
	'app/views/views'
], function($, _, Backbone, EventDispatcher, BrowserEvents, MatchMedia, AjaxHandler, Router, Views) {

	'use strict';

	/**
	 * Make sure that there is only a single instance of the Application
	 * @url http://addyosmani.com/resources/essentialjsdesignpatterns/book/#singletonpatternjavascript
	 */
	return (function() {

		// Store a reference to the single Application instance
		var instance;

		/**
		 * @constructor
		 */
		function Application() {
			this.eventDispatcher = {};
			this.matchMedia = {};
			this.ajaxHandler = {};
			this.router = {};
		}

		Application.prototype.initialize = function() {
			this.eventDispatcher = EventDispatcher.getInstance();

			// Simply call the constructor as we don't use the resulting object
			BrowserEvents.getInstance();

			this.matchMedia = MatchMedia.getInstance();

			// Enable deep linking
			this.router = Router.getInstance();

			// Initialize ajax handler
			this.ajaxHandler = AjaxHandler.getInstance();

			// Initialize generic views
			this.initMainViews();

			// Initialize module views
			this.initModuleViews();

			// Start moitoring hashchange events
			Backbone.history.start();
		};

		/**
		 * Every view that is going to be created here
		 * must be defined in app/views/{{name}}-view.js
		 * and added to the dependencies in app/views/views.js
		 */
		Application.prototype.initMainViews = function() {
			Views.AppView({
				el: document.getElementsByTagName('body')[0]
			});

			Views.HeaderView({
				el: document.getElementById('header')
			});
		};

		/**
		 * Every view that is going to be created here
		 * must be defined in app/views/{{name}}-view.js
		 * and added to the dependencies in app/views/views.js
		 */
		Application.prototype.initModuleViews = function() {
			/*
			 * All views that contain a app-view="..." attribute are unified triggered
			 * here. To pass further parameters simply add them into data-attributes for
			 * the modules for example and read them out inside your module.
			 *
			 * Example:
			 * <div class="module-name" app-view="ModuleNameView">...</div>
			 *
			 * You may also combine common view helpers with custom business logic views
			 * by separating the view names via pipe.
			 *
			 * Example:
			 * <div class="module-name" app-view="HelperEqualHeightView|ModuleNameView">...</div>
			 *
			 * Modules are selected via tha "app-view" attribute that contains
			 * the view name from views.js as value. This way lots of dom selectors
			 * are saved to boost (especially) mobile performance.
			 */
			$('[app-view]').each(function(index, element) {
				var $module = $(element),
					moduleViews = $module.attr('app-view').split('|');

				$.each(moduleViews, function(key, moduleView) {
					if (Views[moduleView]) {
						Views[moduleView]({
							el: element
						});

						return;
					}

					throw [
						'Given view',
						'"' + moduleView + '"',
						'is not available in Backbone Views for module',
						'"' + $module.attr('class') + '".'
					].join(' ');
				});
			});
		};

		return {
			// Get the single Application instance if one exists or create one if it doesn't
			getInstance: function() {
				if (instance === undefined) {
					instance = new Application();
				}

				return instance;
			}
		};
	}());
});
