define([
	'jquery',
	'underscore',
	'backbone'
], function($, _, Backbone) {

	'use strict';

	/**
	 * Make sure that there is only a single instance of the EventDispatcher object
	 * @url http://addyosmani.com/resources/essentialjsdesignpatterns/book/#singletonpatternjavascript
	 */
	return (function() {

		// Store a reference to the single Router instance
		var instance;

		var Router = Backbone.Router.extend({

			routes: {
				'check(/:arg)': 'check'
			},

			initialize: function() {
				this.eventDispatcher = window.app.eventDispatcher;
			},

			check: function(arg) {
				var eventCheck = $.Event('check', {
					arg: arg
				});

				this.eventDispatcher.trigger('router:check', eventCheck);
			}
		});

		return {
			// Get the single EventDispatcher instance if one exists or create one if it doesn't
			getInstance: function() {
				if (instance === undefined) {
					instance = new Router();
				}

				return instance;
			}
		};
	}());
});
