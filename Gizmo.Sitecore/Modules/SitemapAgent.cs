﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using Gizmo.General.String;
using Gizmo.SitecoreGizmo.Items;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Sites;
using Sitecore.Globalization;
using Sitecore.Diagnostics;
using System.Threading;
using Gizmo.General.IO;

namespace Gizmo.SitecoreGizmo.Modules
{
    /// <summary>
    /// Generate sitemap for given sites.
    /// The parameter "sitemapDomainPath" supports two numbered parameters. The first one is the site language
    /// and the second is the site name (make sure it only contains path compatible characters). 
    /// The parameter "startItemPath" is optional (keep empty). If empty start path of indexing is the content start path of the site node.
    /// </summary>
    /// <example>
    ///     <agent type="Gizmo.SitecoreGizmo.Modules.SitemapAgent" method="Run" interval="00:00:00">
    ///        <param desc="databaseName">web</param>
    ///        <param desc="sitemapPath">C:\Inetpub\wwwroot\zweb\Website\Sitemap.xml</param>
    ///        <param desc="sitemapDomainPath"> C:\Inetpub\wwwroot\zweb\Website\Sitemap-{0}.xml</param>
    ///        <param desc="startItemPath"></param>
    ///        <param desc="templateReferenceNames">Page Template</param>
    ///     </agent>
    /// </example>
    public class SitemapAgent
    {
        private const string NamespaceSeparator = "NAMESPACESEPARATOR";

        private object Lock = new object();

        public string DatabaseName { get; private set; }
        public string SitemapPath { get; private set; }
        public string SitemapDomainPath { get; private set; }
        public string StartItemPath { get; set; }
        public IEnumerable<ID> Templates { get; private set; }

        /// <param name="databaseName">Name of the database to read.</param>
        /// <param name="sitemapPath">Path to the index sitemap.</param>
        /// <param name="sitemapDomainPath">Template to domain sitemaps.</param>
        /// <param name="startItemPath">Sitecore path to start item.</param>
        /// <param name="templateReferenceNames">ScReference names of page templates.</param>
        public SitemapAgent(string databaseName, string sitemapPath, string sitemapDomainPath, string startItemPath, string templateReferenceNames)
        {
            DatabaseName = databaseName;
            SitemapPath = sitemapPath;
            SitemapDomainPath = sitemapDomainPath;
            StartItemPath = startItemPath;
            Templates = templateReferenceNames.ToList().Select(x => ScReference.Instance().GetItem(x)).Where(x => x != null).Select(x => x.ID).ToList();
        }

        /// <summary>
        /// Start the agent.
        /// </summary>
        public void Run()
        {
            // Prevent execution of multiple instances. Shouldn't happen but who knows.
            if (Monitor.TryEnter(Lock))
            {
                try
                {
                    // Validate
                    if (string.IsNullOrWhiteSpace(DatabaseName) || string.IsNullOrWhiteSpace(SitemapPath) || string.IsNullOrWhiteSpace(SitemapDomainPath) || !Templates.Any())
                    {
                        Log.Warn(
                            string.Format(
                                "Missing parameters: DatabaseName = '{0}', SitemapPath = '{1}', SitemapDomainPath = '{2}', StartItemPath = '{3}', Templates = '{4}'", 
                                DatabaseName, SitemapPath, SitemapDomainPath, StartItemPath, Templates.Count()), 
                            this);
                        return;
                    }

                    CreateSitemaps();
                }
                finally
                {
                    Monitor.Exit(Lock);
                }
            }
            else
            {
                Log.Warn("SitemapAgent already running", this);
            }
        }

        /// <summary>
        /// Create sitemaps.
        /// </summary>
        protected virtual void CreateSitemaps()
        {
            var sites = Sitecore.Configuration.Factory.GetSiteInfoList().Where(x => x.Properties["sitemap"].ToBool());

            // Collect all entries for the sitemap.
            var siteMapFiles = new List<SiteMapFileItem>();
            foreach (var site in sites)
            {
                var sitemapItems = new List<SitemapItem>();
                var siteContext = SiteContext.GetSite(site.Name);
                using (new SiteContextSwitcher(siteContext))
                {
                    var languages = new List<string>();
                    languages.Add(site.Language);
                    if (!string.IsNullOrWhiteSpace(site.Properties["moreLanguages"]))
                    {
                        languages.AddRange(site.Properties["moreLanguages"].Split(','));
                    }

                    foreach (var language in languages)
                    {
                        using (new LanguageSwitcher(Language.Parse(language)))
                        {
                            sitemapItems.AddRange(PageSitemapItems());
                        }
                    }

                    string name = CreateSiteMapFile(sitemapItems, site.Language);
                    var sitemapFileItem = new SiteMapFileItem()
                    {
                        Loc = name
                    };
                    siteMapFiles.Add(sitemapFileItem);
                }
            }

            // Generate sitemap xml from entries.
            XmlDocument xml = new XmlDocument();
            XmlDeclaration declaration = xml.CreateXmlDeclaration("1.0", "UTF-8", null);
            xml.AppendChild(declaration);
            XmlElement root = xml.CreateElement("sitemapindex");
            xml.AppendChild(root);

            foreach (var item in siteMapFiles)
            {
                XmlNode siteMapNode = xml.CreateElement("sitemap");

                XmlNode locNode = xml.CreateElement("loc");
                locNode.InnerText = item.Loc;
                siteMapNode.AppendChild(locNode);

                XmlNode lastModNode = xml.CreateElement("lastmod");
                lastModNode.InnerText = DateTime.Now.ToString("yyyy-MM-dd");
                siteMapNode.AppendChild(lastModNode);
                root.AppendChild(siteMapNode);
            }

            // Yes the following is ugly but it seems very complicated to generated a valid sitemap.xml with namespaces.
            string xmlString = xml.OuterXml;
            xmlString = xmlString.Replace("<sitemapindex>", "<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\">");
            xmlString = xmlString.Replace(NamespaceSeparator, ":");
            File.WriteAllText(SitemapPath, xmlString);
        }

        /// <summary>
        /// Additional overridable filter for page items. Default implementation filters 
        /// by optional checkbox field "Hide from Search Bots".
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected virtual bool FilterPageItem(Item item)
        {
            return ScValue.Has(item, "Hide from Search Bots") && ScValue.GetBool(item, "Hide from Search Bots");
        }

        /// <summary>
        /// Collect all pages for the current sitemap.
        /// </summary>
        /// <returns></returns>
        protected virtual IEnumerable<SitemapItem> PageSitemapItems()
        {
            var sitemapItems = new List<SitemapItem>();

            Database database = Sitecore.Configuration.Factory.GetDatabase(DatabaseName);

            var startPath = StartItemPath;
            if (string.IsNullOrWhiteSpace(startPath))
            {
                startPath = Sitecore.Context.Site.StartPath;
            }
            Log.Info("Start path is " + startPath, this);

            Queue<Item> newPages = new Queue<Item>();
            var startItem = database.GetItem(startPath);
            if (startItem != null)
            {
                newPages.Enqueue(startItem);
            }
            else
            {
                Log.Warn(string.Format("Start item '{0}' not found", startPath), this);
            }

            while (newPages.Any())
            {
                Item pageItem = newPages.Dequeue();

                // Filter by template.
                if (!Templates.Contains(pageItem.TemplateID))
                {
                    continue;
                }

                // Additional filter.
                if (FilterPageItem(pageItem))
                {
                    continue;
                }

                var sitemapItem = new SitemapItem()
                {
                    LastModified = pageItem.Publishing.PublishDate != DateTime.MinValue ? pageItem.Publishing.PublishDate : DateTime.Now,
                    Url = ScLink.GetUrl(pageItem, true)
                };
                sitemapItems.Add(sitemapItem);

                foreach (Item childItem in pageItem.Children)
                {
                    newPages.Enqueue(childItem);
                }
            }

            return sitemapItems;
        }

        /// <summary>
        /// Create a sitemap file for a given language.
        /// </summary>
        /// <param name="sitemapItems"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        protected string CreateSiteMapFile(List<SitemapItem> sitemapItems, string lang)
        {
            XmlDocument xml = new XmlDocument();
            XmlDeclaration declaration = xml.CreateXmlDeclaration("1.0", "UTF-8", null);
            xml.AppendChild(declaration);
            XmlElement root = xml.CreateElement("urlset");
            xml.AppendChild(root);

            foreach (var sitemapItem in sitemapItems)
            {
                XmlNode urlNode = xml.CreateElement("url");

                XmlNode locNode = xml.CreateElement("loc");
                locNode.InnerText = sitemapItem.Url;
                urlNode.AppendChild(locNode);

                if (sitemapItem.LastModified != DateTime.MinValue)
                {
                    XmlNode lastModNode = xml.CreateElement("lastmod");
                    lastModNode.InnerText = sitemapItem.LastModified.ToString("yyyy-MM-dd");
                    urlNode.AppendChild(lastModNode);
                }

                root.AppendChild(urlNode);
            }

            // Yes the following is ugly but it seems very complicated to generated a valid sitemap.xml with namespaces.
            string xmlString = xml.OuterXml;
            xmlString = xmlString.Replace("<urlset>", "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\">");
            xmlString = xmlString.Replace(NamespaceSeparator, ":");

            var mapFilePath = SitemapFilePath(lang);
            File.WriteAllText(mapFilePath, xmlString);

            var fileInfo = new FileInfo(mapFilePath);
            var url = string.Format("{0}://{1}/{2}", Sitecore.Context.Site.SiteInfo.Scheme, Sitecore.Context.Site.HostName, fileInfo.Name);
            return url;
        }

        protected string SitemapFilePath(string lang)
        {
            return string.Format(SitemapDomainPath, lang, Sitecore.Context.Site.Name);
        } 
    }

    public class SitemapItem
    {
        public string Url { get; set; }
        public DateTime LastModified { get; set; }
    }

    public class SiteMapFileItem
    {
        public string Loc { get; set; }
        public DateTime LastModified { get; set; }
    }
}
