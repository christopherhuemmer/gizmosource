﻿using Sitecore.Pipelines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmo.SitecoreGizmo.Modules
{
    /// <summary>
    /// Reset dictionaries at startup
    /// </summary>
    /// <example>
    ///    <pipelines>
    ///     <initialize>
    ///       <processor type="Gizmo.SitecoreGizmo.Modules.ResetDictionaryCache, Gizmo.Sitecore" />
    ///     </initialize>
    ///    </pipelines>
    /// </example>
    public class ResetDictionaryCache
    {
        public virtual void Process(PipelineArgs args)
        {
            Sitecore.Globalization.Translate.ResetCache(true);
        }
    }
}
