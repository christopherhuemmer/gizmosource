﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Pipelines.GetRenderingDatasource;
using Sitecore.Text;

namespace Gizmo.SitecoreGizmo.Module
{
    /// <summary>
    /// Query support for datasource location.
    /// </summary>
    ///  <example>
    ///    <pipelines>
    ///       <getRenderingDatasource>
    ///          <processor patch:before="*[@type='Sitecore.Pipelines.GetRenderingDatasource.GetDatasourceLocation, Sitecore.Kernel']" 
    ///                     type="Gizmo.SitecoreGizmo.Module.GetQueryableDatasourceLocation, Gizmo.Sitecore" />
    ///       </getRenderingDatasource>
    ///    </pipelines>
    /// </example>
    /// <see cref="https://www.cognifide.com/blogs/sitecore/reduce-multisite-chaos-with-sitecore-queries/"/>
    public class GetQueryableDatasourceLocation
    {
        public void Process(GetRenderingDatasourceArgs args)
        {
            foreach (var location in new ListString(args.RenderingItem["Datasource Location"]))
            {
                if (location.StartsWith("query:"))
                {
                    Sitecore.Data.Items.Item contextItem = args.ContentDatabase.Items[args.ContextItemPath];
                    if (contextItem != null)
                    {
                        string query = location.Substring("query:".Length);
                        Sitecore.Data.Items.Item queryItem = contextItem.Axes.SelectSingleItem(query);
                        if (queryItem != null)
                        {
                            args.DatasourceRoots.Add(queryItem);
                        }
                    }
                }
            }
        }
    }
}