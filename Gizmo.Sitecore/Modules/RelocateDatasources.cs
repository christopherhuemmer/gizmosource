﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Query;
using Sitecore.Diagnostics;
using Sitecore.Events;
using Sitecore.Exceptions;
using Sitecore.Globalization;
using Sitecore.Jobs;
using Sitecore.Shell.Framework.Pipelines;
using Sitecore.Web.UI.Sheer;

namespace Gizmo.SitecoreGizmo.Module
{
    /// <summary>
    /// The following classes relocate datasources of existing modules after copying, insert from branch... a page.
    /// 
    /// Important: For "Gizmo.SitecoreGizmo.Module.DuplicateItem" you must disable the uiDuplicateItem-processor 
    /// of Sitecore.Buckets.config.
    /// </summary>
    /// <example>
    ///    <events>
    ///       <event name="item:added">
    ///       <handler type="Gizmo.SitecoreGizmo.Module.AddFromTemplate, Gizmo.Sitecore" method="OnItemAdded" />
    ///       </event>
    ///    </events>
    ///    <processors>
    ///       <uiDuplicateItem>
    ///          <processor type="Sitecore.Shell.Framework.Pipelines.DuplicateItem,Sitecore.Kernel">
    ///             <patch:attribute name="type">Gizmo.SitecoreGizmo.Module.DuplicateItem, Gizmo.Sitecore</patch:attribute>
    ///          </processor>
    ///       </uiDuplicateItem>
    ///       
    ///       <uiCopyItems>
    ///          <processor mode="on" type="Gizmo.SitecoreGizmo.Module.CopyOrCloneItem, Gizmo.Sitecore" method="ProcessFieldValues" />
    ///       </uiCopyItems>
    ///       
    ///       <uiCloneItems>
    ///          <processor mode="on" type="Gizmo.SitecoreGizmo.Module.CopyOrCloneItem, Gizmo.Sitecore" method="ProcessFieldValues" />
    ///       </uiCloneItems>
    ///    </processors>
    /// </example>
    /// <see cref="http://reasoncodeexample.com/2013/01/13/changing-sitecore-item-references-when-creating-copying-duplicating-and-cloning/"/>

    public class AddFromTemplate
    {
        public void OnItemAdded(object sender, EventArgs args)
        {
            Sitecore.Data.Items.Item targetItem = Event.ExtractParameter(args, 0) as Sitecore.Data.Items.Item;
            if (targetItem == null)
                return;
            if (targetItem.Branch == null)
                return;
            if (targetItem.Branch.InnerItem.Children.Count != 1)
                return;
            Sitecore.Data.Items.Item branchRoot = targetItem.Branch.InnerItem.Children[0];
            new ReferenceReplacementJob(branchRoot, targetItem).StartAsync();
        }
    }

    public class DuplicateItem
    {
        private Sitecore.Data.Items.Item _itemToDuplicate;

        public void Execute(ClientPipelineArgs args)
        {
            Sitecore.Data.Items.Item copy = Duplicate(args);
            if (copy == null)
                return;

            if (_itemToDuplicate == null)
                return;

            new ReferenceReplacementJob(_itemToDuplicate, copy).StartAsync();
        }

        private Sitecore.Data.Items.Item Duplicate(ClientPipelineArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            Sitecore.Data.Items.Item item = GetItemToDuplicate(args);
            _itemToDuplicate = item;
            if (item == null)
            {
                Sitecore.Diagnostics.Log.Warn("Sitecore.Data.Items.Item not found.", this);
                SheerResponse.Alert("Sitecore.Data.Items.Item not found.", new string[0]);
                args.AbortPipeline();
            }
            else
            {
                Sitecore.Data.Items.Item parent = item.Parent;
                if (parent == null)
                {
                    Sitecore.Diagnostics.Log.Warn("Cannot duplicate the root item.", this);
                    SheerResponse.Alert("Cannot duplicate the root item.", new string[0]);
                    args.AbortPipeline();
                }
                else if (parent.Access.CanCreate())
                {
                    Log.Audit(this, "Duplicate item: {0}", new string[] { AuditFormatter.FormatItem(item) });
                    return Context.Workflow.DuplicateItem(item, args.Parameters["name"]);
                }
                else
                {
                    Sitecore.Diagnostics.Log.Warn("You do not have permission to duplicate this item.", this);
                    SheerResponse.Alert(Translate.Text("You do not have permission to duplicate \"{0}\".", new object[] { item.DisplayName }), new string[0]);
                    args.AbortPipeline();
                }
            }
            return null;
        }

        private Sitecore.Data.Items.Item GetItemToDuplicate(ClientPipelineArgs args)
        {
            Language language;
            Database database = Factory.GetDatabase(args.Parameters["database"]);
            Assert.IsNotNull(database, args.Parameters["database"]);
            string str = args.Parameters["id"];
            if (!Language.TryParse(args.Parameters["language"], out language))
            {
                language = Context.Language;
            }
            return database.GetItem(ID.Parse(str), language);
        }
    }

    public class CopyOrCloneItem : Sitecore.Shell.Framework.Pipelines.CopyItems
    {
        public virtual void ProcessFieldValues(CopyItemsArgs args)
        {
            Sitecore.Data.Items.Item sourceRoot = Sitecore.Shell.Framework.Pipelines.CopyItems.GetItems(args).FirstOrDefault();
            Assert.IsNotNull(sourceRoot, "sourceRoot is null.");

            Sitecore.Data.Items.Item copyRoot = args.Copies.FirstOrDefault();
            Assert.IsNotNull(copyRoot, "copyRoot is null.");

            new ReferenceReplacementJob(sourceRoot, copyRoot).StartAsync();
        }
    }

    public class ReferenceReplacementJob
    {
        private readonly Sitecore.Data.Items.Item _source;
        private readonly Sitecore.Data.Items.Item _target;

        public ReferenceReplacementJob(Sitecore.Data.Items.Item source, Sitecore.Data.Items.Item target)
        {
            Assert.ArgumentNotNull(source, "source");
            Assert.ArgumentNotNull(target, "target");
            _source = source;
            _target = target;
        }

        public void StartAsync()
        {
            string jobCategory = typeof(ReferenceReplacementJob).Name;
            string siteName = Context.Site == null ? "No Site Context" : Context.Site.Name;
            var jobOptions = new DefaultJobOptions(GetJobName(), jobCategory, siteName, this, "Start");
            JobManager.Start(jobOptions);
        }

        private string GetJobName()
        {
            return string.Format("Resolving item references between source {0} and target {1}.", AuditFormatter.FormatItem(_source), AuditFormatter.FormatItem(_target));
        }

        public void Start()
        {
            ItemPathTranslator translator = new ItemPathTranslator(_source, _target);
            IEnumerable<Sitecore.Data.Items.Item> sourceDescendants = GetDescendantsAndSelf(_source);
            ItemReferenceReplacer replacer = InitializeReplacer(sourceDescendants, translator);
            foreach (Sitecore.Data.Items.Item equivalentTarget in replacer.OtherItems)
            {
                replacer.ReplaceItemReferences(equivalentTarget);
            }
        }

        private IEnumerable<Sitecore.Data.Items.Item> GetDescendantsAndSelf(Sitecore.Data.Items.Item source)
        {
            return Query.SelectItems("descendant-or-self::*", source) ?? Enumerable.Empty<Sitecore.Data.Items.Item>();
        }

        private ItemReferenceReplacer InitializeReplacer(IEnumerable<Sitecore.Data.Items.Item> sourceDescendants, ItemPathTranslator translator)
        {
            ItemReferenceReplacer replacer = new ItemReferenceReplacer(ExcludeStandardSitecoreFieldsExceptLayout);
            foreach (Sitecore.Data.Items.Item sourceDescendant in sourceDescendants)
            {
                if (!translator.CanTranslatePath(sourceDescendant))
                    continue;

                Sitecore.Data.Items.Item equivalentTarget = sourceDescendant.Database.GetItem(translator.TranslatePath(sourceDescendant));
                if (equivalentTarget == null)
                    continue;

                replacer.AddItemPair(sourceDescendant, equivalentTarget);
            }
            return replacer;
        }

        private bool ExcludeStandardSitecoreFieldsExceptLayout(Field field)
        {
            Assert.ArgumentNotNull(field, "field");
            return field.ID == FieldIDs.LayoutField || field.ID == FieldIDs.FinalLayoutField || !field.Name.StartsWith("__");
        }
    }

    public class ItemPathTranslator
    {
        private readonly Sitecore.Data.Items.Item _source;
        private readonly Sitecore.Data.Items.Item _sourceRoot;
        private readonly Sitecore.Data.Items.Item _target;
        private readonly Sitecore.Data.Items.Item _targetRoot;

        public ItemPathTranslator(Sitecore.Data.Items.Item source, Sitecore.Data.Items.Item target)
            : this(source, source, target, target)
        {
        }

        public ItemPathTranslator(Sitecore.Data.Items.Item source, Sitecore.Data.Items.Item sourceRoot, Sitecore.Data.Items.Item target, Sitecore.Data.Items.Item targetRoot)
        {
            Assert.ArgumentNotNull(source, "source");
            Assert.ArgumentNotNull(sourceRoot, "sourceRoot");
            Assert.ArgumentNotNull(target, "target");
            Assert.ArgumentNotNull(targetRoot, "targetRoot");
            _source = source;
            _sourceRoot = sourceRoot;
            _target = target;
            _targetRoot = targetRoot;
        }

        public bool CanTranslatePath(Sitecore.Data.Items.Item item)
        {
            Assert.ArgumentNotNull(item, "item");
            return IsDescendantOrSelf(item, _source) || IsDescendantOrSelf(item, _sourceRoot);
        }

        private bool IsDescendantOrSelf(Sitecore.Data.Items.Item item, Sitecore.Data.Items.Item otherItem)
        {
            return item.ID == otherItem.ID || item.Axes.IsDescendantOf(otherItem);
        }

        public string TranslatePath(Sitecore.Data.Items.Item item)
        {
            Assert.ArgumentNotNull(item, "item");
            if (IsDescendantOrSelf(item, _source))
                return GetFullPath(_target, _source, item);

            if (IsDescendantOrSelf(item, _sourceRoot))
                return GetFullPath(_targetRoot, _sourceRoot, item);

            throw new InvalidItemException(string.Format("Sitecore.Data.Items.Item {0} is not a descendant of {1} or {2}.",
              AuditFormatter.FormatItem(item),
              AuditFormatter.FormatItem(_source),
              AuditFormatter.FormatItem(_sourceRoot)));
        }

        private string GetFullPath(Sitecore.Data.Items.Item closestEquivalentAncestor, Sitecore.Data.Items.Item closestAncestor, Sitecore.Data.Items.Item item)
        {
            string startPathPart = closestEquivalentAncestor.Paths.FullPath;
            string relativePathPart = GetRelativePath(closestAncestor, item);
            return StringUtil.EnsurePostfix('/', startPathPart) + StringUtil.RemovePrefix('/', relativePathPart);
        }

        private string GetRelativePath(Sitecore.Data.Items.Item closestAncestor, Sitecore.Data.Items.Item item)
        {
            return item.Paths.FullPath.Replace(closestAncestor.Paths.FullPath, string.Empty);
        }
    }

    public class ItemReferenceReplacer
    {
        private readonly Func<Field, bool> _fieldFilter;
        private readonly ICollection<ItemPair> _itemPairs = new HashSet<ItemPair>();

        public IEnumerable<Sitecore.Data.Items.Item> Items
        {
            get { return _itemPairs.Select(pair => pair.Item).ToArray(); }
        }

        public IEnumerable<Sitecore.Data.Items.Item> OtherItems
        {
            get { return _itemPairs.Select(pair => pair.OtherItem).ToArray(); }
        }

        public ItemReferenceReplacer(Func<Field, bool> fieldFilter)
        {
            Assert.ArgumentNotNull(fieldFilter, "fieldFilter");
            _fieldFilter = fieldFilter;
        }

        public void AddItemPair(Sitecore.Data.Items.Item item, Sitecore.Data.Items.Item otherItem)
        {
            Assert.ArgumentNotNull(item, "item");
            Assert.ArgumentNotNull(otherItem, "otherItem");
            _itemPairs.Add(new ItemPair(item, otherItem));
        }

        public void ReplaceItemReferences(Sitecore.Data.Items.Item item)
        {
            IEnumerable<Field> fields = GetFieldsToProcess(item);
            foreach (Field field in fields)
            {
                foreach (Sitecore.Data.Items.Item itemVersion in GetVersionsToProcess(item))
                {
                    Field itemVersionField = itemVersion.Fields[field.ID];
                    ProcessField(itemVersionField);
                }
            }
        }

        private IEnumerable<Field> GetFieldsToProcess(Sitecore.Data.Items.Item item)
        {
            item.Fields.ReadAll();
            return item.Fields.Where(_fieldFilter).ToArray();
        }

        private IEnumerable<Sitecore.Data.Items.Item> GetVersionsToProcess(Sitecore.Data.Items.Item item)
        {
            return item.Versions.GetVersions(true);
        }

        private void ProcessField(Field field)
        {
            string initialValue = GetInitialFieldValue(field);
            if (string.IsNullOrEmpty(initialValue))
                return;

            StringBuilder value = new StringBuilder(initialValue);
            foreach (ItemPair itemPair in _itemPairs)
            {
                ReplaceID(itemPair.Item, itemPair.OtherItem, value);
                ReplaceShortID(itemPair.Item, itemPair.OtherItem, value);
                ReplaceFullPath(itemPair.Item, itemPair.OtherItem, value);
                ReplaceContentPath(itemPair.Item, itemPair.OtherItem, value);
            }
            UpdateFieldValue(field, initialValue, value);
        }

        private string GetInitialFieldValue(Field field)
        {
            return field.GetValue(true, true);
        }

        private void ReplaceID(Sitecore.Data.Items.Item item, Sitecore.Data.Items.Item otherItem, StringBuilder value)
        {
            value.Replace(item.ID.ToString(), otherItem.ID.ToString());
        }

        private void ReplaceShortID(Sitecore.Data.Items.Item item, Sitecore.Data.Items.Item otherItem, StringBuilder value)
        {
            value.Replace(item.ID.ToShortID().ToString(), otherItem.ID.ToShortID().ToString());
        }

        private void ReplaceFullPath(Sitecore.Data.Items.Item item, Sitecore.Data.Items.Item otherItem, StringBuilder value)
        {
            value.Replace(item.Paths.FullPath, otherItem.Paths.FullPath);
        }

        private void ReplaceContentPath(Sitecore.Data.Items.Item item, Sitecore.Data.Items.Item otherItem, StringBuilder value)
        {
            if (item.Paths.IsContentItem)
                value.Replace(item.Paths.ContentPath, otherItem.Paths.ContentPath);
        }

        private void UpdateFieldValue(Field field, string initialValue, StringBuilder value)
        {
            if (initialValue.Equals(value.ToString()))
                return;

            using (new EditContext(field.Item))
            {
                field.Value = value.ToString();
            }
        }

        [DebuggerDisplay("Sitecore.Data.Items.Item: \"{Sitecore.Data.Items.Item.Paths.Path}\", OtherItem: \"{OtherItem.Paths.Path}\"")]
        private class ItemPair
        {
            public Sitecore.Data.Items.Item Item { get; private set; }
            public Sitecore.Data.Items.Item OtherItem { get; private set; }

            public ItemPair(Sitecore.Data.Items.Item item, Sitecore.Data.Items.Item otherItem)
            {
                Assert.ArgumentNotNull(item, "item");
                Assert.ArgumentNotNull(otherItem, "otherItem");
                Item = item;
                OtherItem = otherItem;
            }

            public override bool Equals(object instance)
            {
                return instance is ItemPair && instance.GetHashCode() == GetHashCode();
            }

            public override int GetHashCode()
            {
                return string.Concat(Item.ID, OtherItem.ID).GetHashCode();
            }
        }
    }
}
