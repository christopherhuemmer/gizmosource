﻿using System;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Events;
using Sitecore.Layouts;

namespace Gizmo.SitecoreGizmo.Module
{
    /// <summary>
    /// Event for relocating datasources in renderings
    /// </summary>
    /// <example>
    ///    <events>
    ///       <event name="item:added">
    ///          <handler patch:after="processor[@type='Sitecore.Data.Fields.ItemEventHandler, Sitecore.Kernel']" 
    ///                   type="Gizmo.SitecoreGizmo.Module.RelocateDatasourcesItemsFromBranches, Gizmo.Sitecore" method="OnItemAdded" />
    ///       </event>
    ///    </events>
    /// </example>
    /// <see cref="http://www.suneco.nl/over-suneco/blog/2014/creating-items-from-a-branch-relocating-datasource.aspx"/>
    public class RelocateDatasourcesItemsFromBranches
    {
        /// <summary>
        /// The branche folder path
        /// </summary>
        private const string BrancheFolderPath = "/sitecore/templates/Branches";

        /// <summary>
        /// Called when [item added].
        /// </summary>
        /// <param name="sender">The sender.
        /// <param name="args">The <see cref="EventArgs"> instance containing the private event data.
        protected void OnItemAdded(object sender, EventArgs args)
        {
            // The first [0] item contains the target item
            Sitecore.Data.Items.Item target = Event.ExtractParameter(args, 0) as Sitecore.Data.Items.Item;
            if (target != null)
            {
                if (target.Branch != null)
                {
                    var db = Sitecore.Context.ContentDatabase ?? Sitecore.Context.Database;
                    this.CorrectDatasourcesTargetingBranch(db, target, target, target.Branch.InnerItem);
                }
            }
        }

        /// <summary>
        /// Corrects datasources that are targeting items that are located under the branch item.
        /// </summary>
        /// <private param name="db">The database.
        /// <private param name="item">The private item that private needs to be checked.
        /// <private param name="rootItem">The private root item that is private actually the private root item that is private created from the branch.
        /// <private param name="rootBranchItem">The private root branch item, the item.
        private void CorrectDatasourcesTargetingBranch(Database db, Sitecore.Data.Items.Item item, Sitecore.Data.Items.Item rootItem, Sitecore.Data.Items.Item rootBranchItem)
        {
            bool updated = false;
            LayoutField layoutField = new LayoutField(item.Fields[Sitecore.FieldIDs.LayoutField]);
            LayoutDefinition layoutDefinition = LayoutDefinition.Parse(layoutField.Value);
            var renderings = item.Visualization.GetRenderings(Sitecore.Context.Device, false);
            foreach (RenderingReference rendering in renderings)
            {
                if (rendering != null && !string.IsNullOrWhiteSpace(rendering.Settings.DataSource))
                {
                    var datasource = db.GetItem(rendering.Settings.DataSource);
                    if (datasource != null && datasource.Paths.FullPath.StartsWith(BrancheFolderPath))
                    {
                        var oldPath = datasource.Paths.FullPath;
                        var newPath = oldPath.Replace(string.Format("{0}/$name", rootBranchItem.Paths.FullPath), rootItem.Paths.FullPath);
                        Sitecore.Data.Items.Item newDatasource = db.GetItem(newPath);
                        if (newDatasource != null)
                        {
                            foreach (DeviceDefinition deviceDefinition in layoutDefinition.Devices)
                            {
                                if (deviceDefinition != null)
                                {
                                    RenderingDefinition renderingDefinition = deviceDefinition.GetRendering(rendering.RenderingID.ToString());
                                    if (renderingDefinition != null)
                                    {
                                        renderingDefinition.Datasource = newDatasource.ID.ToString();
                                        updated = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (updated)
            {
                item.Editing.BeginEdit();
                layoutField.Value = layoutDefinition.ToXml();
                item.Editing.EndEdit();
            }
            foreach (var childItem in item.Axes.GetDescendants())
            {
                this.CorrectDatasourcesTargetingBranch(db, childItem, rootItem, rootBranchItem);
            }
        }
    }
}