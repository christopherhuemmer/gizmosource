﻿using Sitecore.Pipelines.RenderField;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Gizmo.SitecoreGizmo.Modules
{
    /// <summary>
    /// Remvoe width and height from image fields.
    /// </summary>
    ///  <example>
    ///    <pipelines>
    ///       <renderField>
    ///          <processor patch:after="*[@type='Sitecore.Pipelines.RenderField.GetImageFieldValue, Sitecore.Kernel']" 
    ///                     type="Gizmo.SitecoreGizmo.Modules.ImageFieldSizeAttributesRemover, Gizmo.Sitecore" />
    ///       </renderField>
    ///    </pipelines>
    /// </example>
    /// <see cref="http://aharb.me/sitecore-remove-height-and-width-attributes-on-images-2/"/>
    public class ImageFieldSizeAttributesRemover  
    {
        public void Process(RenderFieldArgs args)
        {
            if (args.FieldTypeKey != "image")
                return;

            string imageTag = args.Result.FirstPart;
            imageTag = Regex.Replace(imageTag, @"(<img[^>]*?)\s+height\s*=\s*\S+", "$1", RegexOptions.IgnoreCase);
            imageTag = Regex.Replace(imageTag, @"(<img[^>]*?)\s+width\s*=\s*\S+", "$1", RegexOptions.IgnoreCase);
            imageTag = Regex.Replace(imageTag, @"(<img[^>]*?)\s+responsive\s*=\s*\S+", "$1", RegexOptions.IgnoreCase);
            args.Result.FirstPart = imageTag;

        }
    }
}
