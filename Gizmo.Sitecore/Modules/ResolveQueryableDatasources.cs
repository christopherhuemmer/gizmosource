﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Layouts;
using Sitecore.Pipelines.InsertRenderings;

namespace Gizmo.SitecoreGizmo.Module
{
    /// <summary>
    /// Query support for datasources.
    /// </summary>
    /// <example>
    ///    <pipelines>
    ///       <insertRenderings>
    ///          <processor patch:before="*[@type='Sitecore.Pipelines.InsertRenderings.Processors.EvaluateConditions, Sitecore.Kernel']"
    ///                     type="Gizmo.SitecoreGizmo.Module.ResolveQueryableDatasources, Gizmo.Sitecore"/>
    ///       </insertRenderings>
    ///    </pipelines>
    /// </example>
    /// <see cref="https://www.cognifide.com/blogs/sitecore/reduce-multisite-chaos-with-sitecore-queries/"/>
    public class ResolveQueryableDatasources : InsertRenderingsProcessor
    {
        public override void Process(InsertRenderingsArgs args)
        {
            foreach (RenderingReference reference in args.Renderings)
            {
                string dataSource = reference.Settings.DataSource;
                if (dataSource.StartsWith("query:"))
                {
                    string query = dataSource.Substring("query:".Length);
                    Sitecore.Data.Items.Item queryItem = args.ContextItem.Axes.SelectSingleItem(query);
                    if (queryItem != null)
                    {
                        reference.Settings.DataSource = queryItem.Paths.FullPath;
                    }
                }
            }
        }
    }
}