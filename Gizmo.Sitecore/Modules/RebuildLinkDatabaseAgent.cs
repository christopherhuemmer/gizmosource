﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using Gizmo.General.String;
using Gizmo.SitecoreGizmo.Items;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Sites;
using Sitecore.Globalization;
using Sitecore.Diagnostics;
using System.Threading;

namespace Gizmo.SitecoreGizmo.Modules
{
    /// <summary>
    /// Update link database. Execute on the server which should have the updated database.
    /// </summary>
    /// <see cref="http://stackoverflow.com/a/1106524"/>
    /// <example>
    ///     <agent type="Gizmo.SitecoreGizmo.Modules.RebuildLinkDatabaseAgent" method="Run" interval="00:00:00">
    ///        <param desc="databaseName">web</param>
    ///     </agent>
    /// </example>
    public class RebuildLinkDatabaseAgent
    {
        public string DatabaseName { get; private set; }


        /// <param name="databaseName">Name of the database to read.</param>
        /// <param name="sitemapPath">Path to the index sitemap.</param>
        /// <param name="sitemapDomainPath">Template to domain sitemaps.</param>
        /// <param name="startItemPath">Sitecore path to start item.</param>
        /// <param name="templateReferenceNames">ScReference names of page templates.</param>
        public RebuildLinkDatabaseAgent(string databaseName)
        {
            DatabaseName = databaseName;
        }

        /// <summary>
        /// Start the agent.
        /// </summary>
        public void Run()
        {
            Sitecore.Globals.LinkDatabase.Rebuild(Sitecore.Configuration.Factory.GetDatabase(DatabaseName));
        }
    }
}
