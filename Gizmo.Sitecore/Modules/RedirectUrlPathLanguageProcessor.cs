﻿using Sitecore;
using Sitecore.Configuration;
using Sitecore.Diagnostics;
using Sitecore.Pipelines.HttpRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizmo.General.String;
using Sitecore.Globalization;

namespace Gizmo.SitecoreGizmo.Modules
{
    /// <summary>
    /// Replace the language found by the url path with another one.
    /// </summary>
    /// <example>
    ///    <pipelines>
    ///         <httpRequestBegin>
    ///             <processor patch:before="*[@type='Sitecore.Pipelines.HttpRequest.LanguageResolver, Sitecore.Kernel']"
    ///                        type="Gizmo.SitecoreGizmo.Modules.RedirectUrlPathLanguageProcessor, Gizmo.Sitecore">
    ///                 <param desc="Redirects">language_from1 => language_to1, language_from2 => language_to2</param>
    ///             </processor>
    ///         </httpRequestBegin>
    ///    </pipelines>
    /// </example>
    public class  RedirectUrlPathLanguageProcessor : HttpRequestProcessor
    {
        public static Dictionary<string, string> Redirects { get; private set; }

        public RedirectUrlPathLanguageProcessor(string redirects)
        {
            // Parse redirects.
            if (string.IsNullOrWhiteSpace(redirects) || Redirects != null)
            {
                return;
            }
            Redirects = redirects.ToList().ToDictionary(x => x.ToList(true, "=>").First().Trim(), y => y.ToList(true, "=>").Last().Trim());
        }

        public override void Process(HttpRequestArgs args)
        {
            Assert.ArgumentNotNull((object) args, "args");

            if (Context.Data.FilePathLanguage == null)
            {
                return;
            }

            // Replace language if necessary.
            if (Redirects.ContainsKey(Context.Data.FilePathLanguage.Name))
            {
                Language lang = null;
                if (Language.TryParse(Redirects[Context.Data.FilePathLanguage.Name], out lang))
                {
                    Log.Debug(string.Format("RedirectUrlPathLanguageProcessor: Found language redirect from {0} to {1}", Context.Data.FilePathLanguage.Name, lang.Name), this);
                    Context.Data.FilePathLanguage = lang;
                }
            }
        }
    }
}
