﻿using System;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Gizmo.Web;
using Sitecore.Abstractions;
using Sitecore.Configuration;

namespace Gizmo.SitecoreGizmo.Modules
{
    /// <summary>
    /// Working (!) custom error page with optional multisite support. By default it uses "ItemNotFoundUrl". But 
    /// you can define site based urls by concating "ItemNotFoundUrl." and the site name. 
    /// For example for the site "homepage" the setting would be named "ItemNotFoundUrl.homepage".
    /// </summary>
    /// <example>
    ///    <pipelines>
    ///         <httpRequestBegin>
    ///             <processor type="Sitecore.Pipelines.HttpRequest.ExecuteRequest, Sitecore.Kernel">
    ///                 <patch:attribute name="type">Gizmo.SitecoreGizmo.Modules.CustomErrorExecuteRequest, Gizmo.Sitecore</patch:attribute>
    ///             </processor>
    ///         </httpRequestBegin>
    ///    </pipelines>
    /// </example>
    /// <see cref="http://stackoverflow.com/a/21191193"/>
    public class CustomErrorExecuteRequest : Sitecore.Pipelines.HttpRequest.ExecuteRequest
    {
        protected override void RedirectOnItemNotFound(string url)
        {
            var context = System.Web.HttpContext.Current;
            var redirectUrl = RedirectUrl();

            // Check if we have an problem with the errorpage and are redirecting to itself.
            var itemValue = Regex.Replace(url, @"item=([^&]+)", "$1");
            var redirectUrlEnc = HttpUtility.UrlEncode(RedirectPath());
            if (itemValue.Contains(redirectUrlEnc))
            {
                Sitecore.Diagnostics.Log.Error(
                    string.Format("Loop redirect while error redirect. Param-Url = '{0}', IP = '{1}', Redirect = '{2}', Site = '{3}', Req-Url = '{4}'",
                        url, IpUtil.ClientIp(), redirectUrl, Sitecore.Context.Site.Name, context.Request.Url.ToString()),
                    this
                );
                context.Response.Write("404 Not found.");
                context.Response.End();
                return;
            }

            try
            {
                DoErrorRedirect(url);
            }
            catch (Exception ex)
            {
                // If our plan fails for any reason, fall back to the base method
                Sitecore.Diagnostics.Log.Error(
                    string.Format("Exception while error redirect, trying default redirect. Param-Url = '{0}', IP = '{1}', Redirect = '{2}', Site = '{3}', Req-Url = '{4}'",
                        url, IpUtil.ClientIp(), redirectUrl, Sitecore.Context.Site.Name, context.Request.Url.ToString()),
                    ex,
                    this
                );
                try
                {
                    base.RedirectOnItemNotFound(url);
                }
                catch (Exception redirectEx)
                {
                    Sitecore.Diagnostics.Log.Error(
                        string.Format("Exception while error redirect, trying simple redirect. Url = '{0}', IP = '{1}', Redirect = '{2}', Site = '{3}', Req-Url = '{4}'",
                        url, IpUtil.ClientIp(), redirectUrl, Sitecore.Context.Site.Name, context.Request.Url.ToString()),
                        redirectEx,
                        this
                    );
                    context.Response.Redirect(redirectUrl);
                }
            }

            // Must be outside the try/catch, cause Response.End() throws an exception
            context.Response.End();
        }

        /// <summary>
        /// This helper allows to redirect to the error page. Even outside this class.
        /// </summary>
        /// <param name="url">The current url not the redirect url (currently unused and only for compatiblity).</param>
        public static void DoErrorRedirect(string url)
        {
            // Request the contents of the 'not found' page using a web request.
            string content = "";
            using (WebClient client = new WebClient())
            {
                // Do NOT do this on any other page than our own.
                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateServerCertificate);
                var redirectUrl = RedirectUrl();
                client.Encoding = Encoding.UTF8;
                content = client.DownloadString(redirectUrl);
            }

            // Send the content to the client with a 404 status code
            var context = HttpContext.Current;
            context.Response.TrySkipIisCustomErrors = true;
            context.Response.StatusCode = 404;
            context.Response.Write(content);
        }

        /// <summary>
        /// Dummy validation. Use only in non critical environments.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="certificate"></param>
        /// <param name="chain"></param>
        /// <param name="sslPolicyErrors"></param>
        /// <returns></returns>
        private static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        private static string RedirectPath()
        {
            // Get 'not found page' setting for current site.
            string notFoundUrl = Settings.GetSetting(string.Concat("ItemNotFoundUrl.", Sitecore.Context.Site.Name));
            if (string.IsNullOrWhiteSpace(notFoundUrl))
            {
                notFoundUrl = Settings.GetSetting("ItemNotFoundUrl");
            }
            return notFoundUrl;
        }

        private static string RedirectUrl()
        {
            var notFoundUrl = RedirectPath();

            // Redirect url.
            string domain = HttpContext.Current.Request.Url.GetComponents(UriComponents.Scheme | UriComponents.Host, UriFormat.Unescaped);
            var redirectUrl = string.Concat(domain, notFoundUrl, "?sc_lang=", Sitecore.Context.Language.Name);

            return redirectUrl;
        }

        public CustomErrorExecuteRequest(BaseSiteManager siteManager, BaseItemManager itemManager) : base(siteManager, itemManager)
        {
        }
    }
}
