﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;
using Sitecore.Links;
using Sitecore.Resources.Media;

namespace Gizmo.SitecoreGizmo.Items
{
    /// <summary>
    /// Link related helper functions.
    /// </summary>
    /// <see cref="https://briancaos.wordpress.com/2012/08/24/sitecore-links-with-linkmanager-and-mediamanager/"/>
    public static class ScLink
    {
        /// <summary>
        /// Get url of an item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="absolute">Indicates if the url must be absolute.</param>
        /// <returns>The url.</returns>
        public static string GetUrl(Sitecore.Data.Items.Item item, bool absolute = false)
        {
            string result = "";
            if (item != null)
            {
                var urlOptions = (UrlOptions)UrlOptions.DefaultOptions.Clone();
                urlOptions.AlwaysIncludeServerUrl = absolute;
                result = LinkManager.GetItemUrl(item, urlOptions);
                result = RemovePort(result);
            }
            return result;
        }

        /// <summary>
        /// Get url of an item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="urlOptions">The UrlOptions to use</param>
        /// <returns>The url.</returns>
        public static string GetUrl(Sitecore.Data.Items.Item item, UrlOptions urlOptions)
        {
            string result = "";
            if (item != null)
            {
                result = LinkManager.GetItemUrl(item, urlOptions);
                result = RemovePort(result);
            }
            return result;
        }

        /// <summary>
        /// Get url of an field of an item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The field key.</param>
        /// <param name="absolute">Indicates if the url must be absolute.</param>
        /// <returns>The url.</returns>
        public static string GetUrl(Sitecore.Data.Items.Item item, string fieldKey, bool absolute = false)
        {
            return GetUrl(ScValue.GetLink(item, fieldKey), absolute);
        }

        /// <summary>
        /// Get url of an field of an item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The field key.</param>
        /// <param name="urlOptions">The UrlOptions to use</param>
        /// <returns>The url.</returns>
        public static string GetUrl(Sitecore.Data.Items.Item item, string fieldKey, UrlOptions urlOptions)
        {
            var linkField = ScValue.GetLink(item, fieldKey);
            string result = string.Empty;
            if (linkField != null)
            {
                switch (linkField.LinkType.ToLower())
                {
                    case "anchor":
                        // Prefix anchor link with # if link if not empty.
                        result = !string.IsNullOrWhiteSpace(linkField.Anchor) ? "#" + linkField.Anchor.TrimStart('#') : string.Empty;
                        break;

                    case "internal":
                        // Use LinkMananger for internal links, if link is not empty.
                        result = linkField.TargetItem != null ? LinkManager.GetItemUrl(linkField.TargetItem, urlOptions) : string.Empty;

                        // If available add anchor.
                        if (!string.IsNullOrWhiteSpace(linkField.Anchor))
                        {
                            result += "#" + linkField.Anchor.TrimStart('#');
                        }
                        break;

                    case "media":
                        result = GetUrl((Sitecore.Data.Items.MediaItem)linkField.TargetItem, urlOptions);
                        break;

                    default:
                        result = linkField.Url;
                        break;
                }
            }
            result = RemovePort(result);
            return result;
        }

        /// <summary>
        /// Get url of an link field.
        /// </summary>
        /// <param name="linkField">The linkField.</param>
        /// <param name="absolute">Indicates if the url must be absolute. Ignored if urlOptions is set.</param>
        /// <returns>The url.</returns>
        public static string GetUrl(LinkField linkField, bool absolute = false)
        {
            string result = "";
            if (linkField != null)
            {
                switch (linkField.LinkType.ToLower())
                {
                    case "anchor":
                        // Prefix anchor link with # if link if not empty.
                        result = !string.IsNullOrWhiteSpace(linkField.Anchor) ? "#" + linkField.Anchor.TrimStart('#') : string.Empty;
                        break;

                    case "internal":
                        // Use LinkMananger for internal links, if link is not empty.
                        var urlOptions = (UrlOptions)UrlOptions.DefaultOptions.Clone();
                        urlOptions.AlwaysIncludeServerUrl = absolute;
                        result = linkField.TargetItem != null ? LinkManager.GetItemUrl(linkField.TargetItem, urlOptions) : string.Empty;

                        // If available add anchor.
                        if (!string.IsNullOrWhiteSpace(linkField.Anchor))
                        {
                            result += "#" + linkField.Anchor.TrimStart('#');
                        }
                        break;

                    case "media":
                        result = GetUrl((Sitecore.Data.Items.MediaItem)linkField.TargetItem, absolute);
                        break;

                    default:
                        result = linkField.Url;
                        break;
                }
            }
            result = RemovePort(result);
            return result;
        }

        /// <summary>
        /// Get url of an media item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="absolute">Indicates if the url must be absolute.</param>
        /// <returns>The url.</returns>
        public static string GetUrl(Sitecore.Data.Items.MediaItem item, bool absolute = false)
        {
            string result = "";
            if (item != null)
            {
                var mediaUrlOptions = (MediaUrlOptions)MediaUrlOptions.Empty;
                mediaUrlOptions.AlwaysIncludeServerUrl = absolute;
                result = MediaManager.GetMediaUrl(item, mediaUrlOptions);
                result = RemovePort(result);
            }
            return result;
        }

        /// <summary>
        /// Get url of an media item. This function allows to specify custom media url options and will also apply hash protection.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="options">Custom media options.</param>
        /// <returns>The url.</returns>
        public static string GetUrl(Sitecore.Data.Items.MediaItem item, MediaUrlOptions mediaUrlOptions)
        {
            string result = "";
            if (item != null)
            {
                result = MediaManager.GetMediaUrl(item, mediaUrlOptions);
                result = HashingUtils.ProtectAssetUrl(result);
                // The original code used HttpUtility.UrlPathEncode(result) to encode the path. This doesn't seem necessary anymore.
                result = RemovePort(result);
            }
            return result;
        }

        /// <summary>
        /// Linkmanager adds unnecessary ports. Let's remove them.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private static string RemovePort(string url)
        {
            // Remove port. Stupid linkmanager.
            return url.Replace(":443", string.Empty);
        }
    }
}