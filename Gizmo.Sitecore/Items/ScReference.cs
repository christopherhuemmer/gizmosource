﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Data;
using Sitecore.Diagnostics;
using Sitecore.Data.Items;

namespace Gizmo.SitecoreGizmo.Items
{
    /// <summary>
    /// Gives access to the global reference item containing template ids...
    /// To activate the buffer:
    /// <setting name = "Gizmo.Sitecore.ScSettings.ScReference.UseBuffer" value="true" />
    /// <setting name = "Gizmo.Sitecore.ScSettings.ScReference.TemplateID" value="{6F2B9648-CD5B-45B9-986C-74B983ECF868}" />
    /// </summary>
    public class ScReference
    {
        public Sitecore.Data.Items.Item ReferenceDir { get; private set; }
        protected bool UseCache { get; set; }
        protected ID ReferenceTemplateID { get; set; }
        private ScReference()
        {
            // It doesn't make sense to execute this on core because there will never be any data. 
            // This only happens if a shell function is executed.

            //AWI 23/05/2016 we changed getting the correct db - now using Settings.
            //var db = Sitecore.Context.Database;
            //if (db.Name == "core")
            //{
            //    db = Database.GetDatabase("master");
            //}

            var db = Database.GetDatabase(Sitecore.Configuration.Settings.GetSetting("Gizmo.Sitecore.ScSettings.ScReferenceDb", "master"));
            var relativePath = Sitecore.Configuration.Settings.GetSetting("Gizmo.Sitecore.ScSettings.ScReference.RelativePath", "references").Trim('/');
            var path = string.Concat("/sitecore/system/", relativePath);
            ReferenceDir = db.GetItem(path);
            Assert.IsNotNull(ReferenceDir, "ReferenceDir is null.");
            UseCache = Sitecore.Configuration.Settings.GetSetting("Gizmo.Sitecore.ScSettings.ScReference.UseBuffer", "false").Equals("true", StringComparison.InvariantCultureIgnoreCase);
            if(UseCache)
            {
                var idSetting = Sitecore.Configuration.Settings.GetSetting("Gizmo.Sitecore.ScSettings.ScReference.TemplateID");
                //if the id of the reference template isn't set we can't use the cache
                if (string.IsNullOrWhiteSpace(idSetting))
                {
                    throw new ArgumentException("Gizmo.Sitecore.ScSettings.ScReference.TemplateID not set, with UseBuffer - this is an invalid setting.");
                }
                //register the event here - it's a must, this way it can't be forgotten
                Sitecore.Events.Event.Subscribe("item:saving", new System.EventHandler(this.OnItemSaveing));
                ReferenceTemplateID = new ID(idSetting);
            }
        }

        /// <returns>An instance of this object.</returns>
        public static ScReference Instance()
        {
            //AW 19.05.2016 the ctor would SOMETIMES throw a "null pointer" because of the 'SingletonInstance = new ScReference();' in the cctor
            //only thing this can be is a race condition where Sitecore.Context.Database is not yet set
            //we give it a littel more time by creating the object here - hope this helps
            //if we keep getting this add somthing to global.asax to check Sitecore.Context.Database
            //AWI 28.10.2016 This fixed it.

            // Singleton disabled for now for testing purpose. We problably need a better solution and must check if it is even necessary.
            //if (SingletonInstance == null)
            //{
            //    SingletonInstance = new ScReference();
            //}
            //return SingletonInstance;
            return new ScReference();
        }

        
        //buffers the key to the item ID a Reference buffer points to
        private static Dictionary<string, ID> _referencedBuffer = new Dictionary<string, ID>();
        //buffers all items a reference item points to 
        private static Dictionary<ID, Item> _referenceItemBuffer = new Dictionary<ID, Item>();
        internal void FlushItemBuffer(Item itm)
        {
            if (itm != null && UseCache)
            {
                lock (_referenceItemBuffer)
                {
                    //if any reference item is changed flush the buffer compleatly, this happens very rarely.
                    if (itm.TemplateID == ReferenceTemplateID)
                    {
                        _referenceItemBuffer.Clear();
                        _referencedBuffer.Clear();
                    }
                    else
                    {
                        _referenceItemBuffer.Remove(itm.ID);
                    }
                }
            }
        }
        /// <summary>
        /// Retrieves an item from the buffer.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>null if not found, else the item referenced by key</returns>
        protected Item FromItemBuffer(string key)
        {
            Item retVal = null; ID id;
            lock (_referenceItemBuffer)
            {
                if (_referencedBuffer.TryGetValue(key, out id))
                {
                    _referenceItemBuffer.TryGetValue(id, out retVal);
                }
            }
            return retVal;
        }
        /// <summary>
        /// Adds item to buffer. This must be the Item the reference points to - not the reference itself
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="itm">The item.</param>
        protected void AddToBuffer(string key, Item itm)
        {
            lock (_referenceItemBuffer)
            {
                _referencedBuffer[key] = itm.ID;
                _referenceItemBuffer[itm.ID] = itm;
            }
        }
        /// <summary>
        /// Called when [item saveing].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void OnItemSaveing(object sender, EventArgs args)
        {
            Sitecore.Events.SitecoreEventArgs eventArgs = args as Sitecore.Events.SitecoreEventArgs;
            Assert.IsNotNull(eventArgs, "eventArgs");
            Item item = eventArgs.Parameters[0] as Item;

            ScReference.Instance().FlushItemBuffer(item);

        }

        /// <summary>
        /// Get the referenced item.
        /// </summary>
        /// <param name="key">Key of the item.</param>
        /// <returns>The item.</returns>
        public Item GetItem(string key)
        {
            Item retVal;
            if (UseCache)
            {
                Profiler.StartOperation("ScReference.GetItem - Cached");
                retVal = FromItemBuffer(key);
                Profiler.EndOperation();
                if (retVal != null)
                {
                    return retVal;
                }
            }

            Profiler.StartOperation("ScReference.GetItem");
            var refItem = ReferenceItem(key);
            retVal = ScValue.GetItem(refItem, "Item");
            if (retVal == null)
            {
                //try the Path field on refItem
                var path = ScValue.GetString(refItem, "Path");
                if (!string.IsNullOrWhiteSpace(path))
                {
                    if (path.StartsWith("/sitecore"))
                    {
                        //full path
                        retVal = refItem.Database.GetItem(path);
                    }
                    else
                    {
                        //relative path
                        path = string.Concat(Sitecore.Context.Site.ContentStartPath, "/", path.Trim('/'));
                        retVal = refItem.Database.GetItem(path);
                    }
                }
            }

            if (UseCache)
            {
                AddToBuffer(key, retVal);
            }
            Profiler.EndOperation();

            return retVal;
        }

        /// <summary>
        /// Get referenced path. This function will first try to read a configured path
        /// and uses the item path as an fallback.
        /// </summary>
        /// <param name="key">Key of the path.</param>
        /// <returns>The path.</returns>
        public string GetPath(string key)
        {
            Sitecore.Diagnostics.Profiler.StartOperation("ScReference.GetPath");

            var result = ScValue.GetString(ReferenceItem(key), "Path");
            if (string.IsNullOrEmpty(result))
            {
                result = GetItem(key).Paths.FullPath;
            }

            Sitecore.Diagnostics.Profiler.EndOperation();

            return result;
        }

        /// <summary>
        /// Checks if a reference exists.
        /// </summary>
        /// <param name="key">Key of the path.</param>
        /// <returns>True if reference exists else false.</returns>
        public bool Has(string key)
        {
            return ReferenceDir.Children.Any(x => x.Name == key);
        }

        /// <summary>
        /// Get an item relative to Instance using ScReference.
        /// </summary>
        /// <param name="referenceName">The name of the reference.</param>
        /// <param name="subRootReferenceName">The sub folder of the instance which should be the root</param>
        /// <param name="contextItem">If not null this item instead of Sitecore.Context.Item will be used as the context.</param>
        /// <returns>The item.</returns>
        public static Item GetItemRelativeToInstance(string referenceName, string subRootReferenceName = null, Item contextItem = null)
        {
            var scReference = Instance();
            var relativePath = scReference.GetPath(referenceName).Trim('/');
            if (!string.IsNullOrWhiteSpace(subRootReferenceName))
            {
                relativePath = string.Concat(scReference.GetPath(subRootReferenceName).Trim('/'), "/", relativePath);
            }

            var path = string.Concat(InstancePath(contextItem), "/", relativePath);
            return Sitecore.Context.Database.GetItem(path);
        }

        /// <summary>
        /// Find the path of the instance item.
        /// </summary>
        /// <param name="contextItem">If not null this item instead of Sitecore.Context.Item will be used as the context.</param>
        /// <returns></returns>
        public static string InstancePath(Item contextItem = null)
        {
            var path = "";

            var scReference = ScReference.Instance();
            if (scReference.Has("Instance Template"))
            {
                try
                {
                    if (contextItem == null)
                    {
                        contextItem = Sitecore.Context.Item;
                    }
                    if (contextItem != null)
                    {
                        var instanceTemplate = scReference.GetItem("Instance Template").ID;
                        var instancePath = contextItem.Axes.GetAncestors().FirstOrDefault(x => x.TemplateID == instanceTemplate)?.Paths.FullPath;
                        if (!string.IsNullOrWhiteSpace(instancePath))
                        {
                            path = instancePath;
                        }
                    }
                }
                catch(Exception ex)
                {
                    Log.Error($"Error in ScReference.InstancePath()", ex, scReference);
                }
            }

            // Fallback
            if (string.IsNullOrWhiteSpace(path))
            {
                path = Sitecore.Context.Site.ContentStartPath;
            }

            return path;
        }

        private Item ReferenceItem(string key)
        {
            return ReferenceDir.Children.Where(x => x.Name == key).First();
        }
    }
    public class ScReferenceSaveEventHandler
    {
        
    }
}