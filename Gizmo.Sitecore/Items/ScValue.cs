﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Gizmo.General.String;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using System.Web.Mvc;
using Sitecore.Mvc;

namespace Gizmo.SitecoreGizmo.Items
{
    /// <summary>
    /// Value retrieving related helper functions. 
    /// </summary>
    /// <see cref="http://seankearney.com/post/Getting-a-Sitecore-field-value"/>
    public static class ScValue
    {
        /// <summary>
        /// Get the bool value of an item field. Use with "Checkbox" fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The value.</returns>
        public static bool GetBool(Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "Checkbox" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            CheckboxField field = item.Fields[fieldKey];
            return field.Checked;
        }

        /// <summary>
        /// Get the date value of an item field. Use with "DateTime" fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The value.</returns>
        public static DateTime GetDate(Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "Date", "DateTime" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            DateField field = item.Fields[fieldKey];
            return field.DateTime.ToLocalTime();
        }

        /// <summary>
        /// Get the double value of an item field. Use with "Number" fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The value.</returns>
        public static double GetDouble(Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "Number" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            var stringValue = GetString(item, fieldKey);
            double result = 0;
            if (!string.IsNullOrEmpty(stringValue))
            {
                result = double.Parse(stringValue, CultureInfo.InvariantCulture);
            }
            return result;
        }

        /// <summary>
        /// Get the file of the item field. Use with "File"
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The value.</returns>
        public static MediaItem GetFile(Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "File" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            FileField field = item.Fields[fieldKey];
            MediaItem result = null;
            if (field.MediaItem != null)
            {
                result = new MediaItem(field.MediaItem);
            }
            return result;
        }

        /// <summary>
        /// Get html of a rich text field.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The value.</returns>
        public static HtmlString GetHtml(Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] {"Single-Line Text", "Multi-Line Text", "Rich Text" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            return new HtmlString(item.Fields[fieldKey].Value); 
        }

        /// <summary>
        /// Get the image of the item field. Use with "Image".
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The value.</returns>
        public static MediaItem GetImage(Item item, string fieldKey, params string[] fieldTypes)
        {
            ImageField field = GetImageField(item, fieldKey, fieldTypes);

            // Everything is ok.
            MediaItem result = null;
            if (field.MediaItem != null)
            {
                result = new MediaItem(field.MediaItem);
            }
            return result;
        }

        /// <summary>
        /// Get the image of the item field. Use with "Image". This method must be used if the alt text from the image field
        /// should be used.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The value.</returns>
        public static ImageField GetImageField(Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "Image" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            ImageField result = item.Fields[fieldKey];
            return result;
        }

        /// <summary>
        /// Get the integer value of an item field. Use with "Integer" fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The value.</returns>
        public static int GetInteger(Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "Integer" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            var stringValue = GetString(item, fieldKey);
            int result = 0;
            if (!string.IsNullOrEmpty(stringValue))
            {
                result = int.Parse(stringValue);
            }
            return result;
        }

        /// <summary>
        /// Get the item value of an item field. Use with "DropLink" fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The value.</returns>
        public static Item GetItem(Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "DropLink", "DropTree" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            ReferenceField field = item.Fields[fieldKey];
            return field.TargetItem;
        }

        /// <summary>
        /// Get the item values of an item field. Use with "MultiList" fields which references items.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The value.</returns>
        public static IEnumerable<Item> GetItems(Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "Multilist", "Treelist", "TreelistEx" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            MultilistField field = item.Fields[fieldKey];
            IList<Item> result = new List<Item>();
            foreach (ID id in field.TargetIDs)
            {
                var foundItem = item.Database.Items[id];
                if (foundItem != null)
                {
                    result.Add(foundItem);
                }
            }
            return result;
        }

        /// <summary>
        /// Get the link value of an item field. Use with "GeneralLink" fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The value.</returns>
        public static LinkField GetLink(Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "General Link" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            return (LinkField)item.Fields[fieldKey];
        }

        /// <summary>
        /// Get the string value of an item field. Use with text (Single-Line Text, Multi-Line Text, Droplist) fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The value.</returns>
        public static string GetString(Item item, string fieldKey)
        {
            // Validate input.
            ValidateField(item, fieldKey);

            // Everything is ok.
            return item.Fields[fieldKey].Value; // Same as item[fieldKey].
        }

        /// <summary>
        /// Get the string value of an item field. Use with "MultiList" fields which references items.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The value.</returns>
        public static IEnumerable<string> GetStrings(Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "Multilist", "Treelist", "TreelistEx" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            MultilistField field = item.Fields[fieldKey];
            IList<string> result = new List<string>();

            foreach (ID id in field.TargetIDs)
            {
                var tmpItem = item.Database.Items[id];
                if (tmpItem != null)
                {
                    result.Add(tmpItem.DisplayName);
                }
            }
            
            return result;
        }

        /// <summary>
        /// Check if an item has a certain field.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>True if the item has given field.</returns>
        public static bool Has(Item item, string fieldKey)
        {
            // Validate input.
            ValidateItemKey(item, fieldKey);

            // Everything is ok.
            return item.Fields[fieldKey] != null;
        }

        /// <summary>
        /// Validate item field. Throws exceptions if something is wrong.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldType">Optional parameter. Validate field type.</param>
        public static void ValidateField(Item item, string fieldKey, params string[] fieldType)
        {
            // Validate the item and the given field key.
            ValidateItemKey(item, fieldKey);

            // Does the field exist?
            Assert.IsNotNull(item.Fields[fieldKey],
                "Item.Fields[" + fieldKey + "] is null. " +
                "Most likely the template is not up to date in the current language. " +
                "Item was \"" + item.Name + "\" with template \"" + item.TemplateName + "\".");

            // Does the field have one of the expected types?
            Assert.IsTrue(
                !fieldType.Any() || fieldType.Any(x => StringUtil.AreEqual(item.Fields[fieldKey].Type, x, true)),
                "Not supported field type " + item.Fields[fieldKey].Type + "." +
                "Expected " + string.Join(",",fieldType) + ". Item was \"" + item.Name + "\" with template \"" + item.TemplateName + "\"."
            );
        }

        /// <summary>
        /// Validate the item and check if the key is not null. Throws exceptions if something is wrong.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        private static void ValidateItemKey(Item item, string fieldKey)
        {
            // Is the item null?
            Assert.IsNotNull(item, "Item is null.");

            // Is the field key null or empty?
            Assert.IsNotNullOrEmpty(fieldKey, "FieldKey is null or empty. Item was \"" + item.Name + "\" with template \"" + item.TemplateName + "\".");

            // Does the item have fields?
            Assert.IsNotNull(item.Fields, "Item.Fields is null. Item was \"" + item.Name + "\" with template \"" + item.TemplateName + "\".");
        }
    }

    /// <summary>
    /// Extension which enables ScEdit on HtmlHelper.
    /// </summary>
    public static class ScEditExtension
    {
         public static ScEdit ScEdit(this HtmlHelper html)
        {
            return new ScEdit(html);
        }
    }

    /// <summary>
    /// Value editing with support for the same validation as ScValue.
    /// </summary>
    public class ScEdit
    {
        private HtmlHelper HtmlHelper { get; set; }

        public ScEdit(HtmlHelper htmlHelper)
        {
            this.HtmlHelper = htmlHelper;
        }

        /// <summary>
        /// Edit date fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The sitecore field edit html.</returns>
        public HtmlString Date(Item item, string fieldKey, params string[] fieldTypes)
        {
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] {"Date", "DateTime" };
            }
            return SitecoreFieldEdit(item, fieldKey, fieldTypes);
        }

        /// <summary>
        /// Edit double fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The sitecore field edit html.</returns>
        public HtmlString Double(Item item, string fieldKey, params string[] fieldTypes)
        {
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] {"Number" };
            }
            return SitecoreFieldEdit(item, fieldKey, fieldTypes);
        }

        /// <summary>
        /// Edit rich text fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The sitecore field edit html.</returns>
       public HtmlString Html(Item item, string fieldKey, params string[] fieldTypes)
        {
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] {"Rich Text"};
            }
            return SitecoreFieldEdit(item, fieldKey, fieldTypes);
        }

        /// <summary>
        /// Edit image fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The sitecore field edit html.</returns>
        public HtmlString Image(Item item, string fieldKey, params string[] fieldTypes)
        {
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] {"Image" };
            }
            return SitecoreFieldEdit(item, fieldKey, fieldTypes);
        }

        /// <summary>
        /// Edit integer fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The sitecore field edit html.</returns>
        public HtmlString Integer(Item item, string fieldKey, params string[] fieldTypes)
        {
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] {"Integer" };
            }
            return SitecoreFieldEdit(item, fieldKey, fieldTypes);
        }

        /// <summary>
        /// Edit normal string fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The sitecore field edit html.</returns>
       public HtmlString String(Item item, string fieldKey, params string[] fieldTypes)
        {
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] {"Single-Line Text", "Multi-Line Text" };
            }
            return SitecoreFieldEdit(item, fieldKey, fieldTypes);
        }

        /// <summary>
        /// Implementation of field edit with validation.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldTypes">Optional field types if other than the normal ones must be supported.</param>
        /// <returns>The sitecore field edit html.</returns>
        private HtmlString SitecoreFieldEdit(Item item, string fieldKey, string[] fieldTypes)
        {
            // Validate input.
            ScValue.ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            return HtmlHelper.Sitecore().Field(fieldKey, item);
        }
    }
}