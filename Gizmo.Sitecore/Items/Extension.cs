﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Data.Items;

namespace Gizmo.SitecoreGizmo.Items
{
    /// <summary>
    /// Extension method for Items.
    /// </summary>
    public static class Extension
    {
        /// <summary>
        /// Check if all fields in the given item exists and is not empty.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldNames">Name of the fields.</param>
        /// <returns>True if fields are not emtpy else false.</returns>
        public static bool AreFieldsSet(this Sitecore.Data.Items.Item item, params string[] fieldNames)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Item may not be null.");
            }

            bool result = true;
            foreach (string fieldName in fieldNames)
            {
                bool isFieldSet = (item.Fields[fieldName] != null) &&
                                  (!string.IsNullOrEmpty(item.Fields[fieldName].Value));
                if (!isFieldSet)
                {
                    result = false;
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// Filter out Items from the collection that dont have a Version, if fieldNames are provided checks if any field contains a Value
        /// </summary>
        /// <param name="fieldNames">The field names.</param>
        public static IEnumerable<Item> WithVersion(this IEnumerable<Item> source, params string[] fieldNames)
        {
            if (source == null)
            {
                return Enumerable.Empty<Item>();
            }
            if (!fieldNames.Any())
            {
                return source.Where(x => x.Versions.Count > 0);
            }

            return source.Where(x => x.Fields.Any(f => fieldNames.Contains(f.Name) && !string.IsNullOrEmpty(f.Value)));
        }
        /// <summary>
        /// Check if this language has a Version, if fieldNames are provided checks if any field contains a Value
        /// </summary>
        /// <param name="fieldNames">The field names.</param>
        /// <returns></returns>
        public static bool HasVersion(this Item source, params string[] fieldNames)
        {
            if(source == null)
            {
                return false;
            }
            if (!fieldNames.Any())
            {
                return source.Versions.Count > 0;
            }
            return source.Fields.Any(f => fieldNames.Contains(f.Name) && !string.IsNullOrEmpty(f.Value));
        }
    }
}
