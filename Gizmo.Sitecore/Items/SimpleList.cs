﻿using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmo.SitecoreGizmo.Items
{
    public static class SimpleList
    {
        private static IEnumerable<Item> GetAsEnumerable(string listName)
        {
            var lstTemplate = ScReference.Instance().GetItem("Simple List Value");
            Assert.IsNotNull(lstTemplate, "Simple List Value not found");

            var lstFolder = ScReference.Instance().GetItem("List Root");
            Assert.IsNotNull(lstFolder, "List Root Reference not found");

            var folder = lstFolder.Children.Where(x => x.Name == listName).FirstOrDefault();
            Assert.IsNotNull(folder, listName + " list not found");

            return folder.Children.Where(x => x.TemplateID == lstTemplate.ID);
        }

        private static List<Item> GetAsList(string listName)
        {
            return GetAsEnumerable(listName).ToList();
        }
        public static Item GetValueItem(string listName, string valueName)
        {
            var valueItem = GetAsEnumerable(listName).Where(x=> x.Name == valueName).FirstOrDefault();
            Assert.IsNotNull(valueItem, valueName + " not found");

            return valueItem;
        }
        public static string GetValue(string listName, string valueName)
        {
            return ScValue.GetString(GetValueItem(listName, valueName), "Value");
        }

        public static List<KeyValuePair<Item, List<Item>>> GetLookupList(string listName)
        {
            var retVal = new List<KeyValuePair<Item, List<Item>>>();
            foreach(Item parentVal in  GetAsEnumerable(listName))
            {
                var oneKvp = new KeyValuePair<Item, List<Item>>(parentVal, parentVal.Children.ToList());
                retVal.Add(oneKvp);
            }
            return retVal;
        }

        public static Dictionary<string, List<string>> GetValueDictionary(string listName)
        {
            Dictionary<string, List<string>> lookup = new Dictionary<string, List<string>>();
            foreach (var kvp in GetLookupList(listName))
            {
                var groupName = ScValue.GetString(kvp.Key, "Value");
                var subList = new List<string>();
                foreach (var item in kvp.Value)
                {
                    var itemVal = ScValue.GetString(item, "Value");
                    subList.Add(itemVal);
                }
                lookup[groupName] = subList;
            }

            return lookup;
        }


        public static Dictionary<string, string> GetReverseValueDictionary(string listName)
        {
            Dictionary<string, string> lookup = new Dictionary<string, string>();
            foreach (var kvp in GetLookupList(listName))
            {
                var groupName = ScValue.GetString(kvp.Key, "Value");
                foreach (var item in kvp.Value)
                {
                    var itemVal = ScValue.GetString(item, "Value");
                    lookup[itemVal] = groupName;
                }
            }

            return lookup;
        }
    }
}
