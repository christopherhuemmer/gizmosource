﻿using System;
using System.Linq;
using Sitecore.Web.UI.HtmlControls;
using System.Collections.Generic;

namespace Gizmo.SitecoreGizmo.CustomFields
{
    /// <summary>
    ///  Implements a simple Generic Key Value Pair Droplist that can be filled in code
    ///  You need to implement a KeyValueProviderBase and pass the full type information in "Source" of the Template field
    ///  e.g. Zwick.Web.CustomFields.KVPJobs, Zwick.Web
    /// </summary>
    /// <example>
    ///     <fieldTypes>
    ///         <fieldType patch:after="*[@type='Sitecore.Data.Fields.ValueLookupField,Sitecore.Kernel']"
    ///             name="KVP Droplist" type="Gizmo.SitecoreGizmo.CustomFields.GenericKeyValueCombobox,Gizmo.Sitecore"/>
    ///  </fieldTypes>
    /// </example>
    /// <seealso cref="Gizmo.SitecoreGizmo.CustomFields.KeyValueProviderBase" />
    public class GenericKeyValueCombobox : Combobox
    {
        public string Source { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            if (Controls.Count == 0)
            {
                try
                {
                    Type kvpProviderType = Type.GetType(Source);
                    KeyValueProviderBase kvpProvider =  (KeyValueProviderBase)Activator.CreateInstance(kvpProviderType);
                    var items = kvpProvider.GetKeyValuePairs();
                    if (kvpProvider.IncludeEmptyElement)
                    {
                        items = Enumerable.Repeat(new KeyValuePair<string, string>(string.Empty, string.Empty), 1).Concat(items);
                    }
                    foreach (var kvp in items)
                    {
                        Controls.Add(new ListItem
                        {
                            ID = Guid.NewGuid().ToString(),
                            Header = kvp.Value,
                            Value = kvp.Key
                        });
                    }
                }
                catch (Exception)
                {
                    Controls.Add(new ListItem
                    {
                        Header = "Could not load Data"
                    });
                }
            }

            base.OnLoad(e);
        }

    }
}
