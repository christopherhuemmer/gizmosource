﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmo.SitecoreGizmo.CustomFields
{
    public abstract class KeyValueProviderBase
    {
        /// <summary>
        /// Gets or sets a value indicating whether to add an empyt entry into the dropdown.
        /// </summary>
        public bool IncludeEmptyElement { get; protected set; } = true;
        public abstract IEnumerable<KeyValuePair<string, string>> GetKeyValuePairs();

    }
}
