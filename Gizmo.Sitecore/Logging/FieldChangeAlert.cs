﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gizmo.General.String;
using Gizmo.SitecoreGizmo.Items;
using Sitecore.Diagnostics;
using Sitecore.Data.Managers;
using Sitecore.Data.Templates;
using Sitecore.Configuration;
using Gizmo.General.Email;
using System.Web;

namespace Gizmo.SitecoreGizmo.Logging
{
    /// <summary>
    /// Alert if certain fields in certain templates change.
    /// </summary>
    /// <example>
    ///     <events>
    ///         <event name="item:saving"> 
    ///             <handler type="Gizmo.SitecoreGizmo.Logging.FieldChangeAlert, Gizmo.Sitecore" method="OnItemSaveing">
    ///                 <param desc="templateFields">{DF0073F9-2024-45F6-BD4E-62A0C7EE7E7C}:Body Id,Product Id|{70D193BE-5CA0-4D5F-87B3-99A2E0B105DD}:*</param>
    ///                 <param desc="emailRecipients">stefan.gfroerer@udg.de</param>
    ///             </handler>
    ///         </event>
    ///     </events>
    /// </example>
    public class FieldChangeAlert
    {
        private Dictionary<ID, IEnumerable<string>> TemplateFields { get; set; }
        private IEnumerable<string> EmailRecipients { get; set; }

        /// <param name="templateFields">Pipe | separated list of template ids and field names which should be checked. The template id is separated by a semicolon from 
        /// a comma separated list of field names of this template. Field names can also be wildcards. * for all custom fields (without standard fields) and ** for all fields. * 
        /// and field names of standard fields can be combined.</param>
        /// <param name="emailRecipients">Optional list of email recipients.</param>
        public FieldChangeAlert(string templateFields, string emailRecipients)
        {
            this.TemplateFields = templateFields.Split('|').Select(x => x.Split(':')).ToDictionary(x => ID.Parse(x.First()), y => y.Last().Split(',').AsEnumerable());
            this.EmailRecipients = emailRecipients.ToList();
        }

        public void OnItemSaveing(object sender, EventArgs args)
        {
            try
            {
                // Try to get original and udpated items.
                var eventArgs = args as SitecoreEventArgs;
                if (eventArgs == null)
                {
                    return;
                }

                var updatedItem = eventArgs.Parameters[0] as Item;
                if (updatedItem == null)
                {
                    return;
                }
                if (!TemplateFields.ContainsKey(updatedItem.TemplateID))
                {
                    return;
                }
                var fieldNames = TemplateFields[updatedItem.TemplateID];

                var existingItem = updatedItem.Database.GetItem(
                    updatedItem.ID,
                    updatedItem.Language,
                    updatedItem.Version // TODO Does this work with increasing versions?
                ); 
                if (existingItem == null)
                {
                    return;
                }

                // Check for changed fields.
                var defaultBaseTemplate = TemplateManager.GetTemplate(Settings.DefaultBaseTemplate, existingItem.Database);
                var changedFields = new List<ChangedField>();
                foreach (var fieldName in fieldNames)
                {
                    // All custom fields.
                    if (fieldName == "*")
                    {
                        var template = TemplateManager.GetTemplate(existingItem.TemplateID, existingItem.Database);
                        var allFields = template.GetFields(true);
                        foreach (var field in allFields.Where(x => !defaultBaseTemplate.ContainsField(x.ID)))
                        {
                            var changedField = CheckFieldForChanges(existingItem, updatedItem, field.Name);
                            if (changedField != null)
                            {
                                changedFields.Add(changedField);
                            }
                        }
                    }
                    // All fields.
                    else if (fieldName == "**")
                    {
                        var template = TemplateManager.GetTemplate(existingItem.TemplateID, existingItem.Database);
                        var allFields = template.GetFields(true);
                        foreach (var field in allFields)
                        {
                            var changedField = CheckFieldForChanges(existingItem, updatedItem, field.Name);
                            if (changedField != null)
                            {
                                changedFields.Add(changedField);
                            }
                        }
                    }
                    // Single field.
                    else if (ScValue.Has(existingItem, fieldName))
                    {
                        var changedField = CheckFieldForChanges(existingItem, updatedItem, fieldName);
                        if (changedField != null)
                        {
                            changedFields.Add(changedField);
                        }
                    }
                    // Unknown field.
                    else
                    {
                        Log.Audit($"[FieldChangeAlert] Unknown field name '{fieldName}' for template id '{existingItem.TemplateID}'.", this);
                    }
                }

                if (!changedFields.Any())
                {
                    return;
                }

                // Log result and send alert if configured.
                var emailLogs = new List<string>();
                foreach (var changedField in changedFields)
                {
                    var logEntry = $"User {Sitecore.Context.User.Name} has changed field '{changedField.FieldName}' of item '{existingItem.Paths.FullPath}'. Old value was '{changedField.OriginalValue}'. New value is '{changedField.NewValue}'.";
                    Log.Audit("[FieldChangeAlert] " + logEntry, this);
                    emailLogs.Add(logEntry);
                }

                if (EmailRecipients.Any())
                {
                    var message = string.Join(Environment.NewLine, emailLogs);
                    var email = Email.Instance()
                        .SetSmtpHostPortFromSystemNet()
                        .AddTo(EmailRecipients)
                        .SetSubject($"FieldChangeAlert for {HttpContext.Current.Request.Url.Host}")
                        .SetBody(message);
                    if (!email.Send())
                    {
                        Log.Error("[FieldChangeAlert] Failed to send email", email.Error, this);
                    }
                }
            }
            catch(Exception ex)
            {
                Log.Error("[FieldChangeAlert] Unknown exception", ex, this);
            }
        }

        private ChangedField CheckFieldForChanges(Item existingItem, Item updatedItem, string fieldName)
        {
            if (ScValue.GetString(existingItem, fieldName) != ScValue.GetString(updatedItem, fieldName))
            {
                var changedField = new ChangedField()
                {
                    FieldName = fieldName,
                    OriginalValue = ScValue.GetString(existingItem, fieldName),
                    NewValue = ScValue.GetString(updatedItem, fieldName)
                };
                return changedField;
            }
            else
            {
                return null;
            }
        }
    }

    class ChangedField
    {
        public string FieldName { get; set; }
        public string OriginalValue { get; set; }
        public string NewValue { get; set; }
    }
}
