﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.WebPages;

namespace Gizmo.SitecoreGizmo.Controller
{
    /// <summary>
    /// Base controller which allows to replace the current view with another based on a parameter. 
    /// </summary>
    public abstract class InterchangeableViewController : System.Web.Mvc.Controller
    {
        private const string ViewParameterName = "view";
        private const string VariantParameterName = "variant";

        protected new internal ViewResult View()
        {
            var viewName = ControllerContext.RouteData.GetRequiredString("action");
            CheckForCustomView(ref viewName, ControllerContext);
            return base.View(viewName, null);
        }

        protected new internal ViewResult View(object model)
        {
            var viewName = ControllerContext.RouteData.GetRequiredString("action");
            CheckForCustomView(ref viewName, ControllerContext);
            return base.View(viewName, model);
        }

        protected new internal ViewResult View(string viewName)
        {
            CheckForCustomView(ref viewName, ControllerContext);
            return base.View(viewName);
        }

        protected new internal ViewResult View(string viewName, object model)
        {
            CheckForCustomView(ref viewName, ControllerContext);
            return base.View(viewName, model);
        }

        /// <summary>
        /// Need this to prevent View(string, object) stealing calls to View(string, string).
        /// </summary>
        protected new internal ViewResult View(string viewName, string masterName)
        {
            return base.View(viewName, masterName);
        }

        /// <summary>
        /// Checks if a custom view is requested and sets its view name.
        /// </summary>
        /// <see cref="http://kevinmontrose.com/2011/07/17/mobile-views-in-asp-net-mvc3/"/>
        /// <param name="viewName">Reference to the current view name. Will be referenced if a custom one is requested.</param>
        /// <param name="context">The current context.</param>
        private void CheckForCustomView(ref string viewName, ControllerContext context)
        {
            // Tell MVC to always use the desktop view because we will select it self.
            HttpContext.SetOverriddenBrowser(BrowserOverride.Desktop);

            // Get the current route if available. Necessary for caching.
            var route = context.RouteData.Route as Route;
            if (route == null)
            {
                return;
            }

            // Get custom view name if available.
            string parametersString;
            try
            {
                parametersString = Sitecore.Mvc.Presentation.RenderingContext.Current.Rendering["Parameters"];
            }
            catch (InvalidOperationException)
            {
                // The call to Sitecore.Mvc.Presentation.RenderingContext.Current cause the exception
                //      InvalidOperationException: Attempt to retrieve context object of type 'Sitecore.Mvc.Presentation.RenderingContext' from empty stack.
                // when used in a controller that is targeted by a route and not a rendering.
                return;
            }
            if (string.IsNullOrWhiteSpace(parametersString))
            {
                return;
            }
            NameValueCollection parameters = Sitecore.Web.WebUtil.ParseUrlParameters(parametersString);
            if (parameters[ViewParameterName] == null && parameters[VariantParameterName] == null)
            {
                return;
            }

            string customViewName = string.Concat(viewName, ".", parameters[VariantParameterName]);
            if (parameters[ViewParameterName] != null)
            {
                customViewName = parameters[ViewParameterName];
            }

            viewName = customViewName;
        }
    }
}
