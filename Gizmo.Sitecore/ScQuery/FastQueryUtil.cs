﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gizmo.SitecoreGizmo.ScQuery
{
    /// <summary>
    /// Utility functions for fast query usage.
    /// </summary>
    public static class FastQueryUtil
    {
        /// <summary>
        /// Escapes a path for fast query. 
        /// </summary>
        /// <param name="path"></param>
        /// <returns>The escaped path without leading or trailing slash.</returns>
        public static string EscapePath(string path)
        {
            path = path.Trim('/');
            var parts = path.Split('/');
            var result = string.Concat("#", string.Join("#/#", parts), "#");
            return result;
        }
    }
}
