﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;

namespace Gizmo.SitecoreGizmo.Items
{
    /// <summary>
    /// Gives access to the settings item of the current website.
    /// </summary>
    public class ScSettings
    {
        public Sitecore.Data.Items.Item ValueItem { get; private set; }

        private ScSettings()
        {
            // It doesn't make sense to execute this on core because there will never be any data. 
            // This only happens if a shell function is executed.
            var db = Sitecore.Context.Database;
            if (db.Name == "core")
            {
                db = Database.GetDatabase("master");
            }
            var relativePath = Sitecore.Configuration.Settings.GetSetting("Gizmo.Sitecore.ScSettings.RelativePath", "settings").Trim('/');
            var path = string.Concat(ScReference.InstancePath(), "/", relativePath);
            ValueItem = db.GetItem(path);
            Assert.IsNotNull(ValueItem, "ValueItem is null.");
        }

        /// <returns>An instance of this object.</returns>
        public static ScSettings Instance()
        {
            // No singleton support because we may have multiple instances with their own settings item in one sitecore installation.
            // If necessary we could use some caching.
            return new ScSettings();
        }

        /// <summary>
        /// Get the bool value of an item field.
        /// </summary>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public bool GetBool(string fieldKey)
        {
            return ScValue.GetBool(ValueItem, fieldKey);
        }

        /// <summary>
        /// Get a child of the settings object. Will throw an exception if the child does not exist.
        /// </summary>
        /// <param name="itemName">The childs item name.</param>
        /// <returns>The child.</returns>
        public Sitecore.Data.Items.Item GetChild(string itemName)
        {
            return ValueItem.Children.Where(x => x.Name == itemName).First();
        }

        /// <summary>
        /// Get the date value of an item field.
        /// </summary>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public DateTime GetDate(string fieldKey)
        {
            return ScValue.GetDate(ValueItem, fieldKey);
        }

        /// <summary>
        /// Get the double value of an item field.
        /// </summary>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public double GetDouble(string fieldKey)
        {
            return ScValue.GetDouble(ValueItem, fieldKey);
        }

        /// <summary>
        /// Get the file of the item field.
        /// </summary>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public Sitecore.Data.Items.MediaItem GetFile(string fieldKey)
        {
            return ScValue.GetFile(ValueItem, fieldKey);
        }

        /// <summary>
        /// Get the image of the item field.
        /// </summary>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public Sitecore.Data.Items.MediaItem GetImage(string fieldKey)
        {
            return ScValue.GetImage(ValueItem, fieldKey);
        }

        /// <summary>
        /// Get the integer value of an item field.
        /// </summary>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public int GetInteger(string fieldKey)
        {
            return ScValue.GetInteger(ValueItem, fieldKey);
        }

        /// <summary>
        /// Get the item value of an item field.
        /// </summary>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public Sitecore.Data.Items.Item GetItem(string fieldKey)
        {
            return ScValue.GetItem(ValueItem, fieldKey);
        }

        /// <summary>
        /// Get the item values of an item field.
        /// </summary>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public IEnumerable<Sitecore.Data.Items.Item> GetItems(string fieldKey)
        {
            return ScValue.GetItems(ValueItem, fieldKey);
        }

        /// <summary>
        /// Get the link value of an item field.
        /// </summary>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public LinkField GetLink(string fieldKey)
        {
            return ScValue.GetLink(ValueItem, fieldKey);
        }

        /// <summary>
        /// Get the string value of an item field.
        /// </summary>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public string GetString(string fieldKey)
        {
            return ScValue.GetString(ValueItem, fieldKey);
        }

        /// <summary>
        /// Get the string value of an item field.
        /// </summary>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public IEnumerable<string> GetStrings(string fieldKey)
        {
            return ScValue.GetStrings(ValueItem, fieldKey);
        }
    }
}