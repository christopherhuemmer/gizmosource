﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Layouts;
using System.Collections;

namespace Gizmo.SitecoreGizmo.Items
{
    /// <summary>
    /// Layout related helper.
    /// </summary>
    public static class ScLayout
    {
        /// <summary>
        /// Get final renderings of given page.
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static IEnumerable<RenderingDefinition> FinalRenderings(Item page)
        {
            var result = Renderings(page, Sitecore.FieldIDs.FinalLayoutField);
            return result;
        }

        /// <summary>
        /// Set final renderings of given page. This will override existing renderings.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="renderings"></param>
        public static void SetFinalRenderings(Item page, IEnumerable<RenderingDefinition> renderings)
        {
            SetRenderings(page, Sitecore.FieldIDs.FinalLayoutField, renderings);
        }

        /// <summary>
        /// Get shared renderings of given page.
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static IEnumerable<RenderingDefinition> SharedRenderings(Item page)
        {
            var result = Renderings(page, Sitecore.FieldIDs.LayoutField);
            return result;
        }

        /// <summary>
        /// Set shared renderings of given page. This will override existing renderings.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="renderings"></param>
        public static void SetSharedRenderings(Item page, IEnumerable<RenderingDefinition> renderings)
        {
            SetRenderings(page, Sitecore.FieldIDs.LayoutField, renderings);
        }

        /// <summary>
        /// Get renderings of given page.
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static IEnumerable<RenderingDefinition> Renderings(Item page, ID fieldId)
        {
            //Get the value of the renderings field
            string renderingXml = LayoutField.GetFieldValue(page.Fields[fieldId]);

            //Create a LayoutDefinition and load in the XML
            LayoutDefinition layoutDefinition = LayoutDefinition.Parse(renderingXml);

            //Get the default device on the LayoutDefinition
            var deviceDefinition = layoutDefinition.GetDevice(ScReference.Instance().GetItem("DefaultDevice").ID.Guid.ToString("B").ToUpper());

            //AWI 29.05.2016
            //Renderings can be null?
            if(deviceDefinition.Renderings == null)
            {
                return Enumerable.Empty<RenderingDefinition>();
            }
            return deviceDefinition.Renderings.Cast<RenderingDefinition>();
        }

        /// <summary>
        /// Set renderings of given page. This will override existing renderings.
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static void SetRenderings(Item page, ID fieldId, IEnumerable<RenderingDefinition> renderings)
        {
            //Get the value of the renderings field
            string renderingXml = LayoutField.GetFieldValue(page.Fields[fieldId]);

            //Create a LayoutDefinition and load in the XML
            LayoutDefinition layoutDefinition = LayoutDefinition.Parse(renderingXml);

            //Get the default device on the LayoutDefinition
            var deviceDefinition = layoutDefinition.GetDevice(ScReference.Instance().GetItem("DefaultDevice").ID.Guid.ToString("B").ToUpper());

            // Set renderings.
            var renderingArray = new ArrayList();
            foreach (var rendering in renderings)
            {
                renderingArray.Add(rendering);
            }
            deviceDefinition.Renderings = renderingArray;
            using (new EditContext(page))
            {
                var newRenderingXml = layoutDefinition.ToXml();
                page[fieldId] = newRenderingXml;
            }
        }
    }
}
