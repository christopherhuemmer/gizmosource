﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Gizmo.General.String;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;

namespace Gizmo.SitecoreGizmo.Items
{
    /// <summary>
    /// Value retrieving related helper functions. 
    /// </summary>
    /// <see cref="http://seankearney.com/post/Getting-a-Sitecore-field-value"/>
    public static class ScValue
    {
        /// <summary>
        /// Get the bool value of an item field. Use with "Checkbox" fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public static bool GetBool(Sitecore.Data.Items.Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "Checkbox" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            CheckboxField field = item.Fields[fieldKey];
            return field.Checked;
        }

        /// <summary>
        /// Get the date value of an item field. Use with "DateTime" fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public static DateTime GetDate(Sitecore.Data.Items.Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "Date", "DateTime" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            DateField field = item.Fields[fieldKey];
            return field.DateTime.ToLocalTime();
        }

        /// <summary>
        /// Get the double value of an item field. Use with "Number" fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public static double GetDouble(Sitecore.Data.Items.Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "Number" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            var stringValue = GetString(item, fieldKey);
            double result = 0;
            if (!string.IsNullOrEmpty(stringValue))
            {
                result = double.Parse(stringValue, CultureInfo.InvariantCulture);
            }
            return result;
        }

        /// <summary>
        /// Get the file of the item field. Use with "File"
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public static MediaItem GetFile(Sitecore.Data.Items.Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "File" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            FileField field = item.Fields[fieldKey];
            MediaItem result = null;
            if (field.MediaItem != null)
            {
                result = new MediaItem(field.MediaItem);
            }
            return result;
        }

        /// <summary>
        /// Get html of a rich text field.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public static HtmlString GetHtml(Sitecore.Data.Items.Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] {"Single-Line Text", "Multi-Line Text", "Rich Text" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            return new HtmlString(item.Fields[fieldKey].Value); 
        }

        /// <summary>
        /// Get the image of the item field. Use with "Image"
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public static MediaItem GetImage(Sitecore.Data.Items.Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "Image" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            ImageField field = item.Fields[fieldKey];
            MediaItem result = null;
            if (field.MediaItem != null)
            {
                result = new MediaItem(field.MediaItem);
            }
            return result;
        }

        /// <summary>
        /// Get the integer value of an item field. Use with "Integer" fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public static int GetInteger(Sitecore.Data.Items.Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "Integer" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            var stringValue = GetString(item, fieldKey);
            int result = 0;
            if (!string.IsNullOrEmpty(stringValue))
            {
                result = int.Parse(stringValue);
            }
            return result;
        }

        /// <summary>
        /// Get the item value of an item field. Use with "DropLink" fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public static Sitecore.Data.Items.Item GetItem(Sitecore.Data.Items.Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "DropLink", "DropTree" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            ReferenceField field = item.Fields[fieldKey];
            return field.TargetItem;
        }

        /// <summary>
        /// Get the item values of an item field. Use with "MultiList" fields which references items.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public static IEnumerable<Sitecore.Data.Items.Item> GetItems(Sitecore.Data.Items.Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "Multilist", "Treelist", "TreelistEx" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            MultilistField field = item.Fields[fieldKey];
            IList<Sitecore.Data.Items.Item> result = new List<Sitecore.Data.Items.Item>();
            foreach (ID id in field.TargetIDs)
            {
                var foundItem = item.Database.Items[id];
                if (foundItem != null)
                {
                    result.Add(foundItem);
                }
            }
            return result;
        }

        /// <summary>
        /// Get the link value of an item field. Use with "GeneralLink" fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public static LinkField GetLink(Sitecore.Data.Items.Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "General Link" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            return (LinkField)item.Fields[fieldKey];
        }

        /// <summary>
        /// Get the string value of an item field. Use with text (Single-Line Text, Multi-Line Text, Droplist) fields.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public static string GetString(Sitecore.Data.Items.Item item, string fieldKey)
        {
            // Validate input.
            ValidateField(item, fieldKey);

            // Everything is ok.
            return item.Fields[fieldKey].Value; // Same as item[fieldKey].
        }

        /// <summary>
        /// Get the string value of an item field. Use with "MultiList" fields which references items.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>The value.</returns>
        public static IEnumerable<string> GetStrings(Sitecore.Data.Items.Item item, string fieldKey, params string[] fieldTypes)
        {
            // Validate input.
            if (!fieldTypes.Any())
            {
                fieldTypes = new string[] { "Multilist", "Treelist", "TreelistEx" };
            }
            ValidateField(item, fieldKey, fieldTypes);

            // Everything is ok.
            MultilistField field = item.Fields[fieldKey];
            Item tmpItem;
            IList<string> result = new List<string>();

            foreach (ID id in field.TargetIDs)
            {
                tmpItem = item.Database.Items[id];
                if (tmpItem != null)
                {
                    result.Add(tmpItem.DisplayName);
                }
                else
                {
                    //NOP
                    //AWI 2016.04.15 
                    //How can this happen? Had null items...
                }
            }
            
            return result;
        }

        /// <summary>
        /// Check if an item has a certain field.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <returns>True if the item has given field.</returns>
        public static bool Has(Sitecore.Data.Items.Item item, string fieldKey)
        {
            // Validate input.
            ValidateItemKey(item, fieldKey);

            // Everything is ok.
            return item.Fields[fieldKey] != null;
        }

        /// <summary>
        /// Validate the item and check if the key is not null.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        private static void ValidateItemKey(Sitecore.Data.Items.Item item, string fieldKey)
        {
            // Is the item null?
            Assert.IsNotNull(item, "Item is null.");

            // Is the field key null or empty?
            Assert.IsNotNullOrEmpty(fieldKey, "FieldKey is null or empty. Item was \"" + item.Name + "\" with template \"" + item.TemplateName + "\".");

            // Does the item have fields?
            Assert.IsNotNull(item.Fields, "Item.Fields is null. Item was \"" + item.Name + "\" with template \"" + item.TemplateName + "\".");
        }

        /// <summary>
        /// Validate item field.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="fieldKey">The key of the field.</param>
        /// <param name="fieldType">Optional parameter. Validate field type.</param>
        private static void ValidateField(Sitecore.Data.Items.Item item, string fieldKey, params string[] fieldType)
        {
            // Validate the item and the given field key.
            ValidateItemKey(item, fieldKey);

            // Does the field exist?
            Assert.IsNotNull(item.Fields[fieldKey],
                "Item.Fields[" + fieldKey + "] is null. " +
                "Most likely the template is not up to date in the current language. " +
                "Item was \"" + item.Name + "\" with template \"" + item.TemplateName + "\".");

            // Does the field have one of the expected types?
            Assert.IsTrue(
                !fieldType.Any() || fieldType.Any(x => StringUtil.AreEqual(item.Fields[fieldKey].Type, x, true)),
                "Not supported field type " + item.Fields[fieldKey].Type + "." +
                "Expected " + string.Join(",",fieldType) + ". Item was \"" + item.Name + "\" with template \"" + item.TemplateName + "\"."
            );
        }
    }
}