﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Web;

namespace Gizmo.SitecoreGizmo.Modules
{
    /// <summary>
    /// Language provider with switch support based on "languageProvider" attribute of site.
    /// </summary>
    /// <see cref="http://maigruen.netzkern.de/linkmanager_for_multisite_sitecore.aspx"/>
    public class SwitchLanguageProvider : Sitecore.Links.LinkProvider
    {
        public override bool AddAspxExtension
        {
            get { return GetContextProvider().AddAspxExtension; }
        }

        public override bool AlwaysIncludeServerUrl
        {
            get { return GetContextProvider().AlwaysIncludeServerUrl; }
        }

        public override bool EncodeNames
        {
            get { return GetContextProvider().EncodeNames; }
        }

        public override LanguageEmbedding LanguageEmbedding
        {
            get { return GetContextProvider().LanguageEmbedding; }
        }

        public override LanguageLocation LanguageLocation
        {
            get { return GetContextProvider().LanguageLocation; }
        }

        public override bool LowercaseUrls
        {
            get { return GetContextProvider().LowercaseUrls; }
        }

        public override bool ShortenUrls
        {
            get { return GetContextProvider().ShortenUrls; }
        }

        public override bool UseDisplayName
        {
            get { return GetContextProvider().UseDisplayName; }
        }

        public override string ExpandDynamicLinks(string text, bool resolveSites)
        {
            return GetContextProvider().ExpandDynamicLinks(text, resolveSites);
        }

        public override UrlOptions GetDefaultUrlOptions()
        {
            return GetContextProvider().GetDefaultUrlOptions();
        }

        public override string GetDynamicUrl(Item item, LinkUrlOptions options)
        {
            return GetContextProvider().GetDynamicUrl(item, options);
        }

        public override string GetItemUrl(Item item, UrlOptions options)
        {
            return GetContextProvider().GetItemUrl(item, options);
        }

        public override bool IsDynamicLink(string linkText)
        {
            return GetContextProvider().IsDynamicLink(linkText);
        }

        public override DynamicLink ParseDynamicLink(string linkText)
        {
            return GetContextProvider().ParseDynamicLink(linkText);
        }

        public override RequestUrl ParseRequestUrl(HttpRequest request)
        {
            return GetContextProvider().ParseRequestUrl(request);
        }

        /// <summary>
        /// Get language provider based on site attribute "languageProvider".
        /// </summary>
        /// <returns></returns>
        private LinkProvider GetContextProvider()
        {
            // Default is always the original sitecore provider
            LinkProvider result = LinkManager.Providers["sitecore"];

            if (Context.Site != null)
            {
                // Custom provider configured and existing?
                String providerName = Context.Site.Properties["languageProvider"];
                if (!string.IsNullOrEmpty(providerName) && LinkManager.Providers[providerName] != null)
                {
                    result = LinkManager.Providers[providerName];
                }
            }

            return result;
        }
    }
}