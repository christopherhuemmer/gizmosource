﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Jobs;

namespace Gizmo.SitecoreGizmo.Wrappers
{
    public abstract class ScJob
    {
        public ScJob()
        {
            Afterlife = TimeSpan.FromMinutes(10);
            EnableSecurity = true;
        }

        string _jobName = null;
        public string JobName
        {
            get { return _jobName ?? "Job"; }
            set { _jobName = value; }
        }


        string _jobCategory = null;
        public string JobCategory
        {
            get { return _jobCategory ?? this.GetType().Name; }
            set { _jobCategory = value; }
        }

        string _jobSiteName;
        public string JobSiteName
        {
            get { return _jobSiteName ?? (Sitecore.Context.Site == null ? "No Site Context" : Sitecore.Context.Site.Name); }
            set { _jobSiteName = value; }
        }

        private int _totalWorkUnits = -1;
        protected int TotalWorkUnits
        {
            get
            {
                return _totalWorkUnits;
            }
            set
            {
                _totalWorkUnits = value;
                if (Sitecore.Context.Job != null)
                {
                    Sitecore.Context.Job.Status.Total = _totalWorkUnits;
                }
            }
        }

        private int _currentWorkUnit = -1;
        protected int CurrentWorkUnit
        {
            get
            {
                return _currentWorkUnit;
            }
            set
            {
                _currentWorkUnit = value;
                if (Sitecore.Context.Job != null)
                {
                    Sitecore.Context.Job.Status.Processed = _currentWorkUnit > _totalWorkUnits ? _totalWorkUnits : _currentWorkUnit;
                }
            }
        }

        public void AddStatusMessage(string message)
        {
            if (Sitecore.Context.Job != null)
            {
                Sitecore.Context.Job.Status.Messages.Add(message);
            }
        }

        public TimeSpan Afterlife { get; set; }
        public bool EnableSecurity { get; set; }

        public bool AtomicExecution { get; set; }
        public void StartAsync(bool onlyIfnotRunning = false)
        {
            if(onlyIfnotRunning)
            {
                var runningJob = JobManager.GetJobs().Where(job => job.IsDone == false && job.Name == JobName && job.Category == JobCategory).FirstOrDefault();
                if(runningJob != null)
                {
                    Sitecore.Diagnostics.Log.Info(string.Concat("Skipped ", JobCategory, " ", JobName, " - Job is already running."), this);
                    return;
                }
            }
            var jobOptions = new DefaultJobOptions(JobName, JobCategory, JobSiteName, this, "Start");
            jobOptions.AfterLife = Afterlife;
            jobOptions.EnableSecurity = EnableSecurity;
            jobOptions.AtomicExecution = AtomicExecution;
            JobManager.Start(jobOptions);
        }

        
        public abstract void Start();
       
    }
}
