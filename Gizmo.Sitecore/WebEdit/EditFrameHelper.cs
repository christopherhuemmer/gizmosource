﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
using Sitecore.Web.UI.WebControls;

namespace Gizmo.SitecoreGizmo.WebEdit
{
    /// <summary>
    ///  Implements edit frames for MVC. Difference to the origianl version by Julia Gavrilova is that
    ///  "buttons" doesn't contain the full path but the name of the button folder in the buttons root folder.
    /// </summary>
    /// <see cref="http://sitecoreexperiences.blogspot.de/2013/05/sitecore-66-mvc-editframe-helper.html"/>
    public static class EditorFrameHelper
    {
        public const string DefaultButtonsRoot = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/";

        public static EditFrame EditFrameControl;

        private class FrameEditor : IDisposable
        {
            private bool disposed;
            private HtmlHelper html;

            /// <summary>
            /// Create new edit frame (for example with "using(...)").
            /// </summary>
            /// <param name="html">The html object.</param>
            /// <param name="dataSource">The datasource.</param>
            /// <param name="buttons">The buttons folder.</param>
            /// <param name="buttonsRoot">The root folder.</param>
            public FrameEditor(HtmlHelper html, string dataSource = null, string buttons = null, string buttonsRoot = DefaultButtonsRoot)
            {
                this.html = html;

                EditorFrameHelper.EditFrameControl = new EditFrame
                {
                    DataSource = dataSource ?? "/sitecore/content/home",
                    Buttons = buttonsRoot + (buttons ?? "Default"),
                    Title = buttons ?? "Default"
                };
                HtmlTextWriter output = new HtmlTextWriter(html.ViewContext.Writer);
                EditorFrameHelper.EditFrameControl.RenderFirstPart(output);
            }

            /// <summary>
            /// Dispose of edit frame.
            /// </summary>
            public void Dispose()
            {
                if (disposed)
                {
                    return;
                }

                disposed = true;

                EditorFrameHelper.EditFrameControl.RenderLastPart(new HtmlTextWriter(html.ViewContext.Writer));
                EditorFrameHelper.EditFrameControl.Dispose();
            }
        }

        /// <summary>
        /// Create new edit frame (for example with "using(...)"). Implemented as an extension method for Html.
        /// </summary>
        /// <param name="html">The html object.</param>
        /// <param name="dataSource">The datasource. It can a path to an item or the item id. The later is recommended because it is unique.</param>
        /// <param name="buttons">The buttons folder.</param>
        /// <param name="buttonsRoot">The root folder.</param>
        /// <returns>The edit frame.</returns>
        public static IDisposable EditFrame(this HtmlHelper html, string dataSource = null, string buttons = null, string buttonsRoot = DefaultButtonsRoot)
        {
            return new FrameEditor(html, dataSource, buttons, buttonsRoot);
        }

        /// <summary>
        /// Create new edit frame (for example with "using(...)"). Implemented as an extension method for Html.
        /// </summary>
        /// <param name="html">The html object.</param>
        /// <param name="dataSource">The datasource item.</param>
        /// <param name="buttons">The buttons folder.</param>
        /// <param name="buttonsRoot">The root folder.</param>
        /// <returns>The edit frame.</returns>
        public static IDisposable EditFrame(this HtmlHelper html, Sitecore.Data.Items.Item dataSource = null, string buttons = null, string buttonsRoot = DefaultButtonsRoot)
        {
            return new FrameEditor(html, dataSource.ID.ToGuid().ToString(), buttons, buttonsRoot);
        }
    }
}
