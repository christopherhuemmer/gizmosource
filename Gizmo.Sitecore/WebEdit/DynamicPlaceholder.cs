﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;

namespace Gizmo.SitecoreGizmo.WebEdit
{
    /// <summary>
    /// Adds support for dynamic placeholders to sitecore. If you use this function also add the modules from Gizmo.SitecoreGizmo.Modules.DynamicPlaceholder to your config.
    /// </summary>
    /// <see cref="http://stackoverflow.com/a/15135796/1931663"/>
    public static class DynamicPlaceholderHelper
    {
        /// <summary>
        /// Add an dynamic placeholder. If you use the same placeholder within the same rendering multiple times use the other version of this function.
        /// </summary>
        /// <param name="helper">The sitecore helper object.</param>
        /// <param name="placeholder">The placeholder name.</param>
        /// <param name="suffix">The suffix name in case if you need 2 or more placeholders in the same rendering with the same name</param>
        /// <returns>The placeholder.</returns>
        public static HtmlString LegacyDynamicPlaceholder(this Sitecore.Mvc.Helpers.SitecoreHelper helper, string placeholder, string suffix = null)
        {
            var newPlaceholder = string.Format("{0}_{1}", placeholder, RenderingContext.Current.Rendering.UniqueId);
            if (!string.IsNullOrWhiteSpace(suffix)) newPlaceholder += "_" + suffix;
            return helper.Placeholder(newPlaceholder);
        }

        /// <summary>
        /// Add an dynamic placeholder. This variant uses a reference item to generate dynamic placeholder within the same rendering item.
        /// </summary>
        /// <param name="helper">The sitecore helper object.</param>
        /// <param name="placeholder">The placeholder name.</param>
        /// <param name="referenceItem">The reference item for this placeholder.</param>
        /// <returns>The placeholder.</returns>
        public static HtmlString LegacyDynamicPlaceholder(this Sitecore.Mvc.Helpers.SitecoreHelper helper, string placeholder, Item referenceItem)
        {
            var id = referenceItem.ID.Guid;
            return helper.Placeholder(string.Format("{0}_{1}", placeholder, id));
        }

        /// <summary>
        /// Add an dynamic placeholder. This variant uses a manual set id. Use only if no other option is available.
        /// </summary>
        /// <param name="helper">The sitecore helper object.</param>
        /// <param name="placeholder">The placeholder name.</param>
        /// <param name="dynamicId">A manual set dynamic id if no item is available.</param>
        /// <returns>The placeholder.</returns>
        public static HtmlString LegacyDynamicPlaceholder(this Sitecore.Mvc.Helpers.SitecoreHelper helper, string placeholder, Guid dynamicId)
        {
            return helper.Placeholder(string.Format("{0}_{1}", placeholder, dynamicId));
        }
    }
}
