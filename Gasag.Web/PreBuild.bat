le:: Parameters
SET SolutionDir=%~1
SET ProjectDir=%~2
SET AssetsDir=%~3
SET PublishDir="C:/Inetpub/wwwroot/gasag/"

:: delete this file to avoid publishing errors
del "%ProjectDir%Properties\PublishProfiles\*.user"
:: Version Number of DLL
SubWCRev "%SolutionDir% " "%ProjectDir%Properties\AssemblyInfo.template.cs " "%ProjectDir%Properties\AssemblyInfo.cs "

:: Assets
robocopy "%SolutionDir%../html/dist/%AssetsDir%/css" "%ProjectDir%css" /mir
robocopy "%SolutionDir%../html/dist/%AssetsDir%/fonts" "%ProjectDir%fonts" /mir
robocopy "%SolutionDir%../html/dist/%AssetsDir%/img" "%ProjectDir%img" /mir
robocopy "%ProjectDir%img-extra" "%ProjectDir%img/layout"
robocopy "%SolutionDir%../html/dist/%AssetsDir%/js" "%ProjectDir%js" /mir
SubWCRev "%SolutionDir% " "%ProjectDir%App_Config\Include\ZZZ.Gasag\Assets.config.template " "%ProjectDir%App_Config\Include\ZZZ.Gasag\Assets.config "