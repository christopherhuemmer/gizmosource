﻿using Gizmo.General.Debug;
using Sitecore.Mvc.Presentation;
using System;
using System.Reflection;
using System.Web.Mvc;

namespace Gasag.Web.Attributes
{
    public class ValidateFormHandlerAttribute : ActionMethodSelectorAttribute
    {
        public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
        {
            var rendering = RenderingContext.CurrentOrNull;
            if (rendering == null)
            {
                Log.Debug($"ValidateFormHandler: Rendering is null");
                return false;
            }
            var action = controllerContext.HttpContext.Request.Form["gsAction"];
            if (string.IsNullOrWhiteSpace(action))
            {
                Log.Debug($"ValidateFormHandler: No action provided.");
                return false;
            }

            if (!action.Equals(methodInfo.Name, StringComparison.InvariantCultureIgnoreCase))
            {
                Log.Debug($"ValidateFormHandler: method != action. method = {methodInfo.Name}, action = {action}");
                return false;
            }

            var formhandleruid = controllerContext.HttpContext.Request.Form["formhandleruid"];
            Guid postedId;
            if (!Guid.TryParse(formhandleruid, out postedId))
            {
                Log.Debug($"ValidateFormHandler: Wasn't able to parse form handler uid. formhandleruid = {formhandleruid}");
                return false;
            }

            if (!postedId.Equals(rendering.Rendering.UniqueId))
            {
                Log.Debug($"ValidateFormHandler: form handler uid != rendering id. formhandleruid = {formhandleruid}, rendering id = {rendering.Rendering.UniqueId}");
                return false;
            }

            return true;
        }
    }
}