<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output standalone="yes" method="text" />
  <xsl:param name="date" />
  <xsl:param name="headline" />
  <xsl:template match="/">
    <xsl:value-of select="$date"/>
    <xsl:text>&#xD;&#xA;</xsl:text>
    <xsl:value-of select="$headline"/>

    <xsl:for-each select="items/item">
      <xsl:text>&#xD;&#xA;</xsl:text>
      <xsl:choose>
        <xsl:when test="@valuename != '' "><xsl:value-of select="@valuename"/>: </xsl:when>
		<xsl:otherwise><xsl:value-of select="@name"/>: </xsl:otherwise>
      </xsl:choose>
      <xsl:choose>
        <xsl:when test="@name = 'feedback' "><xsl:value-of select="@name"/>: 
            <xsl:for-each select="value"><xsl:text>&#xD;&#xA;</xsl:text><xsl:value-of select="."/></xsl:for-each></xsl:when>
        <xsl:otherwise>
          <xsl:for-each select="value">
            <xsl:value-of select="."/><xsl:choose><xsl:when test="position() != last()">, </xsl:when>
            </xsl:choose>
          </xsl:for-each>
        </xsl:otherwise>
      </xsl:choose>

    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>



