﻿using Gasag.Web.Utility;
using Gizmo.General.Debug;
using Gizmo.SitecoreGizmo.Modules;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Gasag.Web
{
    public class MvcApplication : Sitecore.Web.Application
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            // GASAG-37606 Blog %20 in url on live.
            if (Instance.IsWeb && HttpContext.Current != null && 
                !HttpContext.Current.Request.Url.AbsolutePath.Contains(".") && HttpContext.Current.Request.Url.AbsolutePath.Contains("%20"))
            {
                CustomErrorExecuteRequest.DoErrorRedirect(HttpContext.Current.Request.Url.ToString());
                HttpContext.Current.Response.Flush();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }
    }
}
