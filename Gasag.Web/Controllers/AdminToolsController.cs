﻿using Gasag.Web.Utility;
using Gizmo.SitecoreGizmo.Items;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Gasag.Web.Controllers
{
    public class AdminToolsController : Controller
    {
        public ActionResult FindPagesWithEmptyBodyId()
        {
            var pages = ScReference.GetItemRelativeToInstance("Home").Axes.GetDescendants()
                .Where(x => ScUtil.AllPageTemplateIds().Contains(x.TemplateID))
                .Where(x => ScValue.GetItem(x, "Body Id") == null)
                .Select(x => x.Paths.FullPath);
            
            return Content(string.Join(Environment.NewLine, pages), "text/plain");
        }

        public ActionResult FindPagesWithOldReference()
        {
            var startPath = Sitecore.Context.Site.RootPath.ToLower().TrimEnd('/') + '/';

            var pages = ScReference.GetItemRelativeToInstance("Home").Axes.GetDescendants()
                .Where(x => ScUtil.AllPageTemplateIds().Contains(x.TemplateID))
                .Where(x => 
                    ScLayout.FinalRenderings(x)
                        .Where(y => !string.IsNullOrWhiteSpace(y.Datasource))
                        .Select(y => x.Database.GetItem(new ID(y.Datasource)))
                        .Any(y => y != null && !y.Paths.FullPath.ToLower().StartsWith(startPath))
                )
                .Select(x => x.Paths.FullPath);
            
            return Content(string.Join(Environment.NewLine, pages), "text/plain");
        }
    }
}