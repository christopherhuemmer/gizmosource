﻿using Sitecore.Data.Items;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.ContentSearch.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Linq;
using Gizmo.SitecoreGizmo.Items;
using System.Collections.Generic;
using Gasag.Web.Models.Search;
using Gasag.Web.Utility;
using Newtonsoft.Json.Serialization;
using Sitecore.Data;
using Sitecore.Links;
using System.Text.RegularExpressions;
using Sitecore.Search;
using System.Dynamic;
using Gizmo.General.Debug;
using Sitecore.ContentSearch.LuceneProvider;
using Lucene.Net.Index;
using Lucene.Net.Search;
using static Lucene.Net.Index.IndexReader;

namespace Gasag.Web.Controllers
{
    public class SearchController : Controller
    {
        private static JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), NullValueHandling = NullValueHandling.Ignore };

        private const string searchReplacer = "<span class=\"search-text\">{0}</span>";

        private UrlOptions _urlOptions;
        private UrlOptions UrlOptions
        {
            get
            {
                if (_urlOptions == null)
                {
                    _urlOptions = UrlOptions.DefaultOptions;
                    _urlOptions.Language = Sitecore.Context.Language;
                    if (Sitecore.Context.Site.Language == _urlOptions.Language.Name)
                    {
                        _urlOptions.LanguageEmbedding = LanguageEmbedding.Never;
                    }
                }
                return _urlOptions;
            }
        }

        private int PageSize
        {
            get
            {
                int pageSize = ScSettings.Instance().GetInteger("Search Page Size");
                if (pageSize < 1) pageSize = 10;
                return pageSize;
            }
        }

        private int PreviewMaxSize
        {
            get
            {
                int maxSize = ScSettings.Instance().GetInteger("Search Preview Max Size");
                if (maxSize < 1) maxSize = 200;
                return maxSize;
            }
        }

        private Item CategoriesFolder
        {
            get { return ScReference.GetItemRelativeToInstance("Categories Folder"); }
        }

        private Item SiteRootItem
        {
            get { return Sitecore.Context.Database.GetItem(Sitecore.Context.Site.RootPath); }
        }

        public ActionResult AutoComplete(string query)
        {
            var searchUrl = string.Concat(
                ScLink.GetUrl(ScSettings.Instance().GetItem("Search Page Url")),
                "?query={0}"
            );

            var terms = new List<string>();
            IndexReader ir = ((LuceneIndex)GetSearchIndex()).CreateReader(LuceneIndexAccess.ReadOnly);
            WildcardTermEnum wte = new WildcardTermEnum(ir, new Term("pageitemcontent", query + "*"));
            while (wte.Next())
            {
                Term term = wte.Term;
                terms.Add(term.Text);                
            }
            var rx = new Regex("[^a-zA-ZäöüÄÖÜß0-9 ]");
            terms = terms.Select(x => rx.Replace(x, " ").Split(' ').First()).Distinct().ToList();

            var result = new List<ExpandoObject>();
            foreach (var term in terms)
            {
                dynamic autocompleteTerm = new ExpandoObject();
                autocompleteTerm.url = string.Format(searchUrl, term);
                autocompleteTerm.title = term;
                result.Add(autocompleteTerm);
            }

            if (!result.Any())
            {
                // Temporary Workaround: Fake autocomplete and simply return input.
                dynamic autocompleteTerm = new ExpandoObject();
                autocompleteTerm.url = string.Format(searchUrl, query);
                autocompleteTerm.title = query;
                result.Add(autocompleteTerm);
            }

            var json = JsonConvert.SerializeObject(result);
            return Content(json, "application/json; charset=utf-8");
        }

        public ActionResult Search(string query, int page = 0)
        {
            //Log.Debug($"SearchController.Search(): Search query {query} and page {page}");

            return Content(JsonConvert.SerializeObject(FetchItems(query, page), jsonSerializerSettings), "application/json; charset=utf-8");
        }

        public ActionResult SearchItemsInCategory(string category, string query, int page = 0)
        {
            //Log.Debug($"SearchController.SearchItemsInCategory(): Search query {query} and page {page} in category {category}");

            return Content(JsonConvert.SerializeObject(GetItemsInCategory(CategoriesFolder.Children.First(x => x.Key == category), query, page), jsonSerializerSettings), "application/json");
        }

        private ISearchIndex GetSearchIndex()
        {
            return ContentSearchManager.GetIndex(ScSettings.Instance().GetString("Search Index"));
        }

        private IProviderSearchContext GetSearchContext()
        {
            return GetSearchIndex().CreateSearchContext();
        }

        private IDictionary<string, GasagSearchResultCategory> FetchItems(string query, int pageIndex)
        {
            //Log.Debug($"SearchController.FetchItems(): Search query {query} and page {pageIndex}");

            var res = new Dictionary<string, GasagSearchResultCategory>();
            foreach (Item category in CategoriesFolder.Children)
            {
                //Log.Debug($"SearchController.FetchItems(): Search in category {category.Name}");

                if (!ScValue.GetBool(category, "Inactive"))
                {
                    string key = category["Alternative key"];
                    if (string.IsNullOrWhiteSpace(key))
                    {
                        key = category["Key"];
                    }
                    res.Add(key, string.IsNullOrWhiteSpace(query) ? new GasagSearchResultCategory { TotalResults = 0 } : GetItemsInCategory(category, query, pageIndex));
                }
            }
            return res;
        }

        private GasagSearchResultCategory GetItemsInCategory(Item categoryItem, string query, int pageIndex)
        {
            var rx = new Regex("[^a-zA-ZäöüÄÖÜß0-9]");
            query = rx.Replace(query, "*");

            string categoryValue = categoryItem["Value"];
            string categoryKey = categoryItem["Key"];
            using (var context = GetSearchContext())
            {
                var results = context.GetQueryable<CustomSearchResultItem>(new CultureExecutionContext(Sitecore.Context.Language.CultureInfo))
                    .Where(x => 
                        x.ShowInSearch == "ShowInSearch" && 
                        x.PageItemCategory == categoryKey.Replace(" ", "_").Replace("-", "_").ToLowerInvariant() && 
                        x.Paths.Contains(SiteRootItem.ID) &&
                        x.PageItemContent.Contains(query)
                    )
                    .GetResults();

                //Log.Debug($"SearchController.GetItemsInCategory(): Found {results.Count()} for category {categoryItem.DisplayName} and query {query}");

                var selectedItems = results.Skip(PageSize * pageIndex).Take(PageSize).Select(x => ConvertToGasagSearchResultItem(x, query, categoryValue)).ToList();
                return new GasagSearchResultCategory
                {
                    LoadNextUrl = (selectedItems == null || selectedItems.Count() < PageSize) 
                    ? null 
                    : ScUtil.ServiceUrl(
                        "Search", 
                        "SearchItemsInCategory", 
                        true, 
                        new Dictionary<string, string>() { { "query", query }, { "category", categoryItem["Key"] }, { "page", (pageIndex + 1).ToString() } }
                    ),
                    TotalResults = results.Count(),
                    Results = selectedItems
                };
            }
        }

        private GasagSearchResultItem ConvertToGasagSearchResultItem(SearchHit<CustomSearchResultItem> searchHit, string query, string category)
        {
            var item = searchHit.Document.GetItem();
            string url = "#";
            string title = "[undefined]";
            if (item != null && item.Visualization != null && item.Visualization.Layout != null)
            {
                title = item["Title"];
                if (string.IsNullOrWhiteSpace(title)) title = item.DisplayName;
                url = LinkManager.GetItemUrl(item, UrlOptions);
            }
            else
            {
                var r = Sitecore.Globals.LinkDatabase.GetItemReferrers(searchHit.Document.GetItem(), false);
                if (r != null && r.Any())
                {
                    var rItem = Sitecore.Context.Database.GetItem(r[0].SourceItemID);
                    title = rItem["Title"];
                    if (string.IsNullOrWhiteSpace(title)) title = rItem.DisplayName;
                    url = LinkManager.GetItemUrl(rItem, UrlOptions);
                }
            }

            return new GasagSearchResultItem
            {
                Title = FormatSearchString(title, query), //title should be change to actual document title
                Category = category,
                Url = url,
                Description = FormatSearchString(searchHit.Document.PageItemContent ?? "[empty content]", query),
                Date = searchHit.Document.Updated.ToLocalTime().ToShortDateString(),
            };
        }

        private string FormatSearchString(string text, string query)
        {
            query = string.Join("[\\s\\-_]", query.Split(new char[] { ' ', '-', '_', '?', '*', '.', ',', ';' }));
            var mc = Regex.Matches(text, query, RegexOptions.IgnoreCase);
            if (mc.Count > 0)
            {
                var matches = new Match[mc.Count];
                mc.CopyTo(matches, 0);
                var idx = matches.Where(x => x.Success).Select(x => x.Index).Min();
                if (idx >= 0 && idx < text.Length)
                {
                    int start = idx - PreviewMaxSize / 2;
                    int end = start + PreviewMaxSize;
                    if (start < 0)
                    {
                        start = 0;
                        end = PreviewMaxSize < text.Length ? PreviewMaxSize : text.Length - 1;
                    }
                    if (end >= text.Length)
                    {
                        end = text.Length - 1;
                        start = end - PreviewMaxSize;
                        if (start < 0) start = 0;
                    }

                    while (start > 0 && char.IsLetterOrDigit(text[start])) start--;
                    while (end < text.Length - 1 && char.IsLetterOrDigit(text[end])) end++;

                    string result = string.Empty;
                    if (start > 0) result += "... ";
                    result += text.Substring(start, end - start + 1);
                    if (end < text.Length - 1) result += " ...";

                    var mcInternal = Regex.Matches(result, query, RegexOptions.IgnoreCase);
                    if (mcInternal.Count > 0)
                    {
                        var matchesInternal = new Match[mcInternal.Count];
                        mcInternal.CopyTo(matchesInternal, 0);
                        foreach (Match match in matchesInternal.OrderByDescending(x => x.Index))
                        {
                            if (match.Success)
                            {
                                var subString = result.Substring(match.Index, match.Length);
                                result = result.Remove(match.Index, match.Length).Insert(match.Index, string.Format(searchReplacer, subString));
                            }
                        }
                    }
                    return result;
                }
            }
            int length = PreviewMaxSize < text.Length ? PreviewMaxSize : text.Length;
            return text.Substring(0, length) + (length == text.Length ? string.Empty : "...");
        }
    }

    public class CustomSearchResultItem : SearchResultItem
    {
        [IndexField("itemParsedLanguage")]
        public string ItemParsedLanguage { get; set; }

        [IndexField("showInSearch")]
        public string ShowInSearch { get; set; }

        [IndexField("pageItemCategory")]
        public string PageItemCategory { get; set; }

        [IndexField("pageItemContent")]
        public string PageItemContent { get; set; }
    }
}