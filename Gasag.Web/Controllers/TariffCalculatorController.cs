﻿using Gasag.Web.Models.TariffCalculator;
using Sitecore.Data;
using System.Linq;
using System.Web.Mvc;
using Sitecore.Links;
using Sitecore.Mvc.Presentation;
using System.Collections.Generic;
using Gasag.Web.ProductCheckService;
using System;
using System.Globalization;
using Sitecore.Configuration;
using Gasag.Web.LoginService;
using Gizmo.SitecoreGizmo.Items;
using Sitecore.Data.Items;
using System.Web;
using Sitecore.Data.Fields;
using Gasag.Web.Attributes;
using Sitecore.Mvc.Controllers;
using Gizmo.General.Debug;
using System.Dynamic;
using Gizmo.General.Caching;
using Newtonsoft.Json;
using Sitecore.Web.UI.WebControls;
using Gasag.Web.General;
using Gizmo.Web;

namespace Gasag.Web.Controllers
{
    public class TariffCalculatorController : SitecoreController
    {
        private const string TARIFFS = "TariffCalculator.Tariffs";
        private const string CALCULATOR = "TariffCalculator.Calculator";
        private const string SELECTED_TARIFF = "TariffCalculator.SelectedTariff";
        private const string LOGIN_RESPONSE = "TariffCalculator.LoginResponse";
        private const string LOGIN_ITEM = "TariffCalculator.LoginItem";
        private const string ZIP_OBJECT = "TariffCalculator.ZipObject";

        #region Session Properties
        private bool UseSession
        {
            get
            {
                return false;
            }
        }

        private int CacheTimeout
        {
            get
            {
                return 60; // Minutes
            }
        }

        private string CacheKey(string template)
        {
            return string.Concat(template, Session.SessionID);
        }

        private object GetStoredValue(string key)
        {
            if (UseSession)
            {
                return Session[key];
            }
            else
            {
                var cacheKey = CacheKey(key);
                return Cache.Get(cacheKey);
            }
        }

        private void StoreValue(string key, object value)
        {
            if (UseSession)
            {
                Session[key] = value;
            }
            else
            {
                var cacheKey = CacheKey(key);
                Cache.Set(cacheKey, value, CacheTimeout);
            }
        }

        private Calculator CalculatorData
        {
            get
            {
                return GetStoredValue(CALCULATOR) as Calculator;
            }
            set
            {
                StoreValue(CALCULATOR, value);
            }
        }

        private logInVO LoginResponse
        {
            get
            {
                return GetStoredValue(LOGIN_RESPONSE) as logInVO;
            }
            set
            {
                StoreValue(LOGIN_RESPONSE, value);
            }
        }

        private Item LoginItem
        {
            get
            {
                return GetStoredValue(LOGIN_ITEM) as Item;
            }
            set
            {
                StoreValue(LOGIN_ITEM, value);
            }
        }

        private Tariff SelectedTariff
        {
            get
            {
                return GetStoredValue(SELECTED_TARIFF) as Tariff;
            }
            set
            {
                StoreValue(SELECTED_TARIFF, value);
            }
        }

        private List<Tariff> Tariffs
        {
            get
            {
                return GetStoredValue(TARIFFS) as List<Tariff>;
            }
            set
            {
                StoreValue(TARIFFS, value);
            }
        }

        private ZipObject ZipObject
        {
            get
            {
                return GetStoredValue(ZIP_OBJECT) as ZipObject;
            }
            set
            {
                StoreValue(ZIP_OBJECT, value);
            }
        }
        #endregion

        private string IsoContryCode
        {
            get
            {
                //detect user country code ex. by IP.
                // in other case return default
                return Settings.GetSetting("TariffCalculator.DefaultIsoCountryCode");
            }
        }

        private string SystemId
        {
            get { return Settings.GetSetting("TariffCalculator.SystemId"); }
        }

        #region Public Action Methods
        [HttpGet]
        public ActionResult ZipDetails(string zip)
        {
            Log.Debug($"[ZipDetails] Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}");

            var cities = GetCities(zipCode: zip);
            if (cities != null)
            {
                return Json(cities.Select(x => new { zip, town = $"{x.city}, {x.cityPart}" }), JsonRequestBehavior.AllowGet);
            }
            return Json(new string[0], JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult StreetDetails(string zip, string town)
        {
            Log.Debug($"[StreetDetails] Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}");

            if (!string.IsNullOrWhiteSpace(town))
            {
                var details = town.Split(',');
                var streets = GetStreets(cityCode: details[0], cityPartCode: details[1]);
                if (streets != null)
                {
                    return Json(streets.Select(x => new { label = x.streetName, id = $"{x.cityCode},{x.cityPartCode},{x.streetCode}" }), JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new string[0], JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult SearchSuppliers(string supplier, string division)
        {
            Log.Debug($"[SearchSuppliers] Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}");

            if (!string.IsNullOrWhiteSpace(supplier))
            {
                var suppliers = GetSuppliers(division: division);
                if (suppliers != null)
                {
                    return Json(suppliers.Where(x => !string.IsNullOrWhiteSpace(x.serviceProviderName) && x.serviceProviderName.IndexOf(supplier, StringComparison.InvariantCultureIgnoreCase) >=0).Select(x => new { title = x.serviceProviderName }).Take(10), JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new string[0], JsonRequestBehavior.AllowGet);
        }

        public ActionResult Calculate()
        {
            Log.Debug($"[Calculate] Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}");

            return PartialView("/Views/TariffCalculator/InlineTariffCalculator.cshtml", new Calculator { Item = RenderingContext.CurrentOrNull.Rendering.Item });
        }

        [HttpPost]
        [ValidateFormHandler]
        public ActionResult Calculate(Calculator request)
        {
            Log.Debug($"[Calculate POST] Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}");

            try
            {
                return ProcessCalculateAction(request: request, viewPath: "/Views/TariffCalculator/InlineTariffCalculator.cshtml");
            }
            catch (Exception ex)
            {
                return ProcessException(actionName: "Calculate post", pageId: request.ErrorPage, exception: ex);
            }
        }

        public ActionResult BigStageCalculate()
        {
            Log.Debug($"[BigStageCalculate] Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}");

            return PartialView("/Views/Stages/BigStage.cshtml", new Calculator { Item = RenderingContext.CurrentOrNull.Rendering.Item });
        }

        [HttpPost]
        [ValidateFormHandler]
        public ActionResult BigStageCalculate(Calculator request)
        {
            Log.Debug($"[BigStageCalculate POST] Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}");

            try
            {
                return ProcessCalculateAction(request: request, viewPath: "/Views/Stages/BigStage.cshtml");
            }
            catch (Exception ex)
            {
                return ProcessException(actionName: "BigStageCalculate post", pageId: request.ErrorPage, exception: ex);
            }
        }

        [HttpPost]
        public ActionResult SelectTariff(SelectTariffRequest request)
        {
            Log.Debug($"[SelectTariff] Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}");

            try
            {
                return ProcessSelectTariffAction(request);
            }
            catch (Exception ex)
            {
                return ProcessException(actionName: "SelectTariff post", pageId: request.ErrorPage, exception: ex);
            }
        }

        [HttpGet]
        public ActionResult Login()
        {
            Log.Debug($"[Login] Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}");

            try
            {
                LoginResponse = null;
                if (SelectedTariff == null && !Sitecore.Context.PageMode.IsExperienceEditor) return ProcessLostSessionOrUndefinedErrorPage();
                var login = new Login { Item = RenderingContext.CurrentOrNull.Rendering.Item, SelectedTariff = SelectedTariff ?? new Tariff() };
                LoginItem = login.Item;
                return PartialView("/Views/TariffCalculator/TariffCalculatorLogin.cshtml", login);
            }
            catch (Exception ex)
            {
                return ProcessException(actionName: "SelectTariff post", pageId: RenderingContext.CurrentOrNull.Rendering.Item["Error Page"], exception: ex);
            }
        }

        [HttpPost]
        public ActionResult CheckLogin(string username, string password)
        {
            Log.Debug($"[CheckLogin] Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}");

            using (var loginService = new UserClient())
            {
                var loginResponse = loginService.login(user: username, password: password, systemId: SystemId);
                if (loginResponse != null && loginResponse.success)
                {
                    LoginResponse = loginResponse;
                    var nextStepItem = ScValue.GetItem(LoginItem, "Next Page");
                    if (nextStepItem != null)
                    {
                        return Json(new { success = true, redirectUrl = LinkManager.GetItemUrl(nextStepItem) });
                    }
                    else return Json(new { success = false, errorMessage = LoginItem["Unexpected error"] });
                }
                else return Json(new { success = false, errorMessage = LoginItem["Incorrect login or password"] });
            }
        }

        [HttpGet]
        public ActionResult GetBankByIban(string iban)
        {
            Log.Debug($"[GetBankByIban] Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}");

            string bank = null;
            bool success = false;
            using (var bankCatalogueService = new BankCatalogueService.BankClient())
            {
                var bankResponse = bankCatalogueService.getBankByIban(systemId: SystemId, isoCountryCode: iban.Substring(0, 2), ibanCode: iban);
                success = bankResponse != null && bankResponse.success && !string.IsNullOrWhiteSpace(bankResponse.bankName);
                if (success)
                {
                    bank = bankResponse.bankName;
                }
            }
            return Json(new { bank, success }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetCouponCode(string coupon)
        {
            Log.Debug($"[GetCouponCode] Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}");

            var bonus = GetCouponBonus(SelectedTariff, coupon);
            var title = "";
            var value = "";
            if (bonus != null)
            {
                SelectedTariff.Bonus = bonus;
                title = SelectedTariff.Bonus.Title;
                value = $"{SelectedTariff.Bonus.Bonus} {SelectedTariff.Currency}";
                return Json(new { title, value }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return new HttpStatusCodeResult(204);
            }
        }

        [HttpPost]
        public ActionResult Login(LoginRequest request)
        {
            Log.Debug($"[Login] Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}");

            try
            {
                return ProcessLoginAction(request);
            }
            catch (Exception ex)
            {
                return ProcessException(actionName: "Login post", pageId: request.ErrorPage, exception: ex);
            }
        }

        public ActionResult Products()
        {
            Log.Debug($"[Products] Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}");

            try
            {
                if ((CalculatorData == null || Tariffs == null) && !Sitecore.Context.PageMode.IsExperienceEditor)
                {
                    return ProcessLostSessionOrUndefinedErrorPage();
                }
                var tariffsSelection = new TariffsSelection
                {
                    Calculator = CalculatorData ?? new Calculator(),
                    Item = RenderingContext.CurrentOrNull.Rendering.Item,
                    Tariffs = Tariffs ?? new List<Tariff>(),
                    EntryCalculatorPage = GetEntryCalculatorPage()
                };
                for (int i = 0; i < tariffsSelection.Tariffs.Count; i++)
                {
                    tariffsSelection.Tariffs[i].Item = tariffsSelection.Item;
                }
                return PartialView("/Views/TariffCalculator/TariffCalculatorProducts.cshtml", tariffsSelection);
            }
            catch (Exception ex)
            {
                return ProcessException(actionName: "Products", pageId: RenderingContext.CurrentOrNull.Rendering.Item["Error Page"], exception: ex);
            }
        }

        [HttpGet]
        public ActionResult Order()
        {
            Log.Debug($"[Order GET] Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}");

            try
            {
                using (var masterDataService = new MasterDataCatalogueService.MasterDataClient())
                {
                    var masterData = masterDataService.getMasterData(systemId: SystemId, languageIsoCode: Sitecore.Context.Language.CultureInfo.TwoLetterISOLanguageName);
                    if ((SelectedTariff == null || CalculatorData == null || ZipObject == null) && !Sitecore.Context.PageMode.IsExperienceEditor) return ProcessLostSessionOrUndefinedErrorPage();
                    var order = new Order
                    {
                        Item = RenderingContext.CurrentOrNull.Rendering.Item,
                        SelectedTariff = SelectedTariff ?? new Tariff(),
                        MasterData = masterData,
                        Calculator = CalculatorData ?? new Calculator(),
                        ZipObject = ZipObject ?? new ZipObject(),
                        LoginResponse = LoginResponse,
                        EntryCalculatorPage = GetEntryCalculatorPage(),
                        Cities = GetCities(zipCode: ZipObject.ZipParts[0]),
                        Streets = GetStreets(cityCode: ZipObject.ZipCodeResponse.cityCode, cityPartCode: ZipObject.ZipCodeResponse.cityPartCode)
                    };
                    if (order.MasterData == null || !order.MasterData.success) throw new Exception("Cannot get master data");
                    return PartialView("/Views/TariffCalculator/TariffCalculatorOrder.cshtml", order);
                }
            }
            catch (Exception ex)
            {
                return ProcessException(actionName: "Order get", pageId: RenderingContext.CurrentOrNull.Rendering.Item["Error Page"], exception: ex);
            }
        }

        [HttpPost]
        public ActionResult Order(OrderRequest request)
        {
            Log.Debug($"[Order POST] Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}");

            try
            {
                return ProcessOrderAction(request);
            }
            catch (Exception ex)
            {
                return ProcessException(actionName: "Order post", pageId: request.ErrorPage, exception: ex);
            }
        }
        #endregion

        #region Private Helper
        private ActionResult ProcessLostSessionOrUndefinedErrorPage()
        {
            return Redirect(GetEntryCalculatorPage());
        }

        private string GetEntryCalculatorPage()
        {
            var entryPage = Gizmo.Web.CookieManager.GetCookie("TariffCalculator.EntryPage");
            var tariffTeaser = Gizmo.Web.CookieManager.GetCookie("TariffCalculator.TariffTeaser");
            if (!string.IsNullOrWhiteSpace(entryPage) && ShortID.IsShortID(entryPage))
            {
                var pageItem = Sitecore.Context.Database.GetItem(ShortID.DecodeID(entryPage));
                if (pageItem != null)
                {
                    return ScLink.GetUrl(pageItem) + 
                        (
                            string.IsNullOrWhiteSpace(tariffTeaser) ? 
                            string.Empty : 
                            $"?tariffteaser={Server.HtmlEncode(tariffTeaser)}"
                        );
                }
            }
            return "/";
        }

        private ActionResult ProcessOrderAction(OrderRequest request)
        {
            var selectedTariff = SelectedTariff;
            var calculator = CalculatorData;
            request.ParsedAddress = ParseAddress(zipCode:request.Address.Zip ,streetData: request.Address.Street);

            if (selectedTariff != null && calculator != null)
            {
                var couponBonus = GetCouponBonus(tariff: selectedTariff, couponCode: request.Payment.CouponCode);
                if (couponBonus != null)
                {
                    selectedTariff.Bonus = couponBonus;
                }
                using (var parkedDocumentService = new ParkedDocumentService.ParkedDocumentClient())
                {
                    var zo = new ZanoxOrder();
                    zo.Cid = selectedTariff.Portfolio.productId;
                    zo.CurrencySymbol = selectedTariff.Portfolio.currency;
                    zo.TotalPrice = selectedTariff.PricePerYear.ToString("F");

                    if (LoginResponse != null)
                    {
                        var response = parkedDocumentService.createNewContractForExistingCustomer(
                            sessionId: LoginResponse.sessionId,
                            contractAccountId: LoginResponse.businessPartnerId,
                            deliveryAddress: GetDeliveryAddress(request),
                            deviantInvoiceRecipient: GetDeviantInvoiceRecipient(request),
                            selectedProduct: GetSelectedProduct(selectedTariff),
                            bankData: GetBankData(request),
                            additionaInformation: GetAdditionalInformation(request: request, calculator: calculator)
                        );

                        Log.Info($"[Tariff calculator][Order post][parkedDocumentService.createNewContractForExistingCustomer] (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        Log.Info($"sessionId: {LoginResponse.sessionId} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        Log.Info($"contractAccountId: {LoginResponse.businessPartnerId} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        Log.Info($"deliveryAddress: {JsonConvert.SerializeObject(GetDeliveryAddress(request))} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        Log.Info($"deviantInvoiceRecipient: {JsonConvert.SerializeObject(GetDeviantInvoiceRecipient(request))} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        Log.Info($"selectedProduct: {JsonConvert.SerializeObject(GetSelectedProduct(selectedTariff))} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        Log.Info($"bankData: {JsonConvert.SerializeObject(GetBankData(request))} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        Log.Info($"additionaInformation: {JsonConvert.SerializeObject(GetAdditionalInformation(request: request, calculator: calculator))} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        Log.Info($"response: {JsonConvert.SerializeObject(response)} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");

                        if (response != null && response.success)
                        {
                            /* AWI 10.10.2016 - Ticket 37166
                            zo.CustomerId = response.businessPartnerId;
                            zo.OrderId = response.parkedDocumentId;
                            zo.PartnerId = response.businessPartnerId;
                            */
                            var da = GetDeliveryAddress(request);
                            if (da != null)
                            {
                                zo.CustomerId = da.zipCode;
                                zo.OrderId = da.meterSerialNumber;
                                zo.PartnerId = string.Empty;
                            }
                            zo.SetForCurrentUser();
                        }

                        //Log.Info($"[Tariff calculator][Order post][parkedDocumentService.createNewContractForExistingCustomer] request: {Newtonsoft.Json.JsonConvert.SerializeObject(request)} \n response {Newtonsoft.Json.JsonConvert.SerializeObject(response)} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        CookieManager.DeleteCookie("zanpid");
                        return ProcessOrderRequestResult(success: response != null && response.success, request: request);
                    }
                    else
                    {
                        var response = parkedDocumentService.createNewContractForNewCustomer(
                            systemId: SystemId,
                            languageIsoCode: Sitecore.Context.Language.CultureInfo.TwoLetterISOLanguageName.ToUpper(),
                            contractAccount: "",
                            newCustomer: GetNewCustomer(request, calculator),
                            deliveryAddress: GetDeliveryAddress(request),
                            deviantInvoiceRecipient: GetDeviantInvoiceRecipient(request),
                            selectedProduct: GetSelectedProduct(tariff: selectedTariff),
                            bankData: GetBankData(request: request),
                            additionaInformation: GetAdditionalInformation(request: request, calculator: calculator)
                        );

                        Log.Info($"[Tariff calculator][Order post][parkedDocumentService.createNewContractForExistingCustomer] (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        Log.Info($"systemId: {SystemId}, (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        Log.Info($"languageIsoCode: {Sitecore.Context.Language.CultureInfo.TwoLetterISOLanguageName.ToUpper()}, (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        Log.Info($"newCustomer: {JsonConvert.SerializeObject(GetNewCustomer(request, calculator))} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        Log.Info($"deliveryAddress: {JsonConvert.SerializeObject(GetDeliveryAddress(request))} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        Log.Info($"deviantInvoiceRecipient: {JsonConvert.SerializeObject(GetDeviantInvoiceRecipient(request))} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        Log.Info($"selectedProduct: {JsonConvert.SerializeObject(GetSelectedProduct(selectedTariff))} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        Log.Info($"bankData: {JsonConvert.SerializeObject(GetBankData(request))} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        Log.Info($"additionaInformation: {JsonConvert.SerializeObject(GetAdditionalInformation(request: request, calculator: calculator))} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        Log.Info($"response: {JsonConvert.SerializeObject(response)} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");

                        if (response != null && response.success)
                        {
                            /* AWI 10.10.2016 - Ticket 37166
                            zo.CustomerId = response.businessPartnerId;
                            zo.OrderId = response.parkedDocumentId;
                            zo.PartnerId = response.businessPartnerId;
                            */
                            var da = GetDeliveryAddress(request);
                            if (da != null)
                            {
                                zo.CustomerId = da.zipCode;
                                zo.OrderId = da.meterSerialNumber;
                                zo.PartnerId = string.Empty;
                            }
                            zo.SetForCurrentUser();
                        }

                        //Log.Info($"[Tariff calculator][Order post][parkedDocumentService.createNewContractForNewCustomer] request: {Newtonsoft.Json.JsonConvert.SerializeObject(request)} \nresponse {Newtonsoft.Json.JsonConvert.SerializeObject(response)}\n(Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        CookieManager.DeleteCookie("zanpid");
                        return ProcessOrderRequestResult(success: response != null && response.success, request: request);
                    }
                }
            }
            return ProcessLostSessionOrUndefinedErrorPage();
        }

        private ActionResult ProcessOrderRequestResult(bool success, OrderRequest request)
        {
            var pageId = (Sitecore.Context.Database.GetItem(new ID(request.Type)).Name != "Natural Gas") ? request.SuccessPageElectricity : request.SuccessPageGas;
            if (success) return RedirectToOrderRequestResult(pageId: pageId, errorMessage: "Success page field has no proper value");
            return RedirectToOrderRequestResult(pageId: request.ErrorPage, errorMessage: "Error page field has no proper value");
        }

        private ActionResult RedirectToOrderRequestResult(string pageId, string errorMessage)
        {
            if (!string.IsNullOrWhiteSpace(pageId) && ShortID.IsShortID(pageId))
            {
                var pageItem = Sitecore.Context.Database.GetItem(ShortID.DecodeID(pageId));
                if (pageItem != null) return Redirect(LinkManager.GetItemUrl(pageItem));
            }
            throw new Exception(errorMessage);
        }

        private BonusObject GetCouponBonus(Tariff tariff, string couponCode)
        {
            if (!string.IsNullOrWhiteSpace(couponCode))
            {
                var boni = ScValue.GetItems(tariff.TariffItem, "Bonuses").Where(x => 
                    !string.IsNullOrWhiteSpace(ScValue.GetString(x, "Promo Code")) &&
                    ScValue.GetString(x, "Promo Code").ToLower().Trim() == couponCode.ToLower().Trim());
                foreach (var bonus in boni)
                {
                    var bonusObject = GetBonusIfActive(bonusItem: bonus);
                    if (bonusObject != null)
                    { 
                        Log.Info($"[bonus management][coupon code] applied bonus: {bonus.Paths.FullPath} for coupon code: {couponCode}");
                        return bonusObject;
                    }
                }
            }
            return null;
        }

        private AddressObject ParseAddress(string zipCode, string streetData)
        {
            var data = !string.IsNullOrWhiteSpace(streetData) ? streetData.Split(',') : null;
            if (data != null && data.Length == 3)
            {
                var streets = GetStreets(cityCode: data[0], cityPartCode: data[1]);
                if (streets != null)
                {
                    var street = streets.FirstOrDefault(x => x.streetCode == data[2]);
                    if (street != null)
                    {
                        ZipCodeCatalogueService.zipCodeVO zipCodeObject = null;
                        var cities = GetCities(zipCode: zipCode);
                        if (cities != null)
                        {
                            zipCodeObject = cities.FirstOrDefault(x => x.cityCode == data[0] && x.cityPartCode == data[1]);
                        }
                        return new AddressObject
                        {
                            City = zipCodeObject?.city ?? street.city,
                            CityCode = street.cityCode,
                            CityPart = zipCodeObject?.cityPart ?? street.cityPart,
                            CityPartCode = street.cityPartCode,
                            Street = street.streetName,
                            StreetCode = street.streetCode
                        };
                    }
                }
            }
            return null;
        }

        private ParkedDocumentService.customerVO GetNewCustomer(OrderRequest request, Calculator calculator)
        {
            return request == null || request.Customer == null || request.Address == null ? null : new ParkedDocumentService.customerVO
            {
                academicTitle = request.Customer.Title,
                salutation = request.Customer.Salutation,
                firstName = request.Customer.FirstName,
                lastName = request.Customer.LastName,
                email = request.Customer.Email,
                phoneNumber = request.Customer.Phone,
                zipCode = request.Address.Zip,
                birthDate = new DateTime(year: request.CustomerBirthDate.Year, month: request.CustomerBirthDate.Month, day: request.CustomerBirthDate.Day),
                city = request.ParsedAddress?.City ?? request.Address.City,
                streetName = request.ParsedAddress?.Street ?? request.Address.Street,
                streetCode = request.ParsedAddress?.StreetCode,
                houseNumber = request.Address.HouseNumber,
                houseNumberAppendix = request.Address.HouseNumberAppendix,
                partnerType = request.Customer.BusinessPartnerType,
                country = IsoContryCode,
                nameOrg1 = request.Customer.Company,
                division = calculator.Division,
                consumption = calculator.Consumption.ToString(),
                birthDateSpecified = true,
                success = true
            };
        }

        private ParkedDocumentService.deliveryAddressVO GetDeliveryAddress(OrderRequest request)
        {
            return request == null || request.Address == null || request.Delivery == null ? null : new ParkedDocumentService.deliveryAddressVO
            {
                city = request.ParsedAddress?.City ?? request.Address.City,
                houseNumber = request.Address.HouseNumber,
                isoCountryCode = IsoContryCode,
                houseNumberAppendix = request.Address.HouseNumberAppendix,
                meterSerialNumber = request.Delivery.CounterNumber,
                meterInformation = request.Delivery.CounterNumber,
                meterPointIdentifier = request.Delivery.CounterNumber,
                streetName = request.ParsedAddress?.Street ?? request.Address.Street,
                streetCode = request.ParsedAddress?.StreetCode,
                zipCode = request.Address.Zip,
                success = true
            };
        }

        private ParkedDocumentService.deviantInvoiceRecipientVO GetDeviantInvoiceRecipient(OrderRequest request)
        {
            return request == null || request.DeviantInvoiceCustomer == null ? null : new ParkedDocumentService.deviantInvoiceRecipientVO
            {
                salutation = request.DeviantInvoiceCustomer.Salutation,
                academicTitle = request.DeviantInvoiceCustomer.Title,
                firstName = request.DeviantInvoiceCustomer.FirstName,
                lastName = request.DeviantInvoiceCustomer.LastName,
                success = true,
                addressOfRecipient = new ParkedDocumentService.addressVO
                {
                    city = request.DeviantInvoiceAddress.City,
                    streetName = request.DeviantInvoiceAddress.Street,
                    houseNumber = request.DeviantInvoiceAddress.HouseNumber,
                    additionalInformation = request.DeviantInvoiceAddress.HouseNumberAppendix,
                    zipCode = request.DeviantInvoiceAddress.Zip
                },
            };
        }

        private ParkedDocumentService.portfolioVO GetSelectedProduct(Tariff tariff)
        {
            var portfolio = tariff.Portfolio;
            var bonus = portfolio.bonus;
            if (tariff.Bonus != null && !string.IsNullOrWhiteSpace(tariff.Bonus.Bonus))
            {
                bonus = tariff.Bonus.Bonus;
            }
            var tariffOption = portfolio.tariffOption;
            if (tariff.Bonus != null && !string.IsNullOrWhiteSpace(tariff.Bonus.IsuBonus))
            {
                tariffOption = tariff.Bonus.IsuBonus;
            }
            return new ParkedDocumentService.portfolioVO
            {
                basePrice = portfolio.basePrice,
                basePriceKey = portfolio.basePriceKey,
                basePriceNet = portfolio.basePriceNet,
                bonus = bonus,
                consumptionFrom = portfolio.consumptionFrom,
                consumptionTo = portfolio.consumptionTo,
                contractExtensionLength = portfolio.contractExtensionLength,
                contractExtensionLengthUnit = portfolio.contractExtensionLengthUnit,
                currency = portfolio.currency,
                division = portfolio.division,
                minimumContractDuration = portfolio.minimumContractDuration,
                minimumContractDurationUnit = portfolio.minimumContractDurationUnit,
                noticePeriod = portfolio.noticePeriod,
                noticePeriodReference = portfolio.noticePeriodReference,
                noticePeriodUnit = portfolio.noticePeriodUnit,
                onlineTariff = portfolio.onlineTariff,
                portfolioGroup = portfolio.portfolioGroup,
                portfolioId = portfolio.portfolioId,
                productId = portfolio.productId,
                rateOption = portfolio.rateOption,
                rateType = portfolio.rateType,
                workingPrice = portfolio.workingPrice,
                workingPriceKey = portfolio.workingPriceKey,
                workingPriceNet = portfolio.workingPriceNet,

                pricing_id = portfolio.pricing_id,
                paymentMethod = portfolio.paymentMethod,
                paymentMethodDescription = portfolio.paymentMethodDescription,
                paymentTerms = portfolio.paymentTerms,
                price = portfolio.price,
                priceProtectionPeriod = portfolio.priceProtectionPeriod,
                priceProtectionPeriodUnit = portfolio.priceProtectionPeriodUnit,
                priceScales = portfolio.priceScales?.Select(x => new ParkedDocumentService.priceScaleVO()
                {
                    basePrice = x.basePrice,
                    basePriceAmount = x.basePriceAmount,
                    basePriceNetAmount = x.basePriceNetAmount,
                    consumptionFrom = x.consumptionFrom,
                    consumptionTo = x.consumptionTo,
                    currency = x.currency,
                    division = x.division,
                    portfolioId = x.portfolioId,
                    rateOption = x.rateOption,
                    rateType = x.rateType,
                    success = x.success,
                    //vsdSMessages = x.vsdSMessages,
                    workingPrice = x.workingPrice,
                    workingPriceAmount = x.workingPriceAmount,
                    workingPriceNetAmount = x.workingPriceNetAmount
                }).ToArray(),
                rateDescription = portfolio.rateDescription,
                success = portfolio.success,
                tariffModuleGroups = portfolio.tariffModuleGroups?.Select(x => new ParkedDocumentService.tariffModuleVO()
                {
                    basePrice = x.basePrice,
                    basePriceNet = x.basePriceNet,
                    optional = x.optional,
                    price = x.price,
                    priceAffective = x.priceAffective,
                    rateType = x.rateType,
                    success = x.success,
                    tariffModule = x.tariffModule,
                    tariffModuleGroup = x.tariffModuleGroup,
                    transactionCurrency = x.transactionCurrency,
                    //vsdSMessages = x.vsdSMessages,
                    workingPrice = x.workingPrice,
                    workingPriceNet = x.workingPriceNet
                }).ToArray(),
                tariffOption = tariffOption,
                upAndCrossSelling = portfolio.upAndCrossSelling,
                tarifModuleGroupId = portfolio.tarifModuleGroupId,
                validityPeriodCategory = portfolio.validityPeriodCategory
                //vsdSMessages = portfolio.vsdSMessages
            };
        }

        private ParkedDocumentService.additionalInformationVO GetAdditionalInformation(OrderRequest request, Calculator calculator)
        {
            if (request != null && calculator != null)
            {
                var salesPartnerId = calculator.SalesPartnerId;
                if (string.IsNullOrWhiteSpace(salesPartnerId))
                {
                    // Default value from GASAG-37898 - 285 | gasag.de | Kampagnen-ID, Vertriebspartner-ID, Tarifgruppen
                    salesPartnerId = Settings.GetSetting("TariffCalculator.SalesPartnerId.Default", "");
                }
                var salesChannel = calculator.SalesChannel;
                if (string.IsNullOrWhiteSpace(salesChannel))
                {
                    // GASAG-38817 - 230 | gasag.de | Change Request für UDG für die Anbindung BPC-MEINE-GASAG SITECORE-GASAG-DE
                    salesChannel = Settings.GetSetting("TariffCalculator.SalesChannel.Default", "");
                }

                var additionalInformation = new ParkedDocumentService.additionalInformationVO
                {
                    collectionAuthorisation = request.Payment != null && request.Payment.IsIbanProcessing,
                    consumption = calculator.Consumption.ToString(),
                    startOfDeliverySpecified = true,
                    campaignId = calculator.CampaignId,
                    //salesPartnerId = salesPartnerId,
                    //salesChannel = salesChannel,
                    zanoxPartnerId = salesPartnerId
                };

                if (request.Delivery.IsNewEntry)
                {
                    additionalInformation.startOfDelivery = new DateTime(year: request.DeliveryDate.Year, month: request.DeliveryDate.Month, day: request.DeliveryDate.Day);
                    additionalInformation.moveInDate = new DateTime(year: request.DeliveryDate.Year, month: request.DeliveryDate.Month, day: request.DeliveryDate.Day);
                    additionalInformation.moveInDateSpecified = true;
                }
                else
                {
                    additionalInformation.meterReading = request.Delivery.CounterValue;
                    additionalInformation.provider = GetSupplier(division: calculator.Division, supplierName: request.Delivery.Supplier);
                    additionalInformation.startOfDelivery = DateTime.Now;
                }
                return additionalInformation;
            }
            return null;
        }

        private string GetSupplier(string division, string supplierName)
        {
            string result = null;
            if (!string.IsNullOrWhiteSpace(supplierName))
            {
                var supplier = GetSuppliers(division: division)?.FirstOrDefault(x => x.serviceProviderName.Equals(supplierName, StringComparison.InvariantCultureIgnoreCase));
                if (supplier != null)
                {
                    result = supplier.externalId;
                };
            }
            return result;
        }

        private ActionResult ProcessException(string actionName, string pageId, Exception exception)
        {
            Gizmo.General.Debug.Log.Error($"[Tariff calculator][{actionName} action] Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}", exception);
            if (!string.IsNullOrWhiteSpace(pageId))
            {
                var errorPage = Sitecore.Context.Database.GetItem(pageId);
                if (errorPage == null && !string.IsNullOrWhiteSpace(pageId) && ShortID.IsShortID(pageId))
                {
                    errorPage = Sitecore.Context.Database.GetItem(ShortID.DecodeID(pageId));
                    if (errorPage != null) return Redirect(LinkManager.GetItemUrl(errorPage));
                }
            }
            return ProcessLostSessionOrUndefinedErrorPage();
        }

        private ParkedDocumentService.bankDataVO GetBankData(OrderRequest request)
        {
            if (request == null || request.Payment == null || !request.Payment.IsIbanProcessing) return null;
            using (var bankCatalogueService = new BankCatalogueService.BankClient())
            {
                var bankResponse = bankCatalogueService.getBankByIban(systemId: SystemId, isoCountryCode: request.Payment.IBAN.Substring(0, 2), ibanCode: request.Payment.IBAN);
                return new ParkedDocumentService.bankDataVO
                {
                    accountHolder = request.Payment.AccountHolder,
                    bankIsoCountryCode = bankResponse.bankCountryCode,
                    bicKey = bankResponse.bankKey,
                    iban = request.Payment.IBAN,
                    bankName = bankResponse.bankName,
                    success = true
                };
            }
        }

        private ActionResult ProcessLoginAction(LoginRequest request)
        {
            if (!string.IsNullOrWhiteSpace(request.NextPage) && ShortID.IsShortID(request.NextPage))
            {
                var shortId = ShortID.Parse(request.NextPage);
                var nextStepItem = Sitecore.Context.Database.GetItem(shortId.ToID());
                if (nextStepItem != null)
                {
                    switch (request.LoginType)
                    {
                        case LoginType.ExistingCustomerWithAccess:
                            throw new Exception("existing customer should be process over ajax request");
                        case LoginType.ExistingCustomerWithoutAccess:
                        case LoginType.NewCustomer:
                            return Redirect(LinkManager.GetItemUrl(nextStepItem));
                    }
                    throw new Exception("incorrect Login type");
                }
            }
            throw new Exception("request has no valid next page item");
        }

        private ActionResult ProcessSelectTariffAction(SelectTariffRequest request)
        {
            if (!string.IsNullOrWhiteSpace(request.NextPage) && ShortID.IsShortID(request.NextPage))
            {
                var shortId = ShortID.Parse(request.NextPage);
                var nextStepItem = Sitecore.Context.Database.GetItem(shortId.ToID());
                if (nextStepItem != null)
                {
                    SelectedTariff = Tariffs.First(x => x.Portfolio != null && x.Portfolio.productId == request.ProductId);
                    return Redirect(LinkManager.GetItemUrl(nextStepItem));
                    throw new Exception($"cannot find product id {request.ProductId}"); 
                }
            }
            throw new Exception("request has no valid next page item");
        }

        private ActionResult ProcessCalculateAction(Calculator request, string viewPath)
        {
            Log.Debug($"[ProcessCalculateAction] request: {JsonConvert.SerializeObject(request)}");

            if (!string.IsNullOrWhiteSpace(request.NextPage) && ShortID.IsShortID(request.NextPage))
            {
                var shortId = ShortID.Parse(request.NextPage);
                var nextStepItem = Sitecore.Context.Database.GetItem(shortId.ToID());
                if (nextStepItem != null)
                {
                    var zipParts = request.Zip.Split(',').Select(x => x.Trim()).ToArray();
                    Log.Debug($"zipParts = {JsonConvert.SerializeObject(zipParts)}");
                    ZipObject = GetZipCodeObject(zipParts: zipParts);
                    Log.Debug($"ZipObject = {JsonConvert.SerializeObject(ZipObject)}");

                    if (ZipObject != null)
                    {
                        using (var productService = new ProductCheckService.ProductCheckClient())
                        {
                            var priceDate = DateTime.Now.Date;

                            List<filterVO> filters = new List<filterVO>() { new filterVO() { high = "", low = "PK", option = "EQ", sign = "I" } };

                            if(!string.IsNullOrWhiteSpace(request.TarifGroup))
                            { 
                                List<string> getTarifList = new List<string>();
                                var db = Sitecore.Context.Database;
                                foreach (var tarifGroupId in request.TarifGroup.Split(',').ToList())
                                {
                                    var tarifGroup = db.GetItem(new ID(tarifGroupId));
                                    if (tarifGroup != null && tarifGroup.Children.Any(x => ScValue.GetItem(x, "Tarif") != null))
                                    {
                                        foreach (var tarif in tarifGroup.Children.Select(x => ScValue.GetItem(x, "Tarif")).Where(x => x != null))
                                        {
                                            getTarifList.Add(ScValue.GetString(tarif, "Product Id"));
                                        }
                                    }
                                }

                                if (getTarifList.Any())
                                {
                                    foreach (var tarifID in getTarifList.Distinct())
                                    {
                                        filters.Add(new filterVO() { high = "", low = tarifID, option = "EQ", sign = "I" });
                                    }
                                }
                            }                            

                            if (!string.IsNullOrWhiteSpace(request.ProductId))
                            {                                
                                filters.Add(new filterVO() { high = "", low = request.ProductId, option = "EQ", sign = "I" });
                            }                            

                            var cityPartResponse = productService.checkIfCityPartIsRequired(
                                systemId: SystemId, 
                                zipCode: zipParts[0], 
                                cityCode: ZipObject.ZipCodeResponse.cityCode, 
                                division: request.Division, 
                                priceDate: priceDate
                            );

                            var tariffs = productService.getTariffList(
                                systemId: SystemId, 
                                languageIsoCode: Sitecore.Context.Language.CultureInfo.TwoLetterISOLanguageName, 
                                countryCode: null, 
                                zipCode: ZipObject.ZipParts[0], 
                                cityCode: ZipObject.ZipCodeResponse.cityCode, 
                                cityPartCode: cityPartResponse != null && cityPartResponse.success && cityPartResponse.cityPartRequired ? ZipObject.ZipCodeResponse.cityPartCode : null, 
                                streetCode: null, 
                                division: request.Division, 
                                houseNumber: null, 
                                consumption: request.Consumption, 
                                priceDate: priceDate, 
                                filter: new filterListVO() { filter = filters.ToArray() }
                            );

                            Log.Info($"[ProcessCalculateAction] productService.getTariffList(systemId: {SystemId}, languageIsoCode: {Sitecore.Context.Language.CultureInfo.TwoLetterISOLanguageName}, countryCode: {null}, zipCode: {zipParts[0]}, cityCode: {ZipObject.ZipCodeResponse.cityCode}, cityPartCode: {ZipObject.ZipCodeResponse.cityPartCode}, streetCode: {null}, division: {request.Division}, houseNumber: {null}, consumption: {request.Consumption}, priceDate: {priceDate}, filter: {string.Join(", ", filters.Select(x => x.low))}) (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                            Log.Info($"response: {JsonConvert.SerializeObject(tariffs)} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");

                            if (tariffs != null && tariffs.success && tariffs.portfolios != null && tariffs.portfolios.Any())
                            {
                                CalculatorData = request;
                                Tariffs = ProcessTariffs(tariffs.portfolios, request);
                                if (Tariffs.Any())
                                {
                                    return Redirect(LinkManager.GetItemUrl(nextStepItem));
                                }
                            }
                        }
                    }

                    request.Item = RenderingContext.CurrentOrNull.Rendering.Item;

                    // GASAG-39149 - 081 | gasag.de | Tarifrechner für Banner-Kampagne anpassen
                    request.ZipCities = new List<string>();
                    var currentUrl = ScLink.GetUrl(Sitecore.Context.Item, true).Trim('/');
                    var referer = System.Web.HttpContext.Current.Request.UrlReferrer;
                    if (referer != null && !currentUrl.Contains(referer.Host))
                    {
                        Log.Info("[BigStageCalculate] Request from external referer '{0}'. Cookies {1} Session {2}", referer.ToString(), CookieManager.CookiesToString(), Session.SessionID);
                                                
                        Log.Debug($"Zip = {request.Zip}");
                        var cities = GetCities(zipCode: request.Zip);
                        if (cities != null)
                        {
                            Log.Debug($"Found {cities.Count()} cities");
                            request.ZipCities = cities.Select(x => $"{request.Zip}, {x.city}, {x.cityPart}");
                        }
                    }
                    else
                    {
                        // Probably no tariff exists, no tariff detail page for found tariffs exist or street is necessary (which we don't ask for yet).
                        request.ZipErrorMessage = ScValue.GetHtml(request.Item, "Zip Error");
                        Log.Debug($"[Post action from one of calculator][Multiple locations] Cannot choose location, user must select value from dropdown");
                    }
                    
                    return PartialView(viewPath, request);
                }
            }
            throw new Exception("request has no valid next page item");
        }

        private ZipObject GetZipCodeObject(string[] zipParts)
        {
            var cities = GetCities(zipCode: zipParts[0]);
            if (cities != null)
            {
                if (cities.Length == 1)
                {
                    return new ZipObject { ZipCodeResponse = cities[0], ZipParts = zipParts };
                }

                if (zipParts.Length > 1)
                {
                    Func<ZipCodeCatalogueService.zipCodeVO, bool> selector = x => x.city == zipParts[1];
                    if (zipParts.Length > 2)
                    {
                        selector = x => x.city == zipParts[1] && x.cityPart == zipParts[2];
                    }
                    var city = cities.FirstOrDefault(selector);
                    if (city != null)
                    {
                        return new ZipObject { ZipCodeResponse = city, ZipParts = zipParts };
                    }
                }
            }
            return null;
        }

        private List<Tariff> ProcessTariffs(portfolioVO[] portfolios, Calculator calculator)
        {
            var result = new List<Tariff>();
            if (portfolios != null && portfolios.Any())
            {
                var currenciesFolder = ScReference.Instance().GetItem("Currencies");
                var tariffCalculatorTypesFolder = ScReference.Instance().GetItem("Tariff Calculator Types Folder");
                var typeItem = tariffCalculatorTypesFolder.Children
                    .SingleOrDefault(x => calculator.Type.Equals(ScValue.GetString(x, "Value"), StringComparison.InvariantCultureIgnoreCase));
                var teaserInformation = ExtractTeaserInformation(tariffTeaserId: calculator.TariffTeaser);
                bool hasMainTariff = false;
                foreach (var portfolio in portfolios)
                {
                    var tariff = ProcessTariff(portfolio: portfolio, calculator: calculator, currencies: currenciesFolder, typeItem: typeItem);
                    if (tariff == null || tariff.TariffItem == null)
                    {
                        Log.Info($"[Tariff calculator] Cannot find page for product id: {portfolio.productId} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                    }
                    else
                    {
                        if (teaserInformation == null || teaserInformation.MainTariffGroup == tariff.Portfolio.productId || 
                            (teaserInformation.OptionalTariffs != null && teaserInformation.OptionalTariffs.Contains(tariff.Portfolio.portfolioGroup)))
                        {
                            if (teaserInformation != null && teaserInformation.MainTariffGroup == tariff.Portfolio.productId)
                            {
                                if (teaserInformation.Bonus != null)
                                {
                                    tariff.Bonus = teaserInformation.Bonus;
                                }
                                tariff.IsMain = true;
                            }
                            if (teaserInformation == null && !hasMainTariff && ScValue.GetBool(item: tariff.TariffItem, fieldKey: "Is Main Tariff"))
                            {
                                hasMainTariff = tariff.IsMain = true;
                            }
                            result.Add(tariff);
                        }
                        else
                        {
                            Log.Info($"[Tariff calculator] product id: {portfolio.productId} was not specified in main tariff or optional tariffs in tariff teaser {calculator.TariffTeaser} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                        }
                    }
                }
            }

            if (result.Count > 1)
            {
                var mainTariff = result.SingleOrDefault(x => x.IsMain);
                if (mainTariff != null)
                {
                    result.Remove(mainTariff);
                    result.Insert(1, mainTariff);
                }
            }
            else if (result.Count == 1)
            {
                result[0].IsMain = true;
            }

            return result;
        }

        private TeaserObject ExtractTeaserInformation(string tariffTeaserId)
        {
            if (!string.IsNullOrWhiteSpace(tariffTeaserId) && ShortID.IsShortID(tariffTeaserId))
            {
                var tariffTeaserItem = Sitecore.Context.Database.GetItem(ShortID.DecodeID(tariffTeaserId));
                if (tariffTeaserItem != null)
                {
                    var bonus = GetBonusIfActive(ScValue.GetItem(item: tariffTeaserItem, fieldKey: "Bonus"));
                    var mainTariffItem = ScValue.GetItem(item: tariffTeaserItem, fieldKey: "Main Tariff");
                    var optionalTariffs = ScValue.GetItems(item: tariffTeaserItem, fieldKey: "Optional Tariffs");
                    return new TeaserObject
                    {
                        Bonus = bonus,
                        MainTariffGroup = mainTariffItem != null && !string.IsNullOrWhiteSpace(mainTariffItem["Product Id"]) ? mainTariffItem["Product Id"] : null,
                        OptionalTariffs = optionalTariffs != null && optionalTariffs.Any() ? optionalTariffs.Select(x => x["Product Id"]).Where(x => !string.IsNullOrWhiteSpace(x)).ToList() : null
                    };
                }
            }
            return null;
        }

        private Tariff ProcessTariff(portfolioVO portfolio, Calculator calculator, Item currencies, Item typeItem)
        {
            var fastQuery = $"fast:{Sitecore.Context.Site.RootPath + Sitecore.Context.Site.StartItem}//*[@Product Id='{portfolio.productId}' and @Portfolio Type='{typeItem.ID}']";
            var selectItems = Sitecore.Context.Database.SelectItems(fastQuery);
            if (selectItems == null || selectItems.Length == 0) return null;
            var tariffItem = selectItems[0];
            string name = tariffItem["Tariff Name"];
            if (string.IsNullOrWhiteSpace(name))
            {
                name = portfolio.rateDescription;
            } 
            if (string.IsNullOrWhiteSpace(name))
            {
                return null;
            }

            var tariff = new Tariff
            {
                Bonus = new BonusObject() { Bonus = portfolio.bonus, IsuBonus = portfolio.tariffOption },
                Name =  name,
                BasePrice = portfolio.basePrice,
                WorkingPrice = portfolio.workingPrice,
                Currency = new HtmlString(portfolio.currency),
                Portfolio = portfolio,
                TariffItem = tariffItem,
                MinimumContractDuration = portfolio.minimumContractDuration,
                MinimumContractDurationUnit = portfolio.minimumContractDurationUnit,
                MinimumConsumption = portfolio.consumptionFrom,
                PriceProtectionPeriod = portfolio.priceProtectionPeriod,
                PriceProtectionPeriodUnit = portfolio.priceProtectionPeriodUnit
            };

            Log.Info($"[Tariff calculator][ProcessTariff] {JsonConvert.SerializeObject(tariff)} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
            
            if (portfolio.priceScales != null && portfolio.priceScales.Any())
            {
                double from = 0;
                double to = 0;
                double basePrice = 0;
                double workingPrice = 0;
                double minimumConsumption = 0;
                foreach (var price in portfolio.priceScales)
                {
                    var parsable = double.TryParse(price.consumptionFrom, NumberStyles.Any, CultureInfo.InvariantCulture,  out from) 
                        && double.TryParse(price.consumptionTo, NumberStyles.Any, CultureInfo.InvariantCulture,  out to)
                        && double.TryParse(price.basePriceAmount, NumberStyles.Any, CultureInfo.InvariantCulture, out basePrice)
                        && double.TryParse(price.workingPriceAmount, NumberStyles.Any, CultureInfo.InvariantCulture, out workingPrice)
                        && double.TryParse(price.consumptionFrom, NumberStyles.Any, CultureInfo.InvariantCulture, out minimumConsumption);
                    var matching = parsable && from <= calculator.Consumption && calculator.Consumption <= to;

                    Log.Info($"[Tariff calculator][ProcessTariff] Testing priceScale {JsonConvert.SerializeObject(price)}, parsable = {parsable}, matching = {matching} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");

                    if (matching)
                    {
                        tariff.BasePrice = basePrice;
                        tariff.WorkingPrice = workingPrice;
                        tariff.MinimumConsumption = minimumConsumption;
                        tariff.Currency = new HtmlString(price.currency);
                    }
                }
            }
            
            var bonusSource = tariffItem;
            if (!string.IsNullOrWhiteSpace(calculator.TarifGroup))
            {
                foreach (var tarifGroupId in calculator.TarifGroup.Split(',').ToList())
                {
                    var tarifGroup = Sitecore.Context.Database.GetItem(new ID(tarifGroupId));
                    if (tarifGroup != null)
                    {
                        var tarifBoni = tarifGroup.Children.FirstOrDefault(x => ScValue.GetItem(x, "Tarif") != null && ScValue.GetItem(x, "Tarif").ID == tariffItem.ID);
                        if (tarifBoni != null)
                        {
                            bonusSource = tarifBoni;
                        }
                    }
                }
            }
            var bonus = SelectTariffBonus(bonusSource: bonusSource, calculator: calculator);
            if (bonus != null)
            {
                Log.Debug($"Bonus found {bonus.Title} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                tariff.Bonus = bonus;
            }
             
            if (currencies != null)
            {
                var currencyItem = currencies.Children.FirstOrDefault(x => x.Name.Equals(portfolio.currency, StringComparison.InvariantCultureIgnoreCase));
                if (currencyItem != null && !string.IsNullOrWhiteSpace(currencyItem["Value"])) tariff.Currency = new HtmlString(currencyItem["Value"]);
            }

            tariff.PricePerYear = tariff.BasePrice + calculator.Consumption * tariff.WorkingPrice;

            return tariff;
        }

        private BonusObject SelectTariffBonus(Item bonusSource, Calculator calculator)
        {
            var bonuses = ScValue.GetItems(bonusSource, "Bonuses")?.Select(x => GetBonusObject(bonusItem: x)).Where(x => x != null).ToList();
            Log.Debug($"Tariff has boni '{string.Join(", ", bonuses.Select(x => x.Title))}' (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");

            int zip;
            if (int.TryParse(calculator.Zip.Split(',')[0], out zip) && bonuses != null && bonuses.Any())
            {
                Log.Debug($"Searching bonus for {zip} and consumption {calculator.Consumption} (Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID})");
                var currentDate = DateTime.UtcNow.Date;

                // 1. one zip, one consumption.
                var oneZipAndOneConsumption = bonuses.FirstOrDefault(
                    x => x.ZipFrom.HasValue && !x.ZipTo.HasValue 
                    && x.ZipFrom.Value == zip
                    && x.ConsumptionFrom.HasValue && !x.ConsumptionTo.HasValue 
                    && x.ConsumptionFrom.Value == calculator.Consumption
                    && (!x.ActivationDate.HasValue || x.ActivationDate.Value <= currentDate)
                    && (!x.ExpirationDate.HasValue || x.ExpirationDate.Value >= currentDate)
                    && string.IsNullOrWhiteSpace(x.PromoCode)
                );
                if (oneZipAndOneConsumption != null) return oneZipAndOneConsumption;

                // 2. one zip, consumption range.
                var oneZipAndConsumptionRange = bonuses.FirstOrDefault(
                    x => x.ZipFrom.HasValue && !x.ZipTo.HasValue
                    && x.ZipFrom.Value == zip
                    && x.ConsumptionFrom.HasValue && x.ConsumptionTo.HasValue 
                    && x.ConsumptionFrom.Value <= calculator.Consumption && calculator.Consumption <= x.ConsumptionTo.Value
                    && (!x.ActivationDate.HasValue || x.ActivationDate.Value <= currentDate)
                    && (!x.ExpirationDate.HasValue || x.ExpirationDate.Value >= currentDate)
                    && string.IsNullOrWhiteSpace(x.PromoCode)
                );
                if (oneZipAndConsumptionRange != null) return oneZipAndConsumptionRange;

                // 3. zip range, one consumption.
                var oneConsumptionAndZipRange = bonuses.FirstOrDefault(
                    x => x.ZipFrom.HasValue && x.ZipTo.HasValue 
                    && x.ZipFrom.Value <= zip && zip <= x.ZipTo.Value
                    && x.ConsumptionFrom.HasValue && !x.ConsumptionTo.HasValue 
                    && x.ConsumptionFrom.Value == calculator.Consumption
                    && (!x.ActivationDate.HasValue || x.ActivationDate.Value <= currentDate)
                    && (!x.ExpirationDate.HasValue || x.ExpirationDate.Value >= currentDate)
                    && string.IsNullOrWhiteSpace(x.PromoCode)
                );
                if (oneConsumptionAndZipRange != null) return oneConsumptionAndZipRange;

                // 4. one zip, no consumption
                var oneZipNoConsumption = bonuses.FirstOrDefault(
                    x => x.ZipFrom.HasValue && !x.ZipTo.HasValue
                    && x.ZipFrom.Value == zip
                    && !x.ConsumptionFrom.HasValue && !x.ConsumptionTo.HasValue
                    && (!x.ActivationDate.HasValue || x.ActivationDate.Value <= currentDate)
                    && (!x.ExpirationDate.HasValue || x.ExpirationDate.Value >= currentDate)
                    && string.IsNullOrWhiteSpace(x.PromoCode)
                );
                if (oneZipNoConsumption != null) return oneZipNoConsumption;

                // 5. no zip, one consumption
                var noZipOneConsumption = bonuses.FirstOrDefault(
                    x => !x.ZipFrom.HasValue && !x.ZipTo.HasValue
                    && x.ConsumptionFrom.HasValue && !x.ConsumptionTo.HasValue
                    && x.ConsumptionFrom.Value == calculator.Consumption
                    && (!x.ActivationDate.HasValue || x.ActivationDate.Value <= currentDate)
                    && (!x.ExpirationDate.HasValue || x.ExpirationDate.Value >= currentDate)
                    && string.IsNullOrWhiteSpace(x.PromoCode)
                );
                if (noZipOneConsumption != null) return noZipOneConsumption;

                // 6. zip range, consumption range
                var zipRangeConsumptionRange = bonuses.FirstOrDefault(
                    x => x.ZipFrom.HasValue && x.ZipTo.HasValue
                    && x.ZipFrom.Value <= zip && zip <= x.ZipTo.Value
                    && x.ConsumptionFrom.HasValue && x.ConsumptionTo.HasValue
                    && x.ConsumptionFrom <= calculator.Consumption && calculator.Consumption <= x.ConsumptionTo.Value
                    && (!x.ActivationDate.HasValue || x.ActivationDate.Value <= currentDate)
                    && (!x.ExpirationDate.HasValue || x.ExpirationDate.Value >= currentDate)
                    && string.IsNullOrWhiteSpace(x.PromoCode)
                );
                if (zipRangeConsumptionRange != null) return zipRangeConsumptionRange;

                // 7. zip range, no consumption. 
                var zipRangeNoConsumption = bonuses.FirstOrDefault(
                    x => x.ZipFrom.HasValue && x.ZipTo.HasValue
                    && x.ZipFrom.Value <= zip && zip <= x.ZipTo.Value
                    && !x.ConsumptionFrom.HasValue && !x.ConsumptionTo.HasValue
                    && (!x.ActivationDate.HasValue || x.ActivationDate.Value <= currentDate)
                    && (!x.ExpirationDate.HasValue || x.ExpirationDate.Value >= currentDate)
                    && string.IsNullOrWhiteSpace(x.PromoCode)
                );
                if (zipRangeNoConsumption != null) return zipRangeNoConsumption;

                // 8.consumption range no zip
                var noZipConsumptionRange = bonuses.FirstOrDefault(
                    x => !x.ZipFrom.HasValue && !x.ZipTo.HasValue
                    && x.ConsumptionFrom.HasValue && x.ConsumptionTo.HasValue
                    && x.ConsumptionFrom <= calculator.Consumption && calculator.Consumption <= x.ConsumptionTo.Value
                    && (!x.ActivationDate.HasValue || x.ActivationDate.Value <= currentDate)
                    && (!x.ExpirationDate.HasValue || x.ExpirationDate.Value >= currentDate)
                    && string.IsNullOrWhiteSpace(x.PromoCode)
                );
                if (noZipConsumptionRange != null) return noZipConsumptionRange;

                // 9. no zip, no consumption (default)
                var noZipNoConsumption = bonuses.FirstOrDefault(
                    x => !x.ZipFrom.HasValue && !x.ZipTo.HasValue
                    && !x.ConsumptionFrom.HasValue && !x.ConsumptionTo.HasValue
                    && (!x.ActivationDate.HasValue || x.ActivationDate.Value <= currentDate)
                    && (!x.ExpirationDate.HasValue || x.ExpirationDate.Value >= currentDate)
                    && string.IsNullOrWhiteSpace(x.PromoCode)
                );
                if (noZipNoConsumption != null) return noZipNoConsumption;
            }
            return null;
        }

        private BonusObject GetBonusObject(Item bonusItem)
        {
            if (bonusItem == null)
            {
                return null;
            }

            var bonusObject = new BonusObject
            {
                Title = bonusItem["Title"],
                Bonus = bonusItem["Bonus"],
                IsuBonus = bonusItem["ISU Bonus"],
                PromoCode = bonusItem["Promo Code"],
            };
            DateField activationDate = bonusItem.Fields["Activation Date"];
            if (!string.IsNullOrWhiteSpace(activationDate.Value))
            {
                bonusObject.ActivationDate = activationDate.DateTime;
            }
            DateField expirationDate = bonusItem.Fields["Expiration Date"];
            if (!string.IsNullOrWhiteSpace(expirationDate.Value))
            {
                bonusObject.ExpirationDate = expirationDate.DateTime;
            }
            int intValue;
            if (int.TryParse(bonusItem["Zip From"], out intValue))
            {
                bonusObject.ZipFrom = intValue;
            }
            if (int.TryParse(bonusItem["Zip To"], out intValue))
            {
                bonusObject.ZipTo = intValue;
            }
            double doubleValue;
            if (double.TryParse(bonusItem["Consumption From"], out doubleValue))
            {
                bonusObject.ConsumptionFrom = doubleValue;
            }
            if (double.TryParse(bonusItem["Consumption To"], out doubleValue))
            {
                bonusObject.ConsumptionTo = doubleValue;
            }
            return bonusObject;
        }

        private BonusObject GetBonusIfActive(Item bonusItem)
        {
            if (bonusItem != null)
            {
                var bonusObject = GetBonusObject(bonusItem);
                if ((!string.IsNullOrWhiteSpace(bonusObject.Bonus) || !string.IsNullOrWhiteSpace(bonusObject.IsuBonus)) && 
                    (!bonusObject.ActivationDate.HasValue || bonusObject.ActivationDate <= DateTime.UtcNow.Date) && 
                    (!bonusObject.ExpirationDate.HasValue || bonusObject.ExpirationDate >= DateTime.UtcNow.Date))
                {
                    return bonusObject;
                }
            }
            return null;
        }

        public ZipCodeCatalogueService.zipCodeVO[] GetCities(string zipCode)
        {
            ZipCodeCatalogueService.zipCodeVO[] result = null;
            if (!string.IsNullOrWhiteSpace(zipCode) && zipCode.Length == 5)
            {
                var key = $"GetCities#SystemId={SystemId}#zipCode={zipCode}";
                var fromCache = false;
                               
                if (Cache.Has(key))
                {
                    fromCache = true;
                    result = Cache.Get(key) as ZipCodeCatalogueService.zipCodeVO[];
                }
                if (result == null)
                {
                    using (var zipCodeProvider = new ZipCodeCatalogueService.ZipCodeClient())
                    {
                        var response = zipCodeProvider.getCitiesForZipCode(systemId: SystemId, zipCode: zipCode);
                        if (response != null && response.success && response.cities != null && response.cities.Length > 0)
                        {
                            result = response.cities;
                            Cache.Set(key, result);
                        }
                    }
                }

                Log.Debug($"GetCities: zipCode = {zipCode}, fromCache = {fromCache}, cacheKey = {key}, result = {JsonConvert.SerializeObject(result)}");
            }
            return result;
        }

        private StreetCodeCatalogueService.streetVO[] GetStreets(string cityCode, string cityPartCode)
        {
            StreetCodeCatalogueService.streetVO[] result = null;
            if (!string.IsNullOrWhiteSpace(cityCode) && !string.IsNullOrWhiteSpace(cityPartCode))
            {
                var key = $"{nameof(GetStreets)}#{nameof(SystemId)}{SystemId}#{nameof(cityCode)}{cityCode}#{nameof(cityPartCode)}{cityPartCode}";

                if (Cache.Has(key)) result = Cache.Get(key) as StreetCodeCatalogueService.streetVO[];
                if (result == null)
                {
                    using (var streetProvider = new StreetCodeCatalogueService.StreetClient())
                    {
                        var response = streetProvider.getStreetsForCityCode(systemId: SystemId, cityCode: cityCode, cityPartCode: cityPartCode);
                        if (response != null && response.success && response.streets != null && response.streets.Length > 0)
                        {
                            Log.Info($"[GetStreets] Found {response.streets.Count()} streets for cityCode {cityCode} and cityPartCode {cityPartCode} Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}");
                            Log.Info($"[GetStreets] Response {JsonConvert.SerializeObject(response)} Url {HttpContext.Request.Url.ToString()} Cookies {CookieManager.CookiesToString()} Session {Session.SessionID}");
                            result = response.streets;
                            Cache.Set(key, result);
                        }
                    }
                }
            }
            return result;
        }

        private ServiceProviderCatalogueService.serviceProviderVO[] GetSuppliers(string division)
        {
            var key = $"{nameof(GetSuppliers)}#{nameof(SystemId)}{SystemId}#{nameof(IsoContryCode)}{IsoContryCode}#{nameof(division)}{division}";
            ServiceProviderCatalogueService.serviceProviderVO[] result = null;
            if (Cache.Has(key))
            {
                result = Cache.Get(key) as ServiceProviderCatalogueService.serviceProviderVO[];
            }

            if (result == null)
            {
                using (var serviceProvider = new ServiceProviderCatalogueService.ServiceProviderClient())
                {
                    var suppliers = serviceProvider.getAllServiceProviders(systemId: SystemId, isoCountryCode: IsoContryCode, division: division);
                    if (suppliers != null && suppliers.success && suppliers.serviceProviderList != null)
                    {
                        result = suppliers.serviceProviderList;
                        Cache.Set(key, result);
                    }
                }
            }
            return result;
        }
        #endregion
    }
}