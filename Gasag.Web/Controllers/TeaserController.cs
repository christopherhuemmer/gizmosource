﻿using Gizmo.SitecoreGizmo.Items;
using Gizmo.Web;
using Newtonsoft.Json;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;

namespace Gasag.Web.Controllers
{
    public class TeaserController : Controller
    {
        public ActionResult Download(string current, string category)
        {
            int currentTeasers;
            if (!int.TryParse(current, out currentTeasers))
            {
                currentTeasers = 0;
            }
            var fileTemplateID = ScReference.Instance().GetItem("Download Teaser File Template").ID;
           
            var teaserAmount = int.Parse(Sitecore.Configuration.Settings.GetSetting("TeaserLoadValue"));
            var contextItem = Sitecore.Context.Database.GetItem(new ID(CookieManager.GetCookie("SitecoreContextItem")));
            var downloadTeaserFolder = ScReference.GetItemRelativeToInstance("Download Teaser Folder", null, contextItem);

            var downloads = new List<ExpandoObject>();
            if (string.IsNullOrWhiteSpace(category))
            {
                foreach (Item downloadTeaser in downloadTeaserFolder.Children)
                {
                    downloads.Add(fillDownloadItem(downloadTeaser));
                }
            }
            else
            {
                foreach (Item downloadTeaser in downloadTeaserFolder.Children.Where(x => x["Category"] == category))
                {
                    downloads.Add(fillDownloadItem(downloadTeaser));
                }
            }

            dynamic obj = new ExpandoObject();
            obj.maxAvailableDownloads = downloads.Count;
            obj.lastTeaser = downloads.Count <= currentTeasers + teaserAmount;
            obj.downloads = downloads.Skip(currentTeasers).Take(teaserAmount);

            string json = JsonConvert.SerializeObject(obj);
            return Content(json, "application/json");
        }

        public ActionResult Event(string current, string location, string topic)
        {
            int currentTeasers;
            if (!int.TryParse(current, out currentTeasers))
            {
                currentTeasers = 0;
            }
            var settings = ScSettings.Instance();
            var teaserAmount = int.Parse(Sitecore.Configuration.Settings.GetSetting("TeaserLoadValue"));
            var contextItem = Sitecore.Context.Database.GetItem(new ID(CookieManager.GetCookie("SitecoreContextItem")));
            var eventTeaserFolder = ScReference.GetItemRelativeToInstance("Event Folder", null, contextItem);

            var events = new List<ExpandoObject>();
            if (string.IsNullOrWhiteSpace(location) && string.IsNullOrWhiteSpace(topic))
            {
                foreach (Item eventTeaser in eventTeaserFolder.Children)
                {
                    events.Add(fillEventItem(eventTeaser));
                }
            }
            else if (!string.IsNullOrWhiteSpace(location) && string.IsNullOrWhiteSpace(topic))
            {
                foreach (Item eventTeaser in eventTeaserFolder.Children.Where(x => x["Location"] == location))
                {
                    events.Add(fillEventItem(eventTeaser));
                }
            }
            else if (string.IsNullOrWhiteSpace(location) && !string.IsNullOrWhiteSpace(topic))
            {
                foreach (Item eventTeaser in eventTeaserFolder.Children.Where(x => x["Topic"] == topic))
                {
                    events.Add(fillEventItem(eventTeaser));
                }
            }
            else if (!string.IsNullOrWhiteSpace(location) && !string.IsNullOrWhiteSpace(topic))
            {
                foreach (Item eventTeaser in eventTeaserFolder.Children.Where(x => x["Location"] == location && x["Topic"] == topic))
                {
                    events.Add(fillEventItem(eventTeaser));
                }
            }

            dynamic obj = new ExpandoObject();
            obj.maxAvailableEvents = events.Count;
            obj.lastTeaser = events.Count <= currentTeasers + teaserAmount;
            obj.events = events.Skip(currentTeasers).Take(teaserAmount);

            var json = JsonConvert.SerializeObject(obj);
            return Content(json, "application/json");
        }

        public ActionResult TextImage(string current, string coupon, string innovation)
        {
            int currentTeasers;
            if (!int.TryParse(current, out currentTeasers))
            {
                currentTeasers = 0;
            }
            var settings = ScSettings.Instance();
            var teaserAmount = int.Parse(Sitecore.Configuration.Settings.GetSetting("TeaserLoadValue"));
            var contextItem = Sitecore.Context.Database.GetItem(new ID(CookieManager.GetCookie("SitecoreContextItem")));
            var textImageTeaserFolder = ScReference.GetItemRelativeToInstance("ImageText Folder", null, contextItem);

            var textImage = new List<ExpandoObject>();
            if (string.IsNullOrWhiteSpace(coupon) && string.IsNullOrWhiteSpace(innovation))
            {
                foreach (Item textImageTeaser in textImageTeaserFolder.Children)
                {
                    textImage.Add(fillTextImageItem(textImageTeaser));
                }
            }
            else if (!string.IsNullOrWhiteSpace(coupon) && string.IsNullOrWhiteSpace(innovation))
            {
                foreach (Item textImageTeaser in textImageTeaserFolder.Children.Where(x => x["Coupon"] == coupon))
                {
                    textImage.Add(fillTextImageItem(textImageTeaser));
                }
            }
            else if (string.IsNullOrWhiteSpace(coupon) && !string.IsNullOrWhiteSpace(innovation))
            {
                foreach (Item textImageTeaser in textImageTeaserFolder.Children.Where(x => x["Innovation"] == innovation))
                {
                    textImage.Add(fillTextImageItem(textImageTeaser));
                }
            }
            else if (!string.IsNullOrWhiteSpace(coupon) && !string.IsNullOrWhiteSpace(innovation))
            {
                foreach (Item textImageTeaser in textImageTeaserFolder.Children.Where(x => x["Coupon"] == coupon && x["Innovation"] == innovation))
                {
                    textImage.Add(fillTextImageItem(textImageTeaser));
                }
            }

            dynamic obj = new ExpandoObject();
            obj.maxAvailableInnovations = textImage.Count;
            obj.lastTeaser = textImage.Count <= currentTeasers + teaserAmount;
            obj.innovations = textImage.Skip(currentTeasers).Take(teaserAmount);

            var json = JsonConvert.SerializeObject(obj);
            return Content(json, "application/json");
        }

        public ActionResult News(string current, string category, string period)
        {
            int currentTeasers;
            if (!int.TryParse(current, out currentTeasers))
            {
                currentTeasers = 0;
            }
            int periodInDays = 0;
            if(!string.IsNullOrWhiteSpace(period))
            {
                if (!int.TryParse(Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(period)).DisplayName, out periodInDays))
                {
                    periodInDays = 0;
                }
            }
            var time = DateTime.Today.AddDays(-periodInDays);
            var settings = ScSettings.Instance();
            var teaserAmount = int.Parse(Sitecore.Configuration.Settings.GetSetting("TeaserLoadValue"));
            var contextItem = Sitecore.Context.Database.GetItem(new ID(CookieManager.GetCookie("SitecoreContextItem")));
            var newsTeaserFolder = ScReference.GetItemRelativeToInstance("News Folder", null, contextItem);

            var news = new List<Item>();            
            if (string.IsNullOrWhiteSpace(category) && string.IsNullOrWhiteSpace(period))
            {
                foreach (Item newsTeaser in newsTeaserFolder.Children)
                {
                    news.Add(newsTeaser);
                }
            }
            else if (!string.IsNullOrWhiteSpace(category) && string.IsNullOrWhiteSpace(period))
            {
                foreach (Item newsTeaser in newsTeaserFolder.Children.Where(x => x["Bereich"] == category))
                {
                    news.Add(newsTeaser);
                }
            }
            else if (string.IsNullOrWhiteSpace(category) && !string.IsNullOrWhiteSpace(period))
            {
                foreach (Item newsTeaser in newsTeaserFolder.Children.Where(x => (ScValue.GetDate(x, "Date") > time)))
                {
                    news.Add(newsTeaser);
                }
            }
            else if (!string.IsNullOrWhiteSpace(category) && !string.IsNullOrWhiteSpace(period))
            {
                foreach (Item newsTeaser in newsTeaserFolder.Children.Where(x => x["Bereich"] == category && (ScValue.GetDate(x, "Date") > time)))
                {
                    news.Add(newsTeaser);
                }
            }
            news = news.OrderByDescending(x => ScValue.GetDate(x, "Date")).ToList();
            var jsonNews = news.Select(x => fillNewsItem(x));

            dynamic obj = new ExpandoObject();
            obj.maxAvailableNews = jsonNews.Count();
            obj.lastTeaser = jsonNews.Count() <= currentTeasers + teaserAmount;
            obj.news = jsonNews.Skip(currentTeasers).Take(teaserAmount);

            var json = JsonConvert.SerializeObject(obj);
            return Content(json, "application/json");
        }

        public ActionResult Jobs(string current, string category, string businessgroup)
        {
            int currentTeasers;
            if (!int.TryParse(current, out currentTeasers))
            {
                currentTeasers = 0;
            }
            var settings = ScSettings.Instance();
            var teaserAmount = int.Parse(Sitecore.Configuration.Settings.GetSetting("JobTeaserLoadValue"));
            var contextItem = Sitecore.Context.Database.GetItem(new ID(CookieManager.GetCookie("SitecoreContextItem")));
            var jobsTeaserFolder = ScReference.GetItemRelativeToInstance("Jobs Folder", null, contextItem);

            var jobs = new List<ExpandoObject>(); 
            if (string.IsNullOrWhiteSpace(category) && string.IsNullOrWhiteSpace(businessgroup))
            {
                foreach (Item jobsTeaser in jobsTeaserFolder.Children)
                {
                    jobs.Add(fillJobItem(jobsTeaser));
                }
            }
            else if (!string.IsNullOrWhiteSpace(category) && string.IsNullOrWhiteSpace(businessgroup))
            {
                foreach (Item jobsTeaser in jobsTeaserFolder.Children.Where(x => x["Filter Category"] == category))
                {
                    jobs.Add(fillJobItem(jobsTeaser));
                }
            }
            else if (string.IsNullOrWhiteSpace(category) && !string.IsNullOrWhiteSpace(businessgroup))
            {
                foreach (Item jobsTeaser in jobsTeaserFolder.Children.Where(x => x["Business Group"] == businessgroup))
                {
                    jobs.Add(fillJobItem(jobsTeaser));
                }
            }
            else if (!string.IsNullOrWhiteSpace(category) && !string.IsNullOrWhiteSpace(businessgroup))
            {
                foreach (Item jobsTeaser in jobsTeaserFolder.Children.Where(x => x["Filter Category"] == category && x["Business Group"] == businessgroup))
                {
                    jobs.Add(fillJobItem(jobsTeaser));
                }
            }

            dynamic obj = new ExpandoObject();
            obj.maxAvailableJobs = jobs.Count;
            obj.lastTeaser = jobs.Count <= currentTeasers + teaserAmount;
            obj.jobs = jobs.Skip(currentTeasers).Take(teaserAmount);

            var json = JsonConvert.SerializeObject(obj);
            return Content(json, "application/json");
        }

        public dynamic fillDownloadItem(Item downloadTeaser)
        {
            var imageTemplateID = ScReference.Instance().GetItem("Download Teaser Image Template").ID;

            dynamic downloadObject = new ExpandoObject();
            if (downloadTeaser.TemplateID == imageTemplateID)
            {
                downloadObject.imgteaser = true;
            }
            ImageField imageField = downloadTeaser.Fields["Image"];
            if (!string.IsNullOrWhiteSpace(imageField.Value))
            {
                dynamic imageObject = new ExpandoObject();
                imageObject.alt = imageField.Alt;
                imageObject.url = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imageField.MediaItem);
                downloadObject.image = imageObject;
            }
            else
            {
                downloadObject.image = false;
            }

            FileField fileField = downloadTeaser.Fields["File"];
            ImageField imageField72 = downloadTeaser.Fields["Image 72 dpi"];
            ImageField imageField300 = downloadTeaser.Fields["Image 300 dpi"];

            if (fileField != null && !string.IsNullOrWhiteSpace(fileField.Value))
            {                              
                var fileObject = new Dictionary<string, string>() {
                    { "format", "PDF" },
                    { "url", Sitecore.Resources.Media.MediaManager.GetMediaUrl(fileField.MediaItem) },
                    { "size", GetMediaItemSizeKB(fileField.MediaItem.ID.ToString()) + "kb" }                    
                };
                downloadObject.download = fileObject;            
            }
            else if (imageField72 != null && imageField300 != null)
            {
                var image72 = (imageField72.MediaItem != null) ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(imageField72.MediaItem) : "";
                var image300 = (imageField300.MediaItem != null) ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(imageField300.MediaItem) : "";
                var fileObject = new Dictionary<string, string>() {
                    { "format", "JPG" },
                    { "72-dpi", image72 },
                    { "300-dpi", image300 }
                };
            }
            else
            {
                downloadObject.download = false;
            }

            downloadObject.title = downloadTeaser["Title"];
            var categoryItem = ScValue.GetItem(downloadTeaser, "category");
            if (categoryItem != null)
            {
                downloadObject.category = ScValue.GetString(categoryItem, "Value");
            }

            return downloadObject;
        }

        public string GetMediaItemSizeKB(string mediaItemID)
        {
            Item item = Sitecore.Context.Database.GetItem(mediaItemID);            
            MediaItem mediaItem = new MediaItem(item);
            double kb = (double)mediaItem.Size / 1024 ;            
            return kb.ToString("##.#");
        }

        public dynamic fillEventItem(Item eventTeaser)
        {
            dynamic eventObject = new ExpandoObject();
            var image = ScValue.GetImage(eventTeaser, "Image");
            if (image != null)
            {
                dynamic imageObject = new ExpandoObject();
                imageObject.alt = image.Alt;
                imageObject.url = ScLink.GetUrl(image);
                eventObject.image = imageObject;
            }
            else
            {
                eventObject.image = false;
            }

            eventObject.title = eventTeaser["Title"];
            if (ScValue.GetDate(eventTeaser, "Date") > DateTime.MinValue.AddDays(1))
            {
                eventObject.date = string.Format("{0:dd}.{0:MM}.{0:yyyy}, {0:hh}:{0:mm} Uhr", ScValue.GetDate(eventTeaser, "Date"));
            }
            else
            {
                eventObject.date = "";
            }
            eventObject.place = eventTeaser["Place"];
            eventObject.description = eventTeaser["Description"];
            LinkField linkField = eventTeaser.Fields["Link"];
            if (!string.IsNullOrWhiteSpace(linkField.Value))
                eventObject.link = (linkField.LinkType == "internal") ? ScLink.GetUrl(linkField.TargetItem) : linkField.Url;

            return eventObject;
        }

        public dynamic fillJobItem(Item jobsTeaser)
        {
            dynamic jobsObject = new ExpandoObject();
            ImageField imageField = jobsTeaser.Fields["Image"];
            if (!string.IsNullOrWhiteSpace(imageField.Value))
            {
                dynamic imageObject = new ExpandoObject();
                imageObject.alt = imageField.Alt;
                imageObject.url = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imageField.MediaItem);
                jobsObject.image = imageObject;
            }
            else
            {
                jobsObject.image = false;
            }

            jobsObject.title = jobsTeaser["Title"];
            jobsObject.category = jobsTeaser["Category"];
            jobsObject.brand = jobsTeaser["Brand"];
            LinkField linkField = jobsTeaser.Fields["Link"];
            if (!string.IsNullOrWhiteSpace(linkField.Value))
                jobsObject.link = (linkField.LinkType == "internal") ? ScLink.GetUrl(linkField.TargetItem) : linkField.Url;
            return jobsObject;
        }

        public dynamic fillNewsItem(Item newsTeaser)
        {
            dynamic newsObject = new ExpandoObject();
            ImageField imageField = newsTeaser.Fields["Image"];
            if (!string.IsNullOrWhiteSpace(imageField.Value))
            {
                dynamic imageObject = new ExpandoObject();
                imageObject.alt = imageField.Alt;
                imageObject.url = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imageField.MediaItem);
                newsObject.image = imageObject;
            }
            else
            {
                newsObject.image = false;
            }

            newsObject.title = newsTeaser["Title"];
            newsObject.date = string.Format("{0:dd}.{0:MM}.{0:yyyy}", ScValue.GetDate(newsTeaser, "Date"));
            newsObject.category = "";
            var category = ScValue.GetItem(newsTeaser, "Bereich");
            if (category != null)
            {
                newsObject.category = ScValue.GetString(category, "Value");
            }
            newsObject.description = newsTeaser["Description"];
            LinkField linkField = newsTeaser.Fields["Link"];
            if (!string.IsNullOrWhiteSpace(linkField.Value))
                newsObject.link = (linkField.LinkType == "internal") ? ScLink.GetUrl(linkField.TargetItem) : linkField.Url;
            return newsObject;
        }

        public dynamic fillTextImageItem(Item textImageTeaser)
        {
            dynamic textImageObject = new ExpandoObject();
            ImageField imageField = textImageTeaser.Fields["Image"];
            if (!string.IsNullOrWhiteSpace(imageField.Value))
            {
                dynamic imageObject = new ExpandoObject();
                imageObject.alt = imageField.Alt;
                imageObject.url = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imageField.MediaItem);
                textImageObject.image = imageObject;
            }
            else
            {
                textImageObject.image = false;
            }

            textImageObject.title = textImageTeaser["Title"];
            textImageObject.description = textImageTeaser["Description"];
            LinkField linkField = textImageTeaser.Fields["Link"];
            if (!string.IsNullOrWhiteSpace(linkField.Value))
                textImageObject.link = (linkField.LinkType == "internal") ? ScLink.GetUrl(linkField.TargetItem) : linkField.Url;
            return textImageObject;
        }
    }
}