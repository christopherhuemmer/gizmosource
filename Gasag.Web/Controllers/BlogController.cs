﻿using Gasag.Web.Utility;
using Gizmo.General.Debug;
using Gizmo.SitecoreGizmo.Items;
using Newtonsoft.Json;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;

namespace Gasag.Web.Controllers
{
    public class BlogController : Controller
    {
        public ActionResult GetBlogs(int current, string[] tag)
        {
            dynamic obj = FindBlogPages(current, tag);
            string json = JsonConvert.SerializeObject(obj);
            json = json.Replace("desktopbig", "desktop-big");
            return Content(json, "application/json");
        }

        public static ExpandoObject FindBlogPages(int current, string[] tags)
        {
            var amount = ScSettings.Instance().GetInteger("Blog Overview Num Entries");   

            var blogTemplate = ScReference.Instance().GetItem("Blog Page Template").ID;

            var allPages = ScReference.GetItemRelativeToInstance("Home").Axes.GetDescendants()
                .Where(x => x.TemplateID == blogTemplate);
            var pages = allPages
                .Where(x => tags == null || !tags.Any() || tags.Intersect(ScValue.GetStrings(x, "Tags")).Any())
                .Skip(current).Take(amount);

            //var allPages = ScReference.GetItemRelativeToInstance("Home").Axes.GetDescendants().Where(x => x.TemplateID == blogTemplate);

            Log.Debug($"current = {current}, tag = {(tags != null ? string.Join(", ", tags) : "")}, allPages = {allPages.Count()}, pages = {pages.Count()}");

            //var pages = new List<Item>();
            //foreach (var singleTag in tags)
            //{
            //    if (!string.IsNullOrWhiteSpace(singleTag))
            //    {
            //        pages.AddRange(allPages.Where(x => ScValue.GetStrings(x, "Tags").Contains(singleTag)));
            //    }
            //    else
            //    {
            //        pages = allPages.ToList();
            //    }
            //}            
            //pages = pages.Skip(current).Take(amount).ToList();

            var results = new List<ExpandoObject>();
            foreach(var page in pages)
            {
                var image = ScValue.GetImage(page, "Image");
                var alt = ScValue.GetString(page, "Title");
                var isPortrait = false;
                if (image != null)
                {
                    ImageField field = page.Fields["Image"];
                    if (!string.IsNullOrWhiteSpace(field.Alt))
                    {
                        alt = field.Alt;
                    }
                    var width = 0;
                    if (!int.TryParse(field.Width, out width))
                    {
                        width = 0;
                    }
                    var height = 0;
                    if (!int.TryParse(field.Height, out height))
                    {
                        height = 0;
                    }
                    isPortrait = height > width;
                }
                var alignmentItem = ScValue.GetItem(page, "Image Alignment");
                var alignment = "";
                if (alignmentItem != null)
                {
                    alignment = ScValue.GetString(alignmentItem, "Value");
                }
                if (string.IsNullOrWhiteSpace(alignment))
                {
                    alignment = "center";
                }

                dynamic result = new ExpandoObject();
                result.title = ScValue.GetString(page, "Title");
                result.tag = string.Join(" ", ScValue.GetItems(page, "Tags").Select(x => string.Concat("#", x["Value"])));
                result.url = ScLink.GetUrl(page);

                result.image = new ExpandoObject();
                result.image.align = alignment;
                result.image.alt = alt;
                result.image.port = isPortrait;

                var mediaUrlOptions = (MediaUrlOptions)MediaUrlOptions.Empty;

                mediaUrlOptions.Width = isPortrait ? 940 : 1140;
                result.image.desktopbig = ScLink.GetUrl(image, mediaUrlOptions);

                mediaUrlOptions.Width = isPortrait ? 460 : 660;
                result.image.desktop = ScLink.GetUrl(image, mediaUrlOptions);

                mediaUrlOptions.Width = isPortrait ? 650 : 850;
                result.image.tablet = ScLink.GetUrl(image, mediaUrlOptions);

                mediaUrlOptions.Width = isPortrait ? 730 : 930;
                result.image.mobile = ScLink.GetUrl(image, mediaUrlOptions);

                results.Add(result);
            }

            dynamic obj = new ExpandoObject();
            obj.lastTeaser = allPages.Count() <= current + amount;
            obj.results = results;
            return obj;
        }
    }
}