﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Gizmo.SitecoreGizmo.FormValidator;
using Gizmo.Web.Mvc;
using Sitecore.Diagnostics;
using Sitecore.Mvc.Presentation;
using Gasag.Web.General;
using Gasag.Web.Models;
using Gizmo.SitecoreGizmo.FormValidator.Actions;
using Gizmo.SitecoreGizmo.Items;
using System.Xml.Linq;
using Sitecore.Data;
using Sitecore.Data.Items;
using System.Text;
using System.Collections;
using Gasag.Web.Models.Form;

namespace Gasag.Web.Controllers
{
    public class FormsController : Controller
    {
        public static readonly string NameFormId = "_FormId";

        public ActionResult Default()
        {
            var model = new FormModel();
            model.Rendering = RenderingContext.Current.Rendering;
            model.Item = model.Rendering.Item;
            if (model.Item != null && model.Item.TemplateID != ScReference.Instance().GetItem("Form Template").ID)
            {
                model.Item = null;
            }
            if (model.Item == null)
            {
                return View(model);
            }

            model.FormId = model.Item.ID.Guid;
            if (!string.IsNullOrWhiteSpace(Request[NameFormId]) && Guid.Parse(Request[NameFormId]) == model.FormId)
            {
                var html = this.RenderRazorViewToString("/Views/Forms/Default.cshtml", model);
                var errorMessages = new ErrorMessageConfig();

                List<string> actionInfos = new List<string>();

                Sitecore.Data.Fields.MultilistField multi = model.Item.Fields["Form Actions"];
                var sendMailActions = multi.GetItems()?.Where(x => x.TemplateID == ScReference.Instance().GetItem("Send Mail Action Template").ID);
                foreach(Item sendMailAction in sendMailActions)
                {
                    if (sendMailAction != null)
                    {
                        var to = sendMailAction["To"];
                        var from = sendMailAction["From"];
                        var subject = sendMailAction["Subject"];
                        var headline = sendMailAction["Headline"];
                        var cc = sendMailAction["CC"];
                        var bcc = sendMailAction["BCC"];
                        var replyTo = sendMailAction["ReplyTo"];
                        actionInfos.Add(ActionInfoConfig.SendEmail(to, from, subject, "form_kvp.xsl", headline, cc, bcc, replyTo));
                    }
                }
                

                //var saveToBucketAction = multi.GetItems()?.Where(x => x.TemplateID == ScReference.Instance().GetItem("Save to Bucket Action Template").ID).FirstOrDefault();
                //if (saveToBucketAction != null)
                //{
                //    actionInfos.Add(ActionInfoConfig.SaveToBucket(saveToBucketAction["Target Folder"]));
                //}

                var validator = new GasagFormValidator(
                    html,
                    model.FormId.ToString(),
                    errorMessages.ToDictionary(),
                    actionInfos.ToArray(),
                    ScLink.GetUrl(model.Item, "Error Link"),
                    ScLink.GetUrl(model.Item, "Success Link")
                );
                //validator.AddFormActionHandler("savetobucket", new SaveToBucketAction());
                validator.SetFormData(System.Web.HttpContext.Current.Request);

                if (validator.Validate())
                {
                    if (validator.DoActions())
                    {
                        if (validator.HasSuccessUrl)
                        {
                            return Redirect(validator.SuccessUrl);
                        }
                    }
                    else if (validator.HasErrorUrl)
                    {
                        if (validator.LastException != null)
                        {
                            Log.Error(string.Format("Exception while validating form {0}", model.FormId), validator.LastException);
                        }
                        return Redirect(validator.ErrorUrl);
                    }
                }

                return Content(validator.GetPageHTML());
            }
            else
            {
                return View(model);
            }
        }

        public static List<string[]> CreateFormDataGrid(Item item, DateTime? from = null, DateTime? to = null)
        {
            List<string> attributeNames = new List<string>();
            var resultList = new List<string[]>();
            var dictList = ConvertFormDataToDictionaries(item, ref attributeNames);
            foreach (var attribute in attributeNames)
            {
                var x = 0;
                var attrArray = new string[dictList.Count() + 1];
                attrArray[x] = attribute;
                foreach (var dict in dictList)
                {
                    attrArray[++x] = (dict.ContainsKey(attribute)) ? dict[attribute] : string.Empty;
                }
                resultList.Add(attrArray);
            }
            return resultList;
        }

        private static List<Dictionary<string, string>> ConvertFormDataToDictionaries(Item item, ref List<string> attributeNames, DateTime? from = null, DateTime? to = null)
        {
            if (from == null) { from = DateTime.MinValue; }
            if (to == null) { to = DateTime.MaxValue; }
            var formDataItems = item
                .Axes
                .GetDescendants()
                .Where(x => x.Statistics.Created <= to && x.Statistics.Created >= from)
                .Where(x => x.TemplateID == ScReference.Instance().GetItem("Form Data Template").ID);
            List<Dictionary<string, string>> dictList = new List<Dictionary<string, string>>();
            foreach (var formData in formDataItems)
            {
                var formDateDictionary = new Dictionary<string, string>();
                var xmlAsString = formData["Content"];
                var xmlRoot = XDocument.Parse(xmlAsString).Root;
                foreach (var node in xmlRoot.Elements("item"))
                {
                    if (!attributeNames.Contains(node.Attribute("name").Value)) attributeNames.Add(node.Attribute("name").Value);
                    formDateDictionary.Add(node.Attribute("name").Value, node.Value);
                }
                dictList.Add(formDateDictionary);
            }
            return dictList;
        }

        public static StringBuilder DownloadCsv(Item item, DateTime? from = null, DateTime? to = null)
        {
            List<string> attributeNames = new List<string>();
            var dictionary = ConvertFormDataToDictionaries(item, ref attributeNames);
            var uniqueNames = new HashSet<string>();
            foreach (var dict in dictionary)
            {
                foreach (var k in dict.Keys)
                {
                    uniqueNames.Add(k);
                }
            }
            var colNames = uniqueNames.ToList();

            StringBuilder sb = new StringBuilder();
            string oneVal;

            sb.AppendLine(string.Join(";", colNames));
            string[] buffer = new string[colNames.Count];
            int i = 0;
            foreach (var data in dictionary)
            {
                i = 0;
                foreach (var k in colNames)
                {
                    data.TryGetValue(k, out oneVal);
                    buffer[i++] = oneVal ?? string.Empty;
                }
                sb.AppendLine(string.Join(";", buffer));
                //string csv = string.Join(Environment.NewLine,data.Select(d => d.Key + ";" + d.Value + ";"));
                //resultCsv += csv;
            }
            return sb;
            //System.Web.HttpContext.Current.Response.Write(sb);
        }

        public ActionResult Test()
        {
            //var model = new FormModel();
            //model.FormId = RenderingContext.Current.Rendering.UniqueId;

            //if (!string.IsNullOrWhiteSpace(Request["_FormId"]) && Guid.Parse(Request["_FormId"]) == model.FormId)
            //{
            //    var html = this.RenderRazorViewToString("/Views/Forms/Test.cshtml", model);
            //    var errorMessages = new ErrorMessageConfig();
            //    string[] actionInfos = new string[] 
            //    {

            //    };

            //    var validator = new ZwickFormValidator(html, model.FormId.ToString(), errorMessages.ToDictionary(), actionInfos);
            //    validator.SetFormData(System.Web.HttpContext.Current.Request);

            //    if (validator.Validate())
            //    {
            //        if (validator.DoActions())
            //        {
            //            if (validator.HasSuccessUrl)
            //            {
            //                return Redirect(validator.SuccessUrl);
            //            }
            //        }
            //        else if (validator.HasErrorUrl)
            //        {
            //            if (validator.LastException != null)
            //            {
            //                Log.Error(string.Format("Exception while validating form {0}", model.FormId), validator.LastException);
            //            }
            //            return Redirect(validator.ErrorUrl);
            //        }
            //    }

            //    return Content(validator.GetPageHTML());
            //}
            //else
            {
                return View();
            }
        }
    }
}