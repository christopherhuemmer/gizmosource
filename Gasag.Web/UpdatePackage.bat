:: Parameter
SET Target=%USERPROFILE%\Desktop\Gasag-Update-%DATE%

:: Move files
robocopy "%~dp0/App_Config/Include/ZZZ.Gasag" "%Target%/App_Config/Include/ZZZ.Gasag" "AppSettings.config" "Assets.config" "LinkManager.config" "Pipelines.config" "ResetDictionaryCache.config" "RichTextEditorStyles.config" "MediaTypes.config"  "Sitecore.Support.88491.config" /mir
robocopy "%~dp0/App_Config/Include" "%Target%/App_Config/Include" "Sitecore.Commerce.config" /mir
robocopy "%~dp0/App_Code" "%Target%/App_Code" /mir
robocopy "%~dp0/App_Data" "%Target%/App_Data" /mir
robocopy "%~dp0/bin" "%Target%/bin" /mir
robocopy "%~dp0/css" "%Target%/css" /mir
robocopy "%~dp0/fonts" "%Target%/fonts" /mir
robocopy "%~dp0/img" "%Target%/img" /mir
robocopy "%~dp0/js" "%Target%/js" /mir
robocopy "%~dp0/sitecore" "%Target%/sitecore" /e
robocopy "%~dp0/Views" "%Target%/Views" /mir
robocopy "%~dp0/" "%Target%/" "f5-testpage.html"