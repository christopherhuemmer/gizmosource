﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Gizmo.Web;
using Sitecore.Configuration;

namespace Gasag.Web.Utility
{
    /// <summary>
    /// Helper for assets like js and css files.
    /// </summary>
    public static class AssetUtil
    {
        private static readonly string AssetDelimiter = ",";
        private static readonly string AssetAsyncDelimiter = " ";

        /// <returns>List of all relative and absolute css files.</returns>
        public static IEnumerable<Asset> CssPaths()
        {
            return ParseAssets(Settings.GetSetting("Assets.Css"));
        }

        /// <returns>List of all relative and absolute javascript files for the body.</returns>
        public static IEnumerable<Asset> JavascriptBodyPaths()
        {
            return ParseAssets(Settings.GetSetting("Assets.Javascript.Body"));
        }

        /// <returns>List of all relative and absolute javascript files for the head after css.</returns>
        public static string JavascriptHeadAfterCssPaths()
        {
            return Settings.GetSetting("Assets.Javascript.HeadAfterCss");
        }

        /// <returns>List of all relative and absolute javascript files for the head before css.</returns>
        public static IEnumerable<Asset> JavascriptHeadBeforeCssPaths()
        {
            return ParseAssets(Settings.GetSetting("Assets.Javascript.HeadBeforeCss"));
        }

        /// <summary>
        /// Parse assets string.
        /// </summary>
        /// <param name="assets">The assets.</param>
        /// <returns>List of splitted asset entries.</returns>
        private static IEnumerable<Asset> ParseAssets(string assets)
        {
            return assets.Split(new string[] { AssetDelimiter }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.Split(new string[] { AssetAsyncDelimiter }, StringSplitOptions.RemoveEmptyEntries))
                .Select(x => x.Select(y => y.Trim()).ToList())
                .Select(x =>
                    new Asset()
                    {
                        Path = NamedFormat.Do(x[0], new { Language = LangUtil.ContentLanguage() }),
                        Attributes = x.Skip(1)
                    });
        }
    }

    /// <summary>
    /// An asset like css or js.
    /// </summary>
    public class Asset
    {
        public static readonly string AsyncAttribute = "async";
        public static readonly string ModernizrAttribute = "modernizr";
        public static readonly string RequireAttribute = "require";

        public IEnumerable<string> Attributes { get; set; }
        public string Path { get; set; }
    }
}