﻿using Sitecore.Configuration;
using Gizmo.General.String;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Gizmo.SitecoreGizmo.Items;

namespace Gasag.Web.Utility
{
    /// <summary>
    /// Get sitecore instance related information.
    /// </summary>
    public class Instance
    {
        public static bool IsDev
        {
            get
            {
                return Settings.GetSetting("Instance.Dev", "0").ToBool();
            }
        }

        public static bool IsEditor
        {
            get
            {
                return Settings.GetSetting("Instance.Editor", "0").ToBool();
            }
        }

        public static bool IsStage
        {
            get
            {
                return Settings.GetSetting("Instance.Stage", "0").ToBool();
            }
        }

        public static bool IsWeb
        {
            get
            {
                return Settings.GetSetting("Instance.Web", "0").ToBool();
            }
        }

        public static bool IsEdl
        {
            get
            {
                return ScSettings.Instance().GetString("Body Class") == "edl";
            }
        }
    }
}