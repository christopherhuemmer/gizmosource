﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gasag.Web.Utility
{
    /// <summary>
    /// Language related helper functions.
    /// </summary>
    public static class LangUtil
    {
        /// <summary>
        /// The content language aka. cms language..
        /// </summary>
        /// <returns></returns>
        public static string ContentLanguage()
        {
            return Sitecore.Context.Language.Name;
        }
    }
}