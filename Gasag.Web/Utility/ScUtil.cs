﻿using Gizmo.SitecoreGizmo.Items;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Data.Fields;
using Sitecore.SecurityModel;

namespace Gasag.Web.Utility
{
    /// <summary>
    /// Sitecore related utility specific to Gasag.
    /// </summary>
    public static class ScUtil
    {
        /// <summary>
        /// Returns a list of all page template ids.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ID> AllPageTemplateIds()
        {
            var reference = ScReference.Instance();
            var result = new List<ID>()
            {
                reference.GetItem("Blog Overview Page Template").ID,
                reference.GetItem("Blog Page Template").ID,
                reference.GetItem("Generic Content Page Template").ID,
                reference.GetItem("Generic Overview Page Template").ID,
                reference.GetItem("Home Page Template").ID,
                reference.GetItem("Landing Page Template").ID,
                reference.GetItem("Search Page Template").ID
            };
            return result;
        }

        /// <summary>
        /// Checks if an item is a navigatable page.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static bool IsNavigationPage(Item item, IEnumerable<ID> allPageTemplateIds)
        {
            var result = false;
            if (allPageTemplateIds.Contains(item.TemplateID))
            {
                var naviTypeItem = ScValue.GetItem(item, "Type");
                result = naviTypeItem != null && ScValue.GetString(naviTypeItem, "Value") != "not-in-navi";
            }
            return result;
        }

        /// <summary>
        /// Get a sitecore service api url for a certain controller and its method.
        /// </summary>
        /// <param name="controller">The controller name.</param>
        /// <param name="method">The method.</param>
        /// <returns>The url.</returns>
        public static string ServiceUrl(string controller, string method, bool addLanguage = true, Dictionary<string, string> parameters = null)
        {
            if (controller.EndsWith("Controller"))
            {
                controller = controller.Replace("Controller", string.Empty);
            }

            if (parameters == null)
            {
                parameters = new Dictionary<string, string>();
            }

            var url = string.Format("/service/{0}/{1}", controller, method);
            if (addLanguage)
            {
                parameters["language"] = LangUtil.ContentLanguage();
            }
            if (parameters.Any())
            {
                url += "?";
                url += string.Join("&", parameters.Select(x => string.Join("=", x.Key, x.Value)));
            }
            return url;
        }


    }


    public class RemoveUnwantedTags
    {
        public void OnItemSaving(object sender, EventArgs args)
        {
            var eventArgs = args as SitecoreEventArgs;
            if (eventArgs == null) return;

            var item = eventArgs.Parameters[0] as Item;
            if (item == null) return;

            foreach (Sitecore.Data.Fields.Field field in item.Fields)
            {
                if (field.TypeKey.Equals("rich text", StringComparison.InvariantCultureIgnoreCase))
                {
                    var content = field.Value;
                    if (!string.IsNullOrEmpty(content))
                    {
                        // Remove <p> from <strong>
                        content = content.Replace("<p><strong>", "<strong>").Replace("</strong></p>", "</strong>");

                        // Correct lists.
                        content = content.Replace("<ul>", @"<ul class=""list"">");

                        // Save
                        field.Value = content;
                    }
                }

            }
        }
    }
}