﻿namespace Gasag.Web.Models.TariffCalculator
{
    public class Payment
    {
        public string AccountHolder { get; set; }

        public string IBAN { get; set; }

        public string CouponCode { get; set; }

        public bool AuthorizationIBAN { get; set; }

        public bool IsIbanProcessing { get; set; }
    }
}