﻿namespace Gasag.Web.Models.TariffCalculator
{
    public class OrderRequest
    {
        public Customer Customer { get; set; }

        public Date CustomerBirthDate { get; set; }

        public Address Address { get; set; }

        public Customer DeviantInvoiceCustomer { get; set; }

        public Address DeviantInvoiceAddress { get; set; }

        public Delivery Delivery { get; set; }

        public Date DeliveryDate { get; set; }

        public Payment Payment { get; set; }

        public bool AgbChecked { get; set; }

        public string SuccessPageElectricity { get; set; }

        public string SuccessPageGas { get; set; }

        public string Type { get; set; }

        public string ErrorPage { get; set; }
        public AddressObject ParsedAddress { get; set; }
    }
}