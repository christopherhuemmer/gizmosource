﻿namespace Gasag.Web.Models.TariffCalculator
{
    public class Customer
    {
        public string Salutation { get; set; }

        public string Title { get; set; }

        public string BusinessPartnerType { get; set; }

        public string Company { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }
    }
}