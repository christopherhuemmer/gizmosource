﻿namespace Gasag.Web.Models.TariffCalculator
{
    public class Address
    {
        public string Street { get; set; }

        public string HouseNumber { get; set; }

        public string HouseNumberAppendix { get; set; }

        public string Zip { get; set; }

        public string City { get; set; }
    }
}