﻿using Newtonsoft.Json;
using Sitecore.Data.Items;

namespace Gasag.Web.Models.TariffCalculator
{
    public class Login
    {
        [JsonIgnore]
        public Item Item { get; set; }

        public Tariff SelectedTariff { get; set; }
    }
}