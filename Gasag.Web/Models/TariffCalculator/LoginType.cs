﻿namespace Gasag.Web.Models.TariffCalculator
{
    public enum LoginType
    {
        ExistingCustomerWithAccess,
        ExistingCustomerWithoutAccess,
        NewCustomer
    }
}