﻿using Newtonsoft.Json;
using Sitecore.Data.Items;
using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Gasag.Web.Models.TariffCalculator
{
    public class Calculator
    {
        public string TariffTeaser { get; set; }

        [Required]
        public double Consumption { get; set; }

        [JsonIgnore]
        public Item Item { get; set; }

        [Required]
        public string Division { get; set; }

        [Required]
        public string NextPage { get; set; }

        public string ErrorPage { get; set; }

        [Required]
        public string Zip { get; set; }

        public IEnumerable<string> ZipCities { get; set; }

        public HtmlString ZipErrorMessage { get; set; }

        public string Type { get; set; }

        public string ProductId { get; set; }

        public string CampaignId { get; set; }

        public string SalesPartnerId { get; set; }

        public string TarifGroup { get; set; }

        public string SalesChannel { get; set; }
    }
}