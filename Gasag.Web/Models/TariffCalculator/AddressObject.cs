﻿namespace Gasag.Web.Models.TariffCalculator
{
    public class AddressObject
    {
        public string City { get; set; }

        public string CityCode { get; set; }

        public string CityPart { get; set; }

        public string CityPartCode { get; set; }

        public string Street { get; set; }

        public string StreetCode { get; set; }
    }
}