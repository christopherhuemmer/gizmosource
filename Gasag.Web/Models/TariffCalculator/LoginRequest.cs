﻿using System.ComponentModel.DataAnnotations;

namespace Gasag.Web.Models.TariffCalculator
{
    public class LoginRequest
    {
        [Required]
        public string NextPage { get; set; }

        public string ErrorPage { get; set; }
    
        [Required]
        public LoginType LoginType { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}