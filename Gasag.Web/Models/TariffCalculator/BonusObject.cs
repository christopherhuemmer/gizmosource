﻿using System;
using System.Linq;

namespace Gasag.Web.Models.TariffCalculator
{
    public class BonusObject
    {
        public string Title { get; set; }

        public string Bonus { get; set; }

        public int BonusValue
        {
            get
            {
                int result;
                return int.TryParse(Bonus, out result) ? result : 0;
            }
        }

        public string IsuBonus { get; set; }

        // The following function is probably not usable because the value format is too variable.
        //public int IsuBonusValue
        //{
        //    get
        //    {
        //        var value = IsuBonus.Split('#').FirstOrDefault(x => x.ToLower().StartsWith("sofort"))?.Split(':').LastOrDefault();
        //        int result;
        //        return value != null && int.TryParse(value, out result) ? result : 0;
        //    }
        //}

        public string PromoCode { get; set; }

        public DateTime? ActivationDate { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public int? ZipFrom { get; set; }

        public int? ZipTo { get; set; }

        public double? ConsumptionFrom { get; set; }

        public double? ConsumptionTo { get; set; }
    }
}