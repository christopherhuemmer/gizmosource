﻿using Gasag.Web.LoginService;
using Gasag.Web.MasterDataCatalogueService;
using Newtonsoft.Json;
using Sitecore.Data.Items;

namespace Gasag.Web.Models.TariffCalculator
{
    public class Order
    {
        [JsonIgnore]
        public Item Item { get; set; }

        public Tariff SelectedTariff { get; set; }

        public masterDataVO MasterData { get; set; }

        public Calculator Calculator { get; set; }

        public logInVO LoginResponse { get; set; }

        public ZipObject ZipObject { get; set; }

        public string EntryCalculatorPage { get; set; }

        public ZipCodeCatalogueService.zipCodeVO[] Cities { get; set; }

        public StreetCodeCatalogueService.streetVO[] Streets { get; set; }
    }
}