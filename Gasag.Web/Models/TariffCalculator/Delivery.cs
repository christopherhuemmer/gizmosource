﻿namespace Gasag.Web.Models.TariffCalculator
{
    public class Delivery
    {
        public bool IsNewEntry { get;set;}

        public string CounterNumber { get; set; }

        public string CounterValue { get; set; }

        public string Supplier { get; set; }
    }
}