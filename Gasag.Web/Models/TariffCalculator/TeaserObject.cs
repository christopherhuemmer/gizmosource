﻿using System.Collections.Generic;

namespace Gasag.Web.Models.TariffCalculator
{
    public class TeaserObject
    {
        public BonusObject Bonus { get; set; }

        public string MainTariffGroup { get; set; }

        public IList<string> OptionalTariffs { get; set; }
    }
}