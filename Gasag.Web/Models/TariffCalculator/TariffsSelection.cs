﻿using Sitecore.Data.Items;
using System.Collections.Generic;

namespace Gasag.Web.Models.TariffCalculator
{
    public class TariffsSelection
    {
        public Calculator Calculator { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public Item Item { get; set; }

        public List<Tariff> Tariffs { get; set; }

        public string EntryCalculatorPage { get; set; }
    }
}