﻿using System.ComponentModel.DataAnnotations;

namespace Gasag.Web.Models.TariffCalculator
{
    public class SelectTariffRequest
    {
        [Required]
        public string NextPage { get; set; }

        public string ErrorPage { get; set; }

        [Required]
        public string ProductId { get; set; }
    }
}