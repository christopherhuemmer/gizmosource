﻿using Gasag.Web.ZipCodeCatalogueService;

namespace Gasag.Web.Models.TariffCalculator
{
    public class ZipObject
    {
        public string[] ZipParts { get; set; }

        public zipCodeVO ZipCodeResponse { get; set; }
    }
}