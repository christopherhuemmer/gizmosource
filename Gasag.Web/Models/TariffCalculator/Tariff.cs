﻿using Gasag.Web.ProductCheckService;
using Sitecore.Data.Items;
using System.Web;

namespace Gasag.Web.Models.TariffCalculator
{
    public class Tariff
    {
        public BonusObject Bonus { get; set; }

        public double BasePrice { get; set; } 

        public bool IsMain { get; set; }

        public double WorkingPrice { get; set; }

        public double MinimumConsumption { get; set; }

        public string MinimumContractDuration { get; set; }

        public string MinimumContractDurationUnit { get; set; }

        public string PriceProtectionPeriod { get; set; }

        public string PriceProtectionPeriodUnit { get; set; }

        public HtmlString Currency { get; set; }

        public string Name { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public Item Item { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public Item TariffItem { get; set; }

        public portfolioVO Portfolio { get; set; }

        public double PricePerYear { get; set; }
    }
}