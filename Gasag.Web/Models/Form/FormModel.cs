﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;

namespace Gasag.Web.Models.Form
{
    public class FormModel
    {
        public Guid FormId { get; set; }
        public Item Item { get; set; }
        public Rendering Rendering { get; set; }
    }
}