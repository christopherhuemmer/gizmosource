﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gasag.Web.Models.Search
{
    public class GasagSearchResultCategory
    {
        public string LoadNextUrl { get; set; }

        public int TotalResults { get; set; }

        public List<GasagSearchResultItem> Results { get; set; }
    }
}