﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gasag.Web.Models.Search
{
    public class GasagSearchResultItem
    {
        public string Title { get; set; }

        public string Date { get; set; }

        public string Category { get; set; }

        public string Description { get; set; }

        public string Url { get; set; }
    }
}