﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Gizmo.Web.FormValidator;
using HtmlAgilityPack;

namespace Gasag.Web.General
{
    public class GasagFormValidator : HtmlFormValidator
    {
        public GasagFormValidator(string html, string formId, Dictionary<string, string> controlData)
            : base(html, formId, controlData)
        { }

        public GasagFormValidator(string html, string formId, Dictionary<string, string> errorMessages, string[] actionInfos,
            string errorRedirect = null, string successRedirect = null)
            : base(html, formId, errorMessages, actionInfos, errorRedirect, successRedirect)
        { }

        protected override void AddErrorMessage(HtmlFormField formField)
        {
            //var cssClass = formField.Node.Attributes["class"].Value;
            //formField.Node.Attributes.Remove("class");
            //formField.Node.Attributes.Add("class", cssClass + " error");
            //cssClass = formField.Node.ParentNode.Attributes["class"].Value;
            //formField.Node.ParentNode.Attributes.Remove("class");
            //formField.Node.ParentNode.Attributes.Add("class", cssClass + " error");            
        }

        protected override void AddErrorMessage(HtmlFormFieldGroup formFieldGroup)
        {
            //formFieldGroup.Fields.First().Node.Attributes.Add("class", "error");            
        }
    }
}