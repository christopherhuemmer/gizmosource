﻿using Gizmo.General.Caching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gasag.Web.General
{
    public class ZanoxOrder
    {
        public string Cid { get; set; }
        public string CustomerId { get; set; }
        public string OrderId { get; set; }
        public string CurrencySymbol { get; set; }
        public string TotalPrice { get; set; }
        public string PartnerId { get; set; }

        public static ZanoxOrder GetForCurrentUser()
        {
            return Cache.Get(CacheKey()) as ZanoxOrder;
        }

        public void SetForCurrentUser()
        {
            Cache.Set(CacheKey(), this, CacheTimeout());
        }

        private static string CacheKey()
        {
            return $"TariffCalculator.ZanoxOrder.User.{HttpContext.Current.Session.SessionID}";
        }

        private static int CacheTimeout()
        {
            return 10; // Minutes
        }
    }
}