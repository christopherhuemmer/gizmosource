﻿using Gizmo.SitecoreGizmo.Items;
using Sitecore.ContentSearch;
using System.Xml;

namespace Gasag.Web.Lucene.Indexes
{
    public class PageItemCategoryField : BaseComputedField
    {
        protected virtual string ItemFieldName { get; set; }

        protected virtual string NestedFieldName { get; set; }

        protected virtual string DefaultCategory { get; set; }

        public override object ComputeFieldValue(IIndexable indexable)
        {
            var sitecoreIndexableItem = indexable as SitecoreIndexableItem;
            var category = GetCategoryValue(sitecoreIndexableItem: sitecoreIndexableItem, itemFieldName: ItemFieldName, nestedFieldName: NestedFieldName);
            return string.IsNullOrWhiteSpace(category) ? null : category;
        }

        public PageItemCategoryField() : this(null)
        {
        }

        public PageItemCategoryField(XmlNode configurationNode)
        {
            Initialize(configurationNode: configurationNode);
        }

        protected override void Initialize(XmlNode configurationNode)
        {
            if (configurationNode == null)
            {
                return;
            }
            ItemFieldName = GetParameterValue(configurationNode: configurationNode, parameterName: "itemFieldName", attributeName: "value", defaultValue: "Search Category");
            NestedFieldName = GetParameterValue(configurationNode: configurationNode, parameterName: "nestedFieldName", attributeName: "value", defaultValue: "Key");
            DefaultCategory = GetParameterValue(configurationNode: configurationNode, parameterName: "defaultCategory", attributeName: "value", defaultValue: "company");
        }

        
        protected virtual string GetCategoryValue(SitecoreIndexableItem sitecoreIndexableItem, string itemFieldName, string nestedFieldName)
        {
            if (sitecoreIndexableItem == null || 
                sitecoreIndexableItem.Item == null || 
                sitecoreIndexableItem.Item.Visualization == null || 
                sitecoreIndexableItem.Item.Visualization.Layout == null ||
                sitecoreIndexableItem.Item.Fields[itemFieldName] == null) return null;
            var categoryItem = ScValue.GetItem(sitecoreIndexableItem.Item, itemFieldName);
            var result = categoryItem != null ? ScValue.GetString(categoryItem, nestedFieldName) : DefaultCategory;
            return GetComputedNonSeparatedIndexValue(result);
        }
    }
}