﻿using Gizmo.SitecoreGizmo.Items;
using Sitecore.ContentSearch;
using System.Xml;

namespace Gasag.Web.Lucene.Indexes
{
    public class ShowInSearchField : BaseComputedField
    {
        protected virtual string ItemFieldName { get; set; }

        public override object ComputeFieldValue(IIndexable indexable)
        {
            var sitecoreIndexableItem = indexable as SitecoreIndexableItem;
            var value = GetValue(sitecoreIndexableItem: sitecoreIndexableItem, itemFieldName: ItemFieldName);
            return value;
        }

        protected virtual string GetValue(SitecoreIndexableItem sitecoreIndexableItem, string itemFieldName)
        {
            if (sitecoreIndexableItem == null ||
                sitecoreIndexableItem.Item == null ||
                sitecoreIndexableItem.Item.Visualization == null ||
                sitecoreIndexableItem.Item.Visualization.Layout == null ||
                sitecoreIndexableItem.Item.Fields[itemFieldName] == null) return null;
            var value = ScValue.GetBool(item: sitecoreIndexableItem.Item, fieldKey: itemFieldName);
            return value ? null: "ShowInSearch";
        }

        public ShowInSearchField() : this(configurationNode: null)
        {
        }

        public ShowInSearchField(XmlNode configurationNode)
        {
            Initialize(configurationNode: configurationNode);
        }

        protected override void Initialize(XmlNode configurationNode)
        {
            if (configurationNode == null)
            {
                return;
            }
            ItemFieldName = GetParameterValue(configurationNode: configurationNode, parameterName: "itemFieldName", attributeName: "value", defaultValue: "Hide from Search");
        }
    }
}