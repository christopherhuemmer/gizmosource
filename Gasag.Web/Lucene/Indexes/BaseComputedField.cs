﻿using Gizmo.General.Debug;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using System.Collections.Generic;
using System.Xml;

namespace Gasag.Web.Lucene.Indexes
{
    public abstract class BaseComputedField : IComputedIndexField
    {
        public string FieldName { get; set; }
        public string ReturnType { get; set; }

        public abstract object ComputeFieldValue(IIndexable indexable);

        protected virtual void Initialize()
        {
            Initialize(configurationNode: null);
        }

        protected virtual void Initialize(XmlNode configurationNode)
        {
        }

        protected virtual string GetParameterValue(XmlNode configurationNode, string parameterName, string attributeName, string defaultValue)
        {
            if (configurationNode.ChildNodes.Count > 0)
            {
                var xmlNode = configurationNode.SelectSingleNode(parameterName);
                if (xmlNode != null && xmlNode.Attributes != null && xmlNode.Attributes[attributeName] != null)
                {
                    string value = xmlNode.Attributes[attributeName].Value;
                    if (!string.IsNullOrEmpty(value)) return value;
                }
            }
            Log.Error(string.Format("[Gasag CustomComputedField]<{0}> configuration error: \"{1}\" attribute in pageItemCategoryField section cannot be empty.", parameterName, attributeName), this);
            return defaultValue;
        }

        protected string GetComputedNonSeparatedIndexValue(string value)
        {
            if (string.IsNullOrWhiteSpace(value)) return null;
            return value.Replace(" ", "_").Replace("-", "_").ToLowerInvariant();
        }

        protected virtual List<string> GetParameterValues(XmlNode configurationNode, string parameterName, string nestedParameterName, string attributeName)
        {
            var result = new List<string>();
            if (configurationNode.ChildNodes.Count > 0)
            {
                var xmlNode = configurationNode.SelectSingleNode(parameterName);
                if (xmlNode != null)
                {
                    var nestedXmlNodes = xmlNode.SelectNodes(nestedParameterName);
                    if (nestedXmlNodes != null)
                    {
                        foreach (XmlNode nestedXmlNode in nestedXmlNodes)
                        {
                            string value = nestedXmlNode.Attributes[attributeName].Value;
                            if (!string.IsNullOrEmpty(value)) result.Add(value);
                        }
                    }
                }
            }
            return result;
        }
    }
}