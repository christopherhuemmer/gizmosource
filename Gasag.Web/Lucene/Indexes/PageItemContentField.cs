﻿using Gasag.Web.Utility;
using Gizmo.General.Debug;
using Gizmo.SitecoreGizmo.Items;
using HtmlAgilityPack;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.ContentSearch;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Layouts;
using Sitecore.Links;
using Sitecore.Sites;
using Sitecore.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace Gasag.Web.Lucene.Indexes
{
    public class PageItemContentField : BaseComputedField
    {
        private static Regex DulicateSpacesRegex = new Regex(@"\s+", RegexOptions.Compiled);

        protected virtual string ItemSearchContentAttribute { get; set; }

        protected virtual string UrlScheme { get; set; }

        protected virtual List<string> ItemFieldNames { get; set; }

        /// <summary>
        /// The site name to call. This is a site accessable from the server. Not every site may be accessable locally.
        /// </summary>
        protected virtual SiteInfo Site { get; set; }

        /// <summary>
        /// Will be added to SiteName as query parameter "sc_site" to retrive real site.
        /// </summary>
        protected virtual SiteInfo RealSite { get; set; }

        protected virtual IEnumerable<ID> PageTemplates { get; set; }

        public override object ComputeFieldValue(IIndexable indexable)
        {
            if (Site == null)
            {
                return null;
            }

            var sitecoreIndexable = indexable as SitecoreIndexableItem;

            if (sitecoreIndexable == null || sitecoreIndexable.Item == null)
            {
                //Log.Debug("PageItemContentField.ComputeFieldValue(): null");
                return null;
            }

            if (!PageTemplates.Contains(sitecoreIndexable.Item.TemplateID))
            {
                //Log.Debug($"PageItemContentField.ComputeFieldValue(): Unknown template for ID: {sitecoreIndexable.Item.ID} Name: {sitecoreIndexable.Item.Name} Path: {sitecoreIndexable.Item.Paths.FullPath} Language: {sitecoreIndexable.Item.Language.Name}");
                return null;
            }

            var startPath = Site.RootPath;
            if (RealSite != null)
            {
                startPath = RealSite.RootPath;
            }
            startPath = startPath.ToLower();

            if (!sitecoreIndexable.Item.Paths.FullPath.ToLower().StartsWith(startPath))
            {
                //Log.Debug($"PageItemContentField.ComputeFieldValue(): Not in start path for ID: {sitecoreIndexable.Item.ID} Name: {sitecoreIndexable.Item.Name} Path: {sitecoreIndexable.Item.Paths.FullPath} Language: {sitecoreIndexable.Item.Language.Name}");
                return null;
            }

            var content = GetItemContent(sitecoreIndexable.Item);
            //Log.Debug($"PageItemContentField.ComputeFieldValue(): ID: {sitecoreIndexable.Item.ID} Name: {sitecoreIndexable.Item.Name} Path: {sitecoreIndexable.Item.Paths.FullPath} Language: {sitecoreIndexable.Item.Language.Name} " + (content != null && content.Count() > 0 ? "content:" + string.Join(" ", content) : "[no content]"));
            return content != null & content.Count() > 0 ? string.Join(" ", content) : null;
        }

        public PageItemContentField() : this(configurationNode: null)
        {
        }

        public PageItemContentField(XmlNode configurationNode)
        {
            Initialize(configurationNode: configurationNode);
        }

        protected override void Initialize(XmlNode configurationNode)
        {
            if (configurationNode == null)
            {
                return;
            }
            ItemSearchContentAttribute = GetParameterValue(configurationNode: configurationNode, parameterName: "itemSearchContentAttribute", attributeName: "value", defaultValue: "search-content");
            ItemFieldNames = GetParameterValues(configurationNode: configurationNode, parameterName: "itemFieldNames", nestedParameterName: "itemFieldName", attributeName: "value");
            UrlScheme = GetParameterValue(configurationNode: configurationNode, parameterName: "urlScheme", attributeName: "value", defaultValue: "http");
            PageTemplates = ScUtil.AllPageTemplateIds();

            var siteName = GetParameterValue(configurationNode: configurationNode, parameterName: "siteName", attributeName: "value", defaultValue: "");
            if (!string.IsNullOrWhiteSpace(siteName))
            {
                Site = Factory.GetSiteInfoList().FirstOrDefault(x => x.Name == siteName);
            }
            else
            {
                Log.Error("PageItemContentField.Initialize(): No site name provided");
            }
            var realSiteName = GetParameterValue(configurationNode: configurationNode, parameterName: "realSiteName", attributeName: "value", defaultValue: "");
            if (!string.IsNullOrWhiteSpace(realSiteName))
            {
                RealSite = Factory.GetSiteInfoList().FirstOrDefault(x => x.Name == realSiteName);
            }
        }

        /// <summary>
        /// Extracts content
        /// </summary>
        protected virtual IEnumerable<string> GetItemContent(Item item)
        {
            List<string> result = null;
            var content = GetItemContentAsString(item);
            if (!string.IsNullOrWhiteSpace(content))
            {
                result = ExtractItemContent(content, ItemSearchContentAttribute);
            }
            else
            {
                //Log.Debug($"PageItemContentField.GetItemContent(): No page content found for {item.Paths.FullPath}.");
            }

            if (ItemFieldNames != null && ItemFieldNames.Any())
            {
                if (result == null)
                {
                    result = new List<string>();
                }
                foreach (var itemFieldName in ItemFieldNames)
                {
                    var value = item[itemFieldName];
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        result.Add(value);
                    }
                }
            }
            return result;
        }

        private List<string> ExtractItemContent(string pageContent, string dataAttribute)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(pageContent);
            var nodes = doc.DocumentNode.SelectNodes("//*[@data-" + dataAttribute + "]");
            if (nodes != null)
            {
                foreach (var node in nodes)
                {
                    foreach (HtmlNode comment in node.SelectNodes("//comment()"))
                    {
                        comment.ParentNode.RemoveChild(comment);
                    }
                    foreach (HtmlNode script in node.SelectNodes("//script"))
                    {
                        script.ParentNode.RemoveChild(script);
                    }
                }
            }
            
            var result = new List<string>();
            if (nodes != null)
            {
                result = nodes.Select(x => ProcessString(x.InnerText)).Where(x=> !string.IsNullOrWhiteSpace(x)).ToList();
                //Log.Debug($"PageItemContentField.ExtractItemContent(): Found {result.Count()} nodes.");
            }
            return result;
        }

        private string ProcessString(string value)
        {
            if (value == null)
            {
                return value;
            }
            value = HttpUtility.HtmlDecode(value).Replace("\r\n", string.Empty).Replace("\r", string.Empty).Replace("\n", string.Empty);
            return DulicateSpacesRegex.Replace(value, " ");
        }

        private string GetItemContentAsString(Item item)
        {
            var siteContext = new SiteContext(Site);
            using (new SiteContextSwitcher(siteContext))
            {
                var itemUrl = ScLink.GetUrl(item, true);

                if (RealSite != null)
                {
                    itemUrl = string.Concat(
                        itemUrl,
                        "?sc_site=",
                        RealSite.Name
                    );
                }

                //Log.Debug($"PageItemContentField.GetItemContentAsString(): Downloading page content for {itemUrl}");

                string result = null;
                try
                {
                    using (var client = new WebClient())
                    {
                        client.Encoding = Encoding.UTF8;
                        result = client.DownloadString(itemUrl);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(
                        $"PageItemContentField.GetItemContentAsString(): Unable to get data for item [{item.ID}, {item.Name}, {item.Paths.FullPath}, {item.Language.Name}, {itemUrl}]", 
                        ex
                    );
                }
                return result;
            }
        }
    }
}