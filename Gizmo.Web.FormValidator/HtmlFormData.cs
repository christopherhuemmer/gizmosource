﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using Gizmo.Web.FormValidator.Actions;

namespace Gizmo.Web.FormValidator
{
    /// <summary>
    /// Encapsulates the form data and meta data of one input field
    /// </summary>
    public class HtmlFormData
    {
        public string Name { get; set; }
        public string NiceName { get; set; }
        public List<string> Values { get; set; }
        public List<ActionFileContainer> Files { get; set; }
        public List<HtmlFormField> Fields { get; set; }
        public string TypeField { get; set; }
        public bool HasTypeField { get { return !string.IsNullOrWhiteSpace(TypeField); } }

        /// <summary>
        /// Initializes a new instance of the <see cref="HtmlFormData"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="values">The values.</param>
        /// <param name="typeField">The type field.</param>
        /// <param name="niceName">The nice name of the field.</param>
        public HtmlFormData(string name, List<string> values, string typeField, string niceName, IEnumerable<ActionFileContainer> files, IEnumerable<HtmlFormField> fields)
        {
            Name = name;
            NiceName = niceName;
            Values = values;
            TypeField = typeField;
            Files = files.ToList();
            Fields = fields.ToList();
        }

        public string FirstValue
        {
            get
            {
                return Values.First();
            }
        }

        /// <summary>
        /// Convert to XElement for xml Serialization
        /// </summary>
        /// <returns>this instance as XElement</returns>
        public XElement ToXElement(bool includeFilesEncoded = false)
        {
            var item = new XElement("item");
            item.Add(new XAttribute("name", Name));
            item.Add(new XAttribute("valuename", NiceName));
            item.Add(Values.Select(x => new XElement("value", x)));
            if (includeFilesEncoded)
            {
                foreach (var file in Files)
                {
                    var encoded = Convert.ToBase64String(file.RawData);
                    item.Add(new XElement("file", encoded));
                }
            }
            return item;
        }

        public HtmlFormData Clone()
        {
            var clone = new HtmlFormData(Name, Values, TypeField, NiceName, Files, Fields);

            //Clone the values
            clone.Values = new List<string>();
            foreach (string v in this.Values)
            {
                clone.Values.Add((string)v.Clone());
            }

            return clone;
        }
    }
}