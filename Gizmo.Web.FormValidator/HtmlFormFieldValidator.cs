﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using Gizmo.General.String;
using Gizmo.General.Email;
using System.Configuration;

namespace Gizmo.Web.FormValidator
{
    /// <summary>
    /// The actual Validator for a specific type
    /// </summary>
    public class HtmlFormFieldValidator
    {
        protected static string[] ValidatorTypes = { "required", "minlength", "maxlength", "pattern", "data-accept", "validate-captcha" };
        protected static string[] ValidatorInputTypes = { "number", "date", "datetime", "time", "email" };

        public string ValidatorType { get; protected set; }
        public string ValidatorData { get; protected set; }
        public string ValidatorErrorMessage { get; protected set; }

        public bool Success { get; set; }
        public List<string> ErrorCodes { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="HtmlFormFieldValidator"/> class.
        /// </summary>
        /// <param name="validator">The validator.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="data">The data.</param>
        public HtmlFormFieldValidator(string validator, string errorMessage = "", string data = "")
        {
            ValidatorType = validator.Replace("data-rule-","");
            ValidatorData = data;
            ValidatorErrorMessage = errorMessage;
        }

        /// <summary>
        /// Determines whether the specified field is valid.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <returns>true if the field is valid; otherwise false</returns>
        public bool IsValid(HtmlFormField field)
        {
            if ((ValidatorType != "required" && ValidatorType != "validate-captcha") && string.IsNullOrEmpty(field.Value))
            {
                return true;
            }

            switch (ValidatorType)
            {
                case "email":
                    return CheckEmail(field);
                case "required":
                    return CheckRequired(field);
                case "pattern":
                    return CheckPattern(field);
                case "validate-captcha":
                    return CheckCaptcha(field);
                case "minlength":
                case "maxlength":
                    return CheckLength(field, ValidatorType);
                case "number":
                    try
                    {
                        double len = Convert.ToDouble(field.Value);
                        field.Value = len.ToString();
                        return true;
                    }
                    catch { }
                    return false;
                case "date":
                case "datetime":
                case "time":
                    return CheckDate(field, ValidatorType);
                case "data-accept":
                    return CheckFileExtension(field);
                default:
                    return false;
            }
        }

        protected bool CheckFileExtension(HtmlFormField field)
        {
            var extensions = field.Node.GetAttrib("data-accept").Split(',');
            var extension = System.IO.Path.GetExtension(field.Value);
            if (extensions.Any(x => x.IsLowerEqual(extension)))
            {
                return true;
            }

            return false;
        }
        protected static bool CheckCaptcha(HtmlFormField field)
        {
            var provider = field.Node.GetAttrib("validate-captcha").ToLower();
                
            switch (provider)
            {
                case "google-recaptcha":

                    string reCaptchaCode = HttpContext.Current.Request.Form["g-Recaptcha-Response"];

                    if (string.IsNullOrEmpty(reCaptchaCode)) { return false; }

                    var client = new System.Net.WebClient();
                    var secret = ConfigurationManager.AppSettings["google.ReCaptcha"];

                    if (string.IsNullOrEmpty(secret)) { return false; }

                    var googleReply = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, reCaptchaCode));

                    var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                    var reCaptcha = serializer.Deserialize<Dictionary<string,dynamic>>(googleReply);
                                        
                    return reCaptcha["success"];
                                        
                default:
                    return false;
                    
            }
            
        }
        protected static bool CheckEmail(HtmlFormField field)
        {
            return EmailUtil.IsMailAddress(field.Value.NullSafe());
        }
        protected bool CheckDate(HtmlFormField field, string type)
        {
            try{
                object val;
                switch(type)
                {
                    case "date":
                        val = DateTime.Parse(field.Value);
                        break;
                    case "datetime":
                        val = DateTime.Parse(field.Value);
                        break;
                    case "time":
                        val = DateTime.Parse(field.Value);
                        break;
                    default:
                        return false;
                }
                return true;
            }
            catch {}
            
            return false;
        }
        protected bool CheckLength(HtmlFormField field, string type)
        {
            try
            {
                var lengthData = field.Node.GetAttrib(type);
                if(string.IsNullOrEmpty(lengthData)) { lengthData = field.Node.GetAttrib("data-rule-" + type) ; }
                int len = Convert.ToInt32(lengthData);
                if (type == "minlength")
                {
                    return field.Value.Length >= len;
                }
                else if (type == "maxlength")
                {
                    return field.Value.Length <= len;
                }

            }
            catch { }
            return false;
        }
        protected bool CheckPattern(HtmlFormField field)
        {
            try
            {
                var patternData = field.Node.GetAttrib("pattern");
                if(string.IsNullOrEmpty(patternData)) { patternData = field.Node.GetAttrib("data-rule-pattern") ; }
                Regex r = new Regex(patternData);
                return r.IsMatch(field.Value);
            }
            catch { }
            return false;
        }
        protected bool CheckRequired(HtmlFormField field)
        {
            if (field.IsCheckable)
            {
                return field.Node.HasAttrib("checked");
            }

            if (field.IsSelectable)
            {
                return field.Node.ChildNodes.Any(x => x.HasAttrib("selected") && x.GetAttributeValue("value", "") != "");
            }

            if (field.IsFile)
            {
                //return;
            }

            if (!string.IsNullOrWhiteSpace(field.Value))
            {
                return true;
            }
            
            return false;
        }
        /// <summary>
        /// Gets the validators from HTML node. The validators are defined in the html itself.
        /// </summary>
        /// <param name="htmlNode">The HTML node.</param>
        /// <param name="defaultMessages">The default error messages.</param>
        /// <returns>A list of HtmlFormFieldValidator for this specific field</returns>
        public static List<HtmlFormFieldValidator> GetValidatorsFromHtmlNode(HtmlNode htmlNode, Dictionary<string, string> defaultMessages)
        {
            var retVal = new List<HtmlFormFieldValidator>();
            string errorMsg;

            if(htmlNode.Name.IsLowerEqual("input"))
            {
                var typeName = htmlNode.GetAttrib("data-type").ToLower();
                if (string.IsNullOrWhiteSpace(typeName))
                {
                    if(htmlNode.HasAttrib("data-rule-number")) { typeName = "number"; }
                    if(htmlNode.HasAttrib("data-rule-time")) { typeName = "time"; }
                    if(htmlNode.HasAttrib("data-rule-email")) { typeName = "email"; }
                }
                if (string.IsNullOrWhiteSpace(typeName))
                {
                    typeName = htmlNode.GetAttrib("type").ToLower();
                }
                if (ValidatorInputTypes.Contains(typeName))
                {
                    errorMsg = htmlNode.GetAttrib("data-error-" + typeName);
                    if (errorMsg == string.Empty)
                    {
                        if(!defaultMessages.TryGetValue(typeName, out errorMsg))
                        {
                            errorMsg = string.Empty;
                        }
                        
                    }
                    retVal.Add(new HtmlFormFieldValidator(typeName, errorMsg));
                }
            }

            foreach (var s in ValidatorTypes)
            {
                if (htmlNode.HasAttrib(s)||htmlNode.HasAttrib(string.Concat("data-rule-",s)))
                {
                    errorMsg = htmlNode.GetAttrib("data-error-" + s);
                    if(string.IsNullOrEmpty(errorMsg))
                    {
                        errorMsg = htmlNode.GetAttrib("data-msg-" + s);
                    }
                    if (errorMsg == string.Empty)
                    {
                        if (!defaultMessages.TryGetValue(s, out errorMsg))
                        {
                            errorMsg = string.Empty;
                        }
                    }
                    retVal.Add(new HtmlFormFieldValidator(s, errorMsg, htmlNode.GetAttrib(s)));
                }
            }
            return retVal;
        }
    }
}