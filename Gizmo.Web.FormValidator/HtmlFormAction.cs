﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using System.IO;
using System.Text;
using Gizmo.Web.FormValidator.Actions;

namespace Gizmo.Web.FormValidator
{
    /// <summary>
    /// An abstract implementation for a form Action
    /// abstract ActionName: Deriving classes must return their name as it will be found in in "data-action-type"
    /// </summary>
    public abstract class HtmlFormAction
    {
        protected Dictionary<string, HtmlFormData> FormData;
        protected Dictionary<string, object> Parameters;
        public string FormId { get; internal set; }

        public bool IncludeFilesEncoded { get; protected set; }

        public Exception LastException = null;

        [Flags]
        public enum ActionResult
        {
            None = 0,
            Continue = 1,
            Abort = 2,
            Error = 4,
            Success = 8

        }
        /// <summary>
        /// Initializes this instance with specified data. Can be overridden for more complex cases.
        /// </summary>
        /// <param name="jsonData">The json data.</param>
        /// <param name="formData">The form data.</param>
        /// <returns>ActionResult to let the Validator know how to continue.</returns>
        public virtual ActionResult Init(string jsonData, HtmlFormValidator formData)
        {
            //jsonData would be something like this:
            //{"To":" martin.buettner@udg.de, " ,"From":"tobias.widmayer@udg.de" ,"BodyXsl":"form_kvp.xsl"}
            try
            {
                FormData = formData.GetFormData(ActionName);
                var serializer = new JavaScriptSerializer();
                Parameters = (Dictionary<string, object>)serializer.DeserializeObject(jsonData);

                foreach (var val in FormData.Values.Where(x => x.HasTypeField))
                {
                    Parameters[val.TypeField] = string.Join(",", val.Values);
                }
            }
            catch (Exception ex)
            {
                LastException = ex;
                return ActionResult.Abort; 
            }
            
            return ActionResult.Continue;
        }


        /// <summary>
        /// Initializes the needed data objects.
        /// </summary>
        /// <typeparam name="T">The type of object to return</typeparam>
        /// <param name="jsonData">The json data.</param>
        /// <param name="formData">The form data.</param>
        /// <returns>The deserialized object.</returns>
        protected T InitData<T>(string jsonData, HtmlFormValidator formData)
        {
            //jsonData would be something like this:
            //{"To":" martin.buettner@udg.de, " ,"From":"tobias.widmayer@udg.de" ,"BodyXsl":"form_kvp.xsl"}

            FormData = formData.GetFormData(ActionName);
            var serializer = new JavaScriptSerializer();
            var retval = serializer.Deserialize<T>(jsonData);
            MapOverrides(retval);

            return retval;
        }

        /// <summary>
        /// If any data-action-type is set for a specific action this will override the defaults set in Init
        /// </summary>
        /// <param name="dataObject">The data object.</param>
        protected virtual void MapOverrides(object dataObject)
        {
            var propDict = dataObject.GetType().GetProperties().Where(x => x.PropertyType == typeof(string) || x.PropertyType == typeof(List <string>)).ToDictionary(x => x.Name, v => v);
            System.Reflection.PropertyInfo propInfo;

            foreach (var val in FormData.Values.Where(x => x.HasTypeField))
            {
                if (propDict.TryGetValue(val.TypeField, out propInfo))
                {
                    if (propInfo.PropertyType == typeof(string))
                    {
                        propInfo.SetValue(dataObject, string.Join(",", val.Values), null);
                    }
                    else if (propInfo.PropertyType == typeof(List <string>))
                    {
                        var propInfoVal = propInfo.GetValue(dataObject, null) as List<string>;
                        if (propInfoVal != null)
                        {
                            propInfoVal.AddRange(val.Values);
                        }
                        else
                        {
                            propInfo.SetValue(dataObject, val.Values.ToList(), null);
                        }
                    }
                }
            }


        }

        private XElement _formDataAsXElement = null;
        /// <summary>
        /// Gets the form data as XElement to allow for xml Serialization.
        /// </summary>
        /// <value>
        /// The form data as XElement.
        /// </value>
        public XElement FormDataAsXElement
        {
            get
            {
                if (_formDataAsXElement == null)
                {
                    var root = new XElement("items");
                    foreach (var data in FormData.Values)
                    {
                        root.Add(data.ToXElement(IncludeFilesEncoded));
                    }
                    _formDataAsXElement = root;
                }
                return _formDataAsXElement;
            }
        }
        /// <summary>
        /// Gets the form data as XDocument for XML Serialization.
        /// </summary>
        /// <value>
        /// The form data as XDocument.
        /// </value>
        public XDocument FormDataAsXDocument
        {
            get
            {
                XDocument xdoc = new XDocument(new XDeclaration("1.0", "utf-8", null), FormDataAsXElement);
                return xdoc;
            }
        }

        /// <summary>
        /// We use this class to trick the XslCompiledTransform.Transform   
        /// into writing a utf-8 header instead of a utf-16
        /// </summary>
        private class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding
            {
                get { return Encoding.UTF8; }
            }
        }

        /// <summary>
        /// Gets the content of the form transformed by the xsl provided.
        /// If the transform results in an XML the header will always be utf-8
        /// </summary>
        /// <param name="xslRelativePath">The XSL relative path.</param>
        /// <param name="optionalArguments">The optional arguments for the xsl transform.</param>
        /// <returns>The transformed string</returns>
        protected string GetTransformedContent(string xslRelativePath, System.Xml.Xsl.XsltArgumentList optionalArguments = null)
        {
            string xslLoc = System.IO.Path.Combine(HtmlFormValidator.XSLLocation, xslRelativePath);
            XDocument xd = FormDataAsXDocument;
            using (var stringWriter = new Utf8StringWriter())
            using (var xslReader = new System.Xml.XmlTextReader(xslLoc))
            {
                var xslt = new System.Xml.Xsl.XslCompiledTransform();
                xslt.Load(xslReader);

                // Execute the transform and output the results to a writer.
                xslt.Transform(xd.CreateReader(), optionalArguments, stringWriter);
                xslReader.Close();

                return stringWriter.ToString();
            }
        }

        protected IEnumerable<ActionFileContainer> AttachedFiles
        {
            get
            {
                foreach (var fVal in FormData.Values.Where(x => x.Files.Any()))
                {
                    foreach (var f in fVal.Files)
                    {
                        yield return f;
                    }
                }
            }
        }
        public abstract string ActionName { get; }
        public abstract ActionResult DoAction();
        
    }
}