﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Gizmo.Web.FormValidator
{
    public class HtmlFormRequest
    {
                
        public static string GetQueryStringSave(string param)
        {
            var query = HttpContext.Current.Request[param];
            var safeQuery = WebUtility.HtmlEncode(query);

            if (string.IsNullOrEmpty(safeQuery))
            {
                return "";
            }

            return safeQuery;
        }
    }
}
