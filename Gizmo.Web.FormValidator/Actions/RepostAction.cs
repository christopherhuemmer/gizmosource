﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.Net.Mail;
using Gizmo.General.String;
using Gizmo.General.Email;
using System.Xml.Linq;
using System.IO;
using Gizmo.General.Debug;
using System.Net.Configuration;
using System.Configuration;
using System.Web;
using System.Net;
using System.Collections.Specialized;


namespace Gizmo.Web.FormValidator.Actions
{
    /// <summary>
    /// An HtmlFormAction that send an email.
    /// </summary>
    public class RepostAction : HtmlFormAction
    {
        protected class ActionParams
        {
            public string Url { get; set; }
            public bool UseCredentials { get; set; }
            public List<ValueMapping> Mapping { get; set; }

        }

        protected class ValueMapping
        {
            public string Name { get; set; }
            public string NewName { get; set; }

            public Dictionary<string, string> NewValues { get; set; }
        }

        protected ActionParams RepostParams { get; set; }

        /// <summary>
        /// Initializes this instance with specified data. Overrides the default implementation.
        /// </summary>
        /// <param name="jsonData">The json data.</param>
        /// <param name="formData">The form data.</param>
        /// <returns>
        /// ActionResult to let the Validator know how to continue.
        /// </returns>
        public override ActionResult Init(string jsonData, HtmlFormValidator formData)
        {
            try
            {
                RepostParams = InitData<ActionParams>(jsonData, formData);
            }
            catch (Exception ex)
            {
                LastException = ex;
                return ActionResult.Abort; 
            }
            return ActionResult.Continue;
        }

        /// <summary>
        /// Performs the action.
        /// </summary>
        /// <returns></returns>
        public override HtmlFormAction.ActionResult DoAction()
        {
            //using (var client = new WebClient())
            //{
            //    client.
            //    byte[] result = client.UploadFile(url, fileToUpload);
            //    string responseAsString = Encoding.Default.GetString(result);
            //}

            //Mappings
            List<HtmlFormData> data = new List<HtmlFormData>();
            foreach (var fd in FormData)
            {
                data.Add(fd.Value.Clone());
            }
            foreach (var m in this.RepostParams.Mapping)
            {
                var item = data.Where(x => x.Name.ToLower() == m.Name.ToLower()).FirstOrDefault();
                if(item != null)
                {
                    item.Name = m.NewName;
                    if(m.NewValues != null)
                    {
                        for(int i = 0; i<item.Values.Count; i++)
                        {
                            if(m.NewValues.ContainsKey(item.Values[i]))
                            {
                                item.Values[i] = m.NewValues[item.Values[i]];
                            }
                            
                        }

                        
                    }
                }
            }



            PostAsync(this.RepostParams.Url, data);

            return ActionResult.Continue | ActionResult.Success;
        }

        
        public override string ActionName
        {
            get { return "Repost"; }
        }

        private string PostAsync(string actionUrl, List<HtmlFormData> data)
        {
            bool multiForm = data.Where(x => x.Files.Any()).Any();
            string result = null;

#if DEBUG
            if (!actionUrl.StartsWith("http"))
            {
                actionUrl = "http://localhost:" + HttpContext.Current.Request.Url.Port + actionUrl;
            }
#endif

            if (multiForm)
            {
                result = UploadFilesToRemoteUrl(actionUrl, data);
            }
            else
            {
                byte[] response = null;
                NameValueCollection pairs = new NameValueCollection();

                foreach (var v in data)
                {
                    foreach (var singleValue in v.Values)
                    {
                        pairs.Add(v.Name, singleValue);
                    }
                }

                using (WebClient client = new WebClient())
                {
                    if (this.RepostParams.UseCredentials)
                    {
                        //CredentialCache cc = new CredentialCache();
                        //cc.Add(
                        //    new Uri("http://ccw-tools.live.internom.net/"),
                        //    "NTLM",
                        //    new NetworkCredential("ccw-tools", "4Qfez9%1"));
                        //client.Credentials = cc;

                        var credentials = new NetworkCredential(ConfigurationManager.AppSettings["HtmlFormValidator.RepostAction.User"], ConfigurationManager.AppSettings["HtmlFormValidator.RepostAction.Password"]);
                        client.Credentials = credentials;
                    }
                    response = client.UploadValues(actionUrl, pairs);
                }

                result = System.Text.Encoding.UTF8.GetString(response);

            }

            return result;

        }

        /*private System.IO.Stream Upload(string actionUrl, string paramString, Stream paramFileStream, byte[] paramFileBytes)
        {
            //(or .NET 4.0 by adding the Microsoft.Net.Http package from NuGet)
            HttpContent stringContent = new StringContent(paramString);
            HttpContent fileStreamContent = new StreamContent(paramFileStream);
            HttpContent bytesContent = new ByteArrayContent(paramFileBytes);
            using (var client = new HttpClient())
            using (var formData = new MultipartFormDataContent())
            {
                formData.Add(stringContent, "param1", "param1");
                formData.Add(fileStreamContent, "file1", "file1");
                formData.Add(bytesContent, "file2", "file2");
                var response = client.PostAsync(actionUrl, formData).Result;
                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }
                return response.Content.ReadAsStreamAsync().Result;
            }
        }*/

        public string UploadFilesToRemoteUrl(string url, List<HtmlFormData> data)
        {
            NameValueCollection nvc = new NameValueCollection();

            foreach (var v in data.Where(x => !x.Files.Any()))
            {
                foreach (var singleValue in v.Values)
                {
                    nvc.Add(v.Name, singleValue);
                }
            }


            // UNUSED long length = 0;
            string boundary = "----------------------------" +
            DateTime.Now.Ticks.ToString("x");


            HttpWebRequest httpWebRequest2 = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest2.ContentType = "multipart/form-data; boundary=" +
            boundary;
            httpWebRequest2.Method = "POST";
            httpWebRequest2.KeepAlive = true;
            if (this.RepostParams.UseCredentials)
            {
                var credentials = new NetworkCredential(ConfigurationManager.AppSettings["HtmlFormValidator.RepostAction.User"], ConfigurationManager.AppSettings["HtmlFormValidator.RepostAction.Password"]);
                httpWebRequest2.Credentials = credentials;
            }
            else {
                httpWebRequest2.Credentials = System.Net.CredentialCache.DefaultCredentials;
            }



            Stream memStream = new System.IO.MemoryStream();

            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" +
            boundary + "\r\n");


            string formdataTemplate = "\r\n--" + boundary +
            "\r\nContent-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}";

            foreach (string key in nvc.Keys)
            {
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                memStream.Write(formitembytes, 0, formitembytes.Length);
            }


            memStream.Write(boundarybytes, 0, boundarybytes.Length);

            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: application/octet-stream\r\n\r\n";

            foreach (var v in data.Where(x => x.Files.Any()))
            {
                foreach (var singleFile in v.Files)
                {
     
                string header = string.Format(headerTemplate, v.Name, singleFile.FileName);

                byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);

                memStream.Write(headerbytes, 0, headerbytes.Length);

                MemoryStream fileStream = new MemoryStream(singleFile.RawData);

                byte[] buffer = new byte[1024];

                int bytesRead = 0;

                while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                {
                    memStream.Write(buffer, 0, bytesRead);
                }


                memStream.Write(boundarybytes, 0, boundarybytes.Length);


                fileStream.Close();
            
                }
            }

            httpWebRequest2.ContentLength = memStream.Length;

            Stream requestStream = httpWebRequest2.GetRequestStream();

            memStream.Position = 0;
            byte[] tempBuffer = new byte[memStream.Length];
            memStream.Read(tempBuffer, 0, tempBuffer.Length);
            memStream.Close();
            requestStream.Write(tempBuffer, 0, tempBuffer.Length);
            requestStream.Close();


            WebResponse webResponse2 = httpWebRequest2.GetResponse();

            Stream stream2 = webResponse2.GetResponseStream();
            StreamReader reader2 = new StreamReader(stream2);

            var response = reader2.ReadToEnd();


            webResponse2.Close();
            httpWebRequest2 = null;
            webResponse2 = null;

            return response;
        }

        /*public string UploadFilesToRemoteUrl(string url, string[] files, string logpath, NameValueCollection nvc)
        {

            long length = 0;
            string boundary = "----------------------------" +
            DateTime.Now.Ticks.ToString("x");


            HttpWebRequest httpWebRequest2 = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest2.ContentType = "multipart/form-data; boundary=" +
            boundary;
            httpWebRequest2.Method = "POST";
            httpWebRequest2.KeepAlive = true;
            httpWebRequest2.Credentials =
            System.Net.CredentialCache.DefaultCredentials;



            Stream memStream = new System.IO.MemoryStream();

            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" +
            boundary + "\r\n");


            string formdataTemplate = "\r\n--" + boundary +
            "\r\nContent-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}";

            foreach (string key in nvc.Keys)
            {
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                memStream.Write(formitembytes, 0, formitembytes.Length);
            }


            memStream.Write(boundarybytes, 0, boundarybytes.Length);

            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\n Content-Type: application/octet-stream\r\n\r\n";

            for (int i = 0; i < files.Length; i++)
            {

                //string header = string.Format(headerTemplate, "file" + i, files[i]);
                string header = string.Format(headerTemplate, "uplTheFile", files[i]);

                byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);

                memStream.Write(headerbytes, 0, headerbytes.Length);


                FileStream fileStream = new FileStream(files[i], FileMode.Open,
                FileAccess.Read);
                byte[] buffer = new byte[1024];

                int bytesRead = 0;

                while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                {
                    memStream.Write(buffer, 0, bytesRead);

                }


                memStream.Write(boundarybytes, 0, boundarybytes.Length);


                fileStream.Close();
            }

            httpWebRequest2.ContentLength = memStream.Length;

            Stream requestStream = httpWebRequest2.GetRequestStream();

            memStream.Position = 0;
            byte[] tempBuffer = new byte[memStream.Length];
            memStream.Read(tempBuffer, 0, tempBuffer.Length);
            memStream.Close();
            requestStream.Write(tempBuffer, 0, tempBuffer.Length);
            requestStream.Close();


            WebResponse webResponse2 = httpWebRequest2.GetResponse();

            Stream stream2 = webResponse2.GetResponseStream();
            StreamReader reader2 = new StreamReader(stream2);

            var response = reader2.ReadToEnd();
       

            webResponse2.Close();
            httpWebRequest2 = null;
            webResponse2 = null;

            return response;
        }*/

    }
}
