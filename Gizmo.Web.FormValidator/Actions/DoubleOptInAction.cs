﻿using System;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using Gizmo.General;
using System.Net.Mail;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Configuration;
using Gizmo.General.Debug;
using System.Xml;
using System.Xml.Serialization;
using System.Web.Script.Serialization;
using System.Xml.Linq;

namespace Gizmo.Web.FormValidator.Actions
{
    public class DoubleOptInAction : HtmlFormAction
    {

        public class ActionParams
        {
            public string MailBodyFile { get; set; }
            public string MailSubject { get; set; }
            public string FileCachePath { get; set; }
            public string RedirectIntermediatePage { get; set; }
            public string To { get; set; } // Original Empfänger
            public string Email { get; set; } // User entered Email
            public string From { get; set; }
            public string CC { get; set; }
            public string BCC { get; set; }
            public string ReplyTo { get; set; }
            public string AttachmentXsl { get; set; }
            public string AttachmentName { get; set; }
            public string AttachmentMime { get; set; }
            public string BodyXsl { get; set; }
            public string Subject { get; set; }
            public string Headline { get; set; }

            public string BodyXslConfirm { get; set; }
            public string SubjectConfirm { get; set; }
            public bool MailClicked { get; set; }
        }

        protected ActionParams EmailParams { get; set; }

        string doiGuid = DateTime.Now.ToString("yyyy_MM_dd_") + DateTime.Now.Ticks.ToString();

        public override ActionResult Init(string jsonData, HtmlFormValidator formData)
        {
            try
            {
                EmailParams = InitData<ActionParams>(jsonData, formData);

                string path = EmailParams.FileCachePath;
                if (EmailParams.FileCachePath.StartsWith("/"))
                {
                    path = HttpContext.Current.Server.MapPath(EmailParams.FileCachePath);
                }

                path = Path.Combine(path, "Data");
                path = Path.Combine(path, doiGuid);
                Directory.CreateDirectory(path);

                //var serializer = new JavaScriptSerializer();
                //var data = serializer.Deserialize<Dictionary<string, string>>(jsonData);
                
                //XElement xel = new XElement("items");
                
                //foreach (var pair in data)
                //{
                //    XElement cElement = new XElement("item", 
                //                                        new XElement("Value", pair.Value) );
                //    cElement.SetAttributeValue("Key", pair.Key);
                    
                //    xel.Add(cElement);                    
                //}

                //var toElement = new XElement("item",
                //                                new XElement("Value", HttpContext.Current.Request.Form["mail"]));
                //toElement.SetAttributeValue("Key", "To");
                //xel.Add(toElement);

                //var bodyElement = new XElement("item", new XElement("Value", ConfigurationManager.AppSettings["DoubleOptInAction.BodyXsl"]));
                //bodyElement.SetAttributeValue("Key", "BodyXsl");
                //xel.Add(bodyElement);

                //var subElement = new XElement("item", new XElement("Value", ConfigurationManager.AppSettings["DoubleOptInAction.Subject"]));
                //subElement.SetAttributeValue("Key", "Subject");
                //xel.Add(subElement);

                //var mailClicked = new XElement("item", new XElement("Value", EmailParams.MailClicked));
                //mailClicked.SetAttributeValue("Key", "MailClicked");
                //xel.Add(mailClicked);this.EmailParams.Email;

                ////FormDataAsXDocument.Add(xel);
                ////FormDataAsXDocument.Save(Path.Combine(path, "DOIformdata.xml"));
                //xel.Save(Path.Combine(path, "DOIformdata.xml"));

   
                EmailParams.BodyXslConfirm = ConfigurationManager.AppSettings["DoubleOptInAction.BodyXsl"];
                EmailParams.SubjectConfirm = ConfigurationManager.AppSettings["DoubleOptInAction.Subject"];

                var fileName = Path.Combine(path, "DOIformdata.xml");

                using (var writer = new System.IO.StreamWriter(fileName))
                {
                    var xmlSerializer = new XmlSerializer(typeof(ActionParams));
                    xmlSerializer.Serialize(writer, EmailParams);
                    writer.Flush();
                }
                                
            }
            catch (Exception ex)
            {
                LastException = ex;
                return ActionResult.Abort;
            }
            
            return ActionResult.Continue;
        }
        
        public override HtmlFormAction.ActionResult DoAction()
        {
            string path = EmailParams.FileCachePath;
            if (EmailParams.FileCachePath.StartsWith("/"))
            {
                path = HttpContext.Current.Server.MapPath(EmailParams.FileCachePath);
            }
     
            //var doiGuid = DateTime.Now.ToString("yyyy_MM_dd_") + DateTime.Now.Ticks.ToString();
            path = Path.Combine(path, doiGuid);
            Directory.CreateDirectory(path);

            FormDataAsXDocument.Save(Path.Combine(path, "DOIformdata.xml"));
            foreach (var fVal in FormData.Values.Where(x => x.Files.Any()))
            {
                foreach (var f in fVal.Files)
                {
                    using (var fileStream = File.Create(Path.Combine(path, f.FileName)))
                    using (var ms = new MemoryStream(f.RawData))
                    {
                        ms.CopyTo(fileStream);
                        fileStream.Flush();
                        fileStream.Close();
                    }
                }
            }

            List<MemoryStream> attachmentStreams = new List<MemoryStream>();
            try
            {
                using (MailMessage email = new MailMessage())
                {

                    //var emailFromForm = HttpContext.Current.Request.Form["f2360"]; //Email
                                        
                    var emailFromForm = this.EmailParams.Email;

                    email.To.Add(emailFromForm);

                    email.SubjectEncoding = System.Text.Encoding.UTF8;
                    if (string.IsNullOrEmpty(EmailParams.MailSubject))
                    {
                        email.Subject = "<Empty Subject>";
                    }
                    else
                    {
                        email.Subject = EmailParams.MailSubject;
                    }

                    email.BodyEncoding = System.Text.Encoding.UTF8;
                    
                    if (EmailParams.MailBodyFile == null)
                    {
                        email.Body = "<Empty Body>";
                    }
                    else
                    {
                        var argsList = new System.Xml.Xsl.XsltArgumentList();
                        argsList.AddParam("date", "", DateTime.Now);
                        
                        var body = GetTransformedContent(EmailParams.MailBodyFile);
                        email.Body = body.Replace("#doiID#", string.Format("doiID={0}", doiGuid));
                    }

                    string attachmentContent = null;
                    string attachmentName = null;
                    string attachmentType = null;
                                        
                    var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
                    var doiFrom = ConfigurationManager.AppSettings["DoubleOptInAction.EmailFrom"];
                    email.From = new MailAddress(doiFrom); 

                    Log.Info(String.Format("DoubleOptInAction| Server: {0} | From: {1} | To: {2}", smtpSection.Network.Host, email.From.Address, email.To.First().Address));
                    
                    using (SmtpClient client = new SmtpClient())
                    {

                        if (attachmentContent != null)
                        {
                            var xmlStream = new System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(attachmentContent));
                            attachmentStreams.Add(xmlStream);
                            var ct = new System.Net.Mime.ContentType(attachmentType);
                            Attachment attach = new Attachment(xmlStream, ct);
                            attach.ContentDisposition.FileName = attachmentName;
                            email.Attachments.Add(attach);
                        }

                        //AddFileAttachments(email, ref attachmentStreams);
                        client.Send(email);

                    }
                }
            }
            catch (Exception ex)
            {
                LastException = ex;
                Log.Error("DoubleOptInAction|" + ex.ToString());
                return ActionResult.Abort | ActionResult.Error;
            }
            finally
            {
                foreach (var ms in attachmentStreams)
                {
                    ms.Close();
                    ms.Dispose();
                }
            }
            
            return ActionResult.Continue | ActionResult.Success;
        }

        public override string ActionName
        {
            get { return "DoubleOptIn"; }
        }

    }
}
