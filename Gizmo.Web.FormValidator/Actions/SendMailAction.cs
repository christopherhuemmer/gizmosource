﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using System.Net.Mail;
using Gizmo.General.Email;
using System.IO;
using Gizmo.General.Debug;
using System.Net.Configuration;
using System.Configuration;

namespace Gizmo.Web.FormValidator.Actions
{
  /// <summary>
  /// An HtmlFormAction that sends an email.
  /// </summary>
  public class SendMailAction : HtmlFormAction
  {
    protected class ActionParams
    {
      public string To { get; set; }
      public string From { get; set; }
      public string CC { get; set; }
      public string BCC { get; set; }
      public string ReplyTo { get; set; }
      public string AttachmentXsl { get; set; }
      public string AttachmentName { get; set; }
      public string AttachmentMime { get; set; }
      public string BodyXsl { get; set; }
      public string Subject { get; set; }
      public string Headline { get; set; }
    }

    protected ActionParams EmailParams { get; set; }

    /// <summary>
    /// Initializes this instance with specified data. Overrides the default implementation.
    /// </summary>
    /// <param name="jsonData">The json data.</param>
    /// <param name="formData">The form data.</param>
    /// <returns>
    /// ActionResult to let the Validator know how to continue.
    /// </returns>
    public override ActionResult Init(string jsonData, HtmlFormValidator formData)
    {
      try
      {
        EmailParams = InitData<ActionParams>(jsonData, formData);
        //Sonderregel bei email.to
        // Sollte diese von den Form Params mit "Leer" kommen, dann wieder den default nehmen.
        if (String.IsNullOrEmpty(EmailParams.To))
        {
          var serializer = new JavaScriptSerializer();
          var retval = serializer.Deserialize<ActionParams>(jsonData);

          EmailParams.To = retval.To;
        }
      }
      catch (Exception ex)
      {
        LastException = ex;
        return ActionResult.Abort;
      }
      return ActionResult.Continue;
    }

    /// <summary>
    /// Performs the action.
    /// </summary>
    /// <returns></returns>
    public override HtmlFormAction.ActionResult DoAction()
    {
      List<MemoryStream> attachmentStreams = new List<MemoryStream>();
      try
      {
        using (MailMessage email = new MailMessage())
        {
          EmailParams.To.ToMailAddresses().Where(x => !String.IsNullOrEmpty(x.Address)).ToList().ForEach(x => email.To.Add(x));
          EmailParams.CC.ToMailAddresses().Where(x => !String.IsNullOrEmpty(x.Address)).ToList().ForEach(x => email.CC.Add(x));
          EmailParams.BCC.ToMailAddresses().Where(x => !String.IsNullOrEmpty(x.Address)).ToList().ForEach(x => email.Bcc.Add(x));
          if (!string.IsNullOrWhiteSpace(EmailParams.From))
          {
            email.From = EmailParams.From.ToMailAddresses().FirstOrDefault();
          }
          if (!string.IsNullOrEmpty(EmailParams.ReplyTo))
          {
            EmailParams.ReplyTo.ToMailAddresses().ToList().ForEach(x => email.ReplyToList.Add(x));
          }

          email.SubjectEncoding = System.Text.Encoding.UTF8;
          if (string.IsNullOrEmpty(EmailParams.Subject))
          {
            email.Subject = "<Empty Subject>";
          }
          else
          {
            email.Subject = EmailParams.Subject;
          }

          email.BodyEncoding = System.Text.Encoding.UTF8;

          var argsList = new System.Xml.Xsl.XsltArgumentList();
          argsList.AddParam("date", "", DateTime.Now.ToString("g"));

          if (EmailParams.BodyXsl == null)
          {
            email.Body = "<Empty Body>";
          }
          else
          {
            //  var argsList = new System.Xml.Xsl.XsltArgumentList();
            // argsList.AddParam("date", "", DateTime.Now.ToString("g"));

            if (EmailParams.Headline != null)
            {
              argsList.AddParam("headline", "", EmailParams.Headline);
            }

            email.Body = GetTransformedContent(EmailParams.BodyXsl, argsList);
          }

          string attachmentContent = null;
          string attachmentName = null;
          string attachmentType = null;

          if (EmailParams.AttachmentXsl != null)
          {
            attachmentName = EmailParams.AttachmentName ?? "attachment.xml";
            attachmentType = EmailParams.AttachmentMime ?? System.Net.Mime.MediaTypeNames.Text.Xml;
            attachmentContent = GetTransformedContent(EmailParams.AttachmentXsl, argsList);
          }

          var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
          //Log.Info(String.Format("Send Mail| Server: {0} | From: {1} | To: {2}", smtpSection.Network.Host, email.From.Address, email.To.First().Address));
          Log.Info($"Formvalidator->SendMailAction: Server = {smtpSection.Network.Host}, From = {email.From.Address}, To = {string.Join(", ", email.To)}, CC = {string.Join(", ", email.CC)}, BCC = {string.Join(", ", email.Bcc)}, ReplyTo = {string.Join(", ", email.ReplyToList)}, Subject = {email.Subject}, Headline = {EmailParams.Headline}");

          using (SmtpClient client = new SmtpClient())
          {
            if (client.Credentials != null)
            {
              client.EnableSsl = true;
            }

            if (attachmentContent != null)
            {
              var xmlStream = new System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(attachmentContent));
              attachmentStreams.Add(xmlStream);
              var ct = new System.Net.Mime.ContentType(attachmentType);
              Attachment attach = new Attachment(xmlStream, ct);
              attach.ContentDisposition.FileName = attachmentName;
              email.Attachments.Add(attach);
            }

            AddFileAttachments(email, ref attachmentStreams);
            client.Send(email);

          }
        }
      }
      catch (Exception ex)
      {
        LastException = ex;
        Log.Error("Send Mail|" + ex.ToString());

        return ActionResult.Continue | ActionResult.Error;
      }
      finally
      {
        foreach (var ms in attachmentStreams)
        {
          ms.Close();
          ms.Dispose();
        }
      }

      return ActionResult.Continue | ActionResult.Success;
    }

    public override string ActionName
    {
      get { return "SendEmail"; }
    }

    private void AddFileAttachments(MailMessage email, ref List<MemoryStream> streams)
    {
      foreach (var fVal in FormData.Values.Where(x => x.Files.Any()))
      {
        foreach (var f in fVal.Files)
        {
          var ct = new System.Net.Mime.ContentType("application/octet-stream");
          var ms = new MemoryStream(f.RawData);
          streams.Add(ms);
          Attachment attach = new Attachment(ms, ct);
          attach.ContentDisposition.FileName = f.FileName;
          email.Attachments.Add(attach);
        }
      }
    }
  }
}
