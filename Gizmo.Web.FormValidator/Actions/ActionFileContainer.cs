﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;

namespace Gizmo.Web.FormValidator.Actions
{
    public class ActionFileContainer
    {
        public string FileName { get; set; }
        public byte[] RawData { get; private set; }
        public ActionFileContainer(HttpPostedFile file)
        {
            FileName = file.FileName;
            //we have to support multiple reads from one HttpPostedFile - multiple actions read from this
            using (MemoryStream target = new MemoryStream())
            {
                file.InputStream.CopyTo(target);
                RawData = target.ToArray();
            }
        }
    }
}
