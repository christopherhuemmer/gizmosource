﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Text;
//using System.Web.Script.Serialization;
//using Gizmo.General.String;
using System.Net.Mail;
using System.Configuration;
using Gizmo.General.Debug;
using System.Net.Configuration;
//using System.IO;
using System.Web;

namespace Gizmo.Web.FormValidator.Actions
{
    /// <summary>
    /// an HtmlFormAction that sends an auto-respond-email with attachements.
    /// </summary>
    class AutoRespondAction : HtmlFormAction
    {
        protected class ActionParams
        {
            public string From { get; set; }
            public string To{ get; set; }            
            public string AutoResponseBody { get; set; }
            public string Subject { get; set; }         
            public List<string> Attachements { get; set; }  
        }

        protected ActionParams EmailParams { get; set; }
        
        public override ActionResult Init(string jsonData, HtmlFormValidator formData)
        {
            try
            {
                EmailParams = InitData<ActionParams>(jsonData, formData);
            }
            catch (Exception ex)
            {
                LastException = ex;
                return ActionResult.Abort;
            }
            return ActionResult.Continue;
        }

        public override ActionResult DoAction()
        {
            Log.Info(String.Format("Send Mail| Start"));

            try
            {       
                using (MailMessage email = new MailMessage())
                {

                    email.To.Add(EmailParams.To.Trim());
                    email.From = new MailAddress(EmailParams.From);

                    email.SubjectEncoding = System.Text.Encoding.UTF8;
                    if (string.IsNullOrEmpty(EmailParams.Subject ))
                    {
                        email.Subject = "<Empty Subject>";
                    }
                    else
                    {
                        email.Subject = EmailParams.Subject;
                    }

                    email.BodyEncoding = System.Text.Encoding.UTF8;
                    if (EmailParams.AutoResponseBody == null)
                    {
                        email.Body = "<Empty Body>";
                    }
                    else
                    {
                        email.Body = EmailParams.AutoResponseBody;
                    }


                    var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
                    Log.Info(String.Format("Send Mail| Server: {0} | From: {1} | To: {2}", smtpSection.Network.Host, email.From.Address, email.To.First().Address));

                    if (EmailParams.Attachements != null)
                    {
                        foreach (var file in EmailParams.Attachements)
                        {
                            try
                            {
                                string path = HttpContext.Current.Server.MapPath(file);
                                email.Attachments.Add(new Attachment(path));
                            }
                            catch (Exception ex)
                            {
                                Log.Error("Send Mail|" + ex.ToString());
                                //email.Body = email.Body + "\r\n" + "Attachement not found: " + file; 
                                //AK 22.09.2016: Change Request: Attachement-Checkbox wird nun auch als "normale" Checkbox verwendet, daher soll in diesem Fall kein Fehler ausgegeben werden, sondern der value  
                                email.Body = email.Body + "\r\n" + file;
                                continue;
                            }
                        }
                    }

                    using (SmtpClient client = new SmtpClient())
                    {
                        client.Send(email);
                    }
                }
            }
            catch (Exception ex)
            {
                LastException = ex;
                Log.Error("Send Mail|" + ex.ToString());
                return ActionResult.Abort | ActionResult.Error;
            }

            return ActionResult.Continue | ActionResult.Success;
            //return ActionResult.Abort | ActionResult.Success;
        }

        public override string ActionName
        {
            get { return "AutoRespond"; }
        }
    }
}
