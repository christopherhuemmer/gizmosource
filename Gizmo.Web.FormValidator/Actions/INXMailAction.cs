﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Gizmo.Web.FormValidator;
using Gizmo.General.String;
using System.Net;
using System.IO;
using Gizmo.General.Collections;
using System.Web.Script.Serialization;
using Gizmo.General.Debug;

namespace Gizmo.Web.FormValidator.Actions
{
    
    /// <summary>
    /// An HtmlFormAction that performs newsletter registration to INXMail.
    /// </summary>
    public class INXMailAction : HtmlFormAction
    {
        private const string INX_MAIL_SUBSCRIBE_URL = "web.inxmail.com/{0}/subscription/servlet?INXMAIL_SUBSCRIPTION={1}&{2}&INXMAIL_HTTP_REDIRECT=http://web.inxmail.com/success&INXMAIL_HTTP_REDIRECT_ERROR=http://web.inxmail.com/error&INXMAIL_CHARSET=utf-8";   // ohne HTTP:! 
        private const string INX_MAIL_UNSUBSCRIBE_URL = "web.inxmail.com/{0}/subscription/servlet?INXMAIL_UNSUBSCRIPTION={1}&{2}&INXMAIL_HTTP_REDIRECT=http://web.inxmail.com/success&INXMAIL_HTTP_REDIRECT_ERROR=http://web.inxmail.com/error&INXMAIL_CHARSET=utf-8";

        protected class ActionParams
        {
            public string ProxyName { get; set; }
            public int ProxyPort { get; set; }
            public string Action { get; set; }
            public string Newsletter { get; set; }
            public string Email { get; set; }
            public bool UseSsl { get; set; }

            public string ValidAction { get; set; } // If Set, check the value of the Item if true

            private List<NL> _newsletters = null;
            [ScriptIgnore]
            public List<NL> Newsletters
            {
                get
                {
                    if (_newsletters == null)
                    {
                        if (!string.IsNullOrEmpty(Newsletter))
                        {
                            _newsletters = Newsletter.Split(',').Select(x => new NL(x)).ToList();
                        }
                    }
                    return _newsletters;
                }
            }
            public class NL
            {
                public NL(string nl)
                {
                    if (!string.IsNullOrEmpty(nl) && nl.IndexOf('|') != 0)
                    { 
                        var parts = nl.Split('|');
                        Client = parts[0];
                        List = parts[1];
                    }
                }
                public string Client { get; set; }
                public string List { get; set; }
            }

        }

        private ActionParams InxMailParams;
        public override ActionResult Init(string jsonData, HtmlFormValidator formData)
        {
            try
            {
                InxMailParams = InitData<ActionParams>(jsonData, formData);
                //Parameter bereinigen
                InxMailParams.Action = InxMailParams.Action.Replace("\r", "").Replace("\n", "");
            }
            catch (Exception ex)
            {
                LastException = ex;
                return ActionResult.Abort;
            }
            return ActionResult.Continue;
        }

        // Als type-Values gibt es 4 Werte
        // "client" : Der INX Client
        // "list": Die INX Liste an die die Daten gesendet werden
        // "select": "on" als checkbox oder hidden Feld. Wenn aktiv wird diese List/Client verwendet
        // "value": Ein beliebiger Wert der an INX Mail übertragen werden soll (z.B. : email, anrede, vorname, ...)
        public override HtmlFormAction.ActionResult DoAction()
        {
            ActionResult actionresult = ActionResult.Success | ActionResult.Continue;

            string url = "";

            if (InxMailParams.Action.IsLowerEqual("subscribe"))
            {
                url = INX_MAIL_SUBSCRIBE_URL;
            }
            else if (InxMailParams.Action.IsLowerEqual("unsubscribe"))
            {
                url = INX_MAIL_UNSUBSCRIBE_URL;
            }
            else
            {
                throw new ActionConfigurationException("{\"action\" : \"subscribe\" bzw. \"unsubscribe\"} fehlt oder ist falsch.");
            }

            if (InxMailParams.UseSsl)
            {
                url = "https://" + url;
            }
            else
            {
                url = "http://" + url;
            }

            //get the newsletter to subscribe to
            if(InxMailParams.Newsletters == null || !InxMailParams.Newsletter.Any())
            {
                //throw new ActionConfigurationException("Kein Newsletter gefunden. {\"newsletter\" : \"client|newsletterid\"} oder data-action-type=\"INXMail.Email\" mit value \"client|newsletterid\" fehlt.");
                actionresult = ActionResult.Continue;
                return actionresult; 
            }

            
            var formDatas = FormData.Values.Where(x => x.HasTypeField && !x.TypeField.IsLowerEqual("newsletter")).ToList();
            
            if (!string.IsNullOrEmpty(InxMailParams.ValidAction))
            {
                var confMail = formDatas.Where(x => x.TypeField.Contains(InxMailParams.ValidAction)).FirstOrDefault();

                if (confMail.Values.Count == 0)
                {
                    actionresult = ActionResult.Continue;

                    return actionresult;
                }    
            }
            
            //var actionList = new List<string>();
            //foreach (var nls in newsletters)
            //{
            //    actionList.AddRange(nls);
            //}
            ////get other params
            
            var paramList = new List<string>();
            foreach (var v in formDatas)
            {
                paramList.Add(v.TypeField + "=" + HttpUtility.UrlEncode(string.Join(",", v.Values)));
            }

            
            foreach (var nl in InxMailParams.Newsletters)
            {
                var p = String.Join("&", paramList.ToArray());
                var inxmailUrl = String.Format(url, HttpUtility.UrlEncode(nl.Client), HttpUtility.UrlEncode(nl.List), p);

                Log.Debug(String.Format("INXMailAction| URL: {0} ", inxmailUrl));

                string statusCode = "";
                string responseUri = "";
                HttpWebResponse response = null;

                try
                {

                    string responseData = String.Empty;

                    //override default proxy if needed
                    WebProxy proxy = null;
                    if (!String.IsNullOrEmpty(InxMailParams.ProxyName) && InxMailParams.ProxyPort != 0)
                    {
                        proxy = new WebProxy(InxMailParams.ProxyName, (int)InxMailParams.ProxyPort);
                    }

                    //HttpWebRequest request = HttpUtil.CreateHttpWebRequest(inxmailUrl);
                    HttpWebRequest request = HttpUtil.CreateHttpWebRequest(inxmailUrl, proxy);
                    request.Method = "GET";
                    request.Timeout = 5000; // 5 sec timeout

                    ////override default proxy if needed
                    //if (!String.IsNullOrEmpty(InxMailParams.ProxyName) && InxMailParams.ProxyPort != 0)
                    //{
                    //    WebProxy proxy = new WebProxy(InxMailParams.ProxyName, (int)InxMailParams.ProxyPort);
                    //    request.Proxy = proxy;
                    //}

                    response = (HttpWebResponse)request.GetResponse() as HttpWebResponse;

                    using (Stream dataStream = response.GetResponseStream())
                    {
                        statusCode = response.StatusCode.ToString();
                        responseUri = response.ResponseUri.ToString();

                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            //TODO - und jetzt war das nun fehler fall oder ok fall
                            responseData = reader.ReadToEnd();

                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    LastException = ex;

                    // 403 forbidden und 404, leider nicht als Fehler abfangbar
                    if (ex.ToString().Contains("(403)") || ex.ToString().Contains("(404)"))
                    {
                        Log.Info(String.Format("INXMailAction| Success "));

                        actionresult = ActionResult.Continue | ActionResult.Error;
                    }
                    else
                    {
                        Log.Info(String.Format("INXMailAction| Abort "));
                        actionresult = ActionResult.Abort | ActionResult.Error;
                    }

                }
            }

            return actionresult;
        }

        public override string ActionName
        {
            get { return "INXMail"; }
        }
    }
}