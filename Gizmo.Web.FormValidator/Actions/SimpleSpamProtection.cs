﻿using System;

namespace Gizmo.Web.FormValidator.Actions
{
  /// <summary>
  /// An HtmlFormAction that send an email.
  /// </summary>
  public class SimpleSpamProtectionAction : HtmlFormAction
    {


        /// <summary>
        /// Performs the action.
        /// </summary>
        /// <returns></returns>
        public override HtmlFormAction.ActionResult DoAction()
        {
            var theParam = (string)Parameters["HiddenParameter"];
            if (String.IsNullOrEmpty(FormData[theParam].FirstValue))
            {
                return ActionResult.Continue | ActionResult.Success;
            }

            return ActionResult.Abort | ActionResult.Success; // Feld wurde von Spambot ausgefüllt, dann raus

        }
        
        public override string ActionName
        {
            get { return "SimpleSpamProtection"; }
        }
    }
}
