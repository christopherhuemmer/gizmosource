﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Gizmo.Web.FormValidator.Actions;

namespace Gizmo.Web.FormValidator
{

    /// <summary>
    /// This is a "collection class".
    /// It groups all HtmlFormField together with the same name - mainly for check/ radio groups
    /// This does work with other elements aswell but makes little sense and results can be browser specific
    /// e.g. two different text fields with the same name will be filled in order the data is recieved vs. the order in the html document
    /// </summary>
    public class HtmlFormFieldGroup
    {
        public HtmlFormField[] Fields { get; set; }

        public List<HtmlFormFieldValidator> FailedValidators { get; set; }
        public HtmlFormFieldGroup(IEnumerable<HtmlFormField> formFields)
        {
            Fields = formFields.ToArray();
            FailedValidators = new List<HtmlFormFieldValidator>();
        }

        public bool IsSingleField
        {
            get
            {
                return Fields.Length == 1;
            }
        }
        public string Name
        {
            get
            {
                return Fields.First().Name;
            }
        }
        public string NiceName
        {
            get
            {
                return (Fields.Where(x => !string.IsNullOrEmpty(x.NiceName)).Select(x => x.NiceName).FirstOrDefault()) ?? string.Empty;
            }
        }

        /// <summary>
        /// Sets the all values for one group we just take them in order as they come, we dont have any other way to identify them.
        /// </summary>
        /// <param name="values">The values.</param>
        public void SetValues(IEnumerable<string> values, IEnumerable<HttpPostedFile> files)
        {
            var dataLeft = values.ToList();
            var filesLeft = files.ToList();
            
            foreach (var f in Fields)
            {
                if (dataLeft.Count == 0 && filesLeft.Count == 0) { break; }

                //if it is selectable/checkable the value has to match aswell
                if (f.IsSelectable)
                {
                    //set as many options as we can
                    foreach (var opt in f.Node.ChildNodes)
                    {
                        var index = dataLeft.IndexOf(opt.GetAttrib("value"));
                        if (index > -1)
                        {
                            f.Value = dataLeft[index];
                            dataLeft.RemoveAt(index);
                        }
                    }
                }
                else if (f.IsCheckable)
                {
                    var index = dataLeft.IndexOf(f.Value);
                    if (index > -1)
                    {
                        f.Value = dataLeft[index];
                        dataLeft.RemoveAt(index);
                    }
                }
                else if (f.IsFile)
                {
                    f.File = new Actions.ActionFileContainer(filesLeft.First());
                    filesLeft.RemoveAt(0);
                    f.Value = f.File.FileName;

                }
                else if (dataLeft.Any())
                {
                    f.Value = dataLeft.First();
                    dataLeft.RemoveAt(0);
                }
                else
                {
                    f.Value = "";
                }
                
            }
        }

        public bool HasValidation
        {
            get
            {
                return Fields.Any(x => x.HasValidation);
            }
        }
        /// <summary>
        /// Validates this group.
        /// </summary>
        /// <value><c>true</c> if this group is valid; otherwise, <c>false</c></value>
        public bool IsValid
        {
            get 
            {
                if (IsSingleField)
                {
                    return Fields.First().IsValid;
                }

                //for groups we only support required
                var reqField = Fields.Where(x => x.IsRequired).FirstOrDefault();
                if (reqField != null)
                {
                    if (!Fields.Any(x => x.HasSelection))
                    {
                        FailedValidators.Add(reqField.Validators.Where(x => x.ValidatorType == "required").First());
                        return false;
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// Gets the files for this group.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ActionFileContainer> GetGroupFiles()
        {
            return Fields.Where(x => x.IsFile && x.File != null).Select(x => x.File);
        }

        public IEnumerable<HtmlFormField> GetGroupFields()
        {
            return Fields;
        }
        /// <summary>
        /// Gets the group values.
        /// </summary>
        /// <returns>All values for this group</returns>
        public List<string> GetGroupValues()
        {
            var retVal = new List<string>();
            foreach (var f in Fields)
            {
                if(f.IsSelectable)
                {
                    foreach (var opt in f.Node.ChildNodes.Where(x => x.HasAttrib("selected")))
                    {
                        retVal.Add(opt.GetAttrib("value"));
                    }
                }
                else if (f.IsCheckable)
                {
                    if (f.HasSelection)
                    {
                        retVal.Add(f.Value);
                    }
                }
                else
                {
                    retVal.Add(f.Value);
                }
            }

            return retVal;
        }

        /// <summary>
        /// Gets the type of the data.
        /// </summary>
        /// <param name="actionName">Name of the action.</param>
        /// <returns>The string defined in "data-action-type" after ActionName.</returns>
        public string GetDataType(string actionName)
        {
            return Fields.Select(x => x.GetTypeInfo(actionName)).Where(x => !string.IsNullOrEmpty(x)).FirstOrDefault() ?? string.Empty;
        }

    
    }
}