﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HtmlAgilityPack;
using Gizmo.General.String;
using Gizmo.Web.FormValidator.Actions;

namespace Gizmo.Web.FormValidator
{
    public class HtmlFormField
    {
        public HtmlNode Node { get; protected set; }
        public HtmlFormFieldValidator[] Validators { get; protected set; }
        public List<HtmlFormFieldValidator> FailedValidators { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="HtmlFormField"/> class.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="validators">The validators for this field.</param>
        public HtmlFormField(HtmlNode node, IEnumerable<HtmlFormFieldValidator> validators)
        {
            Node = node;
            Validators = validators.ToArray();
            FailedValidators = new List<HtmlFormFieldValidator>();
        }

        private string _name = null;
        public string Name
        {
            get
            {
                return _name ?? ( _name = Node.GetAttrib("name"));
            }
        }

        private string _niceName = null;
        public string NiceName
        {
            get
            {
                return _niceName ?? (_niceName = Node.GetAttrib("data-value-name"));
            }
        }

        private string _htmlNodeType = null;
        public string HtmlNodeType
        {
            get
            {
                return _htmlNodeType ?? (_htmlNodeType = Node.Name.ToLower());
            }
        }
        /// <summary>
        /// Gets or sets the file.
        /// </summary>
        /// <value>
        /// The file uploaded for this field
        /// </value>
        public ActionFileContainer File { get; set; }

        /// <summary>
        /// Gets or sets the value of an input field or set the selected/checked state based on matching values.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        /// <exception cref="NotSupportedException"></exception>
        public string Value
        {
            get
            {
                if (HtmlNodeType == "textarea")
                {
                    return Node.InnerHtml;
                }
                return Node.GetAttrib("value");
            }
            set
            {
                switch (HtmlNodeType)
                {
                    case "textarea":
                        Node.InnerHtml = value;
                        break;
                    case "select":
                        foreach (var node in Node.ChildNodes)
                        {
                            if (node.HasAttrib("value", value))
                            {
                                node.Attributes.Add("selected", "selected");
                            }
                        }
                        break;
                    case "input":
                        if (Node.HasAttrib("type", "checkbox") || Node.HasAttrib("type", "radio"))
                        {
                            Node.SetAttrib("checked", "checked");
                        }
                        else
                        {
                            Node.SetAttrib("value", value);
                        }
                        break;
                    default:
                        throw new NotSupportedException();
                }
            }
        }
        /// <summary>
        /// Gets a value indicating whether this instance is checkable.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is checkbox or radio field; otherwise, <c>false</c>.
        /// </value>
        public bool IsCheckable
        {
            get
            {
                if (HtmlNodeType == "input")
                {
                    if (Node.HasAttrib("type", "checkbox") || Node.HasAttrib("type", "radio"))
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is a file input.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is file input; otherwise, <c>false</c>.
        /// </value>
        public bool IsFile
        {
            get
            {
                if (HtmlNodeType == "input")
                {
                    if (Node.HasAttrib("type", "file"))
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is selectable.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is a select field; otherwise, <c>false</c>.
        /// </value>
        public bool IsSelectable { get { return HtmlNodeType == "select"; } }

        /// <summary>
        /// Gets the type information for a specific action.
        /// </summary>
        /// <param name="actionName">Name of the action.</param>
        /// <returns>Returns the type name for the specified action in the data-action-type attribute of the field</returns>
        public string GetTypeInfo(string actionName)
        {
            string typeString;
            typeString = Node.GetAttrib("data-action-type");
            if (string.IsNullOrEmpty(typeString))
            {
                return string.Empty;
            }

            var parts = typeString.Split(';').Select(x => x.Trim()).Where(x => x.LowerStartsWith(actionName)).ToList();
            
            typeString = parts.Select(x => x.Substring(actionName.Length + 1)).FirstOrDefault();
            if (string.IsNullOrEmpty(typeString))
            {
                //check generic type (valid for all actions)
                typeString = parts.Where(x => !x.Contains('.')).FirstOrDefault();
            }

            if (string.IsNullOrEmpty(typeString))
            {
                return string.Empty;
            }

            return typeString;
        }

        /// <summary>
        /// Removes all selections (selected, checked) from this field.
        /// </summary>
        public void RemoveSelection()
        {
            Node.Attributes.Remove("selected");
            Node.Attributes.Remove("checked");
            if (HtmlNodeType == "select")
            {
                foreach (var node in Node.ChildNodes)
                {
                    node.Attributes.Remove("selected");
                }
            }
        }
        public bool IsRequired { get { return Validators.Any(x => x.ValidatorType == "required"); } }
        public bool HasValidation { get { return Validators.Any(); } }

        /// <summary>
        /// Gets a value indicating whether this instance has a selection.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has a selection; otherwise, <c>false</c>.
        /// </value>
        public bool HasSelection
        {
            get
            {
                if (Node.HasAttrib("checked") || Node.HasAttrib("selected"))
                {
                    return true;
                }
                if (HtmlNodeType == "select")
                {
                    foreach (var node in Node.ChildNodes)
                    {
                        if (node.HasAttrib("selected"))
                        {
                            return true;
                        }
                    }
                }

                if (IsCheckable || IsSelectable)
                {
                    return false;
                }

                //if we get here its some field NOT option or select - its value may not be empty
                return !string.IsNullOrWhiteSpace(Value);
            }
        }

        /// <summary>
        /// Validates this field.
        /// </summary>
        /// <value><c>true</c> if this field is valid; otherwise, <c>false</c></value>
        public bool IsValid
        {
            get
            {
                if (!HasValidation) return true;

                foreach (var validator in Validators)
                {
                    if (!validator.IsValid(this))
                    {
                        FailedValidators.Add(validator);
                    }
                }
                return !FailedValidators.Any();
            }
        }
    }
}