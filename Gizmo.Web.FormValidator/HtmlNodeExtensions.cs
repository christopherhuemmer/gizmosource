﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HtmlAgilityPack;
using Gizmo.General.String;

namespace Gizmo.Web.FormValidator
{
    public static class HtmlNodeExtensions
    {
        /// <summary>
        /// Adds value to the class attribute - creates attribute if needed
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="value">The value.</param>
        public static void AddToClass(this HtmlNode node, string value)
        {
            if (node == null) { return; }
            var attrib = node.Attributes.Where(x => x.Name.IsLowerEqual("class")).FirstOrDefault();
            if (attrib == null)
            {
                node.Attributes.Add("class", value);
            }
            else
            {
                attrib.Value = string.Concat(attrib.Value, " ", value);
            }
        }
        /// <summary>
        /// Determines whether the specified node has a specific attribute.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="attribName">Name of the attribute.</param>
        /// <returns>false if node is null, and true if the attribute has been found</returns>
        public static bool HasAttrib(this HtmlNode node, string attribName)
        {
            if (node == null) { return false; }
            return node.Attributes.Any(x => x.Name.IsLowerEqual(attribName));
        }

        /// <summary>
        /// Determines whether the specified node has a specific attribute that is set to value.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="attribName">Name of the attribute.</param>
        /// <returns>false if node is null, and true if the attribute with the correct value has been found.</returns>
        public static bool HasAttrib(this HtmlNode node, string attribName, string value)
        {
            if (node == null) { return false; }

            foreach (var att in node.Attributes.Where(x => x.Name.IsLowerEqual(attribName)))
            {
                if (att.Value == value)
                {
                    return true;
                }
            }
            return false;
            
        }

        /// <summary>
        /// Returns the string value of the specified attribute.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="attribName">Name of the attribute.</param>
        /// <returns>string.Empty if node is null or not found.</returns>
        public static string GetAttrib(this HtmlNode node, string attribName)
        {
            if (node == null) { return string.Empty; }
            return node.Attributes.Where(x => x.Name.IsLowerEqual(attribName)).Select(x => x.Value).FirstOrDefault() ?? string.Empty;
        }


        /// <summary>
        /// Sets the string value of the specified attribute. If the attribute is not present in the node it will be added.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="attribName">Name of the attribute.</param>
        /// <param name="value">The value.</param>
        public static void SetAttrib(this HtmlNode node, string attribName, string value)
        {
            if (node == null) { return;}
            var attrib = node.Attributes.Where(x => x.Name.IsLowerEqual(attribName)).FirstOrDefault();
            if (attrib == null)
            {
                node.Attributes.Add(attribName, value);
            }
            else
            {
                attrib.Value = value;
            }
        }

        
    }
}