﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
using Gizmo.General;
using Gizmo.General.Debug;

namespace Gizmo.Web.FormValidator.Actions
{
    public class SaveAsFilesAction : HtmlFormAction
    {
        private string _path = GizmoSettings.SavesAsFilesPath;

        /// <summary>
        /// Performs the action.
        /// </summary>
        /// <returns></returns>
        public override HtmlFormAction.ActionResult DoAction()
        {
            try {
                string path = _path.StartsWith("~") ? HttpContext.Current.Server.MapPath(_path) : _path;
                path = Path.Combine(path, FormId, DateTime.Now.ToString("yyyy_MM_dd_") + DateTime.Now.Ticks.ToString());
                Directory.CreateDirectory(path);

                FormDataAsXDocument.Save(Path.Combine(path, "formdata.xml"));
                int count = 0;
                foreach (var fVal in FormData.Values.Where(x => x.Files.Any()))
                {
                    foreach (var f in fVal.Files)
                    {
                        using (var fileStream = File.Create(Path.Combine(path, f.FileName)))
                        using (var ms = new MemoryStream(f.RawData))
                        {
                            ms.CopyTo(fileStream);
                            fileStream.Flush();
                            fileStream.Close();
                        }
                        count++;
                    }
                    
                }

                Log.Info("SaveAsFiles|" + count.ToString());
                return ActionResult.Continue | ActionResult.Success;
            }
            catch (Exception ex)
            {
                LastException = ex;
                Log.Error("SaveAsFiles|" + ex.ToString());
                return ActionResult.Abort | ActionResult.Error;
            }
        }
        public override string ActionName
        {
            get { return "SaveAsFilesAction"; }
        }
    }
}
