﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HtmlAgilityPack;
using Gizmo.General.String;
using Gizmo.Web.FormValidator.Actions;
using System.Configuration;
using Gizmo.General.Debug;

namespace Gizmo.Web.FormValidator
{
    /// <summary>
    /// The abstract base class for a new specific form validator
    /// Although the Form validation is the same the way error messages are displayed can be vastly different.
    /// </summary>
    public abstract class HtmlFormValidator
    {

        private static string[] ValidTypes = { "input", "select", "textarea" };

        public List<HtmlFormField> FormFields { get; protected set; }
        public List<HtmlFormFieldGroup> FormFieldGroups { get; protected set; }
        protected List<KeyValuePair<string, string>> FormData { get; set; }
        protected string[] ActionInfos { get; set; }
        protected Dictionary<string, HtmlFormAction> CustomActions { get; set; }

        public string ErrorUrl { get; set; }
        public bool HasErrorUrl { get { return !string.IsNullOrWhiteSpace(ErrorUrl); } }

        public string SuccessUrl { get; set; }
        public bool HasSuccessUrl { get { return !string.IsNullOrWhiteSpace(SuccessUrl); } }

        protected HtmlDocument Document { get; set; }
        protected HtmlNode RootNode { get; set; }
        protected string FormId { get; set; }

        protected Dictionary<string, string> DefaultErrorMessages;

        public List<Exception> Exceptions = new List<Exception>();

        [Obsolete("LastException is deprecated, please use Exceptions instead.")]
        public Exception LastException = null;

        private static string _xslLocation = null;
        /// <summary>
        /// Gets the XSL location of transformation files.
        /// </summary>
        /// <value>
        /// The XSL location.
        /// </value>
        /// <exception cref="Exception">
        /// HtmlFormValidator.XSLLocation is not set in .config
        /// or
        /// HtmlFormValidator.XSLLocation configuration - leading slash is not allowed.
        /// </exception>
        public static string XSLLocation
        {
            get
            {
                if (_xslLocation == null)
                {
                    var relDir = ConfigurationManager.AppSettings["HtmlFormValidator.XSLLocation"];
                    if (relDir == null)
                    {
                        _xslLocation = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data");
                    }
                    else
                    {
                        if (relDir.StartsWith("\\") || relDir.StartsWith("/"))
                        {
                            throw new Exception("HtmlFormValidator.XSLLocation configuration - leading slash is not allowed.");
                        }
                        _xslLocation = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, relDir);
                    }
                }

                return _xslLocation;
            }
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="HtmlFormValidator"/> class.
        /// </summary>
        /// <param name="html">The raw HTML of the entire page the form is located in.</param>
        /// <param name="formId">The form identifier.</param>
        /// <param name="controlData">The control data</param>
        public HtmlFormValidator(string html, string formId, Dictionary<string, string> controlData)
        {
            var msgPrefix = "error.";
            var errorMsgs = controlData
                .Where(x => x.Key.StartsWith(msgPrefix))
                .Select(x => new KeyValuePair<string, string>(x.Key.Substring(msgPrefix.Length), x.Value)).ToArray();
            var errorMessages = new Dictionary<string, string>();
            if (errorMsgs != null)
            {
                errorMessages = errorMsgs.ToDictionary(k => k.Key, v => v.Value);
            }

            msgPrefix = "action.";
            var actionInfos = controlData
                .Where(x => x.Key.StartsWith(msgPrefix))
                .Select(x => new KeyValuePair<int, string>(Convert.ToInt32(x.Key.Substring(msgPrefix.Length)), x.Value))
                .OrderBy(x => x.Key)
                .Select(x => x.Value)
                .ToArray();

            var errorRedirect = controlData.Where(x => x.Key.IsLowerEqual("redirect.error")).Select(c => c.Value).FirstOrDefault();
            var successRedirect = controlData.Where(x => x.Key.IsLowerEqual("redirect.success")).Select(c => c.Value).FirstOrDefault();

            Init(html, formId, errorMessages, actionInfos, errorRedirect, successRedirect);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HtmlFormValidator"/> class.
        /// </summary>
        /// <param name="html">The raw HTML of the entire page the form is located in.</param>
        /// <param name="formId">The form identifier.</param>
        /// <param name="errorMessages">Error messages in form "errortype" -> "message".</param>
        /// <param name="actionInfos">List of action jsons.</param>
        /// <param name="errorRedirect">Url for error redirect.</param>
        /// <param name="successRedirect">Url for success redirect.</param>
        public HtmlFormValidator(string html, string formId, Dictionary<string, string> errorMessages, string[] actionInfos,
            string errorRedirect = null, string successRedirect = null)
        {
            Init(html, formId, errorMessages, actionInfos, errorRedirect, successRedirect);
        }

        /// <summary>
        /// Init helper.
        /// </summary>
        /// <param name="html"></param>
        /// <param name="formId"></param>
        /// <param name="errorMessages"></param>
        /// <param name="actionInfos"></param>
        /// <param name="errorRedirect"></param>
        /// <param name="successRedirect"></param>
        private void Init(string html, string formId, Dictionary<string, string> errorMessages, string[] actionInfos, string errorRedirect, string successRedirect)
        {
            FormId = formId;
            FormFields = new List<HtmlFormField>();
            FormFieldGroups = new List<HtmlFormFieldGroup>();
            FormData = new List<KeyValuePair<string, string>>();
            CustomActions = new Dictionary<string, HtmlFormAction>();
            //some setup options and form tag would close themselves
            HtmlNode.ElementsFlags.Remove("option");
            HtmlNode.ElementsFlags.Remove("form");
            Document = new HtmlDocument();
            //write <br /> instead of <br>
            Document.OptionWriteEmptyNodes = true;
            Document.OptionOutputOriginalCase = true;
            Document.LoadHtml(html);
            RootNode = Document.DocumentNode;

            ActionInfos = actionInfos;

            ErrorUrl = errorRedirect;
            SuccessUrl = successRedirect;

            DefaultErrorMessages = errorMessages;


            foreach (var node in FormNodes)
            {
                var formField = new HtmlFormField(node, HtmlFormFieldValidator.GetValidatorsFromHtmlNode(node, DefaultErrorMessages));
                formField.RemoveSelection();
                FormFields.Add(formField);
            }

            foreach (var group in FormFields.ToLookup(x => x.Name))
            {
                var ffGroup = new HtmlFormFieldGroup(group);
                FormFieldGroups.Add(ffGroup);
            }
        }

        /// <summary>
        /// Returns the root node of the form to be validated
        /// </summary>
        /// <value>
        /// The form node.
        /// </value>
        protected HtmlNode FormNode
        {
            get
            {
                return RootNode.Descendants().First(x => x.Name == "form" && x.Attributes["id"] != null && x.Attributes["id"].Value == FormId);
            }
        }

        /// <summary>
        /// Returns the nodes of all input elements in the form that should be validated
        /// </summary>
        /// <value>
        /// The form nodes.
        /// </value>
        protected List<HtmlNode> FormNodes
        {
            get
            {
                var retVal = new List<HtmlNode>();
                foreach (var n in FormNode.Descendants().Where(x => ValidTypes.Contains(x.Name.ToLower())))
                {
                    //AWI 30.09.2015 Readded hidden fields...
                    //if (n.Name.IsLowerEqual("input") && n.GetAttrib("type").IsLowerEqual("hidden"))
                    //{
                    //    continue;
                    //}
                    if (n.Name.IsLowerEqual("input") && n.GetAttrib("name").IsLowerEqual("_formId"))
                    {
                        continue;
                    }
                    retVal.Add(n);
                }
                return retVal;
            }
        }

        /// <summary>
        /// Validates this form.
        /// </summary>
        /// <value><c>true</c> if this form is valid; otherwise, <c>false</c></value>
        public bool Validate()
        {
            bool allOk = true;

            foreach (var grp in FormFieldGroups)
            {
                if (!grp.IsValid)
                {
                    if (grp.IsSingleField)
                    {
                        AddErrorMessage(grp.Fields.First());
                    }
                    else
                    {
                        AddErrorMessage(grp);
                    }
                    allOk = false;
                }
            }
            return allOk;
        }

        /// <summary>
        /// Sets the form data.
        /// </summary>
        /// <param name="request">The request.</param>
        public virtual void SetFormData(HttpRequest request)
        {
            var data = request.ParamDictionary();
            var empty = new string[0];
            IEnumerable<HttpPostedFile> files = Enumerable.Empty<HttpPostedFile>();
            string[] rawData;
            foreach (var fg in FormFieldGroups)
            {
                rawData = data.TryGetValue(fg.Name, out rawData) ? rawData : empty;
                if (fg.Fields.Any(x => x.IsFile))
                {
                    files = request.FilesForId(fg.Name);
                }
                fg.SetValues(rawData, files);
            }
        }

        /// <summary>
        /// Gets the page HTML.
        /// </summary>
        /// <returns>The HTML as string.</returns>
        public string GetPageHTML()
        {
            return RootNode.WriteContentTo();
        }

        /// <summary>
        /// Gets the form data for a specific action.
        /// </summary>
        /// <param name="actionName">Name of the action.</param>
        /// <returns></returns>
        public Dictionary<string, HtmlFormData> GetFormData(string actionName)
        {
            return FormFieldGroups.Select(v => new HtmlFormData(v.Name, v.GetGroupValues(), v.GetDataType(actionName), v.NiceName, v.GetGroupFiles(), v.GetGroupFields())).ToDictionary(k => k.Name, v => v);
        }

        //public string GetFormDataAsXML(string actionName)
        //{
        //    var formData = GetFormData(actionName);
        //    return null;
        //}


        /// <summary>
        /// Does all actions defined in the control or aborts if one action requests that.
        /// 
        /// </summary>
        /// <returns>True if at least one action was successful and false if all actions failed or the last action requested an abort and had an error.</returns>
        public bool DoActions()
        {
            var result = false;

            HtmlFormAction currAction;
            foreach (string ai in ActionInfos)
            {
                currAction = GetFormAction(ai);
                if (currAction == null) { continue; }

                Log.Debug("DoAction: {0}", currAction.ActionName);
                var actionResult = currAction.DoAction();

                if (actionResult.HasFlag(HtmlFormAction.ActionResult.Success))
                {
                    // At least one action was successful.
                    result = true;
                    Log.Debug("DoAction: {0} Success", currAction.ActionName);
                }
                else
                {
                    Log.Debug("DoAction: {0} Error", currAction.ActionName);

                    if (currAction.LastException != null)
                    {
                        Exceptions.Add(currAction.LastException);

                        // Obsolete
                        LastException = currAction.LastException;
                    }
                }

                if (actionResult.HasFlag(HtmlFormAction.ActionResult.Abort))
                {
                    result = !actionResult.HasFlag(HtmlFormAction.ActionResult.Error);
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// Add a new custom form action.
        /// </summary>
        /// <param name="key">The form action key.</param>
        /// <param name="formAction">The form action.</param>
        public virtual void AddFormActionHandler(string key, HtmlFormAction formAction)
        {
            key = key.ToLower();
            CustomActions.Add(key, formAction);
        }

        /// <summary>
        /// Gets the form action.
        /// </summary>
        /// <param name="actionInfo">The action information.</param>
        /// <returns>An object with a class derived from HtmlFromAction</returns>
        /// <exception cref="NotSupportedException">The action  + actionInfo +  is not supported.</exception>
        protected virtual HtmlFormAction GetFormAction(string actionInfo)
        {
            HtmlFormAction retVal = null;
            actionInfo = actionInfo.Trim();
            var actionInfoKey = GetKeyFromActionInfo(actionInfo);
            if (actionInfo.LowerStartsWith("SendEmail"))
            {
                retVal = new SendMailAction();
            }
            else if (actionInfo.LowerStartsWith("INXMail"))
            {
                retVal = new INXMailAction();
            }
            else if (actionInfo.LowerStartsWith("DoubleOptIn"))
            {
                retVal = new DoubleOptInAction();
            }
            else if (actionInfo.LowerStartsWith("SimpleSpamProtection"))
            {
                retVal = new SimpleSpamProtectionAction();
            }
            else if (actionInfo.LowerStartsWith("SaveAsFiles"))
            {
                retVal = new SaveAsFilesAction();
                retVal.FormId = FormId;
            }
            else if (actionInfo.LowerStartsWith("Repost"))
            {
                retVal = new RepostAction();
                retVal.FormId = FormId;
            }
            else if (actionInfo.LowerStartsWith("AutoRespond"))
            {
                retVal = new AutoRespondAction();
                retVal.FormId = FormId;
            }
            else if (CustomActions.ContainsKey(actionInfoKey.ToLower()))
            {
                retVal = CustomActions[actionInfoKey.ToLower()];
            }
            else
            {
                throw new NotSupportedException("The action " + actionInfo + " is not supported.");
            }

            retVal.Init(GetJSONFromActionInfo(actionInfo), this);

            return retVal;
        }

        /// <summary>
        /// Gets the json from action information.
        /// </summary>
        /// <param name="actionInfo">The action information.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">Malformed ActionInfo: + actionInfo</exception>
        protected string GetJSONFromActionInfo(string actionInfo)
        {
            //extracts JSON from sonething like this:
            //SendEmail({"To":" martin.buettner@udg.de, " ,"From":"tobias.widmayer@udg.de" ,"BodyXsl":"form_kvp.xsl"})
            int pos = actionInfo.IndexOf('(');
            int posJSON = actionInfo.IndexOf('{');

            if (pos == -1 || posJSON == -1 || posJSON < pos || posJSON - pos > 1)
            {
                throw new ArgumentException("Malformed ActionInfo:" + actionInfo);
            }
            return actionInfo.Substring(posJSON).Trim(' ', ')');
        }

        /// <summary>
        /// Gets the action key from action information.
        /// </summary>
        /// <param name="actionInfo">The action information.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">Malformed ActionInfo: + actionInfo</exception>
        protected string GetKeyFromActionInfo(string actionInfo)
        {
            //extracts Key from sonething like this:
            //SendEmail({"To":" martin.buettner@udg.de, " ,"From":"tobias.widmayer@udg.de" ,"BodyXsl":"form_kvp.xsl"})
            int pos = actionInfo.IndexOf('(');
            if (pos == -1)
            {
                throw new ArgumentException("Malformed ActionInfo:" + actionInfo);
            }
            return actionInfo.Substring(0, pos).Trim();
        }

        protected abstract void AddErrorMessage(HtmlFormField formField);
        protected abstract void AddErrorMessage(HtmlFormFieldGroup formFieldGroup);
    }
}