﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Gizmo.Web
{
    /// <summary>
    /// Simple cookie access interface.
    /// </summary>
    /// <see cref="http://rayaspnet.blogspot.de/2011/04/aspnet-cookie-helper.html"/>
    /// <seealso cref="http://snipplr.com/view/43108/serverside-cookie-helper-methods/"/>
    public static class CookieManager
    {
        /// <summary>
        /// Check if a cookie exits.
        /// </summary>
        /// <param name="name">Name of cookie.</param>
        /// <returns>True if it exists else false.</returns>
        public static bool CookieExists(string name)
        {
            return HttpContext.Current.Request.Cookies[name] != null;
        }

        /// <summary>
        /// Generate a simple string representation of all cookies.
        /// </summary>
        /// <example>cookie1=value1, cookie2=value2, cookie3=value3</example>
        /// <returns>Cookies as comma separated 'key=value' string.</returns>
        public static string CookiesToString()
        {
            return string.Join(", ", GetAllCookies().Select(x => string.Format("{0}={1}", x.Key, x.Value)));
        }

        /// <summary>
        /// Deletes the cookie.
        /// </summary>
        /// <param name="name">Name of cookie.</param>
        public static void DeleteCookie(string name)
        {
            // Expiring in the past deletes the cookie.
            SetCookie(name, "", -1);
        }

        /// <summary>
        /// Get all cookies.
        /// </summary>
        /// <returns>Dictionary of name, value pairs representing all cookies.</returns>
        public static Dictionary<string, string> GetAllCookies()
        {
            Dictionary<string, string> cookies = new Dictionary<string, string>();
            foreach (string key in HttpContext.Current.Request.Cookies.AllKeys.Distinct())
            {
                cookies.Add(key, HttpContext.Current.Request.Cookies[key].Value);
            }
            return cookies;
        }

        /// <summary>
        /// Get the cookie value.
        /// </summary>
        /// <param name="name">Name of cookie.</param>
        /// <returns>The cookie value or empty string if cookie doesn't exist.</returns>
        public static string GetCookie(string name)
        {
            string result = "";
            HttpCookie cookie = HttpContext.Current.Request.Cookies[name];
            if (cookie != null)
            {
                result = cookie.Value;
            }
            return result;
        }

        /// <summary>
        /// Set a cookie.
        /// </summary>
        /// <param name="name">Name of cookie.</param>
        /// <param name="value">Value of cookie</param>
        /// <param name="expires">Expire date. If null it expires after this browser session.</param>
        /// <param name="path">The path.</param>
        /// <param name="httpOnly">If true it cannot access through client-side scripts.</param>
        public static void SetCookie(string name, string value, DateTime? expires = null, string path = "/", bool httpOnly = true)
        {
            HttpCookie Cookie = new HttpCookie(name)
            { 
                Value = value,
                Expires = (expires != null) ? (DateTime)expires : DateTime.MinValue,
                Path = path,
                HttpOnly = httpOnly
            };
            HttpContext.Current.Response.Cookies.Add(Cookie);
        }

        /// <summary>
        /// Set a cookie.
        /// </summary>
        /// <param name="name">Name of cookie.</param>
        /// <param name="value">Value of cookie</param>
        /// <param name="days">Number of days before it expires.</param>
        /// <param name="path">The path.</param>
        /// <param name="httpOnly">If true it cannot access through client-side scripts.</param>
        public static void SetCookie(string name, string value, int days, string path = "/", bool httpOnly = true)
        {
            SetCookie(name, value, DateTime.Now.AddDays(days), path, httpOnly);
        }
    }
}
