﻿using System;
using System.Text;
using System.Web;
using Gizmo.General.IO;
using Gizmo.General.String;

namespace Gizmo.Web
{
    /// <summary>
    /// Collection of IP related helper methods.
    /// </summary>
    public static class IpUtil
    {
        /// <summary>
        /// Get the ip of the current client. 
        /// See http://thepcspy.com/read/getting_the_real_ip_of_your_users/ for more information.
        /// </summary>
        /// <param name="checkForProxy">
        /// If true this function tries to get the real ip even if the user is behind a proxy.
        /// </param>
        /// <returns>The ip as an string.</returns>
        public static string ClientIp(bool checkForProxy = true)
        {
            string ip = "";

            // First check if the user is behind a proxy server.
            if (checkForProxy)
            {
                ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (ip != null && ip.Contains(","))
                {
                    ip = ip.Split(',')[0];
                }
            }

            // If he isn't behind a proxy server get the remote address.
            if (string.IsNullOrEmpty(ip) || ip.IsLowerEqual("unknown"))
            {
                ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }

            return ip;
        }

        /// <summary>
        /// Get country for given ip. Uses the free web service by https://www.wipmania.com/de/api/.
        /// </summary>
        /// <param name="ip">The ip.</param>
        /// <returns>The country as a lower case string.</returns>
        public static string CountryByIp(string ip)
        {
            string result;
            string content = WebUtil.FetchUrl("http://api.wipmania.com/" + ip);
            string[] countryshort = content.Split('>');
            if (countryshort.Length == 2)
            {
                result = countryshort[1];
            }
            else
            {
                result = countryshort[0];
            }
            return result.ToLower();
        }

        /// <summary>
        /// Get the country for the current client. See CountryByIp().
        /// </summary>
        /// <returns>The country as a lower case string.</returns>
        public static string CountryOfClient()
        {
            return CountryByIp(ClientIp());
        }
    }
}
