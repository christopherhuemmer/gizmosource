﻿using Gizmo.General.Caching;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Web;
using System.Web.Hosting;

namespace Gizmo.Web
{
    /// <summary>
    /// Adds a fingerprint string to the path of a asset. This path changes if the file is changed. 
    /// Add following rewrite rule to the system.webServer section of your web.config:
    ///  <rewrite>
    ///    <rules>
    ///      <!-- See Gizmo.Web.AssetFingerprint -->
    ///      <rule name="AssetFingerprint">
    ///        <match url="([\S]+)(/v-[0-9]+/)([\S]+)" />
    ///        <action type="Rewrite" url="{R:1}/{R:3}" />
    ///       </rule>
    ///     </rules>
    ///   </rewrite>
    /// </summary>
    /// <see cref="http://madskristensen.net/post/cache-busting-in-aspnet"/>
    public class AssetFingerprint 
    { 
        public static string Tag(string rootRelativePath, string prefix = "v-") 
        { 
            if (!Cache.Has(rootRelativePath)) 
            { 
                string absolute = HostingEnvironment.MapPath("~" + rootRelativePath);

                DateTime date = File.GetLastWriteTime(absolute); 
                int index = rootRelativePath.LastIndexOf('/');

                string result = rootRelativePath.Insert(index, string.Concat("/", prefix, date.Ticks)); 

                var policy = new CacheItemPolicy();
                policy.ChangeMonitors.Add(new HostFileChangeMonitor(new List<string> { absolute }));
                Cache.Set(rootRelativePath, result, 24 * 60, false, policy);
            }

            return Cache.Get(rootRelativePath) as string; 
        } 
    }
}
