﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Net;
using Gizmo.General;
using System.IO;

namespace Gizmo.Web
{
    public static class HttpUtil
    {
        /// <summary>
        /// Returns all HttpContext.Request.Params as Dictionary&lt;string, string[]&gt
        /// </summary>
        /// <param name="cx">The HttpContext</param>
        /// <param name="sc">The StringComparer by default none is used.</param>
        /// <returns>All HttpContext.Request.Params as Dictionary&lt;string, string[]&gt;</returns>
        public static Dictionary<string, string[]> ParamDictionary(this HttpContext cx, StringComparer sc = null)
        {
            return cx.Request.ParamDictionary(sc);
        }

        /// <summary>
        /// Returns all HttpContext.Request.Params as Dictionary&lt;string, string[]&gt
        /// </summary>
        /// <param name="re">The HttpContext.Request</param>
        /// <param name="sc">The StringComparer by default none is used.</param>
        /// <returns>All HttpContext.Request.Params as Dictionary&lt;string, string[]&gt;</returns>
        public static Dictionary<string, string[]> ParamDictionary(this HttpRequest re, StringComparer sc = null)
        {
            Dictionary<string, string[]> ret;
            if (sc == null)
            {
                ret = new Dictionary<string, string[]>();
            }
            else
            {
                ret = new Dictionary<string, string[]>(sc);
            }
            var pColl = re.Params;
            string key;

            for (int i = 0; i <= pColl.Count - 1; i++)
            {
                key = pColl.GetKey(i);
                ret[key] = pColl.GetValues(i);
            }

            return ret;
        }

        /// <summary>
        /// Get the domain of the given host name.
        /// </summary>
        /// <see cref="http://stackoverflow.com/a/26190029"/>
        /// <param name="url">The url.</param>
        /// <returns>The domain of the host (might be the same).</returns>
        public static string Domain(Uri url)
        {
            return Domain(url.Host);
        }

        /// <summary>
        /// Get the domain of the given host name.
        /// </summary>
        /// <see cref="http://stackoverflow.com/a/26190029"/>
        /// <param name="host">The host name.</param>
        /// <returns>The domain of the host (might be the same).</returns>
        public static string Domain(string host)
        {
            int lastDot = host.LastIndexOf('.');
            int secondToLastDot = -1;
            if (lastDot > -1)
            {
                secondToLastDot = host.Substring(0, lastDot).LastIndexOf('.');
            }

            if (secondToLastDot > -1)
            {
                return host.Substring(secondToLastDot + 1);
            }
            else
            {
                return host;
            }
        }

        /// <summary>
        /// Get files for form input id
        /// </summary>
        /// <param name="re">The HttpContext.Request</param>
        /// <param name="id">The identifier in the form</param>
        /// <returns>Enumerable of HttpPostedFile that had the key id</returns>
        public static IEnumerable<HttpPostedFile> FilesForId(this HttpRequest re, string id)
        {
            for (int i = 0; i < re.Files.Count; i++)
            {
                if (id.Equals(re.Files.Keys[i], StringComparison.OrdinalIgnoreCase))
                {
                    if (re.Files[i].ContentLength > 0)
                    {
                        yield return re.Files[i];
                    }
                }
            }
        }

        /// <summary>
        /// Creates the HTTP web request.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="proxy">Optional - if not null use this proxy instead of default</param>
        /// <returns>HttpWebRequest object</returns>
        public static HttpWebRequest CreateHttpWebRequest(string url, WebProxy proxy = null)
        {
            var user = String.Empty;
            var psw = String.Empty;

            var parts = url.Split('/');
            {
                // ist in der domäne ein @ drin, dann user und psw extrahieren
                if (parts.Length >= 2 && parts[2].Contains("@"))
                {
                    var s1 = parts[2].Split('@');
                    var s2 = s1[0].Split(':');
                    user = s2[0];
                    psw = s2[1];

                    parts[2] = s1[1];
                    url = String.Join("/", parts);
                }
            }


            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            if (proxy == null)
            {
                proxy = GizmoSettings.WebProxy;
            }
            if (proxy != null)
            {
                request.Proxy = proxy ;
            }
            if (!String.IsNullOrEmpty(user) && !String.IsNullOrEmpty(psw))
            {
                request.Credentials = new NetworkCredential(user, psw);
            }

            

            return request;

        }


        /// <summary>
        /// Gets the Response of an HttpGet
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="encoding">The Encoding (default UTF8).</param>
        /// <returns></returns>
        public static string HttpGet(string url)
        {
            return HttpGet(url, Encoding.UTF8);
        }
        public static string HttpGet(string url, Encoding encoding)
        {
            string result = "";

            // Create the web request  
            HttpWebRequest request =CreateHttpWebRequest(url);

            // Get response  
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream(), encoding);

                result = reader.ReadToEnd();
            }

            return result;
        }
    }
}
