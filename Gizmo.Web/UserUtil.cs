﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Gizmo.Web
{
    /// <summary>
    /// Collection of User related helper methods.
    /// </summary>
    public static class UserUtil
    {
        /// <summary>
        /// Get the operating system of the current client based on the user agent.
        /// </summary>
        /// <returns></returns>
        public static string UserOs()
        {
            Dictionary<string, string> osList = new Dictionary<string, string>();
            osList.Add("Windows 3.11", "Win16");
            osList.Add("Windows 95", "(Windows 95|Win95|Windows_95)");
            osList.Add("Windows 98", "(Windows 98|Win98)");
            osList.Add("Windows 2000", "(Windows NT 5.0|Windows 2000)");
            osList.Add("Windows XP", "(Windows NT 5.1|Windows XP)");
            osList.Add("Windows Server 2003", "(Windows NT 5.2)");
            osList.Add("Windows Vista", "(Windows NT 6.0)");
            osList.Add("Windows 7", "Windows NT 6.1");
            osList.Add("Windows 8", "Windows NT 6.2");
            osList.Add("Windows 8.1", "Windows NT 6.3");
            osList.Add("Windows NT 4.0", "(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)");
            osList.Add("Windows ME", "Windows ME");
            osList.Add("Open BSD", "OpenBSD");
            osList.Add("Sun OS", "SunOS");
            osList.Add("Linux", "(Linux|X11)");
            osList.Add("Mac OS", "(Mac_PowerPC|Macintosh)");
            osList.Add("QNX", "QNX");
            osList.Add("BeOS", "BeOS");
            osList.Add("OS/2", "OS/2");
            osList.Add("Search Bot", "(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves/Teoma|ia_archiver)");

            string userAgent = HttpContext.Current.Request.UserAgent;
            string result = "Unknown OS";
            foreach (var kv in osList)
            {
                if (Regex.Match(userAgent, kv.Value).Success)
                {
                    result = kv.Key;
                    break;
                }
            }
            return result;
        }
    }
}
