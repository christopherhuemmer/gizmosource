﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Reflection;
using System.Linq.Expressions;
using Gizmo.General.String;
using Gizmo.General.GenericBinder;

namespace Gizmo.Web.GenericBinder
{
    /// <summary>
    /// Adds automatic support for reading the parameters from the request
    /// </summary>
    public class GenericBinder : Gizmo.General.GenericBinder.GenericBinder
    {
        /// <summary>
        /// Updates the specified object with the Data in HttpRequest.Params
        /// </summary>
        /// <param name="toUpdate">The object to update</param>
        public static void Update(object toUpdate)
        {
            var paramDictionary = HttpContext.Current.ParamDictionary();
            var replace = GetReplaceValues(toUpdate);
            var pd = paramDictionary.ToDictionary(k => k.Key.RemoveAll(replace, StringComparison.OrdinalIgnoreCase), v => v.Value.AsEnumerable(), StringComparer.OrdinalIgnoreCase);
            InternalUpdate(toUpdate, pd);
        }
    }
}
