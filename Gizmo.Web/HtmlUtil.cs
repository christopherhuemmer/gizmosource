﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;

namespace Gizmo.Web
{
    /// <summary>
    /// Html related helper functions.
    /// </summary>
    public static class HtmlUtil
    {
        /// <summary>
        /// Replaces newline characters with HTML breaks.
        /// </summary>
        /// <param name="text">The text with newline characters.</param>
        /// <param name="breakTag">The replacement text.</param>
        /// <returns>The updated text as an HTML string.</returns>
        public static IHtmlString NewlineToBreak(string text, string breakTag = "<br />")
        {
            var result = Regex.Replace(text, @"\r\n?|\n", breakTag);
            return new HtmlString(result);
        }

        /// <summary>
        /// Returns a string encoded with HttpUtility.HtmlEncode.
        /// </summary>
        /// <param name="str">The string which maybe null.</param>
        /// <returns>The HtmlEncoded string or an empty string if it was null.</returns>
        public static string HtmlSafe(this string str)
        {
            str = str ?? string.Empty;
            return System.Web.HttpUtility.HtmlEncode(str);
        }
    }
}
