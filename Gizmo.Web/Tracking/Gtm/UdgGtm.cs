﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Gizmo.Web.Tracking.Gtm
{
    /// <summary>
    /// Collection of utility function to setup GTM in a project according to the UDG standard.
    /// </summary>
    public static class UdgGtm
    {
        /// <summary>
        /// Clean country according to Philip Schneider (philipp.schneider@udg.de).
        /// </summary>
        /// <param name="country">The country</param>
        /// <returns>The corrected country.</returns>
        public static string CleanCountry(string country)
        {
            return country.ToLower();
        }

        /// <summary>
        /// Cleans the path according to Philip Schneider (philipp.schneider@udg.de).
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>The corrected path.</returns>
        public static string CleanPath(string path)
        {
            // Remove whitespaces.
            path = path.Trim();

            // Everything must be lower case.
            path = path.ToLower();

            // Always have slash at the beginning.
            if (!path.StartsWith("/"))
            {
                path = "/" + path;
            }

            // Remove extension.
            int extensionPos = path.LastIndexOf(".");
            if (extensionPos >= 0)
            {
                path = path.Substring(0, extensionPos);
            }

            // Replace some sub strings.
            string replacer = "-";
            string[] toReplace = { " ", "'", "\"", ".", ":" };
            foreach (string replace in toReplace)
            {
                path = path.Replace(replace, replacer);
            }

            // Remove unwanted sub strings.
            string[] toRemove = { ",", "<br>", "<br/>" };
            foreach (string remove in toRemove)
            {
                path = path.Replace(remove, "");
            }

            // Remove multiple replacer strings.
            Regex rx = new Regex(replacer + "{2,}");
            path = rx.Replace(path, replacer);

            // Remove last slash. But only if it is not on root level.
            if (path != "/")
            {
                path = path.TrimEnd(new char[] { '/' });
            }
            path = path.Trim('-');

            return path;
        }

        /// <summary>
        /// Uses CleanPath() to clean the path and then uses only the name (last) part.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>The corrected name.</returns>
        public static string CleanName(string path)
        {
            string name = CleanPath(path);
            int pos = name.LastIndexOf("/");
            if (pos >= 0)
            {
                name = name.Substring(pos);
            }
            return name;
        }

        /// <summary>
        /// Simple helper to check if the IP of the current user is in the defined range.
        /// </summary>
        /// <param name="ipDefinitions">The IP definitions.</param>
        /// <returns></returns>
        public static string CurrentIpIsInDefinition(string ipDefinitions)
        {
            bool checkForProxy = true;
            string ip = IpUtil.ClientIp(checkForProxy);
            IpChecker ipChecker = new IpChecker(ipDefinitions);
            return ipChecker.IsIpKnown(ip) ? "true" : "false";
        }

        /// <summary>
        /// Get the formatted html/javascript code to include the data layer.
        /// </summary>
        /// <param name="pageName">The page name.</param>
        /// <param name="pageTitle">The page title.</param>
        /// <param name="pageId">The page id.</param>
        /// <param name="pageSystemPath">The page system path.</param>
        /// <param name="pageCreated">Date when the page was created.</param>
        /// <param name="pageLastEdit">Date when the page was last edited.</param>
        /// <param name="pageCountry">The page country.</param>
        /// <param name="pageLanguage">The page language.</param>
        /// <param name="isInternal">Indicates if current user is internal.</param>
        /// <returns></returns>
        public static string GtmDataLayer(string pageName, string pageTitle, string pageId, string pageSystemPath, 
                                          DateTime pageCreated, DateTime pageLastEdit, string pageCountry, 
                                          string pageLanguage, string isInternal)
        {
            const string dateFormat = "yyyy-MM-dd";
            return GtmDataLayer(pageName, pageTitle, pageId, pageSystemPath, pageCreated.ToString(dateFormat),
                pageLastEdit.ToString(dateFormat), pageCountry, pageLanguage, isInternal);
        }

        /// <summary>
        /// Get the formatted html/javascript code to include the data layer.
        /// </summary>
        /// <param name="pageName">The page name.</param>
        /// <param name="pageTitle">The page title.</param>
        /// <param name="pageId">The page id.</param>
        /// <param name="pageSystemPath">The page system path.</param>
        /// <param name="pageCreated">Date when the page was created.</param>
        /// <param name="pageLastEdit">Date when the page was last edited.</param>
        /// <param name="pageCountry">The page country.</param>
        /// <param name="pageLanguage">The page language.</param>
        /// <param name="isInternal">Indicates if current user is internal.</param>
        /// <returns></returns>
        public static string GtmDataLayer(string pageName, string pageTitle, string pageId, string pageSystemPath, 
                                          string pageCreated, string pageLastEdit, string pageCountry, 
                                          string pageLanguage, string isInternal)
        {
            string template = @"
                <script type=""text/javascript"">
                dataLayer = [{{
                'pageName':'{0}',
                'pageTitle':'{1}',
                'pageId':'{2}',
                'pageSystemPath':'{3}',
                'pageCreated':'{4}',
                'pageLastEdit':'{5}',
                'pageCountry':'{6}',
                'pageLanguage':'{7}',
                'visitorLoginState':'false',
                'internal':'{8}'
                }}];
                </script>
            ";
            return string.Format(template, pageName, pageTitle, pageId, pageSystemPath, pageCreated, 
                pageLastEdit, pageCountry, pageLanguage, isInternal);
        }

        /// <summary>
        /// Get the formatted html/javascript code to include the google tag manager.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GtmScript(string id)
        {
            string template = @"
                <!-- Google Tag Manager -->
                <noscript><iframe src=""//www.googletagmanager.com/ns.html?id={0}""
                height=""0"" width=""0"" style=""display:none;visibility:hidden""></iframe></noscript>
                <script>(function(w,d,s,l,i){{w[l]=w[l]||[];w[l].push({{'gtm.start':
                new Date().getTime(),event:'gtm.js'}});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                }})(window,document,'script','dataLayer','{0}');
                </script>
                <!-- End Google Tag Manager -->
            ";
            return string.Format(template, id);
        }
    }
}
