﻿using Gizmo.General.Debug;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Gizmo.Web.Tracking.Gtm
{
    /// <summary>
    /// Allows extended ip comparison.
    /// </summary>
    public class IpChecker
    {
        private IEnumerable<string> Ips;

        /// <summary>
        /// Create new IP checker with given definition of known IPs.
        /// </summary>
        /// <param name="ips">
        /// IPs are supported in different text forms (including multiple lines):
        /// 192.168.69.1;192.168.69.2
        /// 192.168.69.1-255;192.168.69-80.1-255
        /// 192.168.69.1; 192.168.69.1-255
        /// </param>
        public IpChecker(string ips) : this(ips, ";", ",")
        {}

        /// <summary>
        /// Create new IP checker with given definition of known IPs.
        /// </summary>
        /// <param name="ips">
        /// IPs are supported in different text forms (including multiple lines):
        /// 192.168.69.1;192.168.69.2
        /// 192.168.69.1-255;192.168.69-80.1-255
        /// 192.168.69.1; 192.168.69.1-255
        /// </param>
        /// <param name="separators">The IP separators.</param>
        public IpChecker(string ips, params string[] separators)
        {
            Debug.Assert(separators != null && separators.Any(), "IpChecker: Parameter 'separators' must not be empty.");

            // Replace line breaks.
            ips = ips.Replace(System.Environment.NewLine, separators.First());
            // Allow comma as seperator

            // Remove non supported characters.
            string regex = @"[^0-9;.-]+";
            ips = Regex.Replace(ips, regex, "");

            // Create single IP definitions.
            Ips = ips.Split(separators, StringSplitOptions.RemoveEmptyEntries);

            // Validate IP definitions.
            const int dots = 3;
            foreach (string ip in Ips)
            {
                // Validate number of dots in ip.
                if (ip.Count(x => x == '.') != dots)
                {
                    throw new GtmConfigurationException(new FormatException(string.Format("Definition of IP {0} is not valid.", ip)));
                }
            }
        }

        /// <summary>
        /// Checks if a IP is in the known definition of this checker.
        /// </summary>
        /// <param name="ip">The IP to compare.</param>
        /// <returns>True if IP is known else false.</returns>
        public bool IsIpKnown(string ip)
        {
            bool result = false;
            foreach (string knownIp in Ips)
            {
                if (IsIpInDefinition(ip, knownIp))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// Compare given IP to a IP definition.
        /// </summary>
        /// <param name="ip">The IP to compare.</param>
        /// <param name="ipDefinition">The IP definition to compare to.</param>
        /// <returns>True if IP is in definition else false.</returns>
        private bool IsIpInDefinition(string ip, string ipDefinition)
        {
            char[] separator = new  char[] { '.' };
            IList<string> ipParts = ip.Split(separator);
            IList<string> defParts = ipDefinition.Split(separator);

            bool result = true;
            for (int i = 0; i < ipParts.Count; ++i)
            {
                string ipPart = ipParts[i];
                string defPart = defParts[i];

                // Strip brackets. Don't know why but the original PHP did it.
                string regex = @"(\[|\])";
                defPart = Regex.Replace(defPart, regex, "");

                // Get range if available.
                IList<string> defRange = new List<string>(defPart.Split(new char[] { '-' }));
                if (defRange.Count == 1)
                {
                    defRange.Add(defPart);
                }

                // Compare.
                int ipPartNum;
                int defPartMin;
                int defPartMax;

                try
                {
                    defPartMin = Convert.ToInt32(defRange[0]);
                    defPartMax = Convert.ToInt32(defRange[1]);
                }
                catch (Exception ex)
                {
                    throw new GtmConfigurationException(ex);
                }

                if (!int.TryParse(ipPart, out ipPartNum))
                {
                    Log.Info($"GTM Unparseable IP {ip}");
                    return false;
                }

                if (ipPartNum < defPartMin || defPartMax < ipPartNum)
                {
                    result = false;
                    break;
                }
            }
            return result;
        }
    }
}
