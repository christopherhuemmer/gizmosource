﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gizmo.Web.Tracking.Gtm
{
    class GtmConfigurationException : General.Exceptions.ConfigurationException
    {
        public GtmConfigurationException(Exception Ex) : base("Invalid GTM Configuration - check CMS", Ex) { }
    }
}
