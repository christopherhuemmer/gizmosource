﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gizmo.Web.Images.SourceSets
{
    [Serializable]
    public class SourceSetConfig
    {
        public List<SourceSet> SourceSets { get; set; }
        public SourceSetConfig()
        {
            SourceSets = new List<SourceSet>();
            //Dummy test data
            //SourceSets.Add(new SourceSet("set-name", 300, new int[] { 1216, 610, 468, 300 }, "(max-width: 640px) calc(100vw - 30px),(max-width: 1024px) calc(50vw - 25px), 300px"));
        }
    }
}
