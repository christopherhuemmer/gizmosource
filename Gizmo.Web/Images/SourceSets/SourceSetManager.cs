﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Gizmo.Web.Images.SourceSets
{
    /*
        This is a General wrapper for sorce set settings.

    */
    public static class SourceSetManager
    {

        private static SourceSetConfig _config = null;
        private static SourceSetConfig Config
        {
            get
            {
                if(_config == null)
                {
                    var path = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data");
                    var parts = new string[] { path, "SourceSets.xml" };
                    path = System.IO.Path.Combine(parts);
                    using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(SourceSetConfig));
                        _config = (SourceSetConfig)ser.Deserialize(fs);
                    }
                }
                return _config;
            }
        }

        private static Dictionary<string, SourceSet> _allSets = null;
        public static Dictionary<string, SourceSet> AllSets
        {
            get
            {
                if(_allSets == null)
                {
                    //this can crash if config is wrong - this can only happen during dev und should crash
                    _allSets = Config.SourceSets.ToDictionary(k => k.Name, v => v);
                }
                return _allSets;
            }
        }

        


        public static string SerializeAsString()
        {
            using (System.IO.StringWriter sw = new System.IO.StringWriter())
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(SourceSetConfig));
                ser.Serialize(sw, Config);
                return sw.ToString();
            }
        }

        public static SourceSet GetSourceSet(string name)
        {
            SourceSet retVal;
            AllSets.TryGetValue(name, out retVal);
            return retVal;
        }
        public static string Render(string name, ISourceSetRenderer renderer)
        {
            var retVal = string.Empty;
            SourceSet ss;
            if(AllSets.TryGetValue(name, out ss))
            {
                retVal = ss.Render(renderer);
            }
            return retVal;
        }
        public static void Render(string name, ISourceSetRenderer renderer, StringBuilder sb)
        {
            SourceSet ss;
            if (AllSets.TryGetValue(name, out ss))
            {
                sb.AppendLine(ss.Render(renderer));
            }
        }
    }
}
