﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Gizmo.Web.Images.SourceSets
{
    public interface ISourceSetRenderer
    {
        string Render(SourceSet set);
        XElement ToXElement(SourceSet set, XElement parent = null);
    }
}
