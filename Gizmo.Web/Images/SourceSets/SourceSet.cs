﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Gizmo.Web.Images.SourceSets
{
    [Serializable]
    public class SourceSet
    {
        public class NamedWidth { public string Name { get; set; } public int Width { get; set; } }
        public string Name { get; set; }
        public int DefaultWidth { get; set;  }
        public List<int> Widths { get; set; }
        public List<NamedWidth> NamedWidths { get; set; }
        public string MediaQuery { get; set; }

        internal SourceSet() { }
        public SourceSet(string name, int defaultWidth, IEnumerable<int> widths,  string mediaQuery)
        {
            Name = name;
            DefaultWidth = defaultWidth;
            Widths = widths.ToList();
            MediaQuery = mediaQuery;
        }

        public string Render(ISourceSetRenderer renderer)
        {
            return renderer.Render(this);
        }

        public XElement ToXElement(ISourceSetRenderer renderer, XElement parent = null)
        {
            return renderer.ToXElement(this, parent);
        }

    }
}
