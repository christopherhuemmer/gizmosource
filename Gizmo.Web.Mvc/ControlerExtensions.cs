﻿using System.IO;
using System.Web.Mvc;

namespace Gizmo.Web.Mvc
{
  /// <summary>
  /// MVC Controller related extensions.
  /// </summary>
  public static class ControlerExtensions
    {
        /// <summary>
        /// Render view to string.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="viewName">The name of the view.</param>
        /// <param name="model">The model.</param>
        /// <returns>The rendered html.</returns>
        public static string RenderRazorViewToString(this System.Web.Mvc.Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View,
                                             controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}
