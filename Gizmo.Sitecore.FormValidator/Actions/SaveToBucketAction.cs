﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
using Gizmo.Web.FormValidator;
using Sitecore.SecurityModel;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace Gizmo.SitecoreGizmo.FormValidator.Actions
{
    public class SaveToBucketAction : HtmlFormAction
    {
        public SaveToBucketAction() : base()
        {
            IncludeFilesEncoded = true;
        }

        protected class ActionParams
        {
            public string BucketPath { get; set; }            
        }

        protected ActionParams BucketParams { get; set; }

        /// <summary>
        /// Initializes this instance with specified data. Overrides the default implementation.
        /// </summary>
        /// <param name="jsonData"></param>
        /// <param name="formData"></param>
        /// <returns>
        /// ActionResult to let the Validator know how to continue.
        /// </returns>
        public override ActionResult Init(string jsonData, HtmlFormValidator formData)
        {
            try
            {
                BucketParams = InitData<ActionParams>(jsonData, formData);                
            }
            catch (Exception ex)
            {
                LastException = ex;
                return ActionResult.Abort;
            }
            return ActionResult.Continue;
        }

        /// <summary>
        /// Performs the action.
        /// </summary>
        /// <returns></returns>
        public override HtmlFormAction.ActionResult DoAction()
        {
            try
            {
                using (new SecurityDisabler())
                {                    
                    //First get the form folder from the database
                    Database db = Sitecore.Context.Database;
                    Item formDataFolder = db.Items[BucketParams.BucketPath];


                    //Now we need to get the template from which the item is created
                    TemplateItem template = db.GetTemplate("User Defined/Forms/Form Data");


                    //Now we can add the new item as a child to the parent
                    var formDataItem = formDataFolder.Add("Form Data", template);


                    //We can now manipulate the fields and publish as in the previous example
                    using (new EditContext(formDataItem))
                    {
                        formDataItem["Content"] = FormDataAsXDocument.ToString().Insert(FormDataAsXDocument.ToString().IndexOf("</items>"),"<item name=\"Timestamp\" valuename=\"\"><value>"+ string.Format("{0:d/M/yyyy HH:mm:ss}",formDataItem.Statistics.Created) +"</value></item>");
                    }
                }            
                
                return ActionResult.Continue | ActionResult.Success;
            }
            catch (Exception ex)
            {
                LastException = ex;                
                return ActionResult.Abort & ActionResult.Error;
            }
        }
        public override string ActionName
        {
            get { return "SaveToBucketAction"; }
        }
    }
}
