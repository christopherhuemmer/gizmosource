﻿using System.Dynamic;
using Newtonsoft.Json;

namespace Gizmo.SitecoreGizmo.FormValidator
{
    /// <summary>
    /// Create action info configuration (in JSON format) for various actions.
    /// </summary>
    public static class ActionInfoConfig
    {
        /// <summary>
        /// Save to file action.
        /// </summary>
        /// <returns></returns>
        public static string SaveAsFiles()
        {
            // Currently no parameters.
            var result = WrapJson("SaveAsFiles", "{}");
            return result;
        }

        /// <summary>
        /// Save to bucket action.
        /// </summary>
        /// <returns></returns>
        public static string SaveToBucket(string bucketPath)
        {
            dynamic obj = new ExpandoObject();
            obj.BucketPath = bucketPath;
            var json = JsonConvert.SerializeObject(obj);
            var result = WrapJson("SaveToBucket", json);
            return result;
        }

        /// <summary>
        /// Send mail action.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="from"></param>
        /// <param name="subject"></param>
        /// <param name="bodyXsl"></param>
        /// <param name="headline"></param>
        /// <param name="cc"></param>
        /// <param name="bcc"></param>
        /// <param name="replyTo"></param>
        /// <param name="attachmentXsl"></param>
        /// <param name="attachmentName"></param>
        /// <param name="attachmentMime"></param>
        /// <returns></returns>
        public static string SendEmail(string to, string from, string subject,
            string bodyXsl = null, string headline = null,
            string cc = null, string bcc = null, string replyTo = null,
            string attachmentXsl = null, string attachmentName = null, string attachmentMime = null)
        {
            dynamic obj = new ExpandoObject();
            obj.To = to;
            obj.From = from;
            obj.CC = cc;
            obj.BCC = bcc;
            obj.ReplyTo = replyTo;
            obj.AttachmentXsl = attachmentXsl;
            obj.AttachmentName = attachmentName;
            obj.AttachmentMime = attachmentMime;
            obj.BodyXsl = bodyXsl;
            obj.Subject = subject;
            obj.Headline = headline;

            var json = JsonConvert.SerializeObject(obj);
            var result = WrapJson("SendEmail", json);
            return result;
        }

        /// <summary>
        /// Spam protection action.
        /// </summary>
        /// <param name="hiddenParameter"></param>
        /// <returns></returns>
        public static string SimpleSpamProtection(string hiddenParameter)
        {
            dynamic obj = new ExpandoObject();
            obj.HiddenParameter = hiddenParameter;

            var json = JsonConvert.SerializeObject(obj);
            var result = WrapJson("SimpleSpamProtection", json);
            return result;
        }

        /// <summary>
        /// Wrap JSON in action format with name.
        /// </summary>
        /// <param name="actionName"></param>
        /// <param name="json"></param>
        /// <returns></returns>
        private static string WrapJson(string actionName, string json)
        {
            return string.Format("{0}({1})", actionName, json);
        }
    }
}
