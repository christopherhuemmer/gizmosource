﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Globalization;

namespace Gizmo.SitecoreGizmo.FormValidator
{
    /// <summary>
    /// Generate error messages from Sitecore translations. 
    /// Setting properties to translations keys or directly 
    /// to messages allowes to override the default messages.
    /// </summary>
    public class ErrorMessageConfig
    {
        public string RequiredError { get; set; }
        public string MinLengthError { get; set; }
        public string MaxLengthError { get; set; }
        public string PatternError { get; set; }
        public string NumberError { get; set; }
        public string DateError { get; set; }
        public string DateTimeError { get; set; }
        public string TimeError { get; set; }
        public string EmailError { get; set; }
        public string AcceptError { get; set; }

        public ErrorMessageConfig()
        {
            RequiredError = "Forms.Error.Required";
            MinLengthError = "Forms.Error.MinLength";
            MaxLengthError = "Forms.Error.MaxLength";
            PatternError = "Forms.Error.Pattern";
            NumberError = "Forms.Error.Number";
            DateError = "Forms.Error.Date";
            DateTimeError = "Forms.Error.DateTime";
            TimeError = "Forms.Error.Time";
            EmailError = "Forms.Error.Email";
            AcceptError = "Forms.Error.Accept";
        }

        /// <summary>
        /// Generate error messages based on properties.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> ToDictionary()
        {
            return new Dictionary<string, string>()
            {
                {"required", Translate.Text(RequiredError)},
                {"minlength", Translate.Text(MinLengthError)},
                {"maxlength", Translate.Text(MaxLengthError)},
                {"pattern", Translate.Text(PatternError)},
                {"number", Translate.Text(NumberError)},
                {"date", Translate.Text(DateError)},
                {"datetime", Translate.Text(DateTimeError)},
                {"time", Translate.Text(TimeError)},
                {"email", Translate.Text(EmailError)},
                {"data-accept", Translate.Text(AcceptError)}
            };
        }
    }
}
