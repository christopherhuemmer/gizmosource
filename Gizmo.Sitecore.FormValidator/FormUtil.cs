﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Gizmo.SitecoreGizmo.Items;
using Sitecore.Data.Items;

namespace Gizmo.SitecoreGizmo.FormValidator
{
    /// <summary>
    /// Form handling related utilities.
    /// </summary>
    public static class FormUtil
    {
        /// <summary>
        ///  Select value of a given field if available.
        /// </summary>
        /// <param name="fieldItem"></param>
        /// <returns></returns>
        public static string Value(Item fieldItem)
        {
            if (Sitecore.Context.PageMode.IsExperienceEditor)
            {
                return "";
            }
            var name = ScValue.GetString(fieldItem, "Name");
            var result = ScValue.GetString(fieldItem, "Default Value").Trim();
            if (!string.IsNullOrWhiteSpace(HttpContext.Current.Request[name]))
            {
                result = HttpContext.Current.Request[name];
            }
            return result;
        }
    }
}
